package com.centrin.veritas.controller;
/**
 * Project Name：veritas
 * File Name：VeritasDefaultDbController
 * Package Name：com.centrin.veritas.controller
 * Date：17/7/21 上午10:03
 */

import com.centrin.common.controller.BaseController;
import com.centrin.common.entity.PagingEntity;
import com.centrin.veritas.entity.VeritasDefaultDbEntity;
import com.centrin.veritas.entity.VeritasOracleDbInstanceEntity;
import com.centrin.veritas.entity.VeritasOracleDbInstanceParamEntity;
import com.centrin.veritas.service.VeritasOracleInstanceService;
import com.centrin.veritas.service.VeritasOracleService;
import com.centrin.veritas.utils.FileUtil;
import com.centrin.veritas.utils.SystemUtils;
import com.centrin.veritas.utils.VeritasConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.controller
 * @ClassName VeritasDefaultDbController
 * @author 张辉
 * @date 17/7/21 上午10:03
 * @version
 */
@Controller
@RequestMapping(value = "/oracle")
public class VeritasOracleController extends BaseController{

    @Autowired
    private VeritasOracleService veritasOracleService;

    @Autowired
    private VeritasOracleInstanceService veritasOracleInstanceService;

    private static final Integer ORACLE_TYPE = 0;

    /*******************************default start***************************************/
    @RequestMapping("/default")
    private String index(){
        return "/veritas/oracle/default";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 根据章节头获取章节参数列表
     * @date 17/7/21
     */
    @RequestMapping(value = "/default/json")
    @ResponseBody
    public PagingEntity<VeritasDefaultDbEntity> findByHead(@RequestParam Map<String, String> param, HttpServletRequest request){
        param.put("type", "0");
        PagingEntity<VeritasDefaultDbEntity> page = getPage(request);
        System.out.println(SystemUtils.getIpReal());
        return veritasOracleService.findPageList(param, page);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 生成默认数据
     * @date 2017/8/17
     */
    @RequestMapping(value = "/default/param/build")
    @ResponseBody
    public String paramBuild(){
        return veritasOracleService.paramBuild(ORACLE_TYPE);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 全局参数修改跳转
     * @date 17/7/21
     */
    @RequestMapping(value = "/default/updateSkip/{id}")
    public String updateSkip(@PathVariable("id") Integer id, Model model){
        model.addAttribute("action", "update");
        model.addAttribute("veritas", veritasOracleService.findById(VeritasDefaultDbEntity.class, id));
        return "/veritas/oracle/defaultForm";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 全局参数修改
     * @date 17/7/21
     */
    @RequestMapping(value = "/default/update")
    @ResponseBody
    public String update(VeritasDefaultDbEntity veritasDefaultDbEntity){
        veritasOracleService.update(veritasDefaultDbEntity);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 生成default文件
     * @date 17/7/22
     */
    @RequestMapping(value = "/default/build")
    @ResponseBody
    public String buildDefault() throws Exception{
        try {
            veritasOracleService.buildDefault();
        } catch (Exception e) {
//            e.printStackTrace();
            return "error";
        }
        return "success";
    }

    @RequestMapping(value = "/default/defaultFileExists")
    @ResponseBody
    public String defaultFileExists(){
        File file = new File(VeritasConstant.ORACLE_DEFAULT_PATH + File.separator + "default.conf");
        if (file.exists()){
            return "success";
        }
        return "error";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 默认文件下载
     * @date 17/7/23
     */
    @RequestMapping(value = "/default/downloadDefault")
    @ResponseBody
    public void downloadDefault(HttpServletResponse response) {
        try {
            FileUtil.download(response, VeritasConstant.ORACLE_DEFAULT_PATH + File.separator + "default.conf");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*******************************default end***************************************/

    /*************************DB begin 暂时未启用**********************************/
    /**
     * @param
     * @return
     * @author 张辉
     * @description db配置文件跳转
     * @date 17/7/23
     */
    @RequestMapping(value = "/db")
    public String dbSkip(){
        return "/veritas/oracle/dbConfig";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 根据章节头获取db章节参数列表
     * @date 17/7/21
     */
    @RequestMapping(value = "/db/json")
    @ResponseBody
    public PagingEntity<VeritasDefaultDbEntity> findDbByHead(@RequestParam Map<String, String> param, HttpServletRequest request){
        param.put("type", "1");
        PagingEntity<VeritasDefaultDbEntity> page = getPage(request);
        return veritasOracleService.findPageList(param, page);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description db配置文件添加跳转
     * @date 17/7/23
     */
    @RequestMapping(value = "/db/createSkip")
    public String createDbSkip(Model model){
        VeritasDefaultDbEntity veritasDefaultDbEntity = new VeritasDefaultDbEntity();
        veritasDefaultDbEntity.setType(1);
        model.addAttribute("action", "create");
        model.addAttribute("veritas", veritasDefaultDbEntity);
        return "/veritas/oracle/dbForm";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description db配置文件创建
     * @date 17/7/23
     */
    @RequestMapping(value = "/db/create")
    @ResponseBody
    public String createDb(VeritasDefaultDbEntity veritasDefaultDbEntity){
        veritasOracleService.add(veritasDefaultDbEntity);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description db配置文件更新跳转
     * @date 17/7/23
     */
    @RequestMapping(value = "/db/update/{id}")
    public String updateSkip(Model model, @PathVariable("id") Integer id){
        model.addAttribute("action", "update");
        model.addAttribute("veritas", veritasOracleService.findById(VeritasDefaultDbEntity.class, id));
        return "/veritas/oracle/dbForm";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description db配置文件更新
     * @date 17/7/23
     */
    @RequestMapping(value = "/db/update")
    @ResponseBody
    public String updateDb(VeritasDefaultDbEntity veritasDefaultDbEntity){
        veritasOracleService.update(veritasDefaultDbEntity);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 生成db配置文件
     * @date 17/7/23
     */
    @RequestMapping(value = "/db/build")
    @ResponseBody
    public String buildDb(){
        try {
            veritasOracleService.buildDb();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description db配置文件是否存在
     * @date 17/7/23
     */
    @RequestMapping(value = "/db/dbFileExists")
    @ResponseBody
    public String dbFileExists(){
        File file = new File(VeritasConstant.ORACLE_DB_PATH + File.separator + "db.conf");
        if (file.exists()){
            return "success";
        }
        return "error";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description db配置文件下载
     * @date 17/7/23
     */
    @RequestMapping(value = "/db/downloadDb")
    @ResponseBody
    public void downloadDb(HttpServletResponse response) {
        try {
            FileUtil.download(response, VeritasConstant.ORACLE_DB_PATH + File.separator + "db.conf");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description db配置文件参数删除
     * @date 17/7/23
     */
    @RequestMapping(value = "/db/delete/{id}")
    @ResponseBody
    public String deleteDb(@PathVariable("id") Integer id){
        veritasOracleService.delete(VeritasDefaultDbEntity.class, id);
        return "success";
    }
    /*************************DB end**********************************/

    /*************************instance start**********************************/
    /**
     * @param
     * @return
     * @author 张辉
     * @description instance配置文件跳转
     * @date 17/7/23
     */
    @RequestMapping(value = "/instance")
    public String instanceSkip(){
        return "/veritas/oracle/instanceConfig";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 根据章节头获instance取章节参数列表
     * @date 17/7/21
     */
    @RequestMapping(value = "/instance/json")
    @ResponseBody
    public PagingEntity<VeritasOracleDbInstanceEntity> findInstanceByHead(@RequestParam Map<String, String> param, HttpServletRequest request){
        PagingEntity<VeritasOracleDbInstanceEntity> page = getPage(request);
        return veritasOracleInstanceService.findPage(page, param);
    }

    @RequestMapping(value = "/instance/allList")
    @ResponseBody
    public List<VeritasOracleDbInstanceEntity> findList(){
        return veritasOracleInstanceService.findList();
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description Instance配置文件添加跳转
     * @date 17/7/23
     */
    @RequestMapping(value = "/instance/createSkip")
    public String createInstanceSkip(Model model){
        VeritasOracleDbInstanceEntity veritasOracleDbInstanceEntity = new VeritasOracleDbInstanceEntity();
        model.addAttribute("action", "create");
        model.addAttribute("veritas", veritasOracleDbInstanceEntity);
        model.addAttribute("instanceOperate", "添加");
        return "/veritas/oracle/instanceForm";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description Instance配置文件创建
     * @date 17/7/23
     */
    @RequestMapping(value = "/instance/create")
    @ResponseBody
    public String createInstance(VeritasOracleDbInstanceEntity veritasOracleDbInstanceEntity, Integer instanceId){
        veritasOracleInstanceService.addParam(veritasOracleDbInstanceEntity, instanceId);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 获取param参数列表
     * @date 17/7/25
     */
    @RequestMapping(value = "/instance/param/json")
    @ResponseBody
    public PagingEntity<VeritasOracleDbInstanceParamEntity> findParamPage(Integer instanceId, HttpServletRequest request){
        PagingEntity<VeritasOracleDbInstanceParamEntity> page = getPage(request);
        List<VeritasOracleDbInstanceParamEntity> list = veritasOracleInstanceService.findParamList(instanceId);
        page.setRows(list);
        return page;
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 添加实例参数
     * @date 17/7/25
     */
    @RequestMapping(value = "/instance/param/add")
    @ResponseBody
    public String createParam(VeritasOracleDbInstanceParamEntity veritasOracleDbInstanceParamEntity){
        veritasOracleInstanceService.add(veritasOracleDbInstanceParamEntity);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 删除实例参数
     * @date 17/7/25
     */
    @RequestMapping(value = "/instance/param/delete/{id}")
    @ResponseBody
    public String deleteParam(@PathVariable("id") Integer id){
        veritasOracleInstanceService.delete(VeritasOracleDbInstanceParamEntity.class, id);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description Instance配置文件更新跳转
     * @date 17/7/23
     */
    @RequestMapping(value = "/instance/update/{id}")
    public String updateInstanceSkip(Model model, @PathVariable("id") Integer id){
        model.addAttribute("action", "update");
        model.addAttribute("instanceOperate", "修改");
        model.addAttribute("veritas", veritasOracleInstanceService.findById(VeritasOracleDbInstanceEntity.class, id));
        return "/veritas/oracle/instanceForm";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description Instance配置文件更新
     * @date 17/7/23
     */
    @RequestMapping(value = "/instance/update")
    @ResponseBody
    public String updateInstance(VeritasOracleDbInstanceEntity veritasOracleDbInstanceEntity){
        veritasOracleInstanceService.update(veritasOracleDbInstanceEntity);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 删除实例
     * @date 17/7/25
     */
    @RequestMapping(value = "/instance/delete/{id}")
    @ResponseBody
    public String deleteInstance(@PathVariable("id") Integer id){
        veritasOracleInstanceService.instanceDelete(id);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description db文件生成
     * @date 17/7/25
     */
    @RequestMapping(value = "/instance/build")
    @ResponseBody
    public String instanceBuild(){
        try {
            veritasOracleInstanceService.instanceBuild();
        } catch (Exception e) {
            return "error";
        }
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 获取可用的实例参数列表
     * @date 2017/8/17
     */
    @RequestMapping(value = "/instance/findUsableList")
    @ResponseBody
    public List<VeritasDefaultDbEntity> findUsableList(@RequestParam Map<String, String> param){
        param.put("type", "0");
        return veritasOracleService.findUsableList(param);
    }

    /*************************instance end**********************************/
}
