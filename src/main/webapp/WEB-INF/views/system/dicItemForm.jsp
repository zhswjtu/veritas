
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html lang="en">
<head>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<title>用户管理</title>
</head>
<body>
	<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
<link rel="stylesheet" href="${ctx}/resource-Main/userList/userList_new.css">
<!--加载效果 start-->
<div id="preloader">
    <div id="status">
        <i class="fa fa-spinner fa-spin"></i>
    </div>
</div>
<!--加载效果 end-->
<section>
    <!--main start-->
    <div class="mainpanel_mian">
        <!-- main start -->
        <div class="contentpanel">
            <div class="row" style="background-color: #fff;">
             <div class="panel panel-default">
                        <div class="panel-heading zj-panel-heading-2 clearfix" style="padding: 10px 0;">
                            <h5 class="panel-title zj-panel-title-2" style="float: left; height: 30px; line-height: 30px;">添加字典项</h5>
                            <button type="button" class="btn btn-primary" style="float: right; height: 30px; width: 70px;" onclick="window.history.go(-1);">
                                <i class="glyphicon glyphicon-backward" style="margin-right: 7px"></i>返回
                            </button>
                        </div>
                    </div>
	    <form class="form-horizontal" role="form" id="mainform_t"  method="post" action="${ctx}/system/dic/item/${action}">
	        <div class="form-group">
	            <label class="col-md-3 control-label">显示名称</label>
	            <div class="col-md-6" id="username_i">
	                <input type="hidden" name="id" id="id1" value="${dicItem.id}" class="form-control">
	                <input type="hidden" name="dicId" value="${dicItem.dicId}" />
	                <input type="text" name="displayName" id="displayName" value="${dicItem.displayName}" class="form-control">
	            </div>
	        </div>
	
	        <div class="form-group">
	            <label class="col-md-3 control-label">键值</label>
	            <div class="col-md-6" id="code_i">
	                <input type="text" name="itemValue" id="itemValue" value="${dicItem.itemValue}" class="form-control">
	            </div>
	        </div>
	
	        <div class="form-group">
	            <label class="col-md-3 control-label">排序号</label>
	            <div class="col-md-6" id="sort_i">
	                <input type="text" name="sort" id="sort" value="${dicItem.sort}" class="form-control">
	            </div>
	        </div>
	
	        <div class="form-group">
	            <label class="col-md-3 control-label">状态</label>
	            <div class="col-md-6">
	                <select class="form-control" name="status" id="status1" value="${dicItem.sort}" style="width: 100px">
	                    <option value="1">启用</option>
	                    <option value="0">禁用</option>
	                </select>
	            </div>
	        </div>
	
	        <div class="form-group">
	            <label class="col-md-3 control-label">描述</label>
	            <div class="col-md-6">
	                <textarea name="description" id="description" rows="4" class="form-control">${dicItem.description}</textarea>
	            </div>
	        </div>
	        <div class="sub_btn">
             <div class="col-sm-3"></div>
                  <div class="col-sm-6" style="text-align: center">
                      <input type="button" id="confirm" class="btn btn-primary sub1" value="确定" onclick="save()">
                       <input type="button" id="close" class="btn btn-primary sub2" onclick="window.history.go(-1);" value="取消">
                    </div>
                  <div class="col-sm-3"></div>
             </div>               
    </form>
   </div>
   </div>
</div>
</section>
<script type="text/javascript">
    var falg_user = false;
    var action = '${action}';

    if(action == 'update'){
        $("#status1 option[value='${dicItem.status}']").attr("selected",true);
    }

    $(document).ready(function() {
        $('#mainform_t').bootstrapValidator({
            container: 'tooltip',
//        trigger: 'blur',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {validators: {notEmpty: {}}},
                code: {validators: {notEmpty: {}}
                }
            }
        }).on('success.form.bv', function(e) {
            var $form = $(e.target);
            $form.ajaxSubmit(function(data){
                
                toastr.success(data);
            });
        });
    });


    function save(){
        $('#mainform_t').bootstrapValidator('validate');
    }

</script>
</body>
</html>
