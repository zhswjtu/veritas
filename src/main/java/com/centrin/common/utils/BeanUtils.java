/*
 * Copyright 2016-2017 the original author or authors.
 */
package com.centrin.common.utils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class BeanUtils {
	
	private static final String getMethodPrefix = "get";
	private static final String setMethodPrefix = "set";

	public static final <T> List<T> copyList(Iterable<?> origIterable, Class<T> destClazz) {
		if (origIterable == null) {
			throw new IllegalArgumentException("original Iterable object is null!");
		}
		
		if (destClazz == null) {
			throw new IllegalArgumentException("the class of the destination object is null!");
		}
		
		List<T> list = new ArrayList<T>();
		
		Iterator<?> iterator = origIterable.iterator();
		while (iterator.hasNext()) {
			list.add(copyProps(iterator.next(), destClazz));
		}
		
		return list;
	}
	
	public static final <T> T copyProps(Object orig, Class<T> destClazz) {
		if (orig == null) {
			throw new IllegalArgumentException("original object is null!");
		}
		
		if (destClazz == null) {
			throw new IllegalArgumentException("destination object of clss is null!");
		}
		
		T dest = newInstance(destClazz);
		copyProperties(orig, dest, false);
		return dest;
	}
	
	public static final <T> T newInstance(Class<T> clazz) {
		if (clazz == null) {
			throw new IllegalArgumentException("No clazz specified!");
		}
		
		T obj = null;
		try {
			obj = clazz.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return obj;
	}

	/**
	 * Bean对象合并，将src对象合并到dest对象中。
	 * <p>注意: 
	 * <ul>
	 * <li>返回对象并不是一个全新的对象，而是返回合并后的dest对象。</li>
	 * <li>在合并过程中，如果src的get方法返回null， 则不会调用dest的对应set方法进行赋值。</li>
	 * </ul>
	 * 
	 * @param src 要合并的对象
	 * @param dest 合并目标对象
	 * @return 合并后的desc对象。
	 */
	public static final <T> T merge(T src, T dest) {
		if (src == null) {
			return dest;
		}
		
		if (dest == null) {
			return src;
		}
		
		copyProperties(src, dest, true);
		
		return dest;
	}
	
    /**
     * JavaBean属性拷贝，完全拷贝，空值（null）同样拷贝。
     * <p>详情请参见 copyProperties(Object src, Object dest, boolean ignorNullVal)方法。
     * 
     * @param src 被拷贝的对象
     * @param dest 拷贝目标对象
     */
    public static final void copyProperties(Object src, Object dest) {
        copyProperties(src, dest, false);
    }
    
    /**
     * JavaBean属性拷贝，将src对象属性拷贝到dest对象中。
     * <p>注意：属性拷贝需要满足以下条件。
     * <ul>
     * <li>1、src、dest对象中拥有相互映射的getter、setter方法。
     * <p>    src中拥有getter方法， dest中拥有setter方法。</li>
     * <li>2、getter方法必须有返回参数， setter方法有且只能有一个入参。</li>
     * <li>3、src对象getter方法返回参数类型必须同dest对象setter方法的入参类型。</li>
     * </ul>
     * @param src 被拷贝的对象
     * @param dest 拷贝目标对象
     * @param ignorNullVal 忽略null值， 
     *          true - src中getter返回null的属性将不进行拷贝; 
     *          false - 完全拷贝
     */
    public static final void copyProperties(Object src, Object dest, 
            boolean ignorNullVal) {
        if (src == null || dest == null) {
            return;
        }
        
        Class<?> srcClazz = src.getClass();
        Class<?> destClazz= dest.getClass();
        Map<String, Method> destMethodMap = 
                toMethodMap(destClazz.getMethods());
        try {
            Method[] methods = srcClazz.getMethods();
            for (Method method : methods) {
                if (method == null
                        || !method.getName().startsWith(getMethodPrefix)) {
                    continue;
                }
                
                String set = setMethodPrefix + method.getName().substring(3);
                Method setMethod = destMethodMap.get(set);
                if (setMethod == null) {
                    continue;
                }
                
                if (!checkParamType(method, setMethod)) {
                    continue;
                }
                
                Object val = method.invoke(src);
                if (ignorNullVal && val == null) {
                    continue;
                }
                setMethod.invoke(dest, val);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
	
	/**
	 * getMethod返回参数与setMethod返回参数类型检查，
	 * 二者参数不一致则返回false， 否则返回 true。
	 * 
	 * @param getMethod
	 * @param setMethod
	 * @return 
	 */
	private static boolean checkParamType(Method getMethod, Method setMethod) {
		if (setMethod.getParameterTypes().length != 1) {
			return false;
		}

		Class<?> setMethodParamType = setMethod.getParameterTypes()[0];
		Class<?>  getMethodReturnType = getMethod.getReturnType();
		if (!setMethodParamType.equals(getMethodReturnType)) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * 将方法数组转换为Map对象。
	 * 
	 * @param methods
	 * @return
	 */
	private static Map<String, Method> toMethodMap(Method[] methods) {
		Map<String, Method> map = new HashMap<String, Method> ();
		if (methods == null || methods.length == 0) {
			return map;
		}
		for (Method method : methods) {
			if (method == null) {
				continue;
			}
			
			map.put(method.getName(), method);
		}
		return map;
	}
	
	
	public static <T> List<T> mapList2BeanList(Class<T> c, List<?> list) {
		List<T> newList = null;
		T bean = null;
		try{
			if(list!=null) {
				newList = new ArrayList<T>();
				for(Object t : list) {
					bean = c.newInstance();
					org.apache.commons.beanutils.BeanUtils.populate(bean,(Map<?, ?>) t);
					newList.add(bean);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return newList;
	}
}
