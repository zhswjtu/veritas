/*
 * Copyright 2016-2017 the original author or authors.
 */
package com.centrin.common.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QueryFilter {

	public enum InItemType {
		NUMBER, STRING;
	}
	
	public enum Order {
		ASC, DESC
	}

	/**
	 * 条件类型
	 * 
	 * @author rick
	 */
	private enum ConditionType {
		AND_IS_NULL(" AND ", " IS NULL"), AND_LK(" AND ", " LIKE "),
		AND_IS_NOT_NULL(" AND ", " IS NOT NULL"),
		AND_EQ(" AND ", " = "), AND_GT(" AND ", " > "), AND_LT(" AND ", " < "), 
		AND_GT_EQ(" AND ", " >= "), AND_LT_EQ(" AND ", " <= "),
		OR_EQ(" OR ", " = "), OR_GT(" OR ", " > "), OR_LT(" OR ", " < "), 
		OR_GT_EQ(" OR ", " >= "), OR_LT_EQ(" OR ", " <= "),
		AND_IN(" AND "," IN "), AND_NOT_IN(" AND "," NOT IN "), WRAP_AND(" AND ", null), WRAP_OR(" OR ", null);
		
		String type;
		String operator;
		
		private ConditionType(String type, String operator) {
			this.type = type;
			this.operator = operator;
		}
		
		public String toString() {
			return type;
		}
	}

	private StringBuilder jpql = new StringBuilder();

	/** 过滤条件列表 */
	private List<Condition> filterConditionList = new ArrayList<Condition>();
	
	/** 查询字段列表 */
	private List<String> queryFieldList;
	
	/** 排序列表 */
	private List<String> orderbyList;
	
	/** 分组字段列表 */
	private List<String> groupbyFieldList;

	private Map<String, Object> filterParam;
	
	private boolean isPagingFilter;

	private int pageNum;

	private int pageSize;

	private QueryFilter() {
	}
	
	private QueryFilter(List<String> queryFieldList) {
		this.queryFieldList = queryFieldList;
	}

	private QueryFilter(Map<String, Object> filterParam) {
		this.filterParam = filterParam;
	}
	
	private QueryFilter(List<String> queryFieldList, Map<String, Object> filterParam) {
		this.queryFieldList = queryFieldList;
		this.filterParam = filterParam;
	}

	public Map<String, Object> getFilterParam() {
		return filterParam;
	}

	public void setFilterParam(Map<String, Object> filterParam) {
		this.filterParam = filterParam;
	}

	public int getStart() {
		return pageSize * (pageNum - 1);
	}

	public int getLimit() {
		return pageSize;
	}
	
	public boolean isPagingFilter() {
		return isPagingFilter;
	}
	
	public QueryFilter andIsNull(String field) {
		filterConditionList.add(new Condition(ConditionType.AND_IS_NULL, field, null));
		return this;
	}
	

	public QueryFilter andIsNotNull(String field) {
		filterConditionList.add(new Condition(ConditionType.AND_IS_NOT_NULL, field, null));
		return this;
	}
	
	
	public QueryFilter andLk(String field, Object value) {
		filterConditionList.add(new Condition(ConditionType.AND_LK, field, value));
		return this;
	}
	
	
	public QueryFilter andEq(String field, Object value) {
		filterConditionList.add(new Condition(ConditionType.AND_EQ, field, value));
		return this;
	}

	public QueryFilter andGt(String field, Object value) {
		filterConditionList.add(new Condition(ConditionType.AND_GT, field, value));
		return this;
	}

	public QueryFilter andLt(String field, Object value) {
		filterConditionList.add(new Condition(ConditionType.AND_LT, field, value));
		return this;
	}

	public QueryFilter andGtEq(String field, Object value) {
		filterConditionList.add(new Condition(ConditionType.AND_GT_EQ, field, value));
		return this;
	}

	public QueryFilter andLtEq(String field, Object value) {
		filterConditionList.add(new Condition(ConditionType.AND_LT_EQ, field, value));
		return this;
	}
	
	public QueryFilter orEq(String field, Object value) {
		filterConditionList.add(new Condition(ConditionType.OR_EQ, field, value));
		return this;
	}
	
	public QueryFilter orGt(String field, Object value) {
		filterConditionList.add(new Condition(ConditionType.OR_GT, field, value));
		return this;
	}
	
	public QueryFilter orLt(String field, Object value) {
		filterConditionList.add(new Condition(ConditionType.OR_LT, field, value));
		return this;
	}
	
	public QueryFilter orGtEq(String field, Object value) {
		filterConditionList.add(new Condition(ConditionType.OR_GT_EQ, field, value));
		return this;
	}
	
	public QueryFilter orLtEq(String field, Object value) {
		filterConditionList.add(new Condition(ConditionType.OR_LT_EQ, field, value));
		return this;
	}

	public QueryFilter wrapAnd(QueryFilter wrapFilter) {
		wrapFilter.filterParam = filterParam;
		filterConditionList.add(new Condition(ConditionType.WRAP_AND, wrapFilter));
		return this;
	}
	
	public QueryFilter wrapOr(QueryFilter wrapFilter) {
		wrapFilter.filterParam = filterParam;
		filterConditionList.add(new Condition(ConditionType.WRAP_OR, wrapFilter));
		return this;
	}
	
	public QueryFilter andIn(String field, InItemType itemType, List<?> itemList) {
		filterConditionList.add(new InCondition(ConditionType.AND_IN, field, itemType, itemList));
		return this;
	}
	
	public QueryFilter andNotIn(String field, InItemType itemType, List<?> itemList) {
		filterConditionList.add(new InCondition(ConditionType.AND_NOT_IN, field, itemType, itemList));
		return this;
	}
	
	public QueryFilter orderBy(String field, Order order) {
		checkSupportOrderBy();
		orderbyList.add(field + " " + order);
		return this;
	}
	
	private void checkSupportOrderBy() {
		if (orderbyList == null) {
			throw new RuntimeException("The PagingFilter and WrapFilter don't support orderBy!");
		}
	}
	
	public QueryFilter groupBy(String field) {
		checkSupportGroupBy();
		groupbyFieldList.add(field);
		return this;
	}
	
	private void checkSupportGroupBy() {
		if (groupbyFieldList == null) {
			throw new RuntimeException("The PagingFilter and WrapFilter don't support groupBy]!");
		}
	}

	public static final QueryFilter newFilter() {
		return newFilter(null);
	}

	public static final QueryFilter newFilter(List<String> queryFieldList) {
		QueryFilter queryFilter = new QueryFilter(queryFieldList, new HashMap<String, Object>());
		queryFilter.orderbyList = new ArrayList<String>();
		queryFilter.groupbyFieldList = new ArrayList<String>();
		return queryFilter;
	}
	
	public static final QueryFilter newPagingFilter(int pageNum, int pageSize) {
		return newPagingFilter(null, pageNum, pageSize);
	}
	
	public static final QueryFilter newPagingFilter(List<String> queryFieldList, int pageNum, int pageSize) {
		QueryFilter queryFilter = newFilter(queryFieldList);
		queryFilter.pageNum = pageNum;
		queryFilter.pageSize = pageSize;
		queryFilter.isPagingFilter = true;
		queryFilter.groupbyFieldList = null; //分页查询不支持分组
		return queryFilter;
	}
	
	public static final QueryFilter newWrapFilter() {
		return new QueryFilter();
	}
	
	public String toWhereJPQL() {
		if (filterConditionList.size() == 0) {
			return "";
		}
		jpql.append(" WHERE ");
		return buildFilterJPQL();
	}
	
	public String buildFilterJPQL() {
		parseFilter();
		return jpql.toString();
	}
	
	public String buildPagingSQL() {
		if (pageNum > 0) {
			return " LIMIT " + (pageSize * (pageNum - 1)) + "," + pageSize;
		}
		return "";
	}
	
	public String buildQueryFiledListJPQL() {
		if (queryFieldList == null || queryFieldList.size() == 0) {
			return "e";
		}
		StringBuilder sb = new StringBuilder();
		for (String field : queryFieldList) {
			sb.append("e.").append(field).append(",");
		}
		sb.setLength(sb.length() - 1);
		return sb.toString();
	}
	
	public String buildOrderbySQL() {
		checkSupportOrderBy();
		return build0(orderbyList, " ORDER BY ", "");
	}
	
	public String buildGroupbySQL() {
		checkSupportGroupBy();
		return build0(groupbyFieldList, " GROUP BY ", "");
	}
	
	private String build0(List<String> list, String sql, String defSql) {
		if (list == null || list.size() == 0) {
			return defSql;
		}
		StringBuilder sb = new StringBuilder(sql);
		sb.append("e.").append(list.get(0));
		for (int i = 1; i < list.size(); i++) {
			sb.append(", e.").append(list.get(i));
		}
		return sb.toString();
	}
	
	private void parseFilter() {
		int count = 0;
		for (Condition condition : filterConditionList) {
			buildJpqlCondition(count, condition);
			count++;
		}
	}
	
	private void buildJpqlCondition(int index, Condition condition) {
		switch(condition.type) {
			case WRAP_AND :
				if (index > 0) {
					jpql.append(condition.type);
				}
				jpql.append("(").append(condition.wrapFilter.buildFilterJPQL()).append(")");
				break;
			case WRAP_OR :
				if (index > 0) {
					jpql.append(condition.type);
				}
				jpql.append("(").append(condition.wrapFilter.buildFilterJPQL()).append(")");
				break;
			case AND_IN :
				buildInItemList(index,(InCondition)condition);
				return;
			case AND_NOT_IN :
				buildInItemList(index,(InCondition)condition);
				return;
			case AND_IS_NULL :
				appendNullValueCondition(index, condition.type, condition.field);
				return;
			case AND_IS_NOT_NULL :
				appendNullValueCondition(index, condition.type, condition.field);
				return;
			default :
				appendCondition(index, condition.type, condition.field);
				break;
		}
		filterParam.put(condition.field + "_" + filterParam.size(), condition.value);
	}
	
	
	private void appendNullValueCondition(int index, ConditionType conditionType, String field) {
		if (index > 0) {
			jpql.append(conditionType.type);
		}
		jpql.append("e.")
		    .append(field)
		    .append(conditionType.operator);
	}
	
	
	
	private void appendCondition(int index, ConditionType conditionType, String field) {
		if (index > 0) {
			jpql.append(conditionType.type);
		}
		jpql.append("e.")
		    .append(field)
		    .append(conditionType.operator)
		    .append(":")
		    .append(field+"_" + filterParam.size());
	}
	
	private void buildInItemList(int index, InCondition condition) {
		if (index > 0) {
			jpql.append(condition.type);
		}
		jpql.append("e.")
			.append(condition.field)
			.append(condition.type.operator)
			.append("(:")
			.append(condition.field+"_" + filterParam.size())
		    .append(")");
		
		StringBuffer sb = new StringBuffer();
		for (Object item : condition.itemList) {
			if (condition.itemType == QueryFilter.InItemType.NUMBER) {
				sb.append(item).append(",");
			} else {
				sb.append("'").append(item).append("',");
			}
		}
		if(sb.length()>0) {
			sb.setLength(sb.length() - 1);
		}
		filterParam.put(condition.field + "_" + filterParam.size(), sb.toString());
	}
	
	private static class Condition {
		
		protected QueryFilter wrapFilter;
		protected String field;
		protected Object value;
		protected ConditionType type;
		
		public Condition(ConditionType type, String field) {
			this.type = type;
			this.field = field;
		}
		
		public Condition(ConditionType type, QueryFilter wrapFilter) {
			this.wrapFilter = wrapFilter;
			this.type = type;
		}
		
		public Condition(ConditionType type, String field, Object value) {
			this.field = field;
			this.value = value;
			this.type = type;
		}
		
	}
	
	private static class InCondition extends Condition {
		
		protected InItemType itemType;
		protected List<?> itemList;
		
		public InCondition(ConditionType type, String field, InItemType itemType, List<?> itemList) {
			super(type,field);
			this.itemType = itemType;
			this.itemList = itemList;
		}
		
	}
	
	public static void main(String[] args) {
		List<Integer> itemList = new ArrayList<Integer>();
		itemList.add(1);
		itemList.add(2);
		QueryFilter qf = QueryFilter.newPagingFilter(1,100)
		.andEq("aaa", "333")
		.andEq("ddd", "444")
		.wrapAnd(QueryFilter.newWrapFilter().andEq("ddd", "111").orEq("ddd", "222"))
		.andIn("ccc",InItemType.STRING, itemList)
		.wrapOr(QueryFilter.newWrapFilter().andNotIn("ccc",InItemType.NUMBER, itemList))
		.orderBy("o1", Order.ASC)
		.orderBy("o2", Order.DESC);
		//System.out.println(qf.toWhereJPQL() + qf.buildOrderbySQL());
		System.out.println(qf.buildFilterJPQL());
	}
}
