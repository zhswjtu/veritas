package com.centrin.veritas.dao.impl;
/**
 * Project Name：veritas
 * File Name：RestoreReportDaoImpl
 * Package Name：com.centrin.veritas.dao.impl
 * Date：17/7/26 下午3:40
 */

import com.centrin.common.dao.BaseDaoImpl;
import com.centrin.veritas.dao.RestoreReportDao;
import org.springframework.stereotype.Repository;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao.impl
 * @ClassName RestoreReportDaoImpl
 * @author 张辉
 * @date 17/7/26 下午3:40
 * @version
 */
@Repository
public class RestoreReportDaoImpl extends BaseDaoImpl implements RestoreReportDao {
}
