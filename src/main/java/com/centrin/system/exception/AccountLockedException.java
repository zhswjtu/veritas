package com.centrin.system.exception;

import org.apache.shiro.authc.AuthenticationException;

public class AccountLockedException extends AuthenticationException {

	private static final long serialVersionUID = 1L;

	public AccountLockedException() {
		super();
	}

	public AccountLockedException(String message, Throwable cause) {
		super(message, cause);
	}

	public AccountLockedException(String message) {
		super(message);
	}

	public AccountLockedException(Throwable cause) {
		super(cause);
	}
}
