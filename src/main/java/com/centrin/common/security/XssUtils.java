/** 
 * Project Name：jfshop 
 * File Name：bbb.java 
 * Package Name：com.centrin.common.security 
 * Date：2016-9-9 下午2:08:15 
 * Copyright (c) 2016, Centrin Data Systems Ltd. All Rights Reserved. 
 * 中金数据系统有限公司 
 */
/**
 * 
 */
package com.centrin.common.security;
import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XssUtils {

	private static Logger log = LoggerFactory.getLogger(XssUtils.class);
	
	public static String cleanXSS(String value) {
		String result = value.trim();
		if(result!=null && !"".equals(result)) {
//			result = result.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
//			result = result.replaceAll("\\(", "&#40;").replaceAll("\\)", "&#41;");
//			result = result.replaceAll("'", "&#39;");
//			result = result.replaceAll("eval\\((.*)\\)", "");
//			result = result.replaceAll("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']","\"\"");
			result = StringEscapeUtils.escapeHtml4(result);
			result = result.replaceAll("(?i)script", "");
			result = result.replaceAll("(?i)iframe", "");
			if(!result.equals(value)){  
				log.warn("输入信息存在XSS或SQL攻击！");  
				log.warn("原始输入信息-->"+value);  
				log.warn("处理后信息-->"+result);  
		    }  
		}  
		return result;
	}
}