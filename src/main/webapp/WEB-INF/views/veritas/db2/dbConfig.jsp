<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <title>工单提交</title>
    <link href="${ctx}/resource-Main/main/css/jquery.datatables.css" rel="stylesheet">
    <%--<link href="${ctx}/resource-Main/main/css/list.css" rel="stylesheet">--%>
    <link href="${ctx}/resource-Main/main/css/demo.css" rel="stylesheet">
    <style>
        .bs-bars{
            width: 100%;
            /*border: 1px solid;*/
            border-bottom: none;
            background-color: #eee;
        }

        /*.zj-list-body .btn {*/
            /*font-size: 13px;*/
            /*height: 20px;*/
        /*}*/

        .dropdown-menu > li > a{
            color: #666;
            padding: 5px 5px;
        }

        .dropdown-menu{
            min-width: 60px;
        }
        .btn-group {
            margin-bottom: 4px;
            margin-top: 4px;
        }
    </style>
</head>
<body>
<!--加载效果 start-->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>
<!--加载效果 end-->
<section>
    <!--main start-->
    <div class="mainpanel_mian">
        <!-- main start -->
        <div class="contentpanel">
            <div class="row">
                <div class="col-sm-12">
                    <!--list start-->
                    <div class="panel panel-default">
                        <div class="panel-heading zj-panel-heading-2">
                            <h5 class="panel-title zj-panel-title-2">DB配置</h5>
                        </div>
                        <div class="panel-body zj-list-body">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <div id="bar_db_11">
                                        <div class="col-sm-6">
                                            <span style="float: left;display: block; margin-top: 4px;" class="glyphicon glyphicon-th" aria-hidden="true">全局参数</span>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="btn-group" style="float: right;">
                                                <button type="button" style="float: right" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
                                                    &nbsp;全局参数<span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="javascript:;" onclick="paramClick('[RMANCAT]')">RMANCAT</a></li>
                                                    <li><a href="javascript:;" onclick="paramClick('[ORCL]')">ORCL</a></li>
                                                    <li><a href="javascript:;" onclick="paramClick('[prd]')">prd</a></li>
                                                    <li><a href="javascript:;" onclick="paramClick('[GLOBAL]')">GLOBAL</a></li>
                                                    <li><a href="javascript:;" onclick="paramClick('[EMAIL]')">EMAIL</a></li>
                                                    <li><a href="javascript:;" onclick="paramClick('[POLICY]')">POLICY</a></li>
                                                    <li><a href="javascript:;" onclick="paramClick('[PFILE]')">PFILE</a></li>
                                                    <li><a href="javascript:;" onclick="paramClick('[INSTANCE]')">INSTANCE</a></li>
                                                </ul>
                                            </div>
                                            <div>
                                                <a href="javascript:;" onclick="buildFile()" style="float: right;display: block; margin-top: 4px;margin-right: 1px;" class="btn btn-default"><span class="glyphicon glyphicon-floppy-saved"></span>&nbsp;生成DB文件</a>
                                                <a href="javascript:;" onclick="downloadDb()" style="float: right;display: block; margin-top: 4px;margin-right: 1px;" class="btn btn-default"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;下载DB文件</a>
                                                <a href="${ctx}/oracle/db/createSkip/" data-toggle="modal" data-target="#add-model" title="添加" style="float: right;display: block; margin-top: 4px;margin-right: 1px;" class="btn btn-default"><span class="glyphicon glyphicon-arrow-up"></span>&nbsp;创建DB参数</a>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-hover table-responsive" id="default_db">
                                    </table>
                                    <input type="hidden" id="chapter">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--list end-->
                </div>
            </div>
        </div>
        <!-- main end -->
    </div>
    <!--main end-->
</section>

<script>
    var $default_db;
    var $url_default_db = '${ctx}/oracle/db/json';
    $(function () {
        $default_db = $('#default_db').bootstrapTable({
            method: "get",
            url: $url_default_db,
            cache: false,
            fit : true,
            fitColumns : true,
            border : false,
            idField : 'id',
            uniqueId: 'id',
            undefinedText: '',
            checkbox:true,
            rownumbers:true,
            showRefresh: false,
            pagination: true,
            sidePagination: 'server',
            pageNumber: 1,
            pageSize: 10,
            pageList: [10, 20, 30, 40, 50],
            singleSelect:true,
            striped:true,
            search:false,
            clickToSelect:true,
            toolbar : '#bar_db_11',
            queryParams: function (params) {
                var param = {
                    limit: params.limit, //页面大小
                    offset: params.offset, //页码
                    head: $('#chapter').val()
                };
                return param;
            },
            columns: [
                {field:'pos',title:'序号',width: 20,align: 'center',
                    formatter: function (value, row, index) {
                        return index+1;
                    }
                },
                {field:'id',title:'id',visible:false},
                {field:'chapterHead',title:'章节',align: 'center',width:80},
                {field:'parameterName',title:'参数',align: 'center',width:150},
                {field:'defaultValue',title:'默认值',align: 'center',width:80},
                {field:'dataType',title:'数据类型',width:80,align: 'center'},
                {field:'dataUnit',title:'单位',align: 'center',width:80},
                {field:'status',title:'状态',align: 'center',width:80,
                    formatter: function(value, row,index) {
                        if(value == 0){
                            return "有效";
                        } else if(value == 1){
                            return "无效";
                        }
                    }
                },
                {field:'description',title:'说明',align: 'center',width:250},
                {field:'opt',title: '操作',align: 'center',width: 80,
                    formatter: function(value, row,index) {
                        var res = '';
                        res +='<a href="${ctx}/oracle/db/update/'+row.id+'" ';
                        res +='data-toggle="modal" data-target="#add-model" class="btn-blue" title="修改"><span  class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>';
                        res += '&nbsp;&nbsp;<span><a href="javascript:del('+row.id+')" class="btn-blue" title="删除"><span  class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>';
                        return res;
                    }
                }
            ],
            dataPlain: true
        });
    });

    function paramClick(str){
        $('#chapter').val(str);
        var opt = {
            url: $url_default_db,
            silent: true,
            query:{
                head: str
            }
        };
        $('#default_db').bootstrapTable('refresh', opt);
    }

    function buildFile(){
        Ewin.confirm({ message: "确认要生成db配置文件吗?" }).on(function (e) {
            if (!e) {
                return;
            }
            $.ajax({
                type: 'post',
                cache: false,
                url: "${ctx}/oracle/db/build",
                success: function(data) {
                    if(data == 'success'){
                        toastr.success('文件生成成功');
                    }else{
                        toastr.error('文件生成失败');
                    }
                },
                error: function () {
                    toastr.error('操作失败');
                }
            });
        });
    }

    function downloadDb(){
        $.ajax({
            url: '${ctx}/oracle/db/dbFileExists',
            type: 'post',
            success: function(data){
                if(data == 'success'){
                    location.href = '${ctx}/oracle/db/downloadDb';
                } else if(data == 'error'){
                    toastr.error('请先生成db文件');
                }
            }
        });
    }

    function del(id){
        Ewin.confirm({ message: "确认要删除该参数吗?" }).on(function (e) {
            if (!e) {
                return;
            }
            $.ajax({
                type: 'post',
                cache: false,
                url: "${ctx}/oracle/db/delete/" + id,
                success: function(data) {
                    if(data == 'success'){
                        toastr.success('参数删除成功');
                        $default_db.bootstrapTable('refresh');
                    }else{
                        toastr.error('参数删除失败');
                    }
                },
                error: function () {
                    toastr.error('操作失败');
                }
            });
        });
    }
</script>
</body>

</html>