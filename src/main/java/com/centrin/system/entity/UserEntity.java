package com.centrin.system.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.centrin.common.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 用户entity
 */
@Entity
@Table(name = "sys_user")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class UserEntity extends BaseEntity implements java.io.Serializable {

    // Fields
    private static final long serialVersionUID = 1L;

    public static final Integer STATUS_VALID = 1;
    public static final Integer STATUS_INVALID = 0;

    public static final Integer DEL_FLAG_YES = 1;
    public static final Integer DEL_FLAY_NO = 0;



    private Integer id;
    private String username;
    private String name;
    private String password;
    private String plainPassword;
    private String salt;
    private String birthday;
    private Integer gender;
    private String email;
    private String phone;
    private String icon;
    private String createTime;
    private Integer status = 1;
    private String description;
    private Integer loginCount = 0;
    private String previousVisit;
    private String lastVisit;
    private Integer delFlag = 0;
    private Integer depId;
    private Set<RoleEntity> roles = new HashSet<RoleEntity>();
    private Set<OrgEntity> orgs = new HashSet<OrgEntity>();

    // Constructors

    /** default constructor */
    public UserEntity() {
    }

    public UserEntity(Integer id) {
        this.id = id;
    }

    /** minimal constructor */
    public UserEntity(String username, String name, String password) {
        this.username = username;
        this.name = name;
        this.password = password;
    }

    /** full constructor */
    public UserEntity(String username, String name, String password, String salt,
                      String birthday, Integer gender, String email, String phone,
                      String icon, String createTime, Integer status, String description,
                      Integer loginCount, String previousVisit, String lastVisit,
                      Integer delFlag) {
        this.username = username;
        this.name = name;
        this.password = password;
        this.salt = salt;
        this.birthday = birthday;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.icon = icon;
        this.createTime = createTime;
        this.status = status;
        this.description = description;
        this.loginCount = loginCount;
        this.previousVisit = previousVisit;
        this.lastVisit = lastVisit;
        this.delFlag = delFlag;
    }

    public UserEntity(String username, String name, String password, String salt,
                      String birthday, Integer gender, String email, String phone,
                      String icon, String createTime, Integer status, String description,
                      Integer loginCount, String previousVisit, String lastVisit,
                      Integer delFlag, Integer depId) {
        this.username = username;
        this.name = name;
        this.password = password;
        this.salt = salt;
        this.birthday = birthday;
        this.gender = gender;
        this.email = email;
        this.phone = phone;
        this.icon = icon;
        this.createTime = createTime;
        this.status = status;
        this.description = description;
        this.loginCount = loginCount;
        this.previousVisit = previousVisit;
        this.lastVisit = lastVisit;
        this.delFlag = delFlag;
        this.depId = depId;
    }

    // Property accessors
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "username", nullable = false, length = 64)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "name", nullable = false, length = 64)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "password", nullable = false, length = 256)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "salt", length = 64)
    public String getSalt() {
        return this.salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Column(name = "birthday", length = 19)
    public String getBirthday() {
        return this.birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Column(name = "gender")
    public Integer getGender() {
        return this.gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    @Column(name = "email", length = 128)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "phone", length = 64)
    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name = "icon", length = 512)
    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Column(name = "create_time", length = 19)
    public String getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Column(name = "status", length = 1)
    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "login_count")
    public Integer getLoginCount() {
        return this.loginCount;
    }

    public void setLoginCount(Integer loginCount) {
        this.loginCount = loginCount;
    }

    @Column(name = "previous_visit", length = 19)
    public String getPreviousVisit() {
        return this.previousVisit;
    }

    public void setPreviousVisit(String previousVisit) {
        this.previousVisit = previousVisit;
    }

    @Column(name = "last_visit", length = 19)
    public String getLastVisit() {
        return this.lastVisit;
    }

    public void setLastVisit(String lastVisit) {
        this.lastVisit = lastVisit;
    }

    @Column(name = "del_flag", length = 1)
    public Integer getDelFlag() {
        return this.delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    // 不持久化到数据库，也不显示在Restful接口的属性.
    @Transient
    @JsonIgnore
    public String getPlainPassword() {
        return plainPassword;
    }

    public void setPlainPassword(String plainPassword) {
        this.plainPassword = plainPassword;
    }


    @JsonIgnore
    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinTable(name = "sys_user_role", joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "role_id", referencedColumnName = "id") })
    // @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
    public Set<RoleEntity> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleEntity> roles) {
        this.roles = roles;
    }

    public void addRole(RoleEntity role) {
        this.roles.add(role);
    }


    public void addRoles(Set<RoleEntity> roles) {
        this.roles.addAll(roles);
    }


    public void removeRole(RoleEntity role) {
        this.roles.remove(role);
    }


    public void removeRoles(Set<RoleEntity> roles){
        this.roles.removeAll(roles);
    }


    public void retainRoles(Set<RoleEntity> roles){
        this.roles.retainAll(roles);
    }


    public void containsRoles(Set<RoleEntity> roles){
        this.roles.containsAll(roles);
    }


    public void clearRoles(){
        this.roles.clear();
    }

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinTable(name = "sys_user_org", joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "org_id", referencedColumnName = "id") })
    public Set<OrgEntity> getOrgs() {
        return orgs;
    }

    public void setOrgs(Set<OrgEntity> orgs) {
        this.orgs = orgs;
    }


    public void clearOrgs(){
        this.orgs.clear();
    }


    public Integer getDepId() {
        return depId;
    }

    public void setDepId(Integer depId) {
        this.depId = depId;
    }
}