package com.centrin.veritas.service.impl;
/**
 * Project Name：veritas
 * File Name：RestoreReportServiceImpl
 * Package Name：com.centrin.veritas.service.impl
 * Date：17/7/26 下午3:43
 */

import com.centrin.common.dao.BaseDao;
import com.centrin.common.dao.QueryFilter;
import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseServiceImpl;
import com.centrin.common.utils.DateUtils;
import com.centrin.common.utils.StringUtils;
import com.centrin.veritas.dao.RestoreReportDao;
import com.centrin.veritas.entity.RestoreReportEntity;
import com.centrin.veritas.service.RestoreReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service.impl
 * @ClassName RestoreReportServiceImpl
 * @author 张辉
 * @date 17/7/26 下午3:43
 * @version
 */
@Service
public class RestoreReportServiceImpl extends BaseServiceImpl<RestoreReportEntity> implements RestoreReportService {

    @Autowired
    private RestoreReportDao restoreReportDao;

    @Override
    protected BaseDao getSuperBaseDao() {
        return restoreReportDao;
    }

    @Override
    public PagingEntity<RestoreReportEntity> findPageList(PagingEntity<RestoreReportEntity> page, Map<String, Object> param) {
        QueryFilter qf = QueryFilter.newPagingFilter(page.getPageNum(), page.getSize());
        if (StringUtils.isNotBlank(param.get("startTime"))){
            String frist = DateUtils.getFristDay(param.get("startTime") + "-01", "yyyy-MM-dd");
            String last = DateUtils.getLastDay(param.get("startTime") + "-01", "yyyy-MM-dd");
            qf.andGtEq("startTime", DateUtils.parse2SimpleFormat(frist));
            qf.andLtEq("startTime", DateUtils.parse2SimpleFormat(last));
        }
        qf.orderBy("startTime", QueryFilter.Order.DESC);
        return findAsPaging(RestoreReportEntity.class, qf);
    }
}
