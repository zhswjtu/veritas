package com.centrin.veritas.utils;
/**
 * Project Name：veritas
 * File Name：VeritasConstant
 * Package Name：com.centrin.veritas.utils
 * Date：17/7/23 下午5:28
 */

import java.util.ResourceBundle;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.utils
 * @ClassName VeritasConstant
 * @author 张辉
 * @date 17/7/23 下午5:28
 * @version
 */
public class VeritasConstant {

    //oracle默认配置文件路径
    public static final String ORACLE_DEFAULT_PATH = ResourceBundle.getBundle("application").getString("oracle.default.path");
    //oracle配置文件路径
    public static final String ORACLE_DB_PATH = ResourceBundle.getBundle("application").getString("oracle.db.path");
    //计划任务执行命令路径
    public static final String RESTORE_ORACLE = ResourceBundle.getBundle("application").getString("oracle.restore");
    //db2默认配置文件路径
    public static final String DB2_DEFAULT_PATH = ResourceBundle.getBundle("application").getString("db2.default.path");
    //db2 db文件路径
    public static final String DB2_DB_PATH = ResourceBundle.getBundle("application").getString("db2.db.path");
    //DB2恢复命令路径
    public static final String DB2_RESTORE = ResourceBundle.getBundle("application").getString("db2.restore");
}
