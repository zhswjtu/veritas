<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <title>参数修改</title>
</head>
<body>
<link rel="stylesheet" href="${ctx}/resource-Main/change/work-order.css">
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/modal.css">
<!--新增 modal-->
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">实例${instanceOperate}</h4>
</div>
<div class="modal-body" style="padding: 0;">
    <form class="form-horizontal" id="form_fibre" action="${ctx}/oracle/instance/${action}">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="group">
                        <label class="col-sm-3 control-label">实例名称:</label>

                        <div class="col-sm-8">
                            <input class="form-control" type="text" id="chapterHead" name="chapterHead" value="${veritas.chapterHead}"/>
                            <input class="form-control" type="hidden" id="id" name="id" value="${veritas.id}"/>
                            <input class="form-control" type="hidden" id="ip" name="ip" value="${veritas.ip}"/>
                            <input class="form-control" type="hidden" id="instanceType" name="instanceType" value="0"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="group">
                        <label class="col-sm-3 control-label">dbid:</label>

                        <div class="col-sm-8">
                            <input class="form-control" type="hidden" id="sidName" name="sidName" value="dbid"/>
                            <input class="form-control" type="text" id="sidValue" name="sidValue" value="${veritas.sidValue}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="group">
                        <label class="col-sm-3 control-label">client name:</label>

                        <div class="col-sm-8">
                            <input class="form-control" type="hidden" id="clientName" name="clientName" value="client name"/>
                            <input class="form-control" type="text" id="clientNameValue" name="clientNameValue" value="${veritas.clientNameValue}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="group">
                        <label class="col-sm-3 control-label">stand by database:</label>

                        <div class="col-sm-4">
                            <input class="form-control" type="hidden" id="standByDatabase" name="standByDatabase" value="stand by database"/>
                            <input class="form-control" readonly type="text" id="standByDatabaseValue" name="standByDatabaseValue" value="${veritas.standByDatabaseValue}"/>
                        </div>

                        <div class="col-sm-4">
                            <select style="width: 100%; height: 40px" class="form-control" id="standByDatabaseStatus" name="standByDatabaseStatus">
                                <option value="0">有效</option>
                                <option value="1">无效</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">参数名:</label>
                    <div class="col-sm-3">
                        <select style="width: 100%; height: 40px" class="form-control" id="key" name="key">
                            <option value=""></option>
                        </select>
                        <input class="form-control" type="hidden" id="instanceId" name="instanceId" value="${veritas.id}"/>
                        <input class="form-control" type="hidden" id="parameterKey" name="parameterKey"/>
                    </div>
                    <label class="col-sm-2 control-label">参数值:</label>
                    <div class="col-sm-3">
                        <input class="form-control" type="text" id="value"/>
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-success btn-sm" style="margin-top: 8px;" href="javascript:;" onclick="addParam()">添加</a>
                    </div>
                </div>
                <div class="form-group">
                    <table class="table mb30" id="param_instance">
                    </table>
                </div>
            </div>
            <!-- panel-body -->
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2 control-center">
                        <button type="button" class="btn btn-primary zj-btn-button" onclick="save();">提交</button>
                    </div>
                    <div class="col-sm-2 control-center">
                        <button type="button" class="btn btn-default zj-btn-button" data-dismiss="modal" id="closeBtn">
                            取消
                        </button>
                    </div>
                    <div class="col-sm-4 "></div>
                </div>
            </div>
            <!-- panel-footer -->
        </div>
    </form>
</div>
<!--新增 modal-->
<script>
    var action = '${action}', $param_instance;
    if(action == 'create'){
        $('#instanceId').val(parseInt(Math.random() * 10000000 + 10000000, 10));
        $('#standByDatabaseValue').val(1);
    }else if(action == 'update'){
        $('#standByDatabaseStatus').val(${veritas.standByDatabaseStatus})
        instanceParam('${veritas.chapterHead}');
    }

    $('#standByDatabaseStatus').on('change', function () {
        if (this.value == 0){
            $('#standByDatabaseValue').val(1);
        }else if (this.value == 1){
            $('#standByDatabaseValue').val(0);
        }
    });

    $(function () {
        $('#form_fibre').bootstrapValidator({
            container: 'tooltip',
            //        trigger: 'blur',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                chapterHead: {
                    group: '.group',
                    validators: {
                        notEmpty: {
                            message: '实例名称必填'
                        }
                    }
                },
                sidValue: {
                    group: '.group',
                    validators: {
                        notEmpty: {
                            message: 'dbid必填'
                        }
                    }
                },
                clientNameValue: {
                    group: '.group',
                    validators: {
                        notEmpty: {
                            message: 'client name必填'
                        }
                    }
                }
            }
        }).on('success.form.bv', function (e) {
            var $form = $(e.target);
            $form.ajaxSubmit(function (data) {
                if (data == 'success') {
                    $default_db.bootstrapTable('refresh');
                    $("#closeBtn").click();
                } else {

                }
            });
        });

        $param_instance = $('#param_instance').bootstrapTable({
            method: "get",
            url: '${ctx}/oracle/instance/param/json',
            cache: false,
            fit : true,
            fitColumns : true,
            border : false,
            idField : 'id',
            uniqueId: 'id',
            undefinedText: '',
            checkbox:true,
            rownumbers:true,
            showRefresh: false,
            pagination: false,
            sidePagination: 'server',
            singleSelect:true,
            striped:true,
            search:false,
            clickToSelect:true,
            queryParams: function (params) {
                var param = {
                    instanceId: $('#instanceId').val()
                };
                return param;
            },
            columns: [
                {field:'pos',title:'序号',width: 20,align: 'center',
                    formatter: function (value, row, index) {
                        return index+1;
                    }
                },
                {field:'id',title:'id',visible:false},
                {field:'parameterKey',title:'参数',align: 'center',width:250},
                {field:'parameterValue',title:'参数值',align: 'center',width:150},
                {field:'opt',title: '操作',align: 'center',width: 80,
                    formatter: function(value, row,index) {
                        var res = '';
                        <%--res +='<a href="${ctx}/oracle/instance/update/'+row.id+'" ';--%>
                        <%--res +='data-toggle="modal" data-target="#add-model" class="btn-blue" title="修改"><span  class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>';--%>
                        res += '&nbsp;&nbsp;<span><a href="javascript:del('+row.id+')" class="btn-blue" title="删除"><span  class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>';
                        return res;
                    }
                }
            ],
            dataPlain: true
        });
    });
    function save() {
        $('#form_fibre').bootstrapValidator('validate');
    }

    $('#key').on('change', function () {
        $('#parameterKey').val(this.options[this.selectedIndex].text);
    });

    function addParam(){
        var key = $('#key').val();
        var value = $('#value').val();
        if (key == ''){
            toastr.error('请添加属性名');
            return false;
        }
        $.ajax({
            url: '${ctx}/oracle/instance/param/add',
            type: 'post',
            data: {'parameterId' : key, 'parameterKey' : $('#parameterKey').val(), 'parameterValue' : value, 'instanceId' : $('#instanceId').val()},
            success: function (data) {
                if(data == 'success') {
                    $('#key').val('');
                    $('#value').val('');
                    $param_instance.bootstrapTable('refresh');
                    instanceParam($('#chapterHead').val());
                }else{
                    toastr.error('添加失败');
                }
            },
            error: function () {
                toastr.error('添加失败');
            }
        });
    }

    function del(id){
        Ewin.confirm({ message: "确认要删除该参数吗?" }).on(function (e) {
            if (!e) {
                return;
            }
            $.ajax({
                type: 'post',
                cache: false,
                url: "${ctx}/oracle/instance/param/delete/" + id,
                success: function(data) {
                    if(data == 'success'){
                        toastr.success('参数删除成功');
                        $param_instance.bootstrapTable('refresh');
                    }else{
                        toastr.error('参数删除失败');
                    }
                },
                error: function () {
                    toastr.error('操作失败');
                }
            });
        });
    }

    $('#chapterHead').on('change', function () {
        console.log(this.value);
        instanceParam(this.value);
    });

    function instanceParam(name) {
        $.ajax({
            type: 'post',
            cache: false,
            url: "${ctx}/oracle/instance/findUsableList",
            data : {'head': name, 'instanceId' : $('#instanceId').val()},
            success: function(data) {
                console.log(data);
                var head = $('#key');
                head.empty();
                var str = '';
                head.append('<option value=""></option>');
                for(var i = 0; i < data.length; i++){
                    str = '<option value = "' + data[i].id + '">' + data[i].parameterName + '</option>';
                    head.append(str);
                }
            },
            error: function () {
                toastr.error('操作失败');
            }
        });
    }
</script>
</body>
</html>