package com.centrin.common.servcie;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.centrin.common.dao.IncrementIdDao;


@Service
public class IncrementIdServiceImpl implements IncrementIdService{
	
	@Autowired
    private IncrementIdDao incrementIdDao;
	
	private final static String DEFAULT_NAME = "default";
	
	private final static long DEFAULT_BEGIN = 1;
	
	private final static boolean DEFAULT_DAY_RESET = false;
	
	public Integer getId() {
		return Integer.valueOf(getIdPro(DEFAULT_NAME, DEFAULT_BEGIN,DEFAULT_DAY_RESET).toString());
	}
	
	public Integer getId(String name) {
		return Integer.valueOf(getIdPro(name, DEFAULT_BEGIN,DEFAULT_DAY_RESET).toString());
	}
	
	
	public Integer getId(String name, long begin) {
		return Integer.valueOf(getIdPro(name, begin,DEFAULT_DAY_RESET).toString());
	}
	
	public Integer getId(String name, long begin, boolean isDayReset) {
		return Integer.valueOf(getIdPro(name, begin, isDayReset).toString());
	}
	
	
	public Long getLongId() {
		return getIdPro(DEFAULT_NAME, DEFAULT_BEGIN,DEFAULT_DAY_RESET);
	}
	
	public Long getLongId(String name) {
		return getIdPro(name, DEFAULT_BEGIN,DEFAULT_DAY_RESET);
	}
	
	public Long getLongId(String name, long begin) {
		return Long.valueOf(getIdPro(name, begin,DEFAULT_DAY_RESET).toString());
	}
	
	public Long getLongId(String name, long begin, boolean isDayReset) {
		return Long.valueOf(getIdPro(name, begin,isDayReset).toString());
	}
	
	private Long getIdPro(String name, long begin, boolean isDayReset) {
		int dayReset = 0;
		if(isDayReset) {
			dayReset = 1;
		}
		String sql = "{call getIncrementId(:name,:begin,:dayReset)}";
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("name", name);
		params.put("begin", begin);
		params.put("dayReset", dayReset);
		Map<String, Object> result  = incrementIdDao.getSingleBySql(sql, params);
		return Long.valueOf(result.get("outId").toString());
	}

}
