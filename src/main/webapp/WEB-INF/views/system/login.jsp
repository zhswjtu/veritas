<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <title>首页</title>
    <link href="${ctx}/resource-Main/main/css/style.default.css" rel="stylesheet">
    <link href="${ctx}/resource-Main/main/css/login.css" rel="stylesheet">
</head>

<body class="signin">
    <section>
        <div class="signinpanel">
            <div class="row">
                <div class="col-md-12 zj-l-login" style="margin-top: 6%;">
                    <h2 class="nomargin">VERITAS备份验证平台</h2>
                    <p class="mt5 mb20"></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 zj-login">
                    <form id="loginForm" method="post" action="${ctx}/login" method="post">
                        <input type="text" id="username" name="username" class="form-control" placeholder="请输入登录账号" />
                        <i class="glyphicon glyphicon-user zj-i-one"></i>
                        <input type="password" id="password" name="password" class="form-control" placeholder="请输入登录密码" />
                        <i class="glyphicon glyphicon-lock zj-i-two"></i>
                        <button class="btn btn-success btn-block">立即登录</button>
                    </form>
                </div>
                <!-- col-sm-6 -->
                <div class="col-md-3"></div>
            </div>
            <!-- row -->
            <div class="row">
                <div class="col-md-12 zj-l-login">
                    <p class="mt5 zj-l-copy"></p>
                </div>
            </div>


        </div>
        <!-- signin -->
    </section>
    <script>
        //提交表单
        $(document).ready(function () {
            $('#loginForm').bootstrapValidator({
                container: 'tooltip',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    username: {
                        validators: {
                            notEmpty: {
                                message: '登录名不能为空'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: '密码不能为空'
                            },
                            stringLength: {
                                /*长度提示*/
                                min: 6,
                                max: 30,
                                message: '密码长度必须在6到30之间'
                            }
                        }
                    }
                }
            }).on('success.form.bv', function (e) {
                var $form = $(e.target);
                $form.ajaxSubmit(function (data) {
                    console.log(data);
                });
            });
        });
    </script>
</body>

</html>