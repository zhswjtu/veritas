<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <title>执行报表</title>
    <link href="${ctx}/resource-Main/main/css/jquery.datatables.css" rel="stylesheet">
    <%--<link href="${ctx}/resource-Main/main/css/list.css" rel="stylesheet">--%>
    <link href="${ctx}/resource-Main/main/css/demo.css" rel="stylesheet">
    <link rel="stylesheet" href="${ctx}/resource-Main/change/list.css">
</head>
<body>
<!--加载效果 start-->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>
<!--加载效果 end-->
<section>
    <!--main start-->
    <div class="mainpanel_mian">
        <!-- main start -->
        <div class="contentpanel">
            <div class="col-sm-12">
                <!--list start-->
                <div class="panel panel-default">
                    <div class="panel-heading zj-panel-heading-2">
                        <h5 class="panel-title zj-panel-title-2">执行报表</h5>
                    </div>
                    <div class="panel-body zj-list-body">
                        <div class="col-sm-12" style="margin-bottom: 10px">
                            <label class="col-sm-2 control-label">恢复验证时间:</label>
                            <div class="col-sm-3">
                                <input class="form-control" type="text" id="start" style="height: 30px"/>
                            </div>
                            <div class="col-sm-2">
                                <a class="btn btn-success btn-sm" href="javascript:;" onclick="paramClick()">查询</a>
                                <a class="btn btn-success btn-sm" href="javascript:;" onclick="paramClean()">清空</a>
                            </div>
                            <div class="col-sm-5">
                                <a href="javascript:;" onclick="downloadRestore()" style="float: right;display: block;" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;导出报表</a>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <%--<div class="table-responsive">--%>
                            <table class="table table-hover table-responsive" id="restore_report">
                            </table>
                            <%--</div>--%>
                        </div>
                    </div>
                </div>
                <!--list end-->
            </div>
        </div>
        <!-- main end -->
    </div>
    <!--main end-->
</section>

<script>
    var $restore_report;
    var $url_restore_report = '${ctx}/oracle/restore/json';

    $("#start").datetimepicker({
        language: 'zh-CN',
        autoclose:true,
        minView: 3,
        forceParse: 2,
        todayHighlight: true,
        startView: 3,
        weekStart: 0,
        todayBtn: 1,
        format: "yyyy-mm"
    });

    $(function () {
        $restore_report = $('#restore_report').bootstrapTable({
            method: "get",
            url: $url_restore_report,
            cache: false,
            fit : true,
            fitColumns : true,
            border : false,
            idField : 'id',
            uniqueId: 'id',
            undefinedText: '',
            checkbox:true,
            rownumbers:true,
            showRefresh: false,
            pagination: true,
            sidePagination: 'server',
            pageNumber: 1,
            pageSize: 10,
            pageList: [10, 20, 30, 40, 50],
            singleSelect:true,
            striped:true,
            search:false,
            clickToSelect:true,
            queryParams: function (params) {
                var param = {
                    limit: params.limit, //页面大小
                    offset: params.offset, //页码
                    startTime: $('#start').val()
                };
                return param;
            },
            columns: [
                {field:'pos',title:'序号',width: 20,align: 'center',
                    formatter: function (value, row, index) {
                        return index+1;
                    }
                },
                {field:'id',title:'id',visible:false},
                {field:'sid',title:'实例名称',align: 'center',width:150},
                {field:'clientName',title:'客户端名称',align: 'center',width:150},
                {field:'backupTime',title:'备份时间',align: 'center',width:250},
                {field:'startTime',title:'恢复验证时间',width:250,align: 'center',
                    formatter: function (value, row, index) {
                        return new Date(value).format("yyyy-MM-dd hh:mm:ss");
                    }
                },
                {field:'elapsed',title:'耗时',width:80,align: 'center'},
                {field:'status',title:'状态',width:80,align: 'center'}
            ],
            dataPlain: true
        });
    });

    function paramClick(){
        var opt = {
            url: $url_restore_report,
            silent: true,
            query:{
                startTime: $('#start').val()
            }
        };
        $('#restore_report').bootstrapTable('refresh', opt);
    }

    function paramClean(){
        $('#start').val('');
    }

    function downloadRestore(){
        var startTime = $('#start').val();
        if(startTime == ''){
            toastr.error('请选定导出的月份');
            return false;
        }
        location.href = '${ctx}/oracle/restore/downloadRestore?startTime=' + startTime;
    }
</script>
</body>

</html>