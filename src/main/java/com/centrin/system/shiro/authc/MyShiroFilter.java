package com.centrin.system.shiro.authc;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.centrin.common.utils.DateUtils;
import com.centrin.common.utils.StringUtils;
import com.centrin.system.entity.LogEntity;
import com.centrin.system.service.LogService;
import com.centrin.system.utils.IPUtil;

import eu.bitwalker.useragentutils.UserAgent;

public class MyShiroFilter extends FormAuthenticationFilter {

	public static final String DEFAULT_CAPTCHA_PARAM = "captcha";

	@Autowired
	private LogService logService;

	protected String getCaptcha(ServletRequest request) {
		return WebUtils.getCleanParam(request, DEFAULT_CAPTCHA_PARAM);
	}

	protected AuthenticationToken createToken(ServletRequest request,
			ServletResponse response) {
		String username = getUsername(request);
		String password = getPassword(request);
		String captcha = getCaptcha(request);
		boolean rememberMe = isRememberMe(request);
		String host = getHost(request);
		return new UsernamePasswordCaptchaToken(username, password, rememberMe,
				host, captcha);
	}

	protected boolean executeLogin(ServletRequest request,
			ServletResponse response) throws Exception {

		AuthenticationToken token = createToken(request, response);
		if (token == null) {
			String msg = "createToken method implementation returned null. A valid non-null AuthenticationToken "
					+ "must be created in order to execute a login attempt.";
			loginLog(request, false);
			throw new IllegalStateException(msg);
		}
		try {
			Subject subject = getSubject(request, response);
			subject.login(token);
			loginLog(request, true);
			return onLoginSuccess(token, subject, request, response);
		} catch (AuthenticationException e) {
			loginLog(request, false);
			return onLoginFailure(token, e, request, response);
		}
	}

	private void loginLog(ServletRequest request, boolean success) {
		HttpServletRequest httpRequest = ((HttpServletRequest) request);
		String username = getUsername(request);
		String password = getPassword(request);
		String requestRri = httpRequest.getRequestURI();
		String uriPrefix = httpRequest.getContextPath();
		String operationCode = StringUtils
				.substringAfter(requestRri, uriPrefix); // 操作编码
		UserAgent userAgent = UserAgent.parseUserAgentString(httpRequest
				.getHeader("User-Agent"));
		String os = userAgent.getOperatingSystem().getName(); // 获取客户端操作系统
		String browser = userAgent.getBrowser().getName(); // 获取客户端浏览器

		String requestParam;
		if (success) {
			requestParam = "{\"msg\":\"登录成功！\",\"username\":\"" + username
					+ "\",\"password\":\"\"}";
		} else {
			requestParam = "{\"msg\":\"登录失败！\",\"username\":\"" + username
					+ "\",\"password\":\"" + password + "\"}";
		}

		LogEntity log = new LogEntity();
		log.setOs(os);
		log.setBrowser(browser);
		log.setIp(IPUtil.getIpAddress(httpRequest));
		log.setOperationCode(operationCode);
		log.setExecuteTime(0);
		log.setCreater(username);
		log.setCreateDate(DateUtils.getSysTimestamp());
		log.setDescription("用户登录");
		log.setRequestParam(requestParam);
		log.setType(LogEntity.TYPE_LOGIN);
		logService.add(log);
	}

}