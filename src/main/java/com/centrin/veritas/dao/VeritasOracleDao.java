package com.centrin.veritas.dao;
/**
 * Project Name：veritas
 * File Name：VeritasOracleDao
 * Package Name：com.centrin.veritas.dao
 * Date：17/7/21 下午3:46
 */

import com.centrin.common.dao.BaseDao;
import com.centrin.veritas.entity.VeritasDefaultDbEntity;
import com.centrin.veritas.entity.VeritasOracleDbInstanceParamEntity;
import com.centrin.veritas.entity.VeritasOracleEntity;

import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao
 * @ClassName VeritasOracleDao
 * @author 张辉
 * @date 17/7/21 下午3:46
 * @version
 */
public interface VeritasOracleDao extends BaseDao{

    List<VeritasDefaultDbEntity> findUsableList(Map<String, String> param, List<VeritasOracleDbInstanceParamEntity> paramList);

    List<VeritasOracleEntity> findAll(Integer type);

    List<VeritasDefaultDbEntity> findByIp(String ip, Integer type);
}
