/*
 *    Copyright 2014-2015 the original author or authors.
 */
package com.centrin.common.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import com.centrin.veritas.utils.SystemUtils;
import net.redhogs.cronparser.CronExpressionDescriptor;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.quartz.CronExpression;
import sun.plugin2.util.SystemUtil;

/**
 * 日期处理工具。
 * <p>处理日期</p>
 */
public class DateUtils {
	
	private static String[] parsePatterns = { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", 
		"yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm" };
	
	/** 简单日期格式（年-月-日 ，yyyy-MM-dd） */
	public static final String DATE_FORMAT_SIMPLE = "yyyy-MM-dd";
	
	/** 完整日期格式（年-月-日 时:分:秒，yyyy-MM-dd hh:mm:ss） */
	public static final String DATE_FORMAT_FULL = "yyyy-MM-dd HH:mm:ss";
	
	/** 一秒钟的毫秒值（一秒钟 = 1000毫秒） */
	public static final long ONE_SECOND_MILLIS = 1000L;
	/** 一分钟的毫秒值（一分钟 = 60000毫秒） */ 
	public static final long ONE_MINUTE_MILLIS = 60 * 1000L;
	/** 一小时的毫秒值（一小时 = 3600000毫秒） */
	public static final long ONE_HOURS_MILLIS = 60 * 60 * 1000L;
	/** 一天的毫秒值（一天 = 86400000毫秒） */
	public static final long ONE_DAY_MILLIS = 24 * 60 * 60 * 1000L;
	
	/** 类型: 一天中的0点时刻 */
	public static final int START_OF_DAY = 1;
	/** 类型: 一天中的23.59.59时刻 */
	public static final int END_OF_DAY = 2;

	private static final String MOMENT_0 = " 00:00:00";
	private static final String MOMENT_24 = " 23:59:59";

	public static final SimpleDateFormat SIMPLE_FORMAT = new SimpleDateFormat(DATE_FORMAT_SIMPLE);
	public static final SimpleDateFormat FULL_FORMAR = new SimpleDateFormat(DATE_FORMAT_FULL);
	
	/**
	 * 通用日期格式化方法，将日期格式化为yyyy-MM-dd形式。
	 * <p>例：1900-01-01
	 * 
	 * @param date 日期
	 * @return 格式化后的日期字符串。
	 */
	public static String toSimpleFormat(Date date) {
		return SIMPLE_FORMAT.format(date);
	}
	
	/**
	 * 通用日期格式化方法，将给定时间毫秒值格式化为yyyy-MM-dd形式。
	 * <p>例：1900-01-01
	 * 
	 * @param timeMillis 时间毫秒值
	 * @return 格式化后的日期字符串。
	 */
	public static String toSimpleFormat(long timeMillis) {
		Timestamp ts = new Timestamp(timeMillis);
		return SIMPLE_FORMAT.format(ts);
	}
	
	/**
	 * 通用时间格式化方法，将日期格式化为yyyy-MM-dd hh:mm:ss形式。
	 * <p>例：1900-01-01 13:00:00
	 * 
	 * @param date 日期
	 * @return 格式化后的时间字符串。
	 */
	public static String toFullTimeFormat(Date date) {
		return FULL_FORMAR.format(date);
	}
	
	/**
	 * 通用时间格式化方法，将给定时间毫秒值格式化为yyyy-MM-dd hh:mm:ss形式。
	 * <p>例：1900-01-01 13:00:00
	 * 
	 * @param timeMillis 时间毫秒值
	 * @return 格式化后的时间字符串。
	 */
	public static String toFullTimeFormat(long timeMillis) {
		Timestamp ts = new Timestamp(timeMillis);
		return FULL_FORMAR.format(ts);
	}
	
	/**
	 * 将Date按指定格式进行格式化处理。
	 * @param date 日期
	 * @param format 格式
	 * @return 格式化后的date字符串。
	 */
	public static String format(Date date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	
	/**
	 * 将 yyyy-MM-dd格式转换为java.util.Date 日期对象，
	 * 解析失败返回null。
	 * 
	 * @param dateText 日期文本
	 * @return Date日期对象
	 */
	public static Date parse2SimpleFormat(String dateText) {
		return parse(dateText, SIMPLE_FORMAT, null);
	}
	
	/**
	 * 将 yyyy-MM-dd格式转换为java.util.Date 日期对象。
	 *
	 * @param dateText 日期文本
	 * @param defaultVal 默认日期
	 * @return Date日期对象
	 */
	public static Date parse2SimpleFormat(String dateText, Date defaultVal) {
		return parse(dateText, SIMPLE_FORMAT, defaultVal);
	}
	
	/**
	 * 将yyyy-MM-dd HH:mm:ss 格式转换为java.util.Date 日期对象，
	 * 解析失败返回null。
	 *
	 * @param dateText 日期文本
	 * @return Date日期对象
	 */
	public static Date parse2FullFormat(String dateText) {
		return parse(dateText, FULL_FORMAR, null);
	}
	
	/**
	 * 将yyyy-MM-dd HH:mm:ss 格式转换为java.util.Date 日期对象。
	 *
	 * @param dateText 日期文本
	 * @param defaultVal 默认日期
	 * @return Date日期对象
	 */
	public static Date parse2FullFormat(String dateText, Date defaultVal) {
		return parse(dateText, FULL_FORMAR, defaultVal);
	}

    /**
     * 得到当前年份字符串 格式（yyyy）
     */
    public static String getYear() {
        return formatDate(new Date(), "yyyy");
    }

    /**
     * 得到当前月份字符串 格式（MM）
     */
    public static String getMonth() {
        return formatDate(new Date(), "MM");
    }

    /**
     * 得到当天字符串 格式（dd）
     */
    public static String getDay() {
        return formatDate(new Date(), "dd");
    }

    /**
     * 得到参数日期年份字符串 格式（yyyy）
     */
    public static String getYear(String date) {
        if(isDate(date)){
            return formatDate( parseDate(date),"yyyy");
        }
        return null;
    }

    /**
     * 得到参数日期月份字符串 格式（MM）
     */
    public static String getMonth(String date) {
        if(isDate(date)){
            return formatDate( parseDate(date),"MM");
        }
        return null;
    }

    /**
     * 得到参数日期天字符串 格式（dd）
     */
    public static String getDay(String date) {
        if(isDate(date)){
            return formatDate( parseDate(date),"dd");
        }
        return null;
    }

    /**
     * @Title: getYearMonth
     * @Description:得到当前系统时间的年月：2015-10
     * @return
     */
    public static String getYearMonth(){
        return getYear()+"-"+getMonth();
    }

    /**
     * @Title: getYearMonth
     * @Description:得到指定时间的年月：2015-10
     * @param date
     * @return
     */
    public static String getYearMonth(String date){
        return getYear(date)+"-"+getMonth(date);
    }

	/**
	 * 将日期字符串转换为 java.util.Date 日期对象，
	 * 解析失败返回null。
	 *
	 * @param dateText 要格式化的字符串
	 * @param format 日期格式字符串, 如果不指定则默认"yyyy-MM-dd"
	 * @return Date日期对象
	 */
	public static final Date parse(String dateText, String format) {
		return parse(dateText, format, null);
	}

	/**
	 * 将日期字符串转换为 java.util.Date 日期对象，
	 * 格式化出错或异常则返回defaultVal。
	 *
	 * @param dateText 要格式化的字符串
	 * @param format 日期格式字符串, 如果不指定则默认"yyyy-MM-dd"
	 * @param defaultVal 默认日期, 格式化错误, 或处理异常时返回默认值
	 * @return Date日期对象
	 */
	public static final Date parse(String dateText, String format, 
			Date defaultVal) {
		// 如果没指定格式,则默认使用"yyyy-MM-dd"格式化日期
		if (StringUtils.isBlank(format)) {
			format = DATE_FORMAT_SIMPLE;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return parse(dateText, sdf, defaultVal);
	}
	
	/**
	 * 将日期字符串转换为 java.util.Date 日期对象，
	 * 格式化异常则返回defaultVal。
	 *
	 * @param dateText 要格式化的字符串
	 * @param format SimpleDateFormat 对象
	 * @param defaultVal 默认日期
	 * @return Date日期对象
	 */
	private static Date parse(String dateText, 
			SimpleDateFormat format, Date defaultVal) {
		if (StringUtils.isBlank(dateText)) {
			return defaultVal;
		}
		
		try {
			return format.parse(dateText);
		} catch (ParseException e) {
			return defaultVal;
		}
	}
	
	/**
	 * 计算某天处于一年中的第几周。
	 * <p>一周起始为星期一，截止为星期日。
	 * @param date 天
	 * @return 周数 最小返回1（第一周）
	 */
	public static int weekOfYear(Calendar date) {
		Calendar firstDayOfFirstWeekOnyear = 
				firstDayOfFirstWeekOnyear(date.get(Calendar.YEAR));
		// 本年度第一周起始时间 大于 要计算的天，则将该天归入上一年度最后一周
		if (firstDayOfFirstWeekOnyear.getTimeInMillis() > date.getTimeInMillis()) {
			firstDayOfFirstWeekOnyear = 
					firstDayOfFirstWeekOnyear(date.get(Calendar.YEAR) - 1);
		}
	    long diff = date.getTimeInMillis() 
	    		- firstDayOfFirstWeekOnyear.getTimeInMillis();
	    int d = (int) (diff / ONE_DAY_MILLIS) + 1;
	    return (int) Math.ceil((double)d / 7);
	}
	
	private static Calendar firstDayOfFirstWeekOnyear(int year) {
		Timestamp firstDayOfYear = Timestamp.valueOf(year + "-01-01 00:00:00");
		Calendar firstDayOfFirstWeek = Calendar.getInstance();
		firstDayOfFirstWeek.setTime(firstDayOfYear);
		int diffD = 0; // 每年的 1月1号 与星期一的便宜，如果1月1日就是星期一，则本年起始周为本周，否则以下周一为第一周起始
		switch (firstDayOfFirstWeek.get(Calendar.DAY_OF_WEEK)) {
			case 1 : // 周天 编译一天 到周一
				diffD = 1;
				break;
			case 2 : // 周一 一年的第一周
				diffD = 0;
				break;
			case 3 : // 周二  编译 4天 到下周一
				diffD = 6;
				break;
			case 4 :
				diffD = 5;
				break;
			case 5 :
				diffD = 4;
				break;
			case 6 :
				diffD = 3;
				break;
			case 7 :
				diffD = 2;
				break;
		}
		// 24 * 60 * 60 * 1000
		firstDayOfFirstWeek.setTimeInMillis(firstDayOfFirstWeek.getTimeInMillis() 
				+ diffD * ONE_DAY_MILLIS);
		return firstDayOfFirstWeek;
	}
	
	/**
	 * 计算某天处于一年中的第几周。
	 * 
	 * @param year 	年
	 * @param month 月   1-12
	 * @param day 	日   1-31
	 * @return 周数 最小返回1（第一周）
	 */
	public static int weekOfYear(int year, int month, int day) {
		Calendar date = Calendar.getInstance();
		date.set(Calendar.YEAR, year);
		date.set(Calendar.MONTH, month - 1);
		date.set(Calendar.DAY_OF_MONTH, day);
		return weekOfYear(date);
	}
	
	/**
	 * 计算某天处于一年中的第几周。
	 * 
	 * @param date 要计算的日期
	 * @return 周数 最小返回1（第一周）
	 */
	public static int weekOfYear(Date date) {
		Calendar d = Calendar.getInstance();
		d.setTime(date);
		return weekOfYear(d);
	}
	
	/**
	 * 计算某天处于一年中的第几周
	 * 
	 * @param millis 时期的毫秒值
	 * @return 周数 最小返回1（第一周）
	 */
	public static int weekOfYear(long millis) {
		Calendar date = Calendar.getInstance();
		date.setTimeInMillis(millis);
		return weekOfYear(date);
	}
	
	/**
	 * 根据年和周数，计算出给定年指定周的第一天日期（仅精确到天，小时后不准确），
	 * 周一 ~ 周日 为一周。
	 * @param year 年
	 * @param week 周 1 - 53
	 * @return 某年第n周的 第一天。
	 */
	public static Date firstDayOfWeek(int year, int week) {
		Calendar firstDayOfFirstWeekOnyear = 
				firstDayOfFirstWeekOnyear(year);
		long diff = (long) (week - 1) * 7 * 86400000;
		Calendar date = Calendar.getInstance();
		date.setTimeInMillis(diff + firstDayOfFirstWeekOnyear.getTimeInMillis());
		return date.getTime();
	}

	/**
	 * 获取一天中的起始时刻。
	 * <p>例：1900-01-01 00:00:00
	 * 
	 * @param date Date日期对象
	 * @return 某天的0点时刻
	 */
	public static final Date getStartOfDay(Date date) {
		return getStartOrEndOfDay(date, START_OF_DAY);
	}

	/**
	 * 获取一天中的最后时刻。
	 * <p>例：1900-01-01 23:59:59
	 * 
	 * @param date Date日期对象
	 * @return 某天的23:59:59时刻
	 */
	public static final Date getEndOfDay(Date date) {
		return getStartOrEndOfDay(date, END_OF_DAY);
	}
	
	/**
	 * 获取日期的第一时刻或最后时刻。
	 * 第一时刻是"yyyy-MM-dd 00:00:00"；
	 * 最后时刻是"yyyy-MM-dd 23:59:59"。
	 * 
	 * @param date
	 *            初始日期
	 * @param type
	 *            类型，1 代表第一时刻, DateUtil.START_OF_DAY，
	 *                 2 代表最后时刻, DateUtil.END_OF_DAY
	 * @return 某日期的第一时刻或最后时刻值
	 */
	public static final Date getStartOrEndOfDay(Date date, int type) {
		if (date == null) {
			throw new IllegalArgumentException("date == null");
		}
		
		String sdate = SIMPLE_FORMAT.format(date);
		if (type == START_OF_DAY) {
			sdate = sdate + MOMENT_0;
		} else if (type == END_OF_DAY) {
			sdate = sdate + MOMENT_24;
		}
		
		try {
			return FULL_FORMAR.parse(sdate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * 获取某一个月第一天和最后一天, 使用yyyy-MM-dd格式化日期。
	 * 
	 * @param date 基准时期
	 * @return 数组, index1: 一月第一天, index2:一月最后一天  
	 */
	public static final String[] getFirstdayAndLastdayOfMonth(Date date) {
		SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_SIMPLE);
		return getFirstdayAndLastdayOfMonth(date, df);
	}
	
	/**
	 * 获取给定日期所处月份的第一天和最后一天。
	 * 
	 * @param dateText 基准时期
	 * @param format 日期格式, 为空则默认"yyyy-MM-dd"
	 * @return 数组, index1: 一月第一天, index2:一月最后一天  
	 */
	public static final String[] getFirstdayAndLastdayOfMonth(String dateText,
			String format) {
		if (StringUtils.isBlank(format)) {
			return getFirstdayAndLastdayOfMonth(parse2SimpleFormat(dateText));
		}
		
		SimpleDateFormat df = new SimpleDateFormat(format);
		try {
			return getFirstdayAndLastdayOfMonth(df.parse(dateText), df);
		} catch (ParseException e) {
			throw new IllegalArgumentException("节气日期文本异常。dateText = "
						+ dateText + "， format = " + format);
		}
	}
	
	/**
	 * 获取给定日期所处月份的第一天和最后一天。
	 * 
	 * @param date 基准时期
	 * @param format 日期格式, 默认"yyyy-MM-dd"
	 * @return 数组, index1: 一月第一天, index2:一月最后一天  
	 */
	public static final String[] getFirstdayAndLastdayOfMonth(Date date,
			String format) {
		if (StringUtils.isBlank(format)) {
			return getFirstdayAndLastdayOfMonth(date);
		}
		
		SimpleDateFormat df = new SimpleDateFormat(format);
		return getFirstdayAndLastdayOfMonth(date, df);
	}
	
	private static final String[] getFirstdayAndLastdayOfMonth(Date date,
			SimpleDateFormat format) {
		if (date == null) {
			throw new IllegalArgumentException("date == null");
		}
		
		// 月第一天
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		String dayFirst = format.format(calendar.getTime());
		
		// 月最后一天
		int daysOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(Calendar.DAY_OF_MONTH, daysOfMonth);
		String dayLast = format.format(calendar.getTime());
		
		return new String[]{dayFirst, dayLast};
	}
	
	/**
	 * 获取系统当前日期
	 * 
	 * @return 系统当前日期
	 */
	public static final Date now() {
		return Calendar.getInstance().getTime();
	}

	/**
	 * 获取系统当前年
	 * 
	 * @return 系统当前年
	 */
	public static final int getCurrYear() {
		return Calendar.getInstance().get(Calendar.YEAR);
	}

	/**
	 * 获取系统当前月
	 * 
	 * @return 系统当前月
	 */
	public static final int getCurrMonth() {
		return Calendar.getInstance().get(Calendar.MONTH) + 1;
	}

	/**
	 * 获取系统当前日期在一月中所处天(一月中第几天)。
	 * 
	 * @return 系统当前日期在一月中所处天
	 */
	public static final int getCurrDay() {
		return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 获取系统当前时刻小时数
	 * 
	 * @return 系统当前时刻小时数
	 */
	public static final int getCurrHour() {
		return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
	}

	public static final int getCurrHour(String date) {
        Calendar ca = Calendar.getInstance();
        ca.setTime(DateUtils.parse(date, "yyyy-MM-dd hh:mm:ss.SS"));
		return ca.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * 获取系统当前时刻分钟值
	 * 
	 * @return 系统当前时刻分钟值
	 */
	public static final int getCurrMinute() {
		return Calendar.getInstance().get(Calendar.MINUTE);
	}
	public static final int getCurrMinute(String date) {
        Calendar ca = Calendar.getInstance();
        ca.setTime(DateUtils.parse(date, "yyyy-MM-dd hh:mm:ss.SS"));
		return ca.get(Calendar.MINUTE);
	}

	/**
	 * 获取系统当前时刻秒值
	 *
	 * @return 系统当前时刻秒值
	 */
	public static final int getCurrSecond() {
		return Calendar.getInstance().get(Calendar.SECOND);
	}
	public static final int getCurrSecond(String date) {
        Calendar ca = Calendar.getInstance();
        ca.setTime(DateUtils.parse(date, "yyyy-MM-dd hh:mm:ss.SS"));
		return ca.get(Calendar.SECOND);
	}

	/**
	 * 获取系统当前时刻毫秒值
	 *
	 * @return 系统当前时刻毫秒值
	 */
	public static final int getCurrMillisecond() {
		return Calendar.getInstance().get(Calendar.MILLISECOND);
	}
	public static final int getCurrMillisecond(String date) {
        Calendar ca = Calendar.getInstance();
        ca.setTime(DateUtils.parse(date, "yyyy-MM-dd hh:mm:ss.SS"));
		return ca.get(Calendar.MILLISECOND);
	}

	/**
	 * 计算并返回从起始日期开始经过N年后的日期
	 * 
	 * @param date 起始日期
	 * @param afterAmount 经过年份
	 * @return
	 */
	public static final Date afterYear(Date date, int afterAmount) {
		return after(date, Calendar.YEAR, afterAmount);
	}
	
	/**
	 * 计算并返回从起始日期开始经过N个月后的日期
	 * 
	 * @param date 起始日期
	 * @param afterAmount 经过月份数量
	 * @return
	 */
	public static final Date afterMonth(Date date, int afterAmount) {
		return after(date, Calendar.MONTH, afterAmount);
	}

	/**
	 * 计算并返回从起始日期开始经过N天后的日期
	 * 
	 * @param date 起始日期
	 * @param afterAmount 经过天数
	 * @return
	 */
	public static final Date afterDay(Date date, int afterAmount) {
		return after(date, Calendar.DATE, afterAmount);
	}

	/**
	 * 计算并返回从起始日期开始经过N小时后的日期
	 * 
	 * @param date 起始日期
	 * @param afterAmount 经过小时数
	 * @return
	 */
	public static final Date afterHour(Date date, int afterAmount) {
		return after(date, Calendar.HOUR_OF_DAY, afterAmount);
	}

	/**
	 * 计算并返回从起始日期开始经过N分钟后的日期
	 * 
	 * @param date 起始日期
	 * @param afterAmount 经过分钟数
	 * @return
	 */
	public static final Date afterMinute(Date date, int afterAmount) {
		return after(date, Calendar.MINUTE, afterAmount);
	}

	/**
	 * 计算并返回从起始日期开始经过N秒钟后的日期
	 * 
	 * @param date 起始日期
	 * @param afterAmount 经过秒数
	 * @return
	 */
	public static final Date afterSecond(Date date, int afterAmount) {
		return after(date, Calendar.SECOND, afterAmount);
	}
	
	/**
	 * 计算并返回从起始日期开始往前N年的日期
	 * 
	 * @param date 起始日期
	 * @param beforeAmount 向前倒推的年份数
	 * @return
	 */
	public static final Date beforeYear(Date date, int beforeAmount) {
		return before(date, Calendar.YEAR, beforeAmount);
	}
	
	/**
	 * 计算并返回从起始日期开始往前N个月的日期
	 * 
	 * @param date 起始日期
	 * @param beforeAmount 向前倒推的月份数
	 * @return
	 */
	public static final Date beforeMonth(Date date, int beforeAmount) {
		return before(date, Calendar.MONTH, beforeAmount);
	}

	/**
	 * 计算并返回从起始日期开始往前N天的日期
	 * 
	 * @param date 起始日期
	 * @param beforeAmount 向前倒推的天数
	 * @return
	 */
	public static final Date beforeDay(Date date, int beforeAmount) {
		return add(date, Calendar.DATE, -beforeAmount);
	}
	
	/**
	 * 计算并返回从起始日期开始往前N小时的日期
	 * 
	 * @param date 起始日期
	 * @param beforeAmount 向前倒推的小时数
	 * @return
	 */
	public static final Date beforeHour(Date date, int beforeAmount) {
		return before(date, Calendar.HOUR_OF_DAY, beforeAmount);
	}
	
	/**
	 * 计算并返回从起始日期开始往前N分钟的日期
	 * 
	 * @param date 起始日期
	 * @param beforeAmount 向前倒推的分钟数
	 * @return
	 */
	public static final Date beforeMinute(Date date, int beforeAmount) {
		return before(date, Calendar.MINUTE, beforeAmount);
	}

	/**
	 * 计算并返回从起始日期开始往前N秒的日期
	 * 
	 * @param date 起始日期
	 * @param beforeAmount 向前倒推的秒数
	 * @return
	 */
	public static final Date beforeSecond(Date date, int beforeAmount) {
		return add(date, Calendar.SECOND, beforeAmount);
	}
	
	private static Date after(Date date, int field, int offset) {
		if (offset < 0) {
			throw new IllegalArgumentException("afterAmount < 0, afterAmount 参数必须大于 0");
		}
		return add(date, field, offset);
	}
	
	private static Date before(Date date, int field, int offset) {
		if (offset < 0) {
			throw new IllegalArgumentException("beforeAmount < 0, beforeAmount 参数必须大于 0");
		}
		return add(date, field, -offset);
	}
	
	private static Date add(Date date, int field, int offset) {
		if (date == null) {
			throw new IllegalArgumentException("date == null");
		}
		if (offset == 0) {
			return date;
		}
		Calendar gc = Calendar.getInstance();
		gc.setTime(date);
		gc.add(field, offset);
		return gc.getTime();
	}
	
	/**
	 * 获取两个日期之间相差的天数。
	 * 
	 * @param dateText1 日期文本，格式要求: yyyy-MM-dd hh:mm:ss
	 * @param dateText2 日期文本，格式要求: yyyy-MM-dd hh:mm:ss
	 * @return 两时间之间相差天数,此值 >= 0
	 */
	public static final long getDiffDays(String dateText1, String dateText2) {
		return getDiffMillis(dateText1, dateText2) / ONE_DAY_MILLIS;
	}
	
	/**
	 * 获取两个日期之间相差的天数。
	 * 
	 * @param date1 日期对象
	 * @param date2 日期对象
	 * @return 两时间之间相差天数,此值 >= 0
	 */
	public static final long getDiffDays(Date date1, Date date2) {
		return getDiffMillis(date1, date2) / ONE_DAY_MILLIS;
	}

	/**
	 * 获取两个日期之间相差的小时数。
	 * 
	 * @param dateText1	日期文本，格式要求: yyyy-MM-dd hh:mm:ss
	 * @param dateText2 日期文本，格式要求: yyyy-MM-dd hh:mm:ss
	 * @return 两时间之间相差小时数,此值 >= 0
	 */
	public static final long getDiffHours(String dateText1, String dateText2) {
		return getDiffMillis(dateText1, dateText2) / ONE_HOURS_MILLIS;
	}

	/**
	 * 获取两个日期之间相差的小时数。
	 * 
	 * @param date1 日期对象
	 * @param date2 日期对象
	 * @return 两时间之间相差小时数,此值 >= 0
	 */
	public static final long getDiffHours(Date date1, Date date2) {
		return getDiffMillis(date1, date2) / ONE_HOURS_MILLIS;
	}

	/**
	 * 获取两个日期之间相差的分钟数。
	 * 
	 * @param dateText1	日期文本，格式要求: yyyy-MM-dd hh:mm:ss
	 * @param dateText2 日期文本，格式要求: yyyy-MM-dd hh:mm:ss
	 * @return 两时间之间相差分钟数,此值 >= 0
	 */
	public static final long getDiffMinutes(String dateText1, String dateText2) {
		return getDiffMillis(dateText1, dateText2) / ONE_MINUTE_MILLIS;
	}

	/**
	 * 获取两个日期之间相差的分钟数。
	 * 
	 * @param date1 日期对象
	 * @param date2 日期对象
	 * @return 两时间之间相差分钟数,此值 >= 0
	 */
	public static final long getDiffMinutes(Date date1, Date date2) {
		return getDiffMillis(date1, date2) / ONE_MINUTE_MILLIS;
	}
	
	/**
	 * 获取两个日期之间相差的秒数。
	 * 
	 * @param dateText1	日期文本，格式要求: yyyy-MM-dd hh:mm:ss
	 * @param dateText2 日期文本，格式要求: yyyy-MM-dd hh:mm:ss
	 * @return 两时间之间相差秒数,此值 >= 0
	 */
	public static final long getDiffSeconds(String dateText1, String dateText2) {
		return getDiffMillis(dateText1, dateText2) / ONE_SECOND_MILLIS;
	}

	/**
	 * 获取两个日期之间相差的秒数。
	 * 
	 * @param date1 日期对象
	 * @param date2 日期对象
	 * @return 两时间之间相差秒数,此值 >= 0
	 */
	public static final long getDiffSeconds(Date date1, Date date2) {
		return getDiffMillis(date1, date2) / ONE_SECOND_MILLIS;
	}

	/**
	 * 获取两个日期之间相差的毫秒值。
	 * 
	 * @param dateText1	日期文本，格式要求: yyyy-MM-dd hh:mm:ss
	 * @param dateText2 日期文本，格式要求: yyyy-MM-dd hh:mm:ss
	 * @return 两时间之间相差毫秒数,此值 >= 0
	 */
	public static final long getDiffMillis(String dateText1, String dateText2) {
		if (StringUtils.isBlank(dateText1)) {
			throw new IllegalArgumentException("date1 == null, 用做比较的日期参数不能为空.");
		}
		if (StringUtils.isBlank(dateText2)) {
			throw new IllegalArgumentException("date2 == null, 用做比较的日期参数不能为空.");
		}
		
		try {
			return getDiffMillis(FULL_FORMAR.parse(dateText1), 
					FULL_FORMAR.parse(dateText2));
		} catch (ParseException e) {
			throw new IllegalArgumentException("日期格式错误,解析失败. date1: " 
					+ dateText1 + " date2: " + dateText2); 
		}
	}

	/**
	 * 获取两个日期之间相差的毫秒值。
	 * 
	 * @param date1 日期对象
	 * @param date2 日期对象
	 * @return 两时间之间相差毫秒数,此值 >= 0
	 */
	public static final long getDiffMillis(Date date1, Date date2) {
		if (date1 == null) {
			throw new IllegalArgumentException("date1 == null, 用做比较的日期参数不能为空.");
		}
		if (date2 == null) {
			throw new IllegalArgumentException("date2 == null, 用做比较的日期参数不能为空.");
		}
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);
		long stamps = cal2.getTime().getTime() - 
				cal1.getTime().getTime();
		return Math.abs(stamps);
	}

	/**
	 * 将年月日合并为给定分隔符的日期字符串
	 * <p>例：year=2013，month=1，day=31，toten=/，
	 *   合并结果 2013/01/31
	 * 
	 * @param year 年
	 * @param month 月 1-12
	 * @param day 日 1-31
	 * @param token 分割符号
	 * @return 日期字符串
	 */
	public static final String combinate2DateText(int year, 
			int month, int day, String token) {
		String m = (month > 9 ? String.valueOf(month) : "0" + 
				month);
		return year + token + m + token + day; 
	}
	
	/**
	 * 获取系统时间Timestamp
	 * @return
	 */
	public static Timestamp getSysTimestamp(){
		return new Timestamp(new Date().getTime());
	}
	
	
	/**
	 * 得到当前时间字符串 格式（yyyy-MM-dds）
	 */
	public static String getDate() {
		return formatDate(new Date(), "yyyy-MM-dd");
	}
	/**
	 * 得到当前时间字符串 格式（HH:mm:ss）
	 */
	public static String getTime() {
		return formatDate(new Date(), "HH:mm:ss");
	}

	/**
	 * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
	 */
	public static String getDateTime() {
		return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
	}
	
	
	/**
	 * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String formatDate(Date date, Object... pattern) {
		String formatDate = null;
		if (pattern != null && pattern.length > 0) {
			formatDate = DateFormatUtils.format(date, pattern[0].toString());
		} else {
			formatDate = DateFormatUtils.format(date, "yyyy-MM-dd");
		}
		return formatDate;
	}

    /**
     * 取得当月天数
     * */
    public static int getCurrentMonthLastDay()
    {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);//把日期设置为当月第一天
        a.roll(Calendar.DATE, -1);//日期回滚一天，也就是最后一天
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    /**
     * 获取指定日期的天
     * @param day 'yyyy-MM-dd'
     * @return
     */
    public static Integer getDayByMounth(String day){
        Date date = parse2SimpleFormat(day);
        Calendar ca = Calendar.getInstance();
        ca.setTime(date);
        return ca.get(Calendar.DAY_OF_MONTH);
    }

    
    /**
	 * 判断字符串是否是日期
	 * @param timeString
	 * @return
	 */
	public static boolean isDate(String timeString){
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
		format.setLenient(false);
		try{
			format.parse(timeString);
		}catch(Exception e){
			return false;
		}
		return true;
	}
   
	
	public static Date getDateStart(Date date) {
		if(date==null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			date= sdf.parse(formatDate(date, "yyyy-MM-dd")+" 00:00:00");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	public static Date getDateEnd(Date date) {
		if(date==null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			date= sdf.parse(formatDate(date, "yyyy-MM-dd") +" 23:59:59");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	
	
	/**
     * 根据所给日期（yyyy-MM-dd）获得该月第一天日期
     * @param day
     * @return
     */
    public static String getFristDay(String day, String dateFormate){
        SimpleDateFormat format = new SimpleDateFormat(dateFormate);
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(format.parse(day));
            calendar.add(Calendar.MONTH, 0);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return format.format(calendar.getTime());
    }

    
    /**
     * 根据所给日期（yyyy-MM-dd）获得该月最后一天日期
     * @param day
     * @return
     */
    public static String getLastDay(String day, String dateFormate){
        SimpleDateFormat format = new SimpleDateFormat(dateFormate);
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(format.parse(day));
            calendar.add(Calendar.MONTH, 1);
            calendar.set(Calendar.DATE, 1);
            calendar.add(Calendar.DATE,-1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return format.format(calendar.getTime());
    }
    
    
    public static String getFristDay(){
		Calendar calendar = Calendar.getInstance();
		//最后一天
        //calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		//第一天
		calendar.set(Calendar.DAY_OF_MONTH,1);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
       return format.format(calendar.getTime())+" 00:00:00";
	}
    
	public static String getEndDay(){
		Calendar calendar = Calendar.getInstance();
		//最后一天
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		//第一天
		//calendar.set(Calendar.DAY_OF_MONTH,1);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(calendar.getTime()) +" 23:59:59";
	}
	
	
	/**
	 * 日期型字符串转化为日期 格式
	 * { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", 
	 *   "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm" }
	 */
	public static Date parseDate(Object str) {
		if (str == null){
			return null;
		}
		try {
			return org.apache.commons.lang3.time.DateUtils.parseDate(str.toString(), parsePatterns);
		} catch (ParseException e) {
			return null;
		}
	}

	
	/**
	 * 得到当前日期字符串 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String getDate(String pattern) {
		return DateFormatUtils.format(new Date(), pattern);
	}
	
	
    public static void main(String[] args){
        String ip = SystemUtils.getIpReal();
        System.out.println("ip:" + ip);
    }
}
