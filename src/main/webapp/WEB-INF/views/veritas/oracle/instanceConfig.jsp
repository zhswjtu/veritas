<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <title>实例配置</title>
    <link href="${ctx}/resource-Main/main/css/jquery.datatables.css" rel="stylesheet">
    <%--<link href="${ctx}/resource-Main/main/css/list.css" rel="stylesheet">--%>
    <link href="${ctx}/resource-Main/main/css/demo.css" rel="stylesheet">
    <%--<link rel="stylesheet" href="${ctx}/resource-Main/change/list.css">--%>
    <style>
        .bs-bars{
            width: 100%;
            /*border: 1px solid;*/
            border-bottom: none;
            background-color: #eee;
        }

        /*.zj-list-body .btn {*/
            /*font-size: 13px;*/
            /*height: 20px;*/
        /*}*/

        .dropdown-menu > li > a{
            color: #666;
            padding: 5px 5px;
        }

        .dropdown-menu{
            min-width: 60px;
        }
        .btn-group {
            margin-bottom: 4px;
            margin-top: 4px;
        }

        .panel-default .panel-body .form-group .zj-rule-cpu{ color: #1e1e1e;}
        .panel-footer .control-center .zj-btn-button{
            padding: 4px 30px;
            border: 1px solid #eee;
            background-color: #fff;
            color: #777;
        }

        .modal-content{
            /*width: 748px;*/
        }
    </style>
</head>
<body>
<!--加载效果 start-->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>
<!--加载效果 end-->
<section>
    <!--main start-->
    <div class="mainpanel_mian">
        <!-- main start -->
        <div class="contentpanel">
            <div class="row">
                <div class="col-sm-12">
                    <!--list start-->
                    <div class="panel panel-default">
                        <div class="panel-heading zj-panel-heading-2">
                            <h5 class="panel-title zj-panel-title-2">DB配置</h5>
                        </div>
                        <div class="panel-body zj-list-body">
                            <div class="col-sm-12" style="margin-bottom: 10px">
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" id="dbid" name="dbid" style="height: 30px" placeholder="实例名称"/>
                                </div>
                                <div class="col-sm-2" style="margin-top: 3px">
                                    <a class="btn btn-success btn-sm" href="javascript:;" onclick="paramClick()">查询</a>
                                    <a class="btn btn-success btn-sm" href="javascript:;" onclick="paramClean()">清空</a>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <div id="bar_db_11">
                                        <div class="col-sm-6">
                                            <span style="float: left;display: block; margin-top: 4px;" class="glyphicon glyphicon-th" aria-hidden="true">全局参数</span>
                                        </div>
                                        <div class="col-sm-6">
                                            <div>
                                                <a href="javascript:;" onclick="buildFile()" style="float: right;display: block; margin-top: 4px;margin-right: 1px;" class="btn btn-default"><span class="glyphicon glyphicon-floppy-saved"></span>&nbsp;生成DB文件</a>
                                                <a href="javascript:;" onclick="downloadDb()" style="float: right;display: block; margin-top: 4px;margin-right: 1px;" class="btn btn-default"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;下载DB文件</a>
                                                <a href="${ctx}/oracle/instance/createSkip/" data-toggle="modal" data-target="#add-model" title="添加" style="float: right;display: block; margin-top: 4px;margin-right: 1px;" class="btn btn-default"><span class="glyphicon glyphicon-arrow-up"></span>&nbsp;创建实例</a>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-hover table-responsive" id="default_db">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--list end-->
                </div>
            </div>
        </div>
        <!-- main end -->
    </div>
    <!--main end-->
</section>

<script>
    var $default_db;
    var $url_default_db = '${ctx}/oracle/instance/json';
    $(function () {
        $default_db = $('#default_db').bootstrapTable({
            method: "get",
            url: $url_default_db,
            cache: false,
            fit : true,
            fitColumns : true,
            border : false,
            idField : 'id',
            uniqueId: 'id',
            undefinedText: '',
            checkbox:true,
            rownumbers:true,
            showRefresh: false,
            pagination: true,
            sidePagination: 'server',
            pageNumber: 1,
            pageSize: 10,
            pageList: [10, 20, 30, 40, 50],
            singleSelect:true,
            striped:true,
            search:false,
            clickToSelect:true,
            toolbar : '#bar_db_11',
            queryParams: function (params) {
                var param = {
                    limit: params.limit, //页面大小
                    offset: params.offset //页码
                };
                return param;
            },
            columns: [
                {field:'pos',title:'序号',width: 20,align: 'center',
                    formatter: function (value, row, index) {
                        return index+1;
                    }
                },
                {field:'id',title:'id',visible:false},
                {field:'chapterHead',title:'实例名',align: 'center',width:80},
                {field:'sidValue',title:'sid',align: 'center',width:150},
                {field:'clientNameValue',title:'client name',align: 'center',width:80},
                {field:'standByDatabaseValue',title:'stand by database',width:80,align: 'center'},
                {field:'opt',title: '操作',align: 'center',width: 80,
                    formatter: function(value, row,index) {
                        var res = '';
                        res +='<a href="${ctx}/oracle/instance/update/'+row.id+'" ';
                        res +='data-toggle="modal" data-target="#add-model" class="btn-blue" title="修改"><span  class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>';
                        res += '&nbsp;&nbsp;<span><a href="javascript:del('+row.id+')" class="btn-blue" title="删除"><span  class="glyphicon glyphicon-trash" aria-hidden="true"></a></span>';
                        res += '&nbsp;&nbsp;<span><a href="javascript:execProcess(' +row.id+ ');" class="btn-blue" title="立即执行"><span  class="glyphicon glyphicon-screenshot" aria-hidden="true"></a></span>';
                        res += '&nbsp;&nbsp;<span><a href="${ctx}/oracle/plan/exec/'+row.id+'" ';
                        res += 'data-toggle="modal" data-target="#add-model" class="btn-blue" title="执行进度"><span  class="glyphicon glyphicon-align-justify" aria-hidden="true"></a></span>';
                        return res;
                    }
                }
            ],
            dataPlain: true
        });
    });

    function paramClick(){
        var opt = {
            url: $url_default_db,
            silent: true,
            query:{
                head: $('#dbid').val()
            }
        };
        $('#default_db').bootstrapTable('refresh', opt);
    }

    function paramClean(){
        $('#dbid').val('');
    }

    function downloadDb(){
        $.ajax({
            url: '${ctx}/oracle/db/dbFileExists',
            type: 'post',
            success: function(data){
                if(data == 'success'){
                    location.href = '${ctx}/oracle/db/downloadDb';
                } else if(data == 'error'){
                    toastr.error('请先生成db文件');
                }
            }
        });
    }

    function del(id){
        Ewin.confirm({ message: "确认要删除该实例吗?" }).on(function (e) {
            if (!e) {
                return;
            }
            $.ajax({
                type: 'post',
                cache: false,
                url: "${ctx}/oracle/instance/delete/" + id,
                success: function(data) {
                    if(data == 'success'){
                        toastr.success('参数删除成功');
                        $default_db.bootstrapTable('refresh');
                    }else{
                        toastr.error('参数删除失败');
                    }
                },
                error: function () {
                    toastr.error('操作失败');
                }
            });
        });
    }

    function buildFile(){
        Ewin.confirm({ message: "确认要生成db配置文件吗?" }).on(function (e) {
            if (!e) {
                return;
            }
            $.ajax({
                type: 'post',
                cache: false,
                url: "${ctx}/oracle/instance/build",
                success: function(data) {
                    if(data == 'success'){
                        toastr.success('文件生成成功');
                    }else{
                        toastr.error('文件生成失败');
                    }
                },
                error: function () {
                    toastr.error('操作失败');
                }
            });
        });
    }

    function execProcess(id){
        Ewin.confirm({ message: "确认要立即执行该实例恢复吗?" }).on(function (e) {
            if (!e) {
                return;
            }
            $.ajax({
                type: 'post',
                cache: false,
                url: "${ctx}/oracle/plan/exec/Immediately/" + id,
                success: function(data) {
                    if(data == 'success'){
//                        toastr.success('执行成功成功');
//                        $time_plan.bootstrapTable('refresh');
                    }else{
                        toastr.error('执行失败');
                    }
                },
                error: function () {
                    toastr.error('操作失败');
                }
            });
        });
    }
</script>
</body>

</html>