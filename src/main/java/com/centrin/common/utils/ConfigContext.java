package com.centrin.common.utils;
/**
 * Project Name：work-platform
 * File Name：ConfigContext
 * Package Name：com.centrin.common.utils
 * Date：15/12/22 上午10:12
 * Copyright (c) 2015, Centrin Data Systems Ltd. All Rights Reserved.
 * 中金数据系统有限公司
 */


import java.util.ResourceBundle;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.common.utils
 * @ClassName ConfigContext
 * @author 张辉
 * @date 15/12/22 上午10:12
 * @version
 */
public class ConfigContext {

    public static class FileConfig{
        public static final String TEMP_PATH = ResourceBundle.getBundle("application").getString("tempPath");

        public static final String FINAL_PATH = ResourceBundle.getBundle("application").getString("finalPath");

        public static final String ZIP_PATH = ResourceBundle.getBundle("application").getString("zipPath");
        
        public static final String DOC_PATH = ResourceBundle.getBundle("application").getString("docPath");
    }
}
