package com.centrin.veritas.dao.impl;
/**
 * Project Name：veritas
 * File Name：SnDaoImpl
 * Package Name：com.centrin.veritas.dao.impl
 * Date：17/8/7 下午1:15
 */

import com.centrin.common.dao.BaseDaoImpl;
import com.centrin.veritas.dao.SnDao;
import com.centrin.veritas.entity.SnEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao.impl
 * @ClassName SnDaoImpl
 * @author 张辉
 * @date 17/8/7 下午1:15
 * @version
 */
@Repository
public class SnDaoImpl extends BaseDaoImpl implements SnDao {

    @Override
    public SnEntity findNew() {
        String sql = "select * from SN";
        List<SnEntity> list = findBySql(SnEntity.class, sql);
        return list.get(0);
    }
}
