package com.centrin.veritas.controller;
/**
 * Project Name：veritas
 * File Name：VeritasDbController
 * Package Name：com.centrin.veritas.controller
 * Date：2017/8/30 上午11:34
 * Copyright (c) 2017, Centrin Data Systems Ltd. All Rights Reserved.
 * 中金数据系统有限公司
 */

import com.alibaba.fastjson.JSONObject;
import com.centrin.common.controller.BaseController;
import com.centrin.common.entity.PagingEntity;
import com.centrin.veritas.entity.*;
import com.centrin.veritas.service.VeritasDbParamService;
import com.centrin.veritas.service.VeritasDbService;
import com.centrin.veritas.service.VeritasOracleInstanceService;
import com.centrin.veritas.service.VeritasOracleService;
import com.centrin.veritas.utils.FileUtil;
import com.centrin.veritas.utils.SystemUtils;
import com.centrin.veritas.utils.VeritasConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.controller
 * @ClassName VeritasDbController
 * @author 张辉
 * @date 2017/8/30 上午11:34
 * @version
 */
@Controller
@RequestMapping(value = "/db2")
public class VeritasDbController extends BaseController{
    @Autowired
    private VeritasOracleService veritasOracleService;

    @Autowired
    private VeritasOracleInstanceService veritasOracleInstanceService;

    @Autowired
    private VeritasDbService veritasDbService;

    @Autowired
    private VeritasDbParamService veritasDbParamService;

    private static final Integer DB_TYPE = 1;

    /*******************************default start***************************************/
    @RequestMapping("/default")
    private String index(){
        return "/veritas/db2/default";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 根据章节头获取章节参数列表
     * @date 17/7/21
     */
    @RequestMapping(value = "/default/json")
    @ResponseBody
    public PagingEntity<VeritasDefaultDbEntity> findByHead(@RequestParam Map<String, String> param, HttpServletRequest request){
        param.put("type", "1");
        PagingEntity<VeritasDefaultDbEntity> page = getPage(request);
        System.out.println(SystemUtils.getIpReal());
        return veritasOracleService.findPageList(param, page);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 生成默认数据
     * @date 2017/8/17
     */
    @RequestMapping(value = "/default/param/build")
    @ResponseBody
    public String paramBuild(){
        return veritasOracleService.paramBuild(DB_TYPE);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 生成default文件
     * @date 17/7/22
     */
    @RequestMapping(value = "/default/build")
    @ResponseBody
    public String buildDefault() throws Exception{
        try {
            veritasOracleService.buildDbDefault();
        } catch (Exception e) {
//            e.printStackTrace();
            return "error";
        }
        return "success";
    }

    @RequestMapping(value = "/default/defaultFileExists")
    @ResponseBody
    public String defaultFileExists(){
        File file = new File(VeritasConstant.DB2_DEFAULT_PATH + File.separator + "default.conf");
        if (file.exists()){
            return "success";
        }
        return "error";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 默认文件下载
     * @date 17/7/23
     */
    @RequestMapping(value = "/default/downloadDefault")
    @ResponseBody
    public void downloadDefault(HttpServletResponse response) {
        try {
            FileUtil.download(response, VeritasConstant.DB2_DEFAULT_PATH + File.separator + "default.conf");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*******************************default end***************************************/
    /*******************************db start***************************************/
    /**
     * @param
     * @return
     * @author 张辉
     * @description instance配置文件跳转
     * @date 17/7/23
     */
    @RequestMapping(value = "/instance")
    public String instanceSkip(){
        return "/veritas/db2/instanceConfig";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description instance取章节参数列表
     * @date 17/7/21
     */
    @RequestMapping(value = "/instance/json")
    @ResponseBody
    public PagingEntity<VeritasDbEntity> findInstanceByHead(@RequestParam Map<String, String> params, HttpServletRequest request){
        PagingEntity<VeritasDbEntity> page = getPage(request);
        return veritasDbService.findPage(page, params);
    }

    @RequestMapping(value = "/instance/allList")
    @ResponseBody
    public List<VeritasOracleDbInstanceEntity> findList(){
        return veritasOracleInstanceService.findList();
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description Instance配置文件添加跳转
     * @date 17/7/23
     */
    @RequestMapping(value = "/instance/createSkip")
    public String createInstanceSkip(Model model){
        VeritasDbEntity veritasDbEntity = new VeritasDbEntity();
        model.addAttribute("action", "create");
        model.addAttribute("veritas", veritasDbEntity);
        model.addAttribute("instanceOperate", "添加");
        return "/veritas/db2/instanceForm";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description Instance配置文件创建
     * @date 17/7/23
     */
    @RequestMapping(value = "/instance/create")
    @ResponseBody
    public String createInstance(VeritasDbEntity veritasDbEntity, Integer instanceId){
        veritasDbService.createDbInstance(veritasDbEntity, instanceId);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 获取param参数列表
     * @date 17/7/25
     */
    @RequestMapping(value = "/instance/param/json")
    @ResponseBody
    public PagingEntity<VeritasDbParamEntity> findParamPage(Integer instanceId, HttpServletRequest request){
        PagingEntity<VeritasDbParamEntity> page = getPage(request);
        List<VeritasDbParamEntity> list = veritasDbParamService.findByInstanceIdList(instanceId);
        page.setRows(list);
        return page;
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 添加实例参数
     * @date 17/7/25
     */
    @RequestMapping(value = "/instance/param/add")
    @ResponseBody
    public String createParam(VeritasDbParamEntity veritasDbParamEntity){
        veritasDbParamService.add(veritasDbParamEntity);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 删除实例参数
     * @date 17/7/25
     */
    @RequestMapping(value = "/instance/param/delete/{id}")
    @ResponseBody
    public String deleteParam(@PathVariable("id") Integer id){
        veritasDbParamService.delete(VeritasDbParamEntity.class, id);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description Instance配置文件更新跳转
     * @date 17/7/23
     */
    @RequestMapping(value = "/instance/update/{id}")
    public String updateInstanceSkip(Model model, @PathVariable("id") Integer id){
        model.addAttribute("action", "update");
        model.addAttribute("instanceOperate", "修改");
        model.addAttribute("veritas", veritasDbService.findById(VeritasDbEntity.class, id));
        return "/veritas/db2/instanceForm";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description Instance配置文件更新
     * @date 17/7/23
     */
    @RequestMapping(value = "/instance/update")
    @ResponseBody
    public String updateInstance(VeritasDbEntity veritasDbEntity){
        veritasDbService.update(veritasDbEntity);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 删除实例
     * @date 17/7/25
     */
    @RequestMapping(value = "/instance/delete/{id}")
    @ResponseBody
    public String deleteInstance(@PathVariable("id") Integer id){
        veritasDbService.deleteById(id);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 校验实例名是否存在
     * @date 2017/8/31
     */
    @RequestMapping(value = "/check")
    @ResponseBody
    public String checkChapterName(String chapterName, Integer id){
        JSONObject json = new JSONObject();
        VeritasDbEntity veritasDbEntity = veritasDbService.findByChapterName(chapterName);
        if (null != veritasDbEntity && !veritasDbEntity.getId().equals(id)){
            json.put("valid", false);
        } else {
            json.put("valid", true);
        }
        return json.toString();
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description db配置文件是否存在
     * @date 17/7/23
     */
    @RequestMapping(value = "/db/dbFileExists")
    @ResponseBody
    public String dbFileExists(){
        File file = new File(VeritasConstant.DB2_DB_PATH + File.separator + "db.conf");
        if (file.exists()){
            return "success";
        }
        return "error";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description db配置文件下载
     * @date 17/7/23
     */
    @RequestMapping(value = "/db/downloadDb")
    @ResponseBody
    public void downloadDb(HttpServletResponse response) {
        try {
            FileUtil.download(response, VeritasConstant.DB2_DB_PATH + File.separator + "db.conf");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description db文件生成
     * @date 17/7/25
     */
    @RequestMapping(value = "/instance/build")
    @ResponseBody
    public String instanceBuild(){
        try {
            veritasDbService.instanceBuild();
        } catch (Exception e) {
            return "error";
        }
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 获取可用的实例参数列表
     * @date 2017/8/17
     */
    @RequestMapping(value = "/instance/findUsableList")
    @ResponseBody
    public List<VeritasDefaultDbEntity> findUsableList(@RequestParam Map<String, String> param){
        param.put("type", "0");
        return veritasOracleService.findUsableList(param);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 执行进度查看
     * @date 17/8/2
     */
    @RequestMapping(value = "/exec/{id}")
    public String execPlan(@PathVariable("id") Integer id, Model model){
        model.addAttribute("instance",
                veritasDbService.findById(VeritasDbEntity.class, id));
        return "/veritas/oracle/oracleProcess";
    }

    /*******************************db end***************************************/
    /*******************************plan start***************************************/
    /**
     * @param
     * @return
     * @author 张辉
     * @description 执行计划跳转
     * @date 17/7/26
     */
    @RequestMapping(value = "/plan")
    public String planIndex(){
        return "/veritas/db2/timePlan";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 执行计划添加跳转
     * @date 17/7/26
     */
    @RequestMapping(value = "/plan/createSkip")
    public String createSkip(Model model){
        model.addAttribute("action", "create");
        model.addAttribute("planType", "添加");
        model.addAttribute("plan", new VeritasOracleTimePlanEntity());
        return "/veritas/db2/timeForm";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 立即执行
     * @date 17/8/17
     */
    @RequestMapping(value = "/plan/exec/Immediately/{id}")
    @ResponseBody
    public String execImmediately(@PathVariable("id") Integer id){
        veritasDbService.execImmediately(id);
        return "success";
    }
    /*******************************plan end***************************************/
}
