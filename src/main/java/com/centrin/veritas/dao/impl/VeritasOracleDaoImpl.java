package com.centrin.veritas.dao.impl;
/**
 * Project Name：veritas
 * File Name：VeritasDefaultDbDaoImpl
 * Package Name：com.centrin.veritas.dao.impl
 * Date：17/7/21 下午3:46
 */

import com.centrin.common.dao.BaseDaoImpl;
import com.centrin.veritas.dao.VeritasOracleDao;
import com.centrin.veritas.entity.VeritasDefaultDbEntity;
import com.centrin.veritas.entity.VeritasOracleDbInstanceParamEntity;
import com.centrin.veritas.entity.VeritasOracleEntity;
import com.centrin.veritas.utils.SystemUtils;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao.impl
 * @ClassName VeritasDefaultDbDaoImpl
 * @author 张辉
 * @date 17/7/21 下午3:46
 * @version
 */
@Repository
public class VeritasOracleDaoImpl extends BaseDaoImpl implements VeritasOracleDao {

    @Override
    public List<VeritasDefaultDbEntity> findUsableList(Map<String, String> param,
                                                       List<VeritasOracleDbInstanceParamEntity> paramList) {
        String ip = SystemUtils.getHostIp(SystemUtils.getInetAddress());
        String sql = "select * from veritas_oracle_db where " +
                "chapter_head = '[" + param.get("head").replace("[", "").replace("]", "") + "]' " +
                "and type = " + param.get("type") + " and ip = '" + ip + "'";
        StringBuffer buffer = new StringBuffer();
        if (null != paramList && paramList.size() > 0){
            for (VeritasOracleDbInstanceParamEntity veritasOracleDbInstanceParamEntity : paramList){
                buffer.append(veritasOracleDbInstanceParamEntity.getParameterId() + ",");
            }
            String str = buffer.toString();
            sql +=  " and id not in (" + str.substring(0, str.length() -1) + ")";
        }
        return findBySql(VeritasDefaultDbEntity.class, sql);
    }

    @Override
    public List<VeritasOracleEntity> findAll(Integer type) {
        String sql = "select * from veritas_oracle where type = " + type;
        return findBySql(VeritasOracleEntity.class, sql);
    }

    @Override
    public List<VeritasDefaultDbEntity> findByIp(String ip, Integer type) {
        String sql = "select * from veritas_oracle_db where ip = '" + ip + "' and type = " + type;
        return findBySql(VeritasDefaultDbEntity.class, sql);
    }
}
