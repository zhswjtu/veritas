/** 
 * Project Name：jfshop 
 * File Name：aaa.java 
 * Package Name：com.centrin.common.security 
 * Date：2016-9-9 下午2:05:06 
 * Copyright (c) 2016, Centrin Data Systems Ltd. All Rights Reserved. 
 * 中金数据系统有限公司 
 */
/**
 * 
 */
package com.centrin.common.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class XssHttpServletRequestWrapper extends HttpServletRequestWrapper {

	public XssHttpServletRequestWrapper(HttpServletRequest servletRequest) {
		super(servletRequest);
	}

	public String[] getParameterValues(String parameter) {
		String[] values = super.getParameterValues(parameter);
		if (values == null) {
			return null;
		}
		int count = values.length;
		String[] encodedValues = new String[count];
		for (int i = 0; i < count; i++) {
			encodedValues[i] = XssUtils.cleanXSS(values[i]);
		}
		return encodedValues;
	}

	public String getParameter(String parameter) {
		String value = super.getParameter(parameter);
		if (value == null) {
			return null;
		}
		return XssUtils.cleanXSS(value);
	}

	public String getHeader(String name) {
		String value = super.getHeader(name);
		if (value == null)
			return null;
//		return cleanXSS(value);
		return value;
	}

	protected static boolean sqlValidate(String str) {  
		   str = str.toLowerCase();//统一转为小写
		   //String badStr = "'|and|exec|execute|insert|select|delete|update|count|drop|chr|mid|master|truncate|char|declare|sitename|net user|xp_cmdshell|or|like";  
		   String badStr = "'|and|exec|execute|insert|select|delete|update|count|drop|chr|mid|master|truncate|char|declare|sitename|net user|xp_cmdshell|or|like";  
		   /*String badStr = "'|and|exec|execute|insert|create|drop|table|from|grant|use|group_concat|column_name|" +  
		                    "information_schema.columns|table_schema|union|where|select|delete|update|order|by|count|*|" +  
		                    "chr|mid|master|truncate|char|declare|or|;|-|--|+|,|like|//|/|%|#";    */    //过滤掉的sql关键字，可以手动添加  
		  String[] badStrs = badStr.split("\\|");  
		  for (int i = 0; i < badStrs.length; i++) {
			  if (str.indexOf(badStrs[i]) !=-1) { 
				  return true;  
			  }  
		  }  
		  return false;  
	}  
}