package com.centrin.veritas.dao;
/**
 * Project Name：veritas
 * File Name：SnDao
 * Package Name：com.centrin.veritas.dao
 * Date：17/8/7 下午1:14
 */

import com.centrin.common.dao.BaseDao;
import com.centrin.veritas.entity.SnEntity;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao
 * @ClassName SnDao
 * @author 张辉
 * @date 17/8/7 下午1:14
 * @version
 */
public interface SnDao extends BaseDao{

    SnEntity findNew();
}
