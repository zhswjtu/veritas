package com.centrin.system.service;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.centrin.common.entity.PagingEntity;
import com.centrin.common.utils.DateUtils;
import com.centrin.system.dao.OrgDao;
import com.centrin.system.dao.RoleDao;
import com.centrin.system.dao.UserDao;
import com.centrin.system.entity.OrgEntity;
import com.centrin.system.entity.RoleEntity;
import com.centrin.system.entity.UserEntity;
import com.centrin.system.error.SysException;
import com.centrin.system.shiro.realm.ShiroUser;


@Service
public class UserService {

	private static final String DEFAULT_PASSWORD = "111111";
    @Autowired
    private UserDao userDao;

    @Autowired
    private OrgDao orgDao;

    @Autowired
    private RoleDao roleDao;
    
    @Autowired
    private PasswordHelper passwordHelper;
    
    public void save(UserEntity userEntity) {
        userDao.add(userEntity);
    }
    
    @Cacheable(value="sysUserCache", key="#root.methodName.concat(#username)")
    public UserEntity getValidUser(String username) {
    	return userDao.getValidUserByUsername(username);
//        String sql = "select * from sys_user WHERE status = "+UserEntity.STATUS_VALID+" AND del_flag = "+UserEntity.DEL_FLAY_NO+" AND username = '" + username + "'";
//        List<UserEntity> list = userDao.findBySql(UserEntity.class,sql,null,-1,-1);
//        return (list == null || list.size() == 0) ? null : list.get(0);
    }
    
//    @Cacheable(value="sysUserCache", key="#root.methodName.concat(#username)")
    public UserEntity getUser(String username) {
    	return userDao.getUserByUsername(username);
//        String sql = "select * from sys_user WHERE status = "+UserEntity.STATUS_VALID+" AND del_flag = "+UserEntity.DEL_FLAY_NO+" AND username = '" + username + "'";
//        List<UserEntity> list = userDao.findBySql(UserEntity.class,sql,null,-1,-1);
//        return (list == null || list.size() == 0) ? null : list.get(0);
    }
    
   
    @Transactional
    public void addUser(UserEntity user){
        user.setCreateTime(DateUtils.getDateTime());
        if(user.getDepId() != null){
	        OrgEntity org = orgDao.findById(OrgEntity.class, user.getDepId());
	        Set<OrgEntity> orgs = new HashSet<OrgEntity>();
	        orgs.add(org);
	        user.setOrgs(orgs);
        }
        userDao.add(user);
        
        //activiti中增加用户
//        activitiService.addUser(user);
    }

    public PagingEntity<UserEntity> getUserList(Map<String, Object> map){
        return this.userDao.findUserList(map);
    }

    public UserEntity get(Integer id){
        return this.userDao.findById(UserEntity.class, id);
    }

    public void update(UserEntity userEntity){
        this.userDao.update(userEntity);
    }

    public void delete(Integer id){
        UserEntity userEntity = this.userDao.findById(UserEntity.class, id);
        userEntity.setDelFlag(UserEntity.DEL_FLAG_YES);
        this.userDao.update(userEntity);
    }

    public List<RoleEntity> getUserRoleIds(Integer id){
        return userDao.getUserRoleIds(id);
    }

    public void deleteUserRoleByUserId(Integer id){
        userDao.deleteUserRoleByUserId(id);
    }

    @Transactional
    public void addUserRole(Integer id, List<Integer> list){
//        userDao.addUserRole(id, list);
        Set<RoleEntity> roles = new HashSet<RoleEntity>();
        UserEntity user = this.userDao.findById(UserEntity.class, id);
        for (int i = 0; i < ((null == list) ? 0: list.size()); i++){
            RoleEntity roleEntity = roleDao.findById(RoleEntity.class, list.get(i));
            roles.add(roleEntity);
        }
        user.clearRoles();
        user.setRoles(roles);
        userDao.update(user);
    }

    public void addUserOrg(Integer userId, Integer orgId){
        userDao.addUserOrg(userId, orgId);
    }

    public List<OrgEntity> getOrgByUserId(Integer userId){
        return userDao.getOrgByUserId(userId);
    }

    public void deleUserOrgByUserId(Integer userId){
        userDao.deleUserOrgByUserId(userId);
    }

    @Transactional
    @CacheEvict(value="sysDicItemCache", allEntries=true)
    public void updateUserInfo(UserEntity userEntity) throws Exception{
        UserEntity newUser = this.userDao.findById(UserEntity.class, userEntity.getId());
        Set<RoleEntity> roles = newUser.getRoles();
        Set<OrgEntity> orgs = newUser.getOrgs();
        
        userEntity.setLoginCount(null); //设置loginCount不更新
        Class<? extends UserEntity> cls = userEntity.getClass();  
        Field[] fields = cls.getDeclaredFields();  
        for(int i=0; i<fields.length; i++){  
            Field f = fields[i];  
            f.setAccessible(true);  
            if(f.get(userEntity)!=null) {
            	BeanUtils.setProperty(newUser, f.getName(), f.get(userEntity));
            }
        }   
  
        if(!newUser.getDepId().equals(userEntity.getDepId()) || orgs.isEmpty()) {
	        //OrgEntity org = orgDao.findById(OrgEntity.class, userEntity.getDepId());
        	orgs = new HashSet<OrgEntity>();
        	OrgEntity org = new OrgEntity();
	        org.setId(userEntity.getDepId());
	        orgs.add(org);
        }
        newUser.setOrgs(orgs);
        newUser.setRoles(roles);
        
        userDao.update(newUser);
    }
    
    /**
     * 修改用户密码
     * @Title: updatePassword
     * @Description:
     * @param shiroUser
     * @param oldPassword
     * @param newPassword
     * @throws SysException
     */
    @Transactional
    public void updatePassword(ShiroUser shiroUser, String oldPassword, String newPassword) throws SysException {
    	UserEntity user = userDao.getValidUserByUsername(shiroUser.getUsername());
    	String encryptOldPassword = passwordHelper.encryptPassword(oldPassword, user.getSalt());
    	if(user.getPassword().equals(encryptOldPassword)){
    		String encryptNewPassword = passwordHelper.encryptPassword(newPassword,user.getSalt());
    		user.setPassword(encryptNewPassword);
       		userDao.update(user);
    	} else {
    		throw new SysException("旧密码不正确！");
    	}
    	
    }
    
    /**
     * 更新用户登录信息
     * @Title: updateLoginInfo
     * @Description:
     * @param userId
     */
    @Transactional
    public void updateLoginInfo(Integer userId) {
    	userDao.updateUserLoginInfo(userId);
    }
    
    
    
    @Transactional
    public void resetPwd(Integer userId) {
    	UserEntity user = userDao.findById(UserEntity.class, userId);
    	String encryptNewPassword = passwordHelper.encryptPassword(DEFAULT_PASSWORD,user.getSalt());
		user.setPassword(encryptNewPassword);
   		userDao.update(user);
    }


    /**
     * 获取机构下的员工信息
     * @param map
     * @return
     */
    public List<UserEntity> getUserByOrg(Map<String, Object> map){
        return userDao.getUserByOrg(map);
    }
    
    
}
