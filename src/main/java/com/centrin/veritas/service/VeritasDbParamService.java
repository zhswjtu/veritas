package com.centrin.veritas.service;
/**
 * Project Name：veritas
 * File Name：VeritasDbParamService
 * Package Name：com.centrin.veritas.service
 * Date：2017/8/30 下午5:17
 */

import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseService;
import com.centrin.veritas.entity.VeritasDbParamEntity;

import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service
 * @ClassName VeritasDbParamService
 * @author 张辉
 * @date 2017/8/30 下午5:17
 * @version
 */
public interface VeritasDbParamService extends BaseService<VeritasDbParamEntity> {

    List<VeritasDbParamEntity> findByInstanceIdList(Integer instanceId);
}
