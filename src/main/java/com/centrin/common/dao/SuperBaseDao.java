package com.centrin.common.dao;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class SuperBaseDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	public enum QueryType {
		QueryBySql, QueryByJpql
	}

	@Deprecated
	public <T> List<T> findByCondition(Class<T> c, String condition,
			String orderBy) {
		return findByCondition(c, condition, orderBy, -1, -1);
	}

	@SuppressWarnings("unchecked")
	@Deprecated
	public <T> List<T> findByCondition(Class<T> c, String condition,
			String orderBy, int start, int limit) {
		String jpql = "select x from " + c.getSimpleName() + " x";
		if (condition != null && !"".equals(condition)) {
			jpql = jpql + " where " + condition;
		}

		if (orderBy != null && !"".equals(orderBy)) {
			jpql = jpql + " order by " + orderBy;
		}

		Query query = entityManager.createQuery(jpql);
		if (start >= 0 && limit > 0) {
			query.setFirstResult(start);
			query.setMaxResults(limit);
		}
		return query.getResultList();
	}

	@Deprecated
	public <T> Integer getCountByCondition(Class<T> c, String condition) {
		String jpql = "select count(x) from " + c.getSimpleName() + " x";
		if (condition != null && !"".equals(condition)) {
			jpql = jpql + " where " + condition;
		}
		return Integer.parseInt(entityManager.createQuery(jpql)
				.getSingleResult().toString());
	}

	
	public <T> T findById(Class<T> c, int id) {
		T result = entityManager.find(c, id);
		return result;
	}

	public <T> List<T> findAll(Class<T> c) {
		return findAll(c, -1, -1, null);
	}

	public <T> List<T> findAll(Class<T> c, String orderBy) {
		return findAll(c, -1, -1, orderBy);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findAll(Class<T> c, int start, int limit, String orderBy) {
		String jpql = "select x from " + c.getSimpleName() + " x";
		if (orderBy != null) {
			jpql = jpql + " order by " + orderBy;
		}
		Query query = entityManager.createQuery(jpql);
		if (start >= 0 && limit > 0) {
			query.setFirstResult(start);
			query.setMaxResults(limit);
		}
		return query.getResultList();
	}
	

	public <T> Integer count(Class<T> c) {
		String jpql = "select count(x) from " + c.getSimpleName() + " x";
		return Integer.parseInt(entityManager.createQuery(jpql)
				.getSingleResult().toString());
	}
	

	@Transactional
	public <T> T update(T t) {
		return entityManager.merge(t);
	}

	@Transactional
	public <T> void add(T t) {
		entityManager.persist(t);
	}
	
	@Transactional
	public <T> void add(List<T> list) {
		for (int i = 0; i < list.size(); i++) {
			entityManager.persist(list.get(i));
		}
	}

	@Transactional
	public <T> void delete(Class<T> c, int id) {
		entityManager.remove(entityManager.getReference(c, id));
	}

	@Transactional
	public <T> void deletes(Class<T> c, List<Integer> ids) {
		for(Integer id : ids) {
			entityManager.remove(entityManager.getReference(c, id));
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public <T> List<T> find(Class<T> c, String value, Map<String, Object> params,
			int start, int limit, QueryType queryType) {
		Query query = null;
		if (queryType.equals(QueryType.QueryByJpql)) {
			if (c != null) {
				query = entityManager.createQuery(value,c);
			} else {
				query = entityManager.createQuery(value);
				query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP); 
			}
		} else {
			if (c != null) {
				if(value.indexOf('*')>-1 && 
						value.indexOf('*') < value.toLowerCase().indexOf("from")) {
					query = entityManager.createNativeQuery(value, c);
				} else {
					query = entityManager.createNativeQuery(value);
					query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(c)); 
				}
			} else {
				query = entityManager.createNativeQuery(value);
				query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP); 
			}
		}
		if (params != null) {
			for(String key :params.keySet()) {
				query.setParameter(key, params.get(key));
			}
		}
		if (start >= 0 && limit > 0) {
			query.setFirstResult(start);
			query.setMaxResults(limit);
		}
		return query.getResultList();
	}

	
	public <T> List<T> findBySql(String sql) {
		return find(null, sql, null, -1, -1, QueryType.QueryBySql);
	}
	
	public <T> List<T> findBySql(Class<T> c, String sql) {
		return find(c, sql, null, -1, -1, QueryType.QueryBySql);
	}
	
	
	public <T> List<T> findBySql(String sql, Map<String, Object> params) {
		return find(null, sql, params, -1, -1, QueryType.QueryBySql);
	}
	
	
	public <T> List<T> findBySql(Class<T> c, String sql, Map<String, Object> params) {
		return find(c, sql, params, -1, -1, QueryType.QueryBySql);
	}
	
	
	public <T> List<T> findBySql(String sql, Map<String, Object> params,
			int start, int limit) {
		return find(null, sql, params, start, limit, QueryType.QueryBySql);
	}
	
	public <T> List<T> findBySql(Class<T> c, String sql, Map<String, Object> params,
			int start, int limit) {
		return find(c, sql, params, start, limit, QueryType.QueryBySql);

	}
	
	
	public <T> List<T> findByJpql(String jpql) {
		return find(null, jpql, null, -1, -1, QueryType.QueryByJpql);
	}

	
	public <T> List<T> findByJpql(Class<T> c, String jpql) {
		return find(c, jpql, null, -1, -1, QueryType.QueryByJpql);
	}
	
	
	public <T> List<T> findByJpql(String jpql, Map<String, Object> params) {
		return find(null, jpql, params, -1, -1, QueryType.QueryByJpql);
	}
	
	public <T> List<T> findByJpql(Class<T> c, String jpql, Map<String, Object> params) {
		return find(c, jpql, params, -1, -1, QueryType.QueryByJpql);
	}
	
	public <T> List<T> findByJpql( String jpql, Map<String, Object> params,
			int start, int limit) {
		return find(null, jpql, params, start, limit, QueryType.QueryByJpql);
	}
	
	public <T> List<T> findByJpql(Class<T> c, String jpql, Map<String, Object> params,
			int start, int limit) {
		return find(c, jpql, params, start, limit, QueryType.QueryByJpql);
	}
	
	public <T> Integer count(String value, Map<String, Object> params, QueryType queryType) {
		Query query = null;
		if (queryType.equals(QueryType.QueryByJpql)) {
			query = entityManager.createQuery(value);
		} else {
			query = entityManager.createNativeQuery(value);
		}
		if (params != null) {
			for(String key : params.keySet()) {
				query.setParameter(key, params.get(key));
			}
		}
		
		return Integer.parseInt(query.getSingleResult().toString());
	}
	
	
	public <T> Integer countBySql(String sql) {
		return count(sql, null, QueryType.QueryBySql);
	}
	
	public <T> Integer countBySql(String sql, Map<String, Object> params) {
		return count(sql, params, QueryType.QueryBySql);
	}
	
	
	public <T> Integer countByJpql(String sql) {
		return count(sql, null, QueryType.QueryByJpql);
	}
	
	public <T> Integer countByJpql(String sql, Map<String, Object> params) {
		return count(sql, params, QueryType.QueryByJpql);
	}
	

	@Transactional
	public void executeSql(String[] sqls) {
		for (String sql : sqls) {
			entityManager.createNativeQuery(sql).executeUpdate();
		}
	}

	
	@Transactional
	public void executeSql(String sql) {
		entityManager.createNativeQuery(sql).executeUpdate();

	}
	
	
	@SuppressWarnings("unchecked")
	public <T> T getSingle(Class<T> c, String value, Map<String, Object> params, QueryType queryType) {
		Query query = null;
		if (queryType.equals(QueryType.QueryByJpql)) {
			if (c != null) {
				query = entityManager.createQuery(value, c);
			} else {
				query = entityManager.createQuery(value);
				query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP); 
			}
		} else {
			if (c != null) {
				if(value.indexOf('*')>-1 && 
						value.indexOf('*') < value.toLowerCase().indexOf("from")) {
					query = entityManager.createNativeQuery(value, c);
				} else {
					query = entityManager.createNativeQuery(value);
					query.unwrap(SQLQuery.class).setResultTransformer(Transformers.aliasToBean(c)); 
				}
			} else {
				query = entityManager.createNativeQuery(value);
				query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP); 
			}
		}
		if (params != null) {
			for(String key :params.keySet()) {
				query.setParameter(key, params.get(key));
			}
		}
		
		List<T> list = query.getResultList();
		if(list!=null && list.size()>0) {
			return list.get(0);
		} else {
			return null;
		}

	}

	public <T> T getSingleByJpql(String Jpql, Map<String, Object> params) {
		 return getSingle(null, Jpql, params, QueryType.QueryByJpql);
	}
	
	
	public <T> T getSingleByJpql(Class<T> c, String Jpql, Map<String, Object> params) {
		 return getSingle(c, Jpql, params, QueryType.QueryByJpql);
	}
	
	
	public <T> T getSingleBySql(String sql, Map<String, Object> params) {
		 return getSingle(null, sql, params, QueryType.QueryBySql);
	}
	
	public <T> T getSingleBySql(Class<T> c, String sql, Map<String, Object> params) {
		 return getSingle(c, sql, params, QueryType.QueryBySql);
	}
	
	
	
	/**
	 * 获取 Session
	 */
	public Session getSession(){  
		return (Session) entityManager.getDelegate();
	}
	
	
	/**
	 * 获取全文Session
	 */
	public FullTextSession getFullTextSession(){
		return Search.getFullTextSession(getSession());
	}
	
	
	/**
	 * 建立索引
	 */
	public <T> void createIndex(Class<T> c){
		try {
			getFullTextSession().createIndexer(c).startAndWait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}	
}
