package com.centrin.system.shiro.realm;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.centrin.system.entity.RoleEntity;

/**
 * 自定义Authentication对象，使得Subject除了携带用户的登录名外还可以携带更多信息.
 */
public class ShiroUser implements Serializable {
	private static final long serialVersionUID = -5071431511927147793L;
	public Integer id;
	public String username;
	public String name;
//	public Integer depId;
//	public List<RoleEntity> roles;

//	public ShiroUser(Integer id, String username, String name, Integer depId, List<RoleEntity> roles) {
	public ShiroUser(Integer id, String username, String name) {
		this.id = id;
		this.username = username;
		this.name = name;
//		this.depId = depId;
//		this.roles = roles;
	}
	
	public Integer getId(){
		return id;
	}

	public String getName() {
		return name;
	}

	public String getUsername() {
		return username;
	}

	
	/**
	 * 本函数输出将作为默认的<shiro:principal/>输出.
	 */
	@Override
	public String toString() {
		return username;
	}

	/**
	 * 重载hashCode,只计算username;
	 */
	@Override
	public int hashCode() {
		return Objects.hashCode(username);
	}

	/**
	 * 重载equals,只计算username;
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ShiroUser other = (ShiroUser) obj;
		if (username == null) {
			if (other.username != null) {
				return false;
			}
		} else if (!username.equals(other.username)) {
			return false;
		}
		return true;
	}
}