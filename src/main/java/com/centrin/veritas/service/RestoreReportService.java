package com.centrin.veritas.service;
/**
 * Project Name：veritas
 * File Name：RestoreReportService
 * Package Name：com.centrin.veritas.service
 * Date：17/7/26 下午3:42
 */

import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseService;
import com.centrin.veritas.entity.RestoreReportEntity;

import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service
 * @ClassName RestoreReportService
 * @author 张辉
 * @date 17/7/26 下午3:42
 * @version
 */
public interface RestoreReportService extends BaseService<RestoreReportEntity>{

    PagingEntity<RestoreReportEntity> findPageList(PagingEntity<RestoreReportEntity> page, Map<String, Object> param);
}
