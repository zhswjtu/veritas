package com.centrin.common.dto;

public class RspBaseInfo {

	public static final String  SUCCESS = "0";
	
	public static final String  ERROR = "1";
	
	
	private String code;

    private String msg;

    private Object data = "";
    
    public RspBaseInfo() {
    	
    }
    
    public RspBaseInfo(String code, String msg) {
    	this.code = code;
    	this.msg = msg;
    }
    
    public RspBaseInfo(String code, String msg, Object data) {
    	this.code = code;
    	this.msg = msg;
    	if(data!=null) this.data = data;
    }
    
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
    
}
