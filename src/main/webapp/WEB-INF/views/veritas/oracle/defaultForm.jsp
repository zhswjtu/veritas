<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <title>参数修改</title>
</head>
<body>
<link rel="stylesheet" href="${ctx}/resource-Main/change/work-order.css">
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/modal.css">
<!--新增 modal-->
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">参数修改</h4>
</div>
<div class="modal-body" style="padding: 0;">
    <form class="form-horizontal" id="form_fibre" action="${ctx}/oracle/default/${action}">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="group">
                        <label class="col-sm-2 control-label">实例名称:</label>
                        <label class="col-sm-2 control-label">${veritas.chapterHead}</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="group">
                        <label class="col-sm-2 control-label">参数名称:</label>

                        <div class="col-sm-4">
                            <input class="form-control" type="text" id="parameterName" name="parameterName" value="${veritas.parameterName}"/>
                            <input class="form-control" type="hidden" id="chapterHead" name="chapterHead" value="${veritas.chapterHead}"/>
                            <input class="form-control" type="hidden" id="id" name="id" value="${veritas.id}"/>
                            <input class="form-control" type="hidden" id="type" name="type" value="${veritas.type}"/>
                            <input class="form-control" type="hidden" id="status" name="status" value="0"/>
                            <input class="form-control" type="hidden" id="ip" name="ip" value="${veritas.ip}"/>
                        </div>
                    </div>
                    <div class="group">
                        <label class="col-sm-2 control-label">参数值:</label>

                        <div class="col-sm-4">
                            <input class="form-control" type="text" id="defaultValue" name="defaultValue" value="${veritas.defaultValue}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom: 20px">
                    <label class="col-sm-2 control-label" style="padding-right:1em">说明:</label>
                    <div class="col-sm-10">
                        <textarea class="form-control area-orgList" type="text" style="height: 90px;" id="description" name="description">${veritas.description}</textarea>
                    </div>
                </div>
            </div>
            <!-- panel-body -->
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2 control-center">
                        <button type="button" class="btn btn-primary zj-btn-button" onclick="save();">提交</button>
                    </div>
                    <div class="col-sm-2 control-center">
                        <button type="button" class="btn btn-default zj-btn-button" data-dismiss="modal" id="closeBtn">
                            取消
                        </button>
                    </div>
                    <div class="col-sm-4 "></div>
                </div>
            </div>
            <!-- panel-footer -->
        </div>
    </form>
</div>
<!--新增 modal-->
<script>
    var action = '${action}';

    $(function () {
        $('#form_fibre').bootstrapValidator({
            container: 'tooltip',
            //        trigger: 'blur',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                parameterName: {
                    group: '.group',
                    validators: {
                        notEmpty: {
                            message: '参数名必填'
                        }
                    }
                }
            }
        }).on('success.form.bv', function (e) {
            var $form = $(e.target);
            $form.ajaxSubmit(function (data) {
                if (data == 'success') {
                    $default_db.bootstrapTable('refresh');
                    $("#closeBtn").click();
                } else {

                }
            });
        });
    });
    function save() {
        $('#form_fibre').bootstrapValidator('validate');
    }
</script>
</body>
</html>