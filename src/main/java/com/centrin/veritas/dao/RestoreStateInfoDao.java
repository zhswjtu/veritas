package com.centrin.veritas.dao;
/**
 * Project Name：veritas
 * File Name：RestoreStateInfoDao
 * Package Name：com.centrin.veritas.dao
 * Date：17/8/8 下午11:22
 */

import com.centrin.common.dao.BaseDao;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao
 * @ClassName RestoreStateInfoDao
 * @author 张辉
 * @date 17/8/8 下午11:22
 * @version
 */
public interface RestoreStateInfoDao extends BaseDao{
}
