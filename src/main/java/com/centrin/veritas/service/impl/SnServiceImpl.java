package com.centrin.veritas.service.impl;
/**
 * Project Name：veritas
 * File Name：SnServiceImpl
 * Package Name：com.centrin.veritas.service.impl
 * Date：17/8/7 下午1:16
 */

import com.centrin.common.dao.BaseDao;
import com.centrin.common.dao.QueryFilter;
import com.centrin.common.servcie.BaseServiceImpl;
import com.centrin.veritas.dao.SnDao;
import com.centrin.veritas.entity.SnEntity;
import com.centrin.veritas.service.SnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service.impl
 * @ClassName SnServiceImpl
 * @author 张辉
 * @date 17/8/7 下午1:16
 * @version
 */
@Service
public class SnServiceImpl extends BaseServiceImpl<SnEntity> implements SnService {

    @Autowired
    private SnDao snDao;

    @Override
    protected BaseDao getSuperBaseDao() {
        return snDao;
    }

    @Override
    @Transactional
    public SnEntity findNew() {
        QueryFilter qf = QueryFilter.newFilter();
        return findAsSingle(SnEntity.class, qf);
    }
}
