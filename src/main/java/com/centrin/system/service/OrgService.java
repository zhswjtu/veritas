package com.centrin.system.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centrin.system.dao.OrgDao;
import com.centrin.system.entity.OrgEntity;

@Service
public class OrgService {
	  private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private OrgDao orgDao;

	public List<OrgEntity> getAllOrgs() {
		return orgDao.getAllOrgs();
	}
	
	public List<OrgEntity> getAvailableOrgs(){
		return orgDao.getAvailableOrgs();
	}
	
	public OrgEntity getId(Integer id){
		return orgDao.getOrgByOrgId(id);
	}

	public void addOrg(OrgEntity orgEntity) {
		log.info(orgEntity.toString());
		orgDao.add(orgEntity);
	}

	public void updateOrg(OrgEntity orgEntity) {
		orgDao.update(orgEntity);
	}

	public void deleteOrg(Integer id) {
		log.info("getId:"+id);
		orgDao.delete(OrgEntity.class, id);
	}

	public OrgEntity getOrgByOrgName(String name) {
		return orgDao.getOrgByOrgName(name);
	}
	
	public OrgEntity getOrgByOrgCode(String code) {
		return orgDao.getOrgByOrgCode(code);
	}
}
