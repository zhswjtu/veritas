package com.centrin.veritas.dao;
/**
 * Project Name：veritas
 * File Name：RestoreReportDao
 * Package Name：com.centrin.veritas.dao
 * Date：17/7/26 下午3:39
 */

import com.centrin.common.dao.BaseDao;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao
 * @ClassName RestoreReportDao
 * @author 张辉
 * @date 17/7/26 下午3:39
 * @version
 */
public interface RestoreReportDao extends BaseDao{
}
