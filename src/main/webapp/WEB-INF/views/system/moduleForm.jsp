
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html lang="en">
<head>
<title>添加组件</title>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<!-- <meta name="decorator" content="default" /> -->
</head>
<body>
	<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
	<link rel="stylesheet" href="${ctx}/resource-Main/main/css/orgList_apply.css">
		<!--加载效果 start-->
				<div id="preloader">
					<div id="status">
						<i class="fa fa-spinner fa-spin"></i>
					</div>
				</div>
				<!--加载效果 end-->
				<section> <!--main start-->
				<div class="mainpanel_mian">
					<!-- main start -->
					<div class="contentpanel">
				<div class="row" style="background-color: #fff;">
					<div class="col-sm-12">
						<!--list start-->
						<div class="panel panel-default">
							<div class="panel-heading zj-panel-heading-2 clearfix"  style="padding: 10px 0;">
								<h5 class="panel-title zj-panel-title-2" style="float: left; height: 30px; line-height: 30px;">添加组件</h5>
								<button type="button" class="btn btn-primary" style="float:right; height: 30px; width:70px;" onclick="window.history.go(-1);"><i class="glyphicon glyphicon-backward" style="margin-right: 7px"></i>返回 </button>
							</div>
						</div>
						<form class="form-horizontal" role="form" id="mainform" action="${ctx}/system/module/${action}" method="post">
							<div class="form-group">
								<label class="col-sm-3 control-label">组件名称：</label>
								<div class="col-sm-6">
									<input type="hidden" name="id" value="${module.id}" /> 
									<input class="form-control"  id="title" name="title" type="text" value="${module.title}"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">组件编码：</label>
								<div class="col-sm-6">
									<input class="form-control" type="text" id="code" name="code" value="${module.code}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" style="padding-right: 2.7em">类型：</label>
								<div class="col-sm-6">
									<select  class="select-o" name="type" value="${module.type}" >
										<option value="1" selected="selected">表格</option>
										<option value="2">列表</option>
										<option value="3">图标</option>
										<option value="4">按钮</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" style="padding-right: 2.7em">排序：</label>
								<div class="col-sm-6">
									<input class="form-control"  id="sort" name="sort" type="text" value="${module.sort}" >
								</div>
							</div>
							<div class="sub_btn">
								<div class="col-sm-3"></div>
								<div class="col-sm-6" style="text-align: center">
										<input type="button" id="confirm" class="btn btn-primary sub1" value="确定" onclick="save()">
										<input type="button" id="close" class="btn btn-primary sub2"  onclick="window.history.go(-1);" value="取消">
								</div>
								<div class="col-sm-3"></div>
							</div>
						</form>
						<!--list end-->
					</div>
				</div>
			</div>
		</div>
	</section>
		<script type="text/javascript">
					var action = "${action}";
					function save() {
						$('#mainform').bootstrapValidator('validate');
					}
					//提交表单
					$(document).ready(function() {

						$('#mainform').bootstrapValidator({
							container : 'tooltip',
							/* trigger: 'blur', */
							feedbackIcons : {
								valid : 'glyphicon glyphicon-ok',
								invalid : 'glyphicon glyphicon-remove',
								validating : 'glyphicon glyphicon-refresh'
							},
							fields : {
								title : {
									validators : {
										notEmpty : {
											message : '组件名称不能为空'
										}
									}
								},
								code : {
									validators : {
										notEmpty : {
											message : '组件编码不能为空'
										}
									}
								},
								sort : {
									validators : {
										notEmpty : {
											message : '排序号不能为空'
										}
									}
								}
							}
						}).on('success.form.bv', function(e) {
							var $form = $(e.target);
							$form.ajaxSubmit(function(data) {
								if (data == 'success') {
									location.href = "${ctx}/system/module";
								} else {

								}
							});
						});
					});
			</script>
</body>
</html>