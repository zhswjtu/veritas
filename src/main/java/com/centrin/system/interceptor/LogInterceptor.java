package com.centrin.system.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import com.centrin.common.mapper.JsonMapper;
import com.centrin.common.utils.DateUtils;
import com.centrin.common.utils.StringUtils;
import com.centrin.system.entity.LogEntity;
import com.centrin.system.service.LogService;
import com.centrin.system.utils.IPUtil;
import com.centrin.system.utils.UserUtil;

import eu.bitwalker.useragentutils.UserAgent;

public class LogInterceptor extends HandlerInterceptorAdapter {
    
	@Autowired
	private LogService logService;
	
	private Long beginTime;// 1、开始时间
	private Long endTime;// 2、结束时间

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		beginTime = System.currentTimeMillis();//计时
        return true;   
	}
	
//	public void postHandle(HttpServletRequest request,HttpServletResponse response, Object handler,
//			ModelAndView modelAndView) throws Exception {
//	}
	
	

	@Override
	public void afterCompletion(HttpServletRequest request,HttpServletResponse response, Object handler, Exception ex) throws Exception {
        
		if(handler.getClass().isAssignableFrom(HandlerMethod.class)){
            WriteLog writeLog = ((HandlerMethod) handler).getMethodAnnotation(WriteLog.class);
            if(writeLog != null) {          
                String content = writeLog.value();
                endTime = System.currentTimeMillis();
                String requestRri = request.getRequestURI();
                String uriPrefix = request.getContextPath();
                String operationCode=StringUtils.substringAfter(requestRri,uriPrefix);	//操作编码
                String requestParam=(new JsonMapper()).toJson(request.getParameterMap());	//请求参数
	
                Long executeTime = endTime - beginTime;
                UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent")); 
                String os=userAgent.getOperatingSystem().getName();	//获取客户端操作系统
                String browser=userAgent.getBrowser().getName();	//获取客户端浏览器
                
                if(UserUtil.getCurrentShiroUser()!=null) {
	                LogEntity log = new LogEntity();
	                log.setOs(os);
	                log.setBrowser(browser);
	                log.setIp(IPUtil.getIpAddress(request));
	                log.setOperationCode(operationCode);
	                log.setExecuteTime(Integer.valueOf(executeTime.toString()));
	                log.setCreater(UserUtil.getCurrentShiroUser().getName());
	                log.setCreateDate(DateUtils.getSysTimestamp());
	                log.setDescription(content);
	                log.setRequestParam(requestParam);
	                if(ex==null) {
	                	log.setType(LogEntity.TYPE_OPERATER);
	                } else {
	                	log.setType(LogEntity.TYPE_ERROR);
	                	log.setErrContent(ex.toString());
	                }
	                logService.add(log);
                }
            }
        }
	}
	
	
	
}
