package com.centrin.common.utils;

import java.net.InetAddress;

public class UUID {

    private static final int IP;

    /**
     * 默认长度为32，分隔符为空;加"1"长度的分隔符，生成的字符串长度加"4"。
     */
    private static final String SEP = "";

    static {
        int ipadd;
        try {
            ipadd = toInt(InetAddress.getLocalHost().getAddress());
        } catch (Exception e) {
            ipadd = 0;
        }
        IP = ipadd;
    }

    private static short counter = (short) 0;
    private static final int JVM = (int) (System.currentTimeMillis() >>> 8);

    /**
     * Unique across JVMs on this machine (unless they load this class in the
     * same quater second - very unlikely)
     */
    private static int getJVM() {
        return JVM;
    }

    /**
     * Unique in a millisecond for this JVM instance (unless there are >
     * Short.MAX_VALUE instances created in a millisecond)
     */
    private static short getCount() {
        synchronized (UUID.class) {
            if (counter < 0)
                counter = 0;
            return counter++;
        }
    }

    /**
     * Unique in a local network
     */
    private static int getIP() {
        return IP;
    }

    /**
     * Unique down to millisecond
     */
    private static short getHiTime() {
        return (short) (System.currentTimeMillis() >>> 32);
    }

    private static int getLoTime() {
        return (int) System.currentTimeMillis();
    }

    private static int toInt(byte[] bytes) {
        int result = 0;
        for (int i = 0; i < 4; i++) {
            result = (result << 8) - Byte.MIN_VALUE + bytes[i];
        }
        return result;
    }

    protected static String format(int intval) {
        String formatted = Integer.toHexString(intval);
        StringBuffer buf = new StringBuffer("00000000");
        buf.replace(8 - formatted.length(), 8, formatted);
        // buf.r
        return buf.toString();
    }

    protected static String format(short shortval) {
        String formatted = Integer.toHexString(shortval);
        StringBuffer buf = new StringBuffer("0000");
        buf.replace(4 - formatted.length(), 4, formatted);
        return buf.toString();
    }

    /**
     * 
     * 
     * 获得32为的唯一的UUID值
     * 
     * 
     * @return String 生产后的uuid值
     * 
     * @throws/exception [异常] [说明异常的含义和异常发生的条件]
     */
    public static String getUUIDHex() {
        return new StringBuffer().append(format(getIP())).append(SEP).append(format(getJVM()))
                .append(SEP).append(format(getHiTime())).append(SEP).append(format(getLoTime()))
                .append(SEP).append(format(getCount())).toString();
    }
}
