package com.centrin.system.utils;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

import com.centrin.system.entity.UserEntity;
import com.centrin.system.shiro.realm.ShiroUser;



public class UserUtil {
	
	//private static final String USER = "user";
	
	private static final String SUPER_ADMIN_USERNAME = "admin";
	
	public static final String SUPER_ADMIN_ROLE = "admin";
	
	/**
	 * 是否为超级用户
	 * @return
	 */
	public static boolean isSuperAdmin1() {
		return SUPER_ADMIN_USERNAME.equals(getCurrentShiroUser().getUsername().toLowerCase());
	}
	
	
	/**
	 * 是否为超级用户
	 * @return
	 */
	public static boolean isSuperAdmin(UserEntity user) {
		if(user==null) {
			return false;
		} else {
			return SUPER_ADMIN_USERNAME.equals(user.getUsername().toLowerCase());
		}
		
	}
	
	/**
	 * 是否为超级用户
	 * @return
	 */
	public static boolean isSuperAdmin(String username) {
		return SUPER_ADMIN_USERNAME.equals(username.toLowerCase());
	}
	
	
	
	/**
	 * 获取当前用户对象shiro
	 * @return shirouser
	 */
	public static ShiroUser getCurrentShiroUser(){
		ShiroUser user =(ShiroUser)SecurityUtils.getSubject().getPrincipal();
		return user;
	}
	
	/**
	 * 获取当前用户session
	 * @return session
	 */
	public static Session getSession(){
		Session session = SecurityUtils.getSubject().getSession();
		return session;
	}
	
//	/**
//	 * 获取当前用户对象
//	 * @return user
//	 */
//	public static UserEntity getCurrentUser(){
//		Session session =SecurityUtils.getSubject().getSession();
//		if(null!=session){
//			return (UserEntity) session.getAttribute(USER);
//		}else{
//			return null;
//		}
//	}
//	
//	/**
//	 * 保存当前用户对象
//	 */
//	public static void putCurrentUser(UserEntity user){
//		Session session =SecurityUtils.getSubject().getSession();
//	    session.setAttribute(USER, user);
//	}
	
}
 