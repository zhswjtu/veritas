
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	var ctx = '${pageContext.request.contextPath}';
</script>
<html lang="en">
<head>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<title>用户管理</title>
<!-- <meta name="decorator" content="default" /> -->
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
</head>

<body>
	<style type="text/css">
		.pre-scrollable {
		            max-height: 800px;
		            overflow-y: scroll
		       
		}
	</style>
	<!--加载效果 start-->
	<div id="preloader">
		<div id="status">
			<i class="fa fa-spinner fa-spin"></i>
		</div>
	</div>
	<section>
    <!--main start-->
    <div class="mainpanel_mian">
        <!-- main start -->
        <div class="contentpanel">
            <div class="row ">
                <div class="panel panel-default">
                    <div class="panel-heading zj-panel-heading-2">
                        <h5 class="panel-title zj-panel-title-2">字典列表</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
	                <div style="margin-top: 15px; margin-left: 0px;">
	                    <input id="roleId" type="hidden" />
	                    <a class="btn btn-primary" id='add' href="${ctx}/system/dic/create" data-toggle="modal"> <i class="zj-icon zj-d-apply"></i>添加
	                    </a>
	                </div>
                <div class="table-responsive zj-table-responsive pre-scrollable row" ">
                    <table class="bootstrap-table mb30 table-hover table-responsive" id="dic_pb"></table>
                </div>
            </div>
            <div class="col-md-6">
                <div class="table-responsive zj-table-responsive pre-scrollable row" style="margin-top: 60px;padding-left: 60px;">
                    <table class="bootstrap-table mb30 table-hover table-responsive" id="dict_pb"></table>
                </div>
            </div>
        </div>
    </div>
	</section>
<script type='text/javascript'>
    var dic_table, dict_table;
    var id_flag = '';
    $(function(){
        dic_table = $('#dic_pb').bootstrapTable({
            //height: getHeight(),
            method: "get",
            url: ctx + '/system/dic/json',
            cache: false,
            fit : true,
            fitColumns : true,
            border : false,
            idField : 'id',
            uniqueId: 'id',
            checkbox:true,
            rownumbers:true,
            showRefresh: false,
            singleSelect:true,
           /*  striped:false, */
            search:false,
            clickToSelect:true,
            onClickRow:onClickRow,
            columns: [
                {field:'id',title:'id',visible:false},
                {field:'name',title:'字典名称',align: 'center',width:100},
                {field:'code',title:'字典编码',align: 'center',width:100},
                {field:'description',title:'描述',align: 'center',width:150},
                {field: 'op',title: '操作',align: 'center',width:200,
                    formatter:function(value, row, index) {
                       return '<a href="${ctx}/system/dic/item/create/'+row.id+'"  class="btn btn-primary" >添加字典项</a>'
                               +'&nbsp;&nbsp;<a href="${ctx}/system/dic/update/'+row.id+'"  class="btn btn-primary" >编辑</a>'
                               +'&nbsp;&nbsp;<a href="javascript:del('+row.id+')"  class="btn btn-primary" >删除</a>';

                    }
                }
            ],
//            toolbar:'#dic_tb'
        });

        dict_table = $('#dict_pb').bootstrapTable({
            //height: getHeight(),
            method: "get",
            url: ctx + '/system/dic/item/json',
            cache: false,
            fit : true,
            fitColumns : true,
            border : false,
            idField : 'id',
            uniqueId: 'id',
            checkbox:true,
            rownumbers:true,
            showRefresh: false,
            scroll:true,
            singleSelect:true,
          /*   striped:true, */
            search:false,
            clickToSelect:true,
            columns: [
                {field:'id',title:'id',visible:false},
                {field:'displayName',title:'显示名称',align: 'center',width:100},
                {field:'itemValue',title:'键值',align: 'center',width:100},
                {field:'description',title:'描述',width:100,tooltip: true},
                {field:'status',title:'状态',align: 'center',width:100,
                    formatter:function(val,rec) {
                        if(val=='1') {
                            return '启用';
                        } else {
                            return '禁用';
                        }
                    }
                },
                {field:'sort',title:'排序号',align: 'center',width:100},
                {field: 'op',title: '操作',align: 'center',width:200,
                    formatter:function(value, row, index) {
                        if(index == 0){
                            return '<div class="dropdown"> '
                                    +'<button class="btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">'
                                    +'操作&nbsp;'
                                    +'<span class="caret"></span>'
                                    +'</button>'
                                    +'<ul class="dropdown-menu pull-right" role="menu">'
                                    +'<li>'
                                    +'<a href="${ctx}/system/dic/item/update/'+row.id+'">编辑</a>'
                                    +'</li>'
                                    +'<li>'
                                    +'<a href="javascript:del_t('+row.id+')" >删除</a>'
                                    +'</li>'
                                    +'</ul>'
                                    +'</div>';
                        }else{
                            return '<div class="btn-group dropup"> '
                                    +'<button class="btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">'
                                    +'操作&nbsp;'
                                    +'<span class="caret"></span>'
                                    +'</button>'
                                    +'<ul class="dropdown-menu pull-right" role="menu">'
                                    +'<li>'
                                    +'<a href="${ctx}/system/dic/item/update/'+row.id+'" >编辑</a>'
                                    +'</li>'
                                    +'<li>'
                                    +'<a href="javascript:del_t('+row.id+')" >删除</a>'
                                    +'</li>'
                                    +'</ul>'
                                    +'</div>';
                        }
                    }
                }
            ],
//            toolbar:'#dic_tb',
//            dataPlain: true
        });
    });

    function onClickRow(row,$element){
        $("#dic_pb").find("tr").each(function(){
            $(this).removeClass("row_active");
        });
        $element.addClass('row_active');
        id_flag = row.id;
        $('#dict_pb').bootstrapTable('refresh',{query: {'dicId': row.id}});
    }

	function del(id) {
		Ewin.confirm({
			message : "确认要删除该字典信息吗？"
		}).on(function(e) {
			if (!e) {
				return;
			}
			$.ajax({
				url : ctx + '/system/dic/delete/' + id,
				method : 'POST',
				success : function(data) {
					dic_table.bootstrapTable('refresh');
					toastr.success(data);
				}
			});
		});
	}

	function del_t(id) {
		Ewin.confirm({
			message : "确认要删除该字典项吗？"
		}).on(function(e) {
			if (!e) {
				return;
			}
			$.ajax({
				url : ctx + '/system/dic/item/delete/' + id,
				method : 'POST',
				success : function(data) {
					$('#dict_pb').bootstrapTable('refresh', {
						query : {
							'dicId' : id_flag
						}
					});
					toastr.success(data);
				}
			});	
		});
	}
</script>
</body>
</html>
