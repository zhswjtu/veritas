package com.centrin.veritas.service.impl;
/**
 * Project Name：veritas
 * File Name：VeritasDbServiceImpl
 * Package Name：com.centrin.veritas.service.impl
 * Date：2017/8/30 下午5:01
 * Copyright (c) 2017, Centrin Data Systems Ltd. All Rights Reserved.
 * 中金数据系统有限公司
 */

import com.centrin.common.dao.BaseDao;
import com.centrin.common.dao.QueryFilter;
import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseService;
import com.centrin.common.servcie.BaseServiceImpl;
import com.centrin.common.utils.StringUtils;
import com.centrin.veritas.dao.SnDao;
import com.centrin.veritas.dao.VeritasDbDao;
import com.centrin.veritas.entity.SnEntity;
import com.centrin.veritas.entity.VeritasDbEntity;
import com.centrin.veritas.entity.VeritasDbParamEntity;
import com.centrin.veritas.service.VeritasDbParamService;
import com.centrin.veritas.service.VeritasDbService;
import com.centrin.veritas.utils.SystemUtils;
import com.centrin.veritas.utils.VeritasConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service.impl
 * @ClassName VeritasDbServiceImpl
 * @author 张辉
 * @date 2017/8/30 下午5:01
 * @version
 */
@Service
public class VeritasDbServiceImpl extends BaseServiceImpl<VeritasDbEntity> implements VeritasDbService {

    @Autowired
    private VeritasDbDao veritasDbDao;

    @Autowired
    private VeritasDbParamService veritasDbParamService;

    @Autowired
    private SnDao snDao;

    @Override
    protected BaseDao getSuperBaseDao() {
        return veritasDbDao;
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 获取实例信息  分页
     * @date 2017/8/30
     */
    @Override
    public PagingEntity<VeritasDbEntity> findPage(PagingEntity<VeritasDbEntity> page, Map<String, String> params) {
        QueryFilter qf = QueryFilter.newPagingFilter(page.getPageNum(), page.getSize());
        if (StringUtils.isNotBlank(params.get("chapterName"))){
            qf.andEq("chapterName", params.get("chapterName"));
        }
        String ip = SystemUtils.getHostIp(SystemUtils.getInetAddress());
        if (StringUtils.isNotBlank(ip)){
            qf.andEq("ip", ip);
        }
        qf.orderBy("id", QueryFilter.Order.DESC);
        return findAsPaging(VeritasDbEntity.class, qf);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 通过章节名获取实例信息
     * @date 2017/8/30
     */
    @Override
    public VeritasDbEntity findByChapterName(String chapterName) {
        QueryFilter qf = QueryFilter.newFilter();
        qf.andEq("chapterName", chapterName);
        String ip = SystemUtils.getHostIp(SystemUtils.getInetAddress());
        qf.andEq("ip", ip);
        return findAsSingle(VeritasDbEntity.class, qf);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 添加db2实例
     * @date 2017/8/30
     */
    @Override
    @Transactional
    public void createDbInstance(VeritasDbEntity veritasDbEntity, Integer instanceId) {
        String ip = SystemUtils.getHostIp(SystemUtils.getInetAddress());
        veritasDbEntity.setIp(ip);
        veritasDbDao.add(veritasDbEntity);
        List<VeritasDbParamEntity> params = veritasDbParamService.findByInstanceIdList(instanceId);
        for (VeritasDbParamEntity veritasDbParamEntity : params){
            veritasDbParamEntity.setInstanceId(veritasDbEntity.getId());
            veritasDbParamService.update(veritasDbParamEntity);
        }
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        List<VeritasDbParamEntity> list = veritasDbParamService.findByInstanceIdList(id);
        veritasDbDao.delete(VeritasDbEntity.class, id);
        for (VeritasDbParamEntity veritasDbParamEntity : list){
            veritasDbParamService.delete(VeritasDbParamEntity.class, veritasDbParamEntity.getId());
        }
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 生成db配置文件
     * @date 2017/8/31
     */
    @Override
    @Transactional
    public void instanceBuild() {
        List<VeritasDbEntity> list = findAllList();
        BufferedWriter bw = null;
        try {
            //获取文件
            File file = new File(VeritasConstant.DB2_DB_PATH + File.separator + "db.conf");
            File fileParent = file.getParentFile();
            if (!fileParent.exists()){
                fileParent.mkdirs();
            }
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            bw = new BufferedWriter(fileWriter);
            for (VeritasDbEntity veritasDbEntity : list){
                String chapterName = veritasDbEntity.getChapterName();
                chapterName = chapterName.replace("[", "")
                        .replace("]", "");
                bw.write("[" + chapterName + "]");
                bw.newLine();
                bw.write(veritasDbEntity.getChannelName()
                        + " = " + nullToString(veritasDbEntity.getChannelValue()));
                bw.newLine();
                bw.write(veritasDbEntity.getClientName()
                        + " = " + nullToString(veritasDbEntity.getClientNameValue()));
                bw.newLine();
                bw.write(veritasDbEntity.getDescriptionName()
                        + " = " + nullToString(veritasDbEntity.getDescriptionValue()));
                bw.newLine();
                bw.write(veritasDbEntity.getInstanceName()
                        + " = " + nullToString(veritasDbEntity.getInstanceValue()));
                bw.newLine();
                bw.write(veritasDbEntity.getLogModeName()
                        + " = " + nullToString(veritasDbEntity.getLogModeValue()));
                bw.newLine();
                bw.write(veritasDbEntity.getLogPolicyName()
                        +  " = " + nullToString(veritasDbEntity.getLogPolicyValue()));
                bw.newLine();
                bw.write(veritasDbEntity.getPolicyName()
                        +  " = " + nullToString(veritasDbEntity.getPolicyNameValue()));
                bw.newLine();
                bw.write(veritasDbEntity.getUserIdName()
                        +  " = " + nullToString(veritasDbEntity.getUserIdValue()));
                bw.newLine();
                bw.write(veritasDbEntity.getVersionName()
                        +  " = " + nullToString(veritasDbEntity.getVersionValue()));
                bw.newLine();
                //获取实例参数
                List<VeritasDbParamEntity> params = veritasDbParamService.findByInstanceIdList(veritasDbEntity.getId());
                for (VeritasDbParamEntity param : params){
                    bw.write(param.getParamName() + " = " + nullToString(param.getParamValue()));
                    bw.newLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 立即执行恢复
     * @date 2017/8/31
     */
    @Override
    @Transactional
    public void execImmediately(Integer id) {
        VeritasDbEntity veritasDbEntity = veritasDbDao.findById(VeritasDbEntity.class, id);
        SnEntity snEntity = snDao.findNew();
        veritasDbEntity.setRid(veritasDbEntity.getChapterName().replace("[", "")
                .replace("]", "") + "_" + snEntity.getSn());
        veritasDbDao.update(veritasDbEntity);
        try {
            String command = VeritasConstant.DB2_RESTORE + " -sid "
                    + veritasDbEntity.getChapterName().replace("[", "").replace("]", "")
                    + " -rid " + snEntity.getSn();
            Process process = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", command});
//            process.waitFor();
            System.out.println(command);
            System.out.println(process.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String nullToString(String str){
        return null == str ? "" : str;
    }

    public List<VeritasDbEntity> findAllList(){
        QueryFilter qf = QueryFilter.newFilter();
        String ip = SystemUtils.getHostIp(SystemUtils.getInetAddress());
        if (StringUtils.isNotBlank(ip)){
            qf.andEq("ip", ip);
        }
        return findAsList(VeritasDbEntity.class, qf);
    }
}
