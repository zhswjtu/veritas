package com.centrin.veritas.controller;
/**
 * Project Name：veritas
 * File Name：VeritasOraclePlanController
 * Package Name：com.centrin.veritas.controller
 * Date：17/7/26 下午4:23
 */

import com.centrin.common.controller.BaseController;
import com.centrin.common.entity.PagingEntity;
import com.centrin.veritas.entity.RestoreStateInfoEntity;
import com.centrin.veritas.entity.VeritasOracleDbInstanceEntity;
import com.centrin.veritas.entity.VeritasOracleTimePlanEntity;
import com.centrin.veritas.service.RestoreStateInfoService;
import com.centrin.veritas.service.VeritasOracleInstanceService;
import com.centrin.veritas.service.VeritasOraclePlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.controller
 * @ClassName VeritasOraclePlanController
 * @author 张辉
 * @date 17/7/26 下午4:23
 * @version
 */
@Controller
@RequestMapping(value = "/oracle/plan")
public class VeritasOraclePlanController extends BaseController{

    @Autowired
    private VeritasOraclePlanService veritasOraclePlanService;

    @Autowired
    private RestoreStateInfoService restoreStateInfoService;

    @Autowired
    private VeritasOracleInstanceService veritasOracleInstanceService;

    /**
     * @param
     * @return
     * @author 张辉
     * @description 执行计划跳转
     * @date 17/7/26
     */
    @RequestMapping(value = "")
    public String index(){
        return "/veritas/oracle/timePlan";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 获取计划列表   分页
     * @date 17/7/26
     */
    @RequestMapping(value = "/json")
    @ResponseBody
    public PagingEntity<VeritasOracleTimePlanEntity> findPage(HttpServletRequest request, @RequestParam Map<String, String> param){
        PagingEntity<VeritasOracleTimePlanEntity> page = getPage(request);
        return veritasOraclePlanService.findPage(page, param);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 执行计划添加跳转
     * @date 17/7/26
     */
    @RequestMapping(value = "/createSkip")
    public String createSkip(Model model){
        model.addAttribute("action", "create");
        model.addAttribute("planType", "添加");
        model.addAttribute("plan", new VeritasOracleTimePlanEntity());
        return "/veritas/oracle/timeForm";
    }

    @RequestMapping(value = "/create")
    @ResponseBody
    public String create(VeritasOracleTimePlanEntity veritasOracleTimePlanEntity){
        veritasOraclePlanService.addPlan(veritasOracleTimePlanEntity);
        return "success";
    }

    @RequestMapping(value = "/update/{id}")
    public String updateSkip(Model model, @PathVariable("id") Integer id){
        model.addAttribute("action", "update");
        model.addAttribute("planType", "修改");
        model.addAttribute("plan", veritasOraclePlanService.findById(VeritasOracleTimePlanEntity.class, id));
        return "/veritas/oracle/timeForm";
    }

    @RequestMapping(value = "update")
    @ResponseBody
    public String update(VeritasOracleTimePlanEntity veritasOracleTimePlanEntity){
        veritasOraclePlanService.updatePlan(veritasOracleTimePlanEntity);
        return "success";
    }

    @RequestMapping(value = "/delete/{id}")
    @ResponseBody
    public String deletePlan(@PathVariable("id") Integer id){
        veritasOraclePlanService.deletePlan(id);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 立即执行
     * @date 17/8/3
     */
    @RequestMapping(value = "/exec/Immediately/{id}")
    @ResponseBody
    public String execImmediately(@PathVariable("id") Integer id){
        veritasOraclePlanService.execImmediately(id);
        return "success";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 执行进度查看
     * @date 17/8/2
     */
    @RequestMapping(value = "/exec/{id}")
    public String execPlan(@PathVariable("id") Integer id, Model model){
        model.addAttribute("instance",
                veritasOracleInstanceService.findById(VeritasOracleDbInstanceEntity.class, id));
        return "/veritas/oracle/oracleProcess";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 执行进度
     * @date 17/8/3
     */
    @RequestMapping(value = "/exec/process")
    @ResponseBody
    public PagingEntity<RestoreStateInfoEntity> execProcess(String rid, HttpServletRequest request){
        PagingEntity<RestoreStateInfoEntity> page = getPage(request);
        return restoreStateInfoService.findByRid(rid, page);
    }
}
