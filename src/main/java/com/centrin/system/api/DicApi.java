package com.centrin.system.api;

import java.util.List;
import java.util.Map;

import com.centrin.system.entity.DicItemEntity;

public interface DicApi {
	public List<DicItemEntity> getDicItems(String dicCode);
	
	public List<Map<String,DicItemEntity>> getDicItemMaps(String dicCode);
}



