<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <title>工单提交</title>
    <link href="${ctx}/resource-Main/main/css/jquery.datatables.css" rel="stylesheet">
    <link href="${ctx}/resource-Main/main/css/list.css" rel="stylesheet">
    <link href="${ctx}/resource-Main/main/css/demo.css" rel="stylesheet">
</head>

<body>
<!--加载效果 start-->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>
<!--加载效果 end-->
<section>
    <!--main start-->
    <div class="mainpanel_mian">
        <!-- main start -->
        <div class="contentpanel">
            <div class="row">
                <div class="col-sm-12">
                    <!--list start-->
                    <div class="panel panel-default">
                        <div class="panel-heading zj-panel-heading-2">
                            <h5 class="panel-title zj-panel-title-2">工单提交</h5>
                        </div>
                        <div class="panel-body zj-list-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <form class="form-horizontal form-bordered">
                                        <div class="form-group" style="padding:0;border:0;">
                                            <label class="col-sm-1 control-label">客户名称：</label>
                                            <div class="col-sm-2">
                                                <select id="customId" class="form-control mb15 zj-table-option"
                                                        style="height: 31px">
                                                    <option value="">全部</option>
                                                </select>
                                            </div>

                                            <label class="col-sm-1 control-label">客户编号：</label>
                                            <div class="col-sm-2">
                                                <input id="code" type="text" placeholder="" class="form-control">
                                            </div>
                                            <div class="col-sm-1">
                                                <button id="searchOrder" class="btn btn-default btn-sm">搜索</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <table class="table mb30" id="order_list">
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--list end-->
                </div>
            </div>
        </div>
        <!-- main end -->
    </div>
    <!--main end-->
</section>

<script>
    var $order_list;
    var $url_order_list= '${ctx}/cus/custom/manager/json';
    $(function () {
        $order_list = $('#order_list').bootstrapTable({
            method: "get",
            url: $url_order_list,
            cache: false,
            fit : true,
            fitColumns : true,
            border : false,
            idField : 'id',
            uniqueId: 'id',
            pagination:true,
            checkbox:true,
            rownumbers:true,
            showRefresh: false,
            pageNumber:1,
            pageSize : 10,
            sidePagination: 'server',
            pageList : [ 10, 20, 30, 40, 50 ],
            singleSelect:true,
            /*  striped:true, */
            search:false,
            clickToSelect:true,
            queryParams: function (params) {
                var param = {
                    limit: params.limit,   //页面大小
                    offset: params.offset,  //页码
                    name: $('#name').val(),
                    code: $('#code').val(),
                    status: $('#status').val()
                };
                return param;
            },
            columns: [
                { field : 'checked', checkbox : true },
                {field:'id',title:'id',visible:false},
                {field:'name',title:'客户名称',align: 'center',width:150},
                {field:'code',title:'客户编号',align: 'center',width:100},
                {field:'level',title:'客户等级',width:80,align: 'center',
                    formatter: function (value, row, index) {
                        var stars;
                        switch(value){
                            case 1:
                                stars="一级";
                                break;
                            case 2:
                                stars="二级";
                                break;
                            case 3:
                                stars="三级";
                                break;
                            default:
                                stars="";
                        }
                        return stars;
                    }
                },
                {field:'status',title:'状态',align: 'center',width:80,
                    formatter:function(value, row, index) {
                        return value == 1? '有效':'无效';
                    }
                }
            ],
            dataPlain: true
        });

        $("#resetBtn").on('click', function(e) {
            document.getElementById("searchFrom").reset();
        });

        $("#searchBtn").on('click', function(e) {
            table_c.bootstrapTable('refresh',{url:url});
        });
    });
</script>
</body>

</html>