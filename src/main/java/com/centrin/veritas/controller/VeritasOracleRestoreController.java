package com.centrin.veritas.controller;
/**
 * Project Name：veritas
 * File Name：VeritasOracleRestoreController
 * Package Name：com.centrin.veritas.controller
 * Date：17/8/1 下午9:18
 */

import com.centrin.common.controller.BaseController;
import com.centrin.common.entity.PagingEntity;
import com.centrin.common.utils.UUID;
import com.centrin.veritas.entity.RestoreReportEntity;
import com.centrin.veritas.service.RestoreReportService;
import net.sf.excelutils.ExcelUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.controller
 * @ClassName VeritasOracleRestoreController
 * @author 张辉
 * @date 17/8/1 下午9:18
 * @version
 */
@Controller
@RequestMapping(value = "/oracle/restore")
public class VeritasOracleRestoreController extends BaseController{

    @Autowired
    private RestoreReportService restoreReportService;

    /**
     * @param
     * @return
     * @author 张辉
     * @description 跳转计划报表
     * @date 17/8/1
     */
    @RequestMapping(value = "")
    public String index(){
        return "/veritas/oracle/restoreList";
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 计划报表列表
     * @date 17/8/1
     */
    @RequestMapping(value = "/json")
    @ResponseBody
    public PagingEntity<RestoreReportEntity> findPage(HttpServletRequest request, @RequestParam Map<String, Object> param){
        PagingEntity<RestoreReportEntity> page = getPage(request);
        return restoreReportService.findPageList(page, param);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 导出报表
     * @date 17/8/8
     */
    @RequestMapping(value = "/downloadRestore")
    @ResponseBody
    public String downloadRestore(HttpServletRequest request, HttpServletResponse response, @RequestParam Map<String, Object> param) {
        try {
            PagingEntity<RestoreReportEntity> page = getPage(request);
            page.setPageNum(0);
            page.setSize(9999);
            page = restoreReportService.findPageList(page, param);
            List<RestoreReportEntity> list = (List<RestoreReportEntity>) page.getRows();
            ExcelUtils.addValue("list", list);
            String fileName = UUID.getUUIDHex() + ".xls";
            String config = "/WEB-INF/views/excel/restore.xls";
            response.reset();
            response.setContentType("application/vnd.ms-excel");
            response.addHeader("content-disposition", "attachment;filename="
                    + java.net.URLEncoder.encode(fileName, "utf-8"));
            ExcelUtils.export(request.getSession()
                    .getServletContext(), config, response.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }
}
