<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <title>工单提交</title>
    <link href="${ctx}/resource-Main/main/css/jquery.datatables.css" rel="stylesheet">
    <%--<link href="${ctx}/resource-Main/main/css/list.css" rel="stylesheet">--%>
    <link href="${ctx}/resource-Main/main/css/demo.css" rel="stylesheet">
    <link href="${ctx}/resource-Main/main/css/list.css" rel="stylesheet">
    <link rel="stylesheet" href="${ctx}/resource-Main/change/list.css">
    <style>

    </style>
</head>
<body>
<!--加载效果 start-->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>
<!--加载效果 end-->
<section>
    <!--main start-->
    <div class="mainpanel_mian">
        <!-- main start -->
        <div class="contentpanel">
            <div class="row">
                <div class="col-sm-12">
                    <!--list start-->
                    <div class="panel panel-default">
                        <div class="panel-heading zj-panel-heading-2">
                            <h5 class="panel-title zj-panel-title-2">默认配置</h5>
                        </div>
                        <div class="panel-body zj-list-body">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <div class="col-sm-12">
                                        <div class="col-sm-6">
                                            <span style="display: block; font-weight: bolder;" class="glyphicon glyphicon-th" aria-hidden="true">&nbsp;全局参数</span>
                                        </div>
                                        <div class="col-sm-6">
                                            <%--<div class="btn-group" style="float: right;">--%>
                                                <%--<button type="button" style="float: right" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--%>
                                                    <%--<span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>--%>
                                                    <%--&nbsp;全局参数<span class="caret"></span>--%>
                                                <%--</button>--%>
                                                <%--<ul class="dropdown-menu">--%>
                                                    <%--<li><a href="javascript:;" onclick="paramClick('[GLOBAL]')">GLOBAL</a></li>--%>
                                                    <%--<li><a href="javascript:;" onclick="paramClick('[EMAIL]')">EMAIL</a></li>--%>
                                                    <%--<li><a href="javascript:;" onclick="paramClick('[POLICY]')">POLICY</a></li>--%>
                                                    <%--<li><a href="javascript:;" onclick="paramClick('[PFILE]')">PFILE</a></li>--%>
                                                    <%--<li><a href="javascript:;" onclick="paramClick('[INSTANCE]')">INSTANCE</a></li>--%>
                                                <%--</ul>--%>
                                            <%--</div>--%>
                                            <div>
                                                <a href="javascript:;" onclick="buildFile()" style="float: right;display: block; margin-top: 4px;margin-right: 1px;" class="btn btn-info"><span class="glyphicon glyphicon-floppy-saved"></span>&nbsp;生成default文件</a>
                                                <a href="javascript:;" onclick="downloadDefault()" style="float: right;display: block; margin-top: 4px;margin-right: 1px;" class="btn btn-info"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;下载default文件</a>
                                                <a href="javascript:;" onclick="bulidDefault()" style="float: right;display: block; margin-top: 4px;margin-right: 1px;" class="btn btn-info"><span class="glyphicon glyphicon-paperclip"></span>&nbsp;生成默认参数</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" aria-disabled="true" role="tablist">
                                            <li role="presentation" class="active"><a href="#sid" aria-controls="sid" role="tab" data-toggle="tab" onclick="paramClick('[GLOBAL]')">GLOBAL</a></li>
                                            <li role="presentation"><a href="#sid" aria-controls="sid" role="tab" data-toggle="tab" onclick="paramClick('[EMAIL]')">EMAIL</a></li>
                                            <li role="presentation"><a href="#sid" aria-controls="sid" role="tab" data-toggle="tab" onclick="paramClick('[POLICY]')">POLICY</a></li>
                                            <li role="presentation"><a href="#sid" aria-controls="sid" role="tab" data-toggle="tab" onclick="paramClick('[PFILE]')">PFILE</a></li>
                                            <li role="presentation"><a href="#sid" aria-controls="sid" role="tab" data-toggle="tab" onclick="paramClick('[INSTANCE]')">INSTANCE</a></li>
                                            <li role="presentation"><a href="#sid" aria-controls="sid" role="tab" data-toggle="tab" onclick="paramClick('[MYSQLDB]')">MYSQLDB</a></li>
                                            <li role="presentation"></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="sid">
                                                <table class="table table-hover table-responsive" id="default_db">
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="chapter" value="[GLOBAL]">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--list end-->
                </div>
            </div>
        </div>
        <!-- main end -->
    </div>
    <!--main end-->
</section>

<script>
    var $default_db;
    var $url_default_db = '${ctx}/oracle/default/json';
    $(function () {
        $default_db = $('#default_db').bootstrapTable({
            method: "get",
            url: $url_default_db,
            cache: false,
            fit : true,
            fitColumns : true,
            border : false,
            idField : 'id',
            uniqueId: 'id',
            undefinedText: '',
            checkbox:true,
            rownumbers:true,
            showRefresh: false,
            pagination: true,
            sidePagination: 'server',
            pageNumber: 1,
            pageSize: 10,
            pageList: [10, 20, 30, 40, 50],
            singleSelect:true,
            striped:true,
            search:false,
            clickToSelect:true,
//            toolbar : '#bar_db_11',
            queryParams: function (params) {
                var param = {
                    limit: params.limit, //页面大小
                    offset: params.offset, //页码
                    head: $('#chapter').val()
                };
                return param;
            },
            columns: [
                {field:'pos',title:'序号',width: 20,align: 'center',
                    formatter: function (value, row, index) {
                        return index+1;
                    }
                },
                {field:'id',title:'id',visible:false},
                {field:'chapterHead',title:'章节',align: 'center',width:80},
                {field:'parameterName',title:'参数',align: 'center',width:150},
                {field:'defaultValue',title:'参数值',align: 'center',width:80},
                {field:'description',title:'说明',align: 'center',width:250},
                {field:'opt',title: '操作',align: 'center',width: 80,
                    formatter: function(value, row,index) {
                        var res = '';
                        res +='<a href="${ctx}/oracle/default/updateSkip/'+row.id+'" ';
                        res +='data-toggle="modal" data-target="#add-model" class="btn-blue" title="修改"><span  class="glyphicon glyphicon-edit" aria-hidden="true"></a></span>';
                        return res;
                    }
                }
            ],
            dataPlain: true
        });
    });

    function paramClick(str){
        $('#chapter').val(str);
        var opt = {
            url: $url_default_db,
            silent: true,
            query:{
                head: str
            }
        };
        $('#default_db').bootstrapTable('refresh', opt);
    }

    function buildFile(){
        Ewin.confirm({ message: "确认要生成default吗?" }).on(function (e) {
            if (!e) {
                return;
            }
            $.ajax({
                type: 'post',
                cache: false,
                url: "${ctx}/oracle/default/build",
                success: function(data) {
                    if(data == 'success'){
                        toastr.success('文件生成成功');
                    }else{
                        toastr.error('文件生成失败');
                    }
                },
                error: function () {
                    toastr.error('操作失败');
                }
            });
        });
    }

    function downloadDefault(){
        $.ajax({
            url: '${ctx}/oracle/default/defaultFileExists',
            type: 'post',
            success: function(data){
                if(data == 'success'){
                    location.href = '${ctx}/oracle/default/downloadDefault';
                } else if(data == 'error'){
                    toastr.error('请先生成default文件');
                }
            }
        });
    }

    function bulidDefault(){
        $.ajax({
            url: '${ctx}/oracle/default/param/build',
            type: 'post',
            success: function(data){
                if(data == '200'){
                    paramClick('[GLOBAL]');
                    toastr.success('生成成功');
                } else if(data == '201'){
                    toastr.error('操作失败请联系管理员');
                } else if (data == '202'){
                    toastr.warning('默认参数已存在');
                }
            }
        });
    }
</script>
</body>

</html>