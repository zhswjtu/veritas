package com.centrin.system.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.centrin.common.dao.SuperBaseDao;
import com.centrin.common.entity.PagingEntity;
import com.centrin.common.utils.StringUtils;
import com.centrin.system.entity.PermissionEntity;
import com.centrin.system.entity.RoleEntity;
@Repository
public class RoleDao extends SuperBaseDao  {
	/**
	 * 
	 * findRoleAll:(这里用一句话描述这个方法的作用). <br/> 
	 * @param map
	 * @return 
	 * @author changtao
	 * @param page 
	 */
	public PagingEntity<RoleEntity> findRoleAll(Map<String, Object> map, PagingEntity<RoleEntity> page) {
		String condition = " delFlag = '0' ";
		page.setRows(findByCondition(RoleEntity.class, condition, page.getSort()+" "+page.getOrder(), page.getFirst(), page.getSize()));
		page.setTotal(getCountByCondition(RoleEntity.class,condition.toString()));
		return page;
	}
	/**
	 * 
	 * getPermissionsByRoleId:(这里用一句话描述这个方法的作用). <br/> 
	 * @param roleId
	 * @return 
	 * @author changtao
	 */
	public List<PermissionEntity> getPermissionsByRoleId(Integer roleId) {
		String sql = "SELECT * FROM sys_permission WHERE status='1' and id in( "
				+ "SELECT permission_id FROM sys_role_permission  where role_id =:roleId);";
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("roleId", roleId);
		List<PermissionEntity> entities = findBySql(PermissionEntity.class, sql, params, 0, 0);
		return entities;
	}
	
	
	/**
	 * 
	 * @Title: getRolesByUserId
	 * @Description:
	 * @param userId
	 * @return
	 */
	public List<RoleEntity> findRolesByUserId(Integer userId) {
		String sql = "SELECT * FROM sys_role WHERE del_flag='0' and id " +
				"in (select role_id from sys_user_role where user_id =:userId)";
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("userId", userId);
		List<RoleEntity> roles = findBySql(RoleEntity.class, sql, params, 0, 0);
		return roles;
	}
	
	
	/**
	 * 
	 * findRoleAll:获取所有角色 <br/> 
	 * @param map
	 * @return 
	 * @author changtao
	 */
	public List<RoleEntity> findRoleAll(Map<String, Object> map) {
		String condition = " delFlag = '0' ";
		return findByCondition(RoleEntity.class, condition,map.get("orderby").toString(),0,0);
	}
	
	
	public int checkRole(Map<String, Object> paramMap) {
		StringBuffer sql = new StringBuffer();
		sql.append(" 1=1 ");
		if(StringUtils.isNotBlank(paramMap.get("name"))){
			sql.append("and name = '");
			sql.append(paramMap.get("name").toString()+"' ");
		}
		if(StringUtils.isNotBlank(paramMap.get("code"))){
			sql.append("and code = '");
			sql.append(paramMap.get("code").toString()+"' ");
		}
		if(StringUtils.isNotBlank(paramMap.get("id"))){
			sql.append("and id != '");
			sql.append(paramMap.get("id").toString()+"' ");
		}
		return getCountByCondition(RoleEntity.class,sql.toString());
	}
}
