package com.centrin.veritas.service;
/**
 * Project Name：veritas
 * File Name：VeritasDbService
 * Package Name：com.centrin.veritas.service
 * Date：2017/8/30 下午5:00
 */

import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseService;
import com.centrin.veritas.entity.VeritasDbEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service
 * @ClassName VeritasDbService
 * @author 张辉
 * @date 2017/8/30 下午5:00
 * @version
 */
public interface VeritasDbService extends BaseService<VeritasDbEntity>{

    PagingEntity<VeritasDbEntity> findPage(PagingEntity<VeritasDbEntity> page, Map<String, String> params);

    VeritasDbEntity findByChapterName(String chapterName);

    void createDbInstance(VeritasDbEntity veritasDbEntity, Integer instanceId);

    void deleteById(Integer id);

    void instanceBuild();

    void execImmediately(Integer id);
}
