package com.centrin.system.job;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.support.TaskUtils;
import org.springframework.stereotype.Service;

import com.centrin.common.utils.DateUtils;
import com.centrin.system.api.LogApi;
import com.centrin.system.entity.LogEntity;
import com.centrin.system.entity.ScheduleJob;

@Service
public class JobTest implements Job{

	@Autowired
	private LogApi logApi;
	
	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		
		ScheduleJob scheduleJob = (ScheduleJob) context.getMergedJobDataMap().get("scheduleJob");  
		System.out.println("scheduleJob" + scheduleJob.getName());
		
		LogEntity log  = new LogEntity();
		log.setType(LogEntity.TYPE_JOB);
		log.setCreateDate(DateUtils.getSysTimestamp());
		log.setCreater("admin");
		log.setOperationCode(scheduleJob.getClassName());
		log.setDescription(scheduleJob.getName() + "任务,执行成功!");
		logApi.write(log);
		System.out.println("test job");
		
	}

}
