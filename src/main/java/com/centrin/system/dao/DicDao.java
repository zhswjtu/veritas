package com.centrin.system.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.centrin.common.dao.SuperBaseDao;
import com.centrin.system.entity.DicDefEntity;
import com.centrin.system.entity.DicItemEntity;

@Repository
public class DicDao extends SuperBaseDao {
	
	public List<DicDefEntity> findDicDefs() {
		String jpql = "select d from DicDefEntity d where d.delFlag='0'";
		return findByJpql(DicDefEntity.class, jpql);
	}
	
	public List<DicItemEntity> findDicItems(Integer dicId) {
		String jpql = "select d from DicItemEntity d where d.delFlag='0' and d.dicId=:dicId order by d.sort asc";
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("dicId", dicId);
		return findByJpql(DicItemEntity.class, jpql, params);
	}
	
	public List<DicItemEntity> findDicItems(String dicCode) {
		String jpql = "select i from DicItemEntity i where i.delFlag='0' and " +
				"i.dicId=(select d.id from DicDefEntity d where d.code=:dicCode and d.delFlag='0') " +
				"order by i.sort asc";
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("dicCode", dicCode);
		return findByJpql(DicItemEntity.class, jpql, params);
	}

    public List<DicItemEntity> findDicItem(String dicCode, String itemValue) {
        String jpql = "select i from DicItemEntity i where i.delFlag='0' and " +
                "i.dicId=(select d.id from DicDefEntity d where d.code=:dicCode and d.delFlag='0') and i.itemValue =:itemValue ";
        Map<String,Object> params = new HashMap<String, Object>();
        params.put("dicCode", dicCode);
        params.put("itemValue", itemValue);
        return findByJpql(DicItemEntity.class, jpql, params);
    }
	
	
	public  DicDefEntity getDicDef(String name, String code) {
		String jpql = "select d from DicDefEntity d where d.delFlag='0' and (d.code=:code or d.name=:name)";
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("name", name);
		params.put("code", code);
		return getSingleByJpql(DicDefEntity.class, jpql, params);
	}
	
	public  DicDefEntity getDicDef(Integer id, String name, String code) {
		String jpql = "select d from DicDefEntity d where d.delFlag='0' " +
				"and (d.code=:code or d.name=:name) and d.id<>:id";
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("id", id);
		params.put("name", name);
		params.put("code", code);
		return getSingleByJpql(DicDefEntity.class, jpql, params);
	}
	
	
	public DicItemEntity getDicItem(Integer dicId, String displayName, String itemValue) {
		String jpql = "select d from DicItemEntity d where d.delFlag='0' " +
				"and d.dicId=:dicId and (d.itemValue=:itemValue or d.displayName=:displayName)";
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("displayName", displayName);
		params.put("itemValue", itemValue);
		params.put("dicId", dicId);
		return getSingleByJpql(DicItemEntity.class, jpql, params);
	}
	
	public DicItemEntity getDicItem(Integer dicId, Integer id, String displayName, String itemValue) {
		String jpql = "select d from DicItemEntity d where d.delFlag='0' " +
				"and d.dicId=:dicId " +
				"and (d.itemValue=:itemValue or d.displayName=:displayName)" + 
				"and d.id<>:id";
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("id", id);
		params.put("displayName", displayName);
		params.put("itemValue", itemValue);
		params.put("dicId", dicId);
		return getSingleByJpql(DicItemEntity.class, jpql, params);
	}
	
	public DicItemEntity getDicItem(String displayName) {
		String jpql = "select d from DicItemEntity d where d.delFlag='0' " +
				" and d.displayName=:displayName";
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("displayName", displayName);
		return getSingleByJpql(DicItemEntity.class, jpql, params);
	}

}
