package com.centrin.system.service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.centrin.common.entity.PagingEntity;
import com.centrin.system.dao.RoleDao;
import com.centrin.system.entity.PermissionEntity;
import com.centrin.system.entity.RoleEntity;


@Service
public class RoleService {
	
	@Autowired
    private RoleDao roleDao;

	@Transactional
	public void add(RoleEntity role) {
		roleDao.add(role);
	}
	
	
	public void update(RoleEntity role) {
		roleDao.update(role);
	}
	
	public void delete(Integer id) {
		RoleEntity role = roleDao.findById(RoleEntity.class, id);
		//roleDao.delete(RoleEntity.class, id);
		role.setDelFlag(1);
		roleDao.update(role);
	}
	

	public RoleEntity get(Integer id) {
		return roleDao.findById(RoleEntity.class, id);
	}
	
	public PagingEntity<RoleEntity> findRoleAll(Map<String, Object> map, PagingEntity<RoleEntity> page) {
		page = roleDao.findRoleAll(map,page);
		return page;
	}
	
	public List<RoleEntity> findRoleAll(Map<String, Object> map) {
		List<RoleEntity> entities = roleDao.findRoleAll(map);
		return entities;
	}
	
	
	public List<RoleEntity> findRolesByUserId(Integer userId) {
		List<RoleEntity> entities = roleDao.findRolesByUserId(userId);
		return entities;
	}
	
	
	
	/**
	 * 
	 * getPermissionsByRoleId:(这里用一句话描述这个方法的作用). <br/> 
	 * @param roleId
	 * @return 
	 * @author changtao
	 */
	public List<PermissionEntity> getPermissionsByRoleId(Integer roleId) {
		return roleDao.getPermissionsByRoleId(roleId);
	}

	/**
	 * 
	 * updatePermission:保存角色权限. <br/> 
	 * @param roleId
	 * @param newPermissionList 
	 * @author changtao
	 */
	@Transactional
	public void updatePermission(Integer roleId, List<Integer> newPermissionList) {
		//roleDao.updateRolePermission(roleId,newPermissionList);
		RoleEntity roleEntity = roleDao.findById(RoleEntity.class, roleId);
		roleEntity.clearPermissions();
		Set<PermissionEntity> permissionEntities = new HashSet<PermissionEntity>();
		for (int i = 0; i < newPermissionList.size(); i++) {
			PermissionEntity permissionEntity = new PermissionEntity();
			permissionEntity.setId(newPermissionList.get(i));;
			permissionEntities.add(permissionEntity);
		}
		roleEntity.setPermissions(permissionEntities);
		roleDao.update(roleEntity);
	}

	
	public int checkRole(Map<String, Object> paramMap) {
		return roleDao.checkRole(paramMap);
	}

}
