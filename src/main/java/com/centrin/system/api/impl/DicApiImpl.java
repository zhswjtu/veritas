package com.centrin.system.api.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.centrin.system.api.DicApi;
import com.centrin.system.entity.DicItemEntity;
import com.centrin.system.service.DicService;

@Service
public class DicApiImpl implements DicApi{

		
	@Autowired
	private DicService dicService ;
	
	
	@Override
	@Cacheable(value="sysCache")
	public List<DicItemEntity> getDicItems(String dicCode) {
		List<DicItemEntity> list = null;
        list = dicService.findDicItems(dicCode);
		return list;
	}

	
	@Override
	public List<Map<String, DicItemEntity>> getDicItemMaps(String dicCode) {
		List<Map<String, DicItemEntity>> list = new ArrayList<Map<String, DicItemEntity>>();
		List<DicItemEntity> dicItems = getDicItems(dicCode);
		for(DicItemEntity dicItem : dicItems) {
			Map<String, DicItemEntity> m = new HashMap<String, DicItemEntity>();
			m.put(dicItem.getItemValue(), dicItem);
			list.add(m);
		}
		return list;
	}





}
