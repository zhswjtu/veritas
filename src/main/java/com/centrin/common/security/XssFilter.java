/** 
 * Project Name：jfshop 
 * File Name：bbb.java 
 * Package Name：com.centrin.common.security 
 * Date：2016-9-9 下午2:08:15 
 * Copyright (c) 2016, Centrin Data Systems Ltd. All Rights Reserved. 
 * 中金数据系统有限公司 
 */
/**
 * 
 */
package com.centrin.common.security;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class XssFilter implements Filter {

	FilterConfig filterConfig = null;
	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	public void destroy() {
		this.filterConfig = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		chain.doFilter(new XssHttpServletRequestWrapper((HttpServletRequest) request), response);
	}

}