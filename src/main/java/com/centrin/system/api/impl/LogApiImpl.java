package com.centrin.system.api.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centrin.system.api.LogApi;
import com.centrin.system.entity.LogEntity;
import com.centrin.system.service.LogService;

@Service
public class LogApiImpl implements LogApi{

	@Autowired
	private LogService logService;
	
	@Override
	public void write(LogEntity log) {
		logService.add(log);
	}

}



