/*
 *    Copyright 2014-2015 the original author or authors.
 */
package com.centrin.common.vo;

import com.google.gson.Gson;

/**
 * 返回结果
  */
public class Result {
	
	public static final int RET_SUCCESS = 0;
	public static final int RET_FAILURE = 1;
	
	public static final Result SUCCESS = new Result(RET_SUCCESS, "成功!", null);
	public static final Result FAILURE = new Result(RET_FAILURE, "失败!", null);

	private int code;
	
	private String message;
	
	private  Object data;

	public Result(int code, String message, Object data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@SuppressWarnings("unchecked")
	public <T> T getData() {
		return (T) data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	public static final Result success(Object data) {
		return new Result(RET_SUCCESS, "成功", data);
	}
	
	public static final Result fail(String message) {
		return new Result(RET_FAILURE, message, null);
	}
	
	public static final Result fail(String message, Object data) {
		return new Result(RET_FAILURE, message, data);
	}
	
	public static final Result build(int code, String message) {
		return new Result(code, message, null);
	}
	
	public static final Result build(int code, Object data) {
		return new Result(code, null, data);
	}
	
	public static final Result build(int code, String message, Object data) {
		return new Result(code, message, data);
	}
	
	public String toJSON() {
		if(this.data instanceof String) {
			StringBuffer sb = new StringBuffer();
			sb.append("{");
			sb.append("\"code\":");
			sb.append(this.code);
			sb.append(",");
			sb.append("\"message\":\"");
			sb.append(this.message);
			sb.append("\",\"data\":");
			if((((String) this.data).startsWith("{") &&
					((String) this.data).endsWith("}")) ||
					(((String) this.data).startsWith("[") &&
					((String) this.data).endsWith("]"))) {
				sb.append(this.data);
			} else {
				sb.append("\"");
				sb.append(this.data);
				sb.append("\"");
			}
			sb.append("}");
			return sb.toString();
		} else {
			Gson gson = new Gson();
			return  gson.toJson(this);
		}
		
	}
}
