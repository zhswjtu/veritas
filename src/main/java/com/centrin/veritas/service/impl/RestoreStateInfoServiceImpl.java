package com.centrin.veritas.service.impl;
/**
 * Project Name：veritas
 * File Name：RestoreStateInfoServiceImpl
 * Package Name：com.centrin.veritas.service.impl
 * Date：17/8/8 下午11:24
 */

import com.centrin.common.dao.BaseDao;
import com.centrin.common.dao.QueryFilter;
import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseServiceImpl;
import com.centrin.veritas.dao.RestoreStateInfoDao;
import com.centrin.veritas.entity.RestoreStateInfoEntity;
import com.centrin.veritas.service.RestoreStateInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service.impl
 * @ClassName RestoreStateInfoServiceImpl
 * @author 张辉
 * @date 17/8/8 下午11:24
 * @version
 */
@Service
public class RestoreStateInfoServiceImpl extends BaseServiceImpl<RestoreStateInfoEntity> implements RestoreStateInfoService {

    @Autowired
    private RestoreStateInfoDao restoreStateInfoDao;

    @Override
    protected BaseDao getSuperBaseDao() {
        return restoreStateInfoDao;
    }

    @Override
    public PagingEntity<RestoreStateInfoEntity> findByRid(String rid, PagingEntity<RestoreStateInfoEntity> page) {
        QueryFilter qf = QueryFilter.newPagingFilter(page.getPageNum(), page.getSize());
        qf.andEq("rid", rid);
        qf.orderBy("id", QueryFilter.Order.DESC);
        return findAsPaging(RestoreStateInfoEntity.class, qf);
    }
}
