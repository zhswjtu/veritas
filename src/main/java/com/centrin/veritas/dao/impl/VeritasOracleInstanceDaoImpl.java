package com.centrin.veritas.dao.impl;
/**
 * Project Name：veritas
 * File Name：VeritasOracleInstanceDaoImpl
 * Package Name：com.centrin.veritas.dao.impl
 * Date：17/7/24 下午8:28
 */

import com.centrin.common.dao.BaseDaoImpl;
import com.centrin.veritas.dao.VeritasOracleInstanceDao;
import org.springframework.stereotype.Repository;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao.impl
 * @ClassName VeritasOracleInstanceDaoImpl
 * @author 张辉
 * @date 17/7/24 下午8:28
 * @version
 */
@Repository
public class VeritasOracleInstanceDaoImpl extends BaseDaoImpl implements VeritasOracleInstanceDao {
}
