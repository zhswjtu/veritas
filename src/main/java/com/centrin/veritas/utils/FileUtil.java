package com.centrin.veritas.utils;
/**
 * Project Name：veritas
 * File Name：FileUtil
 * Package Name：com.centrin.veritas.utils
 * Date：17/7/23 下午5:17
 */

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.utils
 * @ClassName FileUtil
 * @author 张辉
 * @date 17/7/23 下午5:17
 * @version
 */
public class FileUtil {

    public static void download(HttpServletResponse response, String path) throws Exception{
        //获取文件名
        String fileName = path.substring(path.lastIndexOf(File.separator)+1);
        //1.设置文件ContentType类型，这样设置，会自动判断下载文件类型
        response.setContentType("multipart/form-data");
        //2.设置文件头：最后一个参数是设置下载文件名(假如我们叫a.pdf)
        response.setHeader("Content-Disposition", "attachment;fileName=" + fileName);
        FileInputStream inputStream = new FileInputStream(path);
        int len = 0;
        byte[] buffer = new byte[1024];
        OutputStream outputStream = response.getOutputStream();
        while ((len = inputStream.read(buffer)) > 0){
            outputStream.write(buffer, 0, len);
        }
        inputStream.close();
        outputStream.close();
    }
}
