package com.centrin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.centrin.common.entity.PagingEntity;
import com.centrin.system.dao.LogDao;
import com.centrin.system.entity.LogEntity;


@Service
public class LogService {
	
	@Autowired
    private LogDao logDao;

	public PagingEntity<LogEntity> search(PagingEntity<LogEntity>logPage, Map<String, Object> params) {
		return logDao.search(logPage, params);
	}
    
	
	public void add(LogEntity log) {
		logDao.add(log);
	}
	
	
	public void delete(Integer id) {
		logDao.delete(LogEntity.class, id);
	}
    
	@Transactional
	public void deletes(List<Integer> ids) {
		logDao.deletes(LogEntity.class, ids);
	}
}
