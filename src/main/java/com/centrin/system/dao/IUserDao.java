package com.centrin.system.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.centrin.system.entity.UserEntity;

@Repository
public interface IUserDao extends PagingAndSortingRepository<UserEntity, Integer>, JpaSpecificationExecutor<UserEntity> {
	
	UserEntity findByUsernameAndStatusAndDelFlag(String username, Integer status, Integer delFlag);

}
