package com.centrin.common.dto.model.rsp;



import java.io.Serializable;

import com.alibaba.fastjson.JSON;


public class RspBean implements Serializable {
	private static final long serialVersionUID = 1L;

	public String toString() {
		return JSON.toJSONString(this);
	}
}
