package com.centrin.system.dao;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.centrin.common.dao.SuperBaseDao;
import com.centrin.common.entity.PagingEntity;
import com.centrin.common.utils.DateUtils;
import com.centrin.system.entity.LogEntity;

@Repository
public class LogDao extends SuperBaseDao {
	
	public PagingEntity<LogEntity> search(PagingEntity<LogEntity> logPage, Map<String, Object> params){
		StringBuffer sb = new StringBuffer();
		sb.append(" where -1=-1");
		
		if(params.get("creater")!=null && !"".equals(params.get("creater"))) {
			sb.append(" and x.creater like :creater");
			params.put("creater", "%" + params.get("creater") + "%");
		}  else {
			params.remove("creater");
		}
		
		if(params.get("description") != null && !"".equals(params.get("description"))) {
			sb.append(" and x.description like :description");
			params.put("description", "%" + params.get("description") + "%");
		} else {
			params.remove("description");
		}
		
		if(params.get("sDateStart") != null && !"".equals(params.get("sDateStart"))) {
			sb.append(" and x.createDate >= :sDateStart");
			params.put("sDateStart", DateUtils.parseDate(params.get("sDateStart")));
		} else {
			params.remove("sDateStart");
		}
		
		if(params.get("sDateEnd") != null && !"".equals(params.get("sDateEnd"))) {
			sb.append(" and x.createDate <= :sDateEnd");
			params.put("sDateEnd", DateUtils.parseDate(params.get("sDateEnd")));
		} else {
			params.remove("sDateEnd");
		}
//		
		String jpql1 = "select x from LogEntity x" + sb.toString() + " order by x.id desc";
		String jpql2 = "select count(x) from LogEntity x" + sb.toString();
		
		List<LogEntity> list = findByJpql(LogEntity.class, jpql1, params, logPage.getFirst(), logPage.getSize());
		Integer count = countByJpql(jpql2, params);
		logPage.setRows(list);
		logPage.setTotal(count);
		return logPage;
	}
	
}
