package com.centrin.common.utils;

import java.security.SecureRandom;
import java.util.UUID;


public final class UuidUtils {

    /*
     * The most significant 64 bits of this UUID.
     * 
     * @serial
     */
    private final long mostSigBits;

    /*
     * The least significant 64 bits of this UUID.
     * 
     * @serial
     */
    private final long leastSigBits;

    /*
     * The random number generator used by this class to create random based
     * UUIDs. In a holder class to defer initialization until needed.
     */

    // Constructors and Factories

    /*
     * Private constructor which uses a byte array to construct the new UUID.
     */
    private UuidUtils(byte[] data) {
        long msb = 0;
        long lsb = 0;
        assert data.length == 16 : "data must be 16 bytes in length";
        for (int i = 0; i < 8; i++)
            msb = (msb << 8) | (data[i] & 0xff);
        for (int i = 8; i < 16; i++)
            lsb = (lsb << 8) | (data[i] & 0xff);
        this.mostSigBits = msb;
        this.leastSigBits = lsb;
    }

    /**
     * Constructs a new {@code UUID} using the specified data.
     * {@code mostSigBits} is used for the most significant 64 bits of the
     * {@code UUID} and {@code leastSigBits} becomes the least significant 64
     * bits of the {@code UUID}.
     * 
     * @param mostSigBits The most significant bits of the {@code UUID}
     * 
     * @param leastSigBits The least significant bits of the {@code UUID}
     */
    private UuidUtils(long mostSigBits, long leastSigBits) {
        this.mostSigBits = mostSigBits;
        this.leastSigBits = leastSigBits;
    }

    /**
     * 生成long型随机不重复的ID
     * 
     * @return
     */
    public static long randomLongID() {
        return Long.parseLong(randomStingID(), 16);
    }

    /**
     * 生成String型随机不重复的ID
     * 
     * @return
     */
    public static String randomStingID() {
        SecureRandom ng = Holder.numberGenerator;

        byte[] randomBytes = new byte[16];
        ng.nextBytes(randomBytes);
        randomBytes[6] &= 0x0f; /* clear version */
        randomBytes[6] |= 0x40; /* set to version 4 */
        randomBytes[8] &= 0x3f; /* clear variant */
        randomBytes[8] |= 0x80; /* set to IETF variant */

        return new UuidUtils(randomBytes).toString();
    }

    public String toString() {
        return (digits(mostSigBits, 4) + digits(leastSigBits, 12));
    }

    /** Returns val represented by the specified number of hex digits. */
    private static String digits(long val, int digits) {
        long hi = 1L << (digits * 4);
        return Long.toHexString(hi | (val & (hi - 1))).substring(1);
    }

    public int hashCode() {
        long hilo = mostSigBits ^ leastSigBits;
        return ((int) (hilo >> 32)) ^ (int) hilo;
    }

    /**
     * Compares this object to the specified object. The result is {@code true}
     * if and only if the argument is not {@code null}, is a {@code UUID}
     * object, has the same variant, and contains the same value, bit for bit,
     * as this {@code UUID}.
     * 
     * @param obj The object to be compared
     * 
     * @return {@code true} if the objects are the same; {@code false} otherwise
     */
    public boolean equals(Object obj) {
        if ((null == obj) || (obj.getClass() != UUID.class))
            return false;
        UuidUtils id = (UuidUtils) obj;
        return (mostSigBits == id.mostSigBits && leastSigBits == id.leastSigBits);
    }

    // Comparison Operations

    private static class Holder {
        static final SecureRandom numberGenerator = new SecureRandom();
    }
    
    public static void main(String[] args) {
    	System.out.println(UuidUtils.randomStingID());
    }

}
