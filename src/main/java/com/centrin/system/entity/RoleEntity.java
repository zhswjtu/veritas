package com.centrin.system.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.centrin.common.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 角色entity
 */
@Entity
@Table(name = "sys_role")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@DynamicUpdate 
@DynamicInsert
public class RoleEntity extends BaseEntity implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String code;
	private String description;
	private Integer sort;
	private Integer delFlag = 0;
	
//	private Set<User> users = new HashSet<User>(0);
	
	private Set<PermissionEntity> permissions = new HashSet<PermissionEntity>(0);

	// Constructors

	/** default constructor */
	public RoleEntity() {
	}
	
	public RoleEntity(Integer id) {
		this.id=id;
	}

	/** minimal constructor */
	public RoleEntity(String name, String code) {
		this.name = name;
		this.code = code;
	}

	/** full constructor */
	public RoleEntity(String name, String code, String description, Integer sort,
			Integer delFlag) {
		this.name = name;
		this.code = code;
		this.description = description;
		this.sort = sort;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 64)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "code", nullable = false, length = 32)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "description", length = 1024)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "sort")
	public Integer getSort() {
		return this.sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Column(name = "del_flag", length = 1)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}
	
	
//	@JsonIgnore
//	@ManyToMany(cascade = CascadeType.REFRESH, mappedBy = "sys_role", fetch = FetchType.LAZY)
//	public Set<User> getUsers() {
//		return users;
//	}
//
//	public void setUsers(Set<User> users) {
//		this.users = users;
//	}

	
	@JsonIgnore
	@ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	@JoinTable(name = "sys_role_permission", joinColumns = { @JoinColumn(name = "role_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "permission_id", referencedColumnName = "id") })
	public Set<PermissionEntity> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<PermissionEntity> permissions) {
		this.permissions = permissions;
	}

    public void clearPermissions(){
        this.permissions.clear();
    }


}