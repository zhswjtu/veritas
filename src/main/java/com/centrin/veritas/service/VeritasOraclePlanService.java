package com.centrin.veritas.service;
/**
 * Project Name：veritas
 * File Name：VeritasOraclePlanService
 * Package Name：com.centrin.veritas.service
 * Date：17/7/26 下午4:25
 */

import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseService;
import com.centrin.veritas.entity.VeritasOracleTimePlanEntity;

import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service
 * @ClassName VeritasOraclePlanService
 * @author 张辉
 * @date 17/7/26 下午4:25
 * @version
 */
public interface VeritasOraclePlanService extends BaseService{

    void addPlan(VeritasOracleTimePlanEntity veritasOracleTimePlanEntity);

    PagingEntity<VeritasOracleTimePlanEntity> findPage(PagingEntity<VeritasOracleTimePlanEntity> page, Map<String, String> param);

    void updatePlan(VeritasOracleTimePlanEntity veritasOracleTimePlanEntity);

    void deletePlan(Integer id);

    void execImmediately(Integer id);
}
