<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <title>参数修改</title>
</head>
<body>
<link rel="stylesheet" href="${ctx}/resource-Main/change/work-order.css">
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/modal.css">
<!--新增 modal-->
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Oracle计划${planType}</h4>
</div>
<div class="modal-body" style="padding: 0;">
    <form class="form-horizontal" id="form_fibre" action="${ctx}/oracle/plan/${action}">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="group">
                        <label class="col-sm-2 control-label">计划名称:</label>

                        <div class="col-sm-10">
                            <input class="form-control" type="text" id="planName" name="planName" value="${plan.planName}"/>
                            <input class="form-control" type="hidden" id="id" name="id" value="${plan.id}"/>
                            <input class="form-control" type="hidden" id="createTime" name="createTime" value="${plan.createTime}"/>
                            <input class="form-control" type="hidden" id="rid" name="rid" value="${plan.rid}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="group">
                        <label class="col-sm-2 control-label">实例名:</label>

                        <div class="col-sm-4">
                            <select style="width: 100%; height: 40px" class="form-control" id="instanceName" name="instanceName">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <div class="group">
                        <label class="col-sm-2 control-label">分:</label>

                        <div class="col-sm-3">
                            <input class="form-control" type="text" id="minute" name="minute" value="${plan.minute}"/>
                        </div>
                        <%--<label class="col-sm-2 control-label">每分(*或*/1)</label>--%>
                        <i style="margin-top: 10px" class="col-sm-1 glyphicon glyphicon-question-sign" data-placement="left" data-toggle="tooltip" title="分钟1～59 每分钟用*或者 */1表示" data-variation="small inverted"></i>
                    </div>
                </div>
                <div class="form-group">
                    <div class="group">
                        <label class="col-sm-2 control-label">时:</label>

                        <div class="col-sm-3">
                            <input class="form-control" type="text" id="hour" name="hour" value="${plan.hour}"/>
                        </div>
                        <%--<label class="col-sm-2 control-label">每小时(*)</label>--%>
                        <i style="margin-top: 10px" class="col-sm-1 glyphicon glyphicon-question-sign" data-placement="left" data-toggle="tooltip" title="小时1～23（0表示0点）" data-variation="small inverted"></i>
                    </div>
                    <div class="group">
                        <label class="col-sm-2 control-label">日:</label>

                        <div class="col-sm-3">
                            <input class="form-control" type="text" id="day" name="day" value="${plan.day}"/>
                        </div>
                        <%--<label class="col-sm-2 control-label">每天(*)</label>--%>
                        <i style="margin-top: 10px" class="col-sm-1 glyphicon glyphicon-question-sign" data-placement="left" data-toggle="tooltip" title="日期1～31" data-variation="small inverted"></i>
                    </div>
                </div>
                <div class="form-group">
                    <div class="group">
                        <label class="col-sm-2 control-label">月:</label>

                        <div class="col-sm-3">
                            <input class="form-control" type="text" id="month" name="month" value="${plan.month}"/>
                        </div>
                        <%--<label class="col-sm-2 control-label">每月(*)</label>--%>
                        <i style="margin-top: 10px" class="col-sm-1 glyphicon glyphicon-question-sign" data-placement="left" data-toggle="tooltip" title="月份1～12" data-variation="small inverted"></i>
                    </div>
                    <div class="group">
                        <label class="col-sm-2 control-label">周:</label>

                        <div class="col-sm-3">
                            <input class="form-control" type="text" id="week" name="week" value="${plan.week}"/>
                        </div>
                        <%--<label class="col-sm-2 control-label" style="float: left">0表示星期天</label>--%>
                        <i style="margin-top: 10px" class="col-sm-1 glyphicon glyphicon-question-sign" data-placement="left" data-toggle="tooltip" title="星期0～6（0表示星期天）" data-variation="small inverted"></i>
                    </div>
                </div>
                <%--<div class="form-group" style="margin-bottom: 20px">--%>
                    <%--<label class="col-sm-2 control-label" style="padding-right:1em">说明:</label>--%>
                    <%--<div class="col-sm-10">--%>
                        <%--<textarea class="form-control area-orgList" type="text" style="height: 90px;" id="remark" name="remark">${plan.remark}</textarea>--%>
                    <%--</div>--%>
                <%--</div>--%>
            </div>
            <!-- panel-body -->
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-2 control-center">
                        <button type="button" class="btn btn-primary zj-btn-button" onclick="save();">提交</button>
                    </div>
                    <div class="col-sm-2 control-center">
                        <button type="button" class="btn btn-default zj-btn-button" data-dismiss="modal" id="closeBtn">
                            取消
                        </button>
                    </div>
                    <div class="col-sm-4 "></div>
                </div>
            </div>
            <!-- panel-footer -->
        </div>
    </form>
</div>
<!--新增 modal-->
<script>
    var action = '${action}';

    if(action == 'update'){
        $('#status').val('${plan.status}');
    }
    $.ajax({
        url:'${ctx}/oracle/instance/allList',
        type:'post',
        success: function(data){
            var head = $('#instanceName');
            var str = '';
            for(var i = 0; i < data.length; i++){
                if(data[i].chapterHead == '${plan.instanceName}') {
                    str = '<option selected value = "' + data[i].chapterHead + '">' + data[i].chapterHead + '</option>';
                }else {
                    str = '<option value = "' + data[i].chapterHead + '">' + data[i].chapterHead + '</option>';
                }
                head.append(str);
            }
        }
    });

    $(function () {
        $(function () { $("[data-toggle='tooltip']").tooltip(); });

        $('#form_fibre').bootstrapValidator({
            container: 'tooltip',
            //        trigger: 'blur',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                planName: {
                    group: '.group',
                    validators: {
                        notEmpty: {
                            message: '计划名称必填'
                        }
                    }
                },
                minute: {
                    group: '.group',
                    validators: {
                        notEmpty: {
                            message: '必填项'
                        }
                    }
                },
                hour: {
                    group: '.group',
                    validators: {
                        notEmpty: {
                            message: '必填项'
                        }
                    }
                },
                day: {
                    group: '.group',
                    validators: {
                        notEmpty: {
                            message: '必填项'
                        }
                    }
                },
                month: {
                    group: '.group',
                    validators: {
                        notEmpty: {
                            message: '必填项'
                        }
                    }
                },
                week: {
                    group: '.group',
                    validators: {
                        notEmpty: {
                            message: '必填项'
                        }
                    }
                },
                instanceName: {
                    group: '.group',
                    validators: {
                        notEmpty: {
                            message: '请选择实例名'
                        }
                    }
                }
            }
        }).on('success.form.bv', function (e) {
            var $form = $(e.target);
            $form.ajaxSubmit(function (data) {
                if (data == 'success') {
                    $time_plan.bootstrapTable('refresh');
                    $("#closeBtn").click();
                } else {
                    toastr.error('添加失败');
                }
            });
        });
    });
    function save() {
        $('#form_fibre').bootstrapValidator('validate');
    }
</script>
</body>
</html>