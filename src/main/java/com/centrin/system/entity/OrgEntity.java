package com.centrin.system.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.centrin.common.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 组织部门entity
 */
@Entity
@Table(name = "sys_org")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class OrgEntity extends BaseEntity implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	
	public static final Integer STATE_VALID = 1;
	public static final Integer STATE_INVALID = 0;
	
	public static final Integer DEL_FLAG_YES = 1;
	public static final Integer DEL_FLAY_NO = 0;
	
	
	
	private Integer id;
	private String name;
	private String code;
	private Integer sort;
	private Integer status = 1;
	private String description;
	private Integer delFlag = 0;
	private Set<UserEntity> users = new HashSet<UserEntity>();

	// Constructors

	/** default constructor */
	public OrgEntity() {
	}

	public OrgEntity(Integer id) {
		this.id = id;
	}

	/** minimal constructor */
	public OrgEntity(String code, String name, String password) {
		this.code = code;
		this.name = name;
	}

	/** full constructor */
	public OrgEntity( String name,String code,Integer sort,Integer status, String description, Integer delFlag) {
		this.name = name;
		this.code = code;
		this.status = status;
		this.sort = sort;
		this.description = description;
		this.delFlag = delFlag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "code", nullable = false, length = 64)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "name", nullable = false, length = 64)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Column(name = "status", length = 1)
	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "del_flag", length = 1)
	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}
	

	@Column(name = "sort")
	public Integer getSort() {
		return this.sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	
	@JsonIgnore
	@ManyToMany(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	@JoinTable(name = "sys_user_org", joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id") }, inverseJoinColumns = { @JoinColumn(name = "org_id", referencedColumnName = "id") })
	// @Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
	public Set<UserEntity> getUsers() {
		return users;
	}

	public void setUsers(Set<UserEntity> users) {
		this.users = users;
	}
	
	
	public void addUser(UserEntity user) {
        this.users.add(user);
    }
    
	
	public void addUsers(Set<UserEntity> users) {
        this.users.addAll(users);
    }
	
	
	 public void removeUser(UserEntity user) {
	        this.users.remove(user);
	 }


	 public void removeUsers(Set<UserEntity> users){
	    this.users.removeAll(users);
	 }
	    
	    
	 public void retainUsers(Set<UserEntity> users){
	   	this.users.retainAll(users);
	 }
	    
	    
    public void containsUsers(Set<UserEntity> users){
    	this.users.containsAll(users);
    }
	    
	
    public void clearUsers(){
	     this.users.clear();
	}
	
	 

}