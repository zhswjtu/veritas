package com.centrin.system.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.centrin.common.controller.BaseController;
import com.centrin.system.entity.DicDefEntity;
import com.centrin.system.entity.DicItemEntity;
import com.centrin.system.error.SysException;
import com.centrin.system.interceptor.WriteLog;
import com.centrin.system.service.DicService;

/**
 * 字典controller
 */
@Controller
@RequestMapping("system/dic")
public class DicController extends BaseController{

	@Autowired
	private DicService dicService;
	
	

	
	
	
	/**
	 * 默认页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String list(){
		return "system/dicList";
	}
	
	
	/**
	 * 获取字典列表
	 */
	@RequiresPermissions("sys:dic:view")
	@RequestMapping("/json")
	@ResponseBody
	public List<DicDefEntity> listDatas(@RequestParam Map<String, Object> paramMap,HttpServletRequest request){
		return dicService.findDicDefs();
	}
	
	
	/**
	 * 获取字典项列表
	 */
	@RequiresPermissions("sys:dic:view")
	@RequestMapping("/item/json")
	@ResponseBody
	public List<DicItemEntity> listDatas(Integer dicId){
		if(dicId==null) dicId = -1;
		return dicService.findDicItems(dicId);
	}
	
	
	/**
	 * 添加字典跳转
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String createDefForm(Model model) {
		model.addAttribute("dicDef", new DicDefEntity());
		model.addAttribute("action", "create");
		return "system/dicDefForm";
	}
	
	/**
	 * 添加字典项跳转
	 */
	@RequestMapping(value = "/item/create/{dicId}", method = RequestMethod.GET)
	public String createItemForm(@PathVariable("dicId") Integer dicId, Model model) {
		DicItemEntity dicItem = new DicItemEntity();
		dicItem.setDicId(dicId);
		model.addAttribute("dicItem", dicItem);
		model.addAttribute("action", "create");
		return "system/dicItemForm";
	}
	
	/**
	 * 添加字典
	 */
	@WriteLog("字典管理/添加字典")
	@RequiresPermissions("sys:dic:add")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public String createDef(@ModelAttribute("dicDef") DicDefEntity dicDef, Model model) {
		try {
			dicService.defAdd(dicDef);
			return "添加成功";
		} catch(SysException e) {
			return e.getErrMsg();
		}
		
	}
	
	/**
	 * 添加字典项
	 */
	@WriteLog("字典管理/添加字典项")
	@RequiresPermissions("sys:dic:add")
	@RequestMapping(value = "/item/create", method = RequestMethod.POST)
	@ResponseBody
	public String createItem(@ModelAttribute("dicItem") DicItemEntity dicItem, Model model) {
		try {
			dicService.itemAdd(dicItem);
			return "success";
		} catch(SysException e) {
			return e.getErrMsg();
		}
	}
	
	
	/**
	 * 修改字典
	 */
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String updateFormDef(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("dicDef", dicService.defGet(id));
		model.addAttribute("action", "update");
		return "system/dicDefForm";
	}
	

	/**
	 * 修改项
	 */
	@RequestMapping(value = "/item/update/{id}", method = RequestMethod.GET)
	public String updateFormItem(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("dicItem", dicService.itemGet(id));
		model.addAttribute("action", "update");
		return "system/dicItemForm";
	}
	
	
	
	/**
	 * 修改字典
	 */
	@WriteLog("字典管理/修改字典")
	@RequiresPermissions("sys:dic:update")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String updateDef(@ModelAttribute("dicDef") DicDefEntity dicDef, Model model) {
		try {
			dicService.defUpdate(dicDef);
			return "修改成功";
		} catch(SysException e) {
			return e.getErrMsg();
		}
	}
	
	
	/**
	 * 修改字典项
	 */
	@WriteLog("字典管理/修改字典项")
	@RequiresPermissions("sys:dic:update")
	@RequestMapping(value = "/item/update", method = RequestMethod.POST)
	@ResponseBody
	public String updateItem(@ModelAttribute("dicItem") DicItemEntity dicItem, Model model) {
		try {
			dicService.itemUpdate(dicItem);
			return "修改成功";
		} catch(SysException e) {
			return e.getErrMsg();
		}
	}
	
	
	
	
	/**
	 * 删除字典
	 */
	@WriteLog("字典管理/删除字典")
	@RequiresPermissions("sys:dic:delete")
	@RequestMapping(value = "delete/{id}")
	@ResponseBody
	public String deleteDef(@PathVariable("id") Integer id) {
		dicService.defDelete(id);
		return "删除成功";
	}
	
	/**
	 * 根据字典code 获取字典项内容列表
	 * @Title: listItemsByCode
	 * @Description:
	 * @param dicCode
	 * @return
	 */
	@RequestMapping("{dicCode}/item/json")
	@ResponseBody
	public List<DicItemEntity> listItemsByCode(@PathVariable("dicCode") String dicCode){
		return dicService.findDicItems(dicCode);
	}
	
	
	
	/**
	 * 删除字典项
	 */
	@WriteLog("字典管理/删除字典项")
	@RequiresPermissions("sys:dic:delete")
	@RequestMapping(value = "/item/delete/{id}")
	@ResponseBody
	public String deleteItem(@PathVariable("id") Integer id) {
		dicService.itemDelete(id);
		return "删除成功";
	}
	

	
	
}
