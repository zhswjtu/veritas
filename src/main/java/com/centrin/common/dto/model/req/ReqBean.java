package com.centrin.common.dto.model.req;
import java.io.Serializable;
import com.alibaba.fastjson.JSON;


public abstract class ReqBean implements Serializable{
	private static final long serialVersionUID = 1L;
	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}

}
