package com.centrin.veritas.service;
/**
 * Project Name：veritas
 * File Name：RestoreStateInfoService
 * Package Name：com.centrin.veritas.service
 * Date：17/8/8 下午11:23
 */

import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseService;
import com.centrin.veritas.entity.RestoreStateInfoEntity;

import java.util.List;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service
 * @ClassName RestoreStateInfoService
 * @author 张辉
 * @date 17/8/8 下午11:23
 * @version
 */
public interface RestoreStateInfoService extends BaseService<RestoreStateInfoEntity>{

    PagingEntity<RestoreStateInfoEntity> findByRid(String rid, PagingEntity<RestoreStateInfoEntity> page);
}
