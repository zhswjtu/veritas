/*
 * Copyright 2016-2017 the original author or authors.
 */
package com.centrin.common.entity;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;

public class PagingEntity<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// -- 公共变量 --//
	public static final String ASC = "asc";
	public static final String DESC = "desc";

	public static final int DEFAULT_FIRST = 1;
	public static final int DEFAULT_SIZE = 15;

	// -- 分页参数 --//
	private Integer first = null;
	private Integer size = null;
	private String sort = null;
	private String order = null;
	private Integer pageNum = null;

	
	// -- 返回结果 --//
	private List<?> rows = Lists.newArrayList();
	private Integer total = -1;
	private Integer pageSum = -1;
	
	// -- 构造函数 --//
	public PagingEntity() {

	}

	public PagingEntity(int first, int size, String sort, String order) {
		this.first = first;
		this.size = size;
		this.sort = sort;
		this.order = order;
	}

	public int getSize() {
		int s;
		if (size != null) {
			s = size;
		} else {
			s = DEFAULT_SIZE;
		}
		return s;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public int getFirst() {
		int f;
		if (first != null) {
			f = first;
		} else {
			if (pageNum != null) {
				f = (pageNum - 1) * this.getSize() + 1;
			} else {
				f = DEFAULT_FIRST;
			}
		}
		return f;
	}

	public int getPageNum() {
		if (pageNum == null) {
			int first = getFirst();
			int size = getSize();
			pageNum = (first / size) + 1;
		}
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getOrder() {
		return order;
	}

	public List<?> getRows() {
		return rows;
	}

	public void setRows(List<?> rows) {
		this.rows = rows;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getPageSum() {
		if(total<=0) {
			pageSum = 0;
		}
		if(size==null) {
			size = DEFAULT_SIZE;
		}
		if(total%size==0){
			pageSum = total/size;
		} else {
			pageSum = total/size + 1;
		}
		return pageSum;
	}

//	public final static void  main(String[] args) {
//		System.out.println(7/8);
//	}

	
	

}
