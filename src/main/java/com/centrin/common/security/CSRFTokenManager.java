/** 
 * Project Name：jfshop 
 * File Name：aaa.java 
 * Package Name：com.centrin.common.security 
 * Date：2016-9-9 下午1:51:49 
 * Copyright (c) 2016, Centrin Data Systems Ltd. All Rights Reserved. 
 * 中金数据系统有限公司 
 */
/**
 * 
 */
package com.centrin.common.security;

import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public final class CSRFTokenManager {

    static final String CSRF_PARAM_NAME = "CSRFToken";

    public static final  String CSRF_TOKEN_FOR_SESSION_ATTR_NAME = CSRFTokenManager.class.getName() + ".tokenval";

    public static String getTokenForSession(HttpSession session) {
        String token = null;
        synchronized (session) {
            token = (String) session
                    .getAttribute(CSRF_TOKEN_FOR_SESSION_ATTR_NAME);
            if (null == token) {
                token = UUID.randomUUID().toString();
                session.setAttribute(CSRF_TOKEN_FOR_SESSION_ATTR_NAME, token);
            }

        }
        return token;
    }


    public static String getTokenFromRequest(HttpServletRequest request) {
        return request.getParameter(CSRF_PARAM_NAME);
    }


    private CSRFTokenManager() {

    };



}