package com.centrin.common.controller;

import java.beans.PropertyEditorSupport;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jgroups.util.UUID;
//import org.apache.commons.lang.StringEscapeUtils;
//import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.centrin.common.entity.BaseParamsEntity;
import com.centrin.common.entity.PagingEntity;
import com.centrin.common.utils.DateUtils;
import com.centrin.common.utils.StringUtils;
import com.centrin.system.shiro.realm.ShiroUser;
import com.centrin.system.utils.UserUtil;

/**
 * 基础控制器 其他控制器继承此控制器获得日期字段类型转换和防止XSS攻击的功能
 */
@Controller
public class BaseController {

	public final static String CSRF_PARAM_NAME = "csrfToken";

	public static final String CSRF_SESSION_NAME = BaseController.class.getName() + ".tokenval";

	@InitBinder
	public void initBinder(HttpServletRequest request, WebDataBinder binder) {

		// // String类型转换，将所有传递进来的String进行HTML编码，防止XSS攻击
		// binder.registerCustomEditor(String.class, new PropertyEditorSupport()
		// {
		// @Override
		// public void setAsText(String text) {
		// String value = text;
		// if(value!=null&&!"".equals(value)) {
		//// value = StringEscapeUtils.escapeHtml4(value.trim());
		// value = XssUtils.cleanXSS(value);
		//// value = StringEscapeUtils.escapeJavaScript(value);
		//// value = StringEscapeUtils.escapeSql(value);
		// }
		// setValue(value);
		// }
		//
		// @Override
		// public String getAsText() {
		// Object value = getValue();
		// return value != null ? value.toString() : "";
		// }
		// });

		// // Map 类型转换
		// binder.registerCustomEditor(Map.class, new PropertyEditorSupport() {
		// @Override
		// public void setAsText(String text) {
		// String value = text;
		// if(value!=null&&!"".equals(value)) {
		// value = XssUtils.cleanXSS(value);
		// }
		// setValue(value);
		// }
		// });

		// Date 类型转换
		binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) {
				setValue(DateUtils.parse2FullFormat(text));
			}
		});

		// Timestamp 类型转换
		binder.registerCustomEditor(Timestamp.class, new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) {
				Date date = DateUtils.parse2FullFormat(text);
				setValue(date == null ? null : new Timestamp(date.getTime()));
			}
		});
	}

	// /**
	// * 获取page对象
	// *
	// * @param request
	// * @return page对象
	// */
	// public <T> Page<T> getPage(HttpServletRequest request) {
	// int pageNo = 1; // 当前页码
	// int size = 20; // 每页行数
	// String orderBy = null; // 排序字段
	// String order = "desc"; // 排序顺序
	// if (StringUtils.isNotEmpty(request.getParameter("offset")))
	// pageNo = Integer.valueOf(request.getParameter("offset"));
	// if (StringUtils.isNotEmpty(request.getParameter("limit")))
	// size = Integer.valueOf(request.getParameter("limit"));
	// if (StringUtils.isNotEmpty(request.getParameter("sort")))
	// orderBy = request.getParameter("or").toString();
	// if (StringUtils.isNotEmpty(request.getParameter("orderBy")))
	// orderBy = request.getParameter("orderBy").toString();
	// if (StringUtils.isNotEmpty(request.getParameter("order")))
	// order = request.getParameter("order").toString();
	// return new Page<T>(pageNo, size, orderBy, order);
	// }

	/**
	 * 获取page对象
	 * 
	 * @param request
	 * @return page对象
	 */
	public <T> PagingEntity<T> getPage(HttpServletRequest request) {
		int first = 0; // 当前页码
		int size = 20; // 每页行数
		String sort = null; // 排序字段
		String order = "desc"; // 排序顺序
		if (StringUtils.isNotEmpty(request.getParameter("offset")))
			first = Integer.valueOf(request.getParameter("offset"));
		if (StringUtils.isNotEmpty(request.getParameter("limit")))
			size = Integer.valueOf(request.getParameter("limit"));
		if (StringUtils.isNotEmpty(request.getParameter("sort")))
			sort = request.getParameter("sort").toString();
		if (StringUtils.isNotEmpty(request.getParameter("order")))
			order = request.getParameter("order").toString();
		return new PagingEntity<T>(first, size, sort, order);
	}

	 /**
	 * 获取分页数据
	 *
	 * @param page
	 * @return map对象
	 */
	 public <T> Map<String, Object> getData(PagingEntity<T> page) {
	 Map<String, Object> map = new HashMap<String, Object>();
	 map.put("rows", page.getRows());
	 map.put("total", page.getTotal());
	 return map;
	 }
	/**
	 * 设置基本参数
	 * 
	 * @Title: setBaseParams
	 * @Description:
	 * @param entity
	 */
	public void setBaseParams(BaseParamsEntity entity) {
		ShiroUser shiroUser = UserUtil.getCurrentShiroUser();
		if (entity.getId() == null) {
			entity.setCreateId(shiroUser.getId());
			entity.setCreateName(shiroUser.getName());
			entity.setCreateTime(DateUtils.getDateTime());
		} else {
			entity.setUpdateId(shiroUser.getId());
			entity.setUpdateName(shiroUser.getName());
			entity.setUpdateTime(DateUtils.getDateTime());
		}
	}

	public void setCsrfToken(HttpServletRequest request) {
		String token = UUID.randomUUID().toString();
		request.getSession().setAttribute(CSRF_SESSION_NAME, token);
		request.setAttribute(CSRF_PARAM_NAME, token);
	}

	public boolean checkCsrfToken(HttpServletRequest request) {
		String csrf1 = request.getParameter(CSRF_PARAM_NAME);
		String csrf2 = String.valueOf(request.getSession().getAttribute(CSRF_SESSION_NAME));
		if (StringUtils.isNotBlank(csrf1) && csrf1.equals(csrf2)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 获取二级域名别名
	 * 
	 * @Title: getSld
	 * @Description:
	 * @param url
	 * @return
	 */
	public String getSldAlias(String url) {
		int beginIndex = url.indexOf("//") + 2;
		int endIndex = url.indexOf(".");
		if (beginIndex < endIndex) {
			return url.substring(beginIndex, endIndex);
		} else {
			return "hsd";
		}

	}

}
