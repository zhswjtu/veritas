package com.centrin.system.shiro.realm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import jodd.util.StringUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.centrin.system.dao.RoleDao;
import com.centrin.system.dao.UserDao;
import com.centrin.system.entity.PermissionEntity;
import com.centrin.system.entity.RoleEntity;
import com.centrin.system.entity.UserEntity;
import com.centrin.system.error.IncorrectCaptchaException;
import com.centrin.system.service.PermissionService;
import com.centrin.system.service.RoleService;
import com.centrin.system.service.UserService;
import com.centrin.system.shiro.authc.UsernamePasswordCaptchaToken;
import com.centrin.system.utils.UserUtil;

public class MyRealm extends AuthorizingRealm implements Serializable {

	private static final long serialVersionUID = -1880741033756482743L;

	private static final Logger log = LoggerFactory.getLogger(MyRealm.class);

	@Autowired
	private UserDao userDao;

	@Autowired
	private PermissionService permissionService;
	
	@Autowired
	private RoleDao roleDao;
	
	/*
	 * 认证信息处理
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken authcToken) throws AuthenticationException {
		UsernamePasswordCaptchaToken token = (UsernamePasswordCaptchaToken) authcToken;
		String username = token.getUsername();
		UserEntity user = userDao.getValidUserByUsername(username);
		//if (user != null && captchaValidate(token)) {
		if (user != null) {
			log.info("账号为" + user.getUsername() + "的用户登录。");
//			List<RoleEntity> roles = roleDao.findRolesByUserId(user.getId());
			ShiroUser shiroUser = new ShiroUser(user.getId(),
					user.getUsername(), user.getName());
			return new SimpleAuthenticationInfo(shiroUser, user.getPassword(),
					ByteSource.Util.bytes(user.getSalt()), getName());
		} else {
			return null;
		}
	}

	/**
	 * 验证码校验
	 * 
	 * @param token
	 * @return boolean
	 * @throws IncorrectCaptchaException
	 */
	protected boolean captchaValidate(UsernamePasswordCaptchaToken token) {
		String captcha = (String) SecurityUtils
				.getSubject()
				.getSession()
				.getAttribute(
						com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
		if (captcha != null && !captcha.equalsIgnoreCase(token.getCaptcha())) {
			throw new IncorrectCaptchaException("验证码错误！");
		}
		return true;
	}

	/*
	 * 授权信息处理
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(
			PrincipalCollection principals) {
		//String username = principals.getPrimaryPrincipal().toString();
		ShiroUser user = (ShiroUser) principals.getPrimaryPrincipal();
		//UserEntity user = userService.getUser(username);
		if (user != null) {
			SimpleAuthorizationInfo authorization = (SimpleAuthorizationInfo) SecurityUtils.getSubject().getSession().getAttribute(String.valueOf(user.getId()));
			if(authorization!=null) {
				return authorization;
			}
			authorization = new SimpleAuthorizationInfo();
			List<PermissionEntity>  permissions = permissionService.getPermissionsByUser(user);
			for(PermissionEntity permission : permissions) {
				if(StringUtil.isNotBlank(permission.getPermCode())) {
					authorization.addStringPermission(permission.getPermCode());
				}
			}
			if(UserUtil.isSuperAdmin(user.getUsername())) {
				authorization.addRole(UserUtil.SUPER_ADMIN_ROLE);
			}

			SecurityUtils.getSubject().getSession().setAttribute(String.valueOf(user.getId()),authorization);
			return authorization;
		} 
		return null;
	}

	@Override
	public void clearCachedAuthorizationInfo(PrincipalCollection principals) {
		super.clearCachedAuthorizationInfo(principals);
	}

	@Override
	public void clearCachedAuthenticationInfo(PrincipalCollection principals) {
		super.clearCachedAuthenticationInfo(principals);
	}

	@Override
	public void clearCache(PrincipalCollection principals) {
		super.clearCache(principals);
	}

	public void clearAllCachedAuthorizationInfo() {
		getAuthorizationCache().clear();
	}

	public void clearAllCachedAuthenticationInfo() {
		getAuthenticationCache().clear();
	}

	public void clearAllCache() {
		clearAllCachedAuthenticationInfo();
		clearAllCachedAuthorizationInfo();
	}

}
