<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html lang="en">
<head>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<title>用户管理</title>
</head>

<body>
	<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
	<link rel="stylesheet" href="${ctx}/resource-Main/userList/userList_new.css">
	<!--加载效果 start-->
<div id="preloader">
    <div id="status">
        <i class="fa fa-spinner fa-spin"></i>
    </div>
</div>
<!--加载效果 end-->
<section>
    <!--main start-->
    <div class="mainpanel_mian">
        <!-- main start -->
                <div class="col-sm-12">
                    <!--list start-->
                    <div class="panel panel-default">
                        <div class="panel-heading zj-panel-heading-2">
                            <h5 class="panel-title zj-panel-title-2">用户管理</h5>
                        </div>
                        <div class="panel-body zj-list-body">
                            <div class="row" id="tb">
                                <div class="col-sm-12">
                                    <form class="col-sm-10 form-horizontal form-bordered" style="margin-bottom: 20px;" id="searchForm">
                                        <ul class="form-group" style="padding: 0;">
                                            <li style="width: 13%;">
                                                <input type="text" placeholder="昵称" class="form-control" id="filter_LIKES_name">
                                            </li>
                                            <li style="width: 13%; margin-left: 3%">
                                                <input type="text" placeholder="电话" class="form-control" id="filter_LIKES_phone">
                                            </li>
                                            <li style="width: 13%; margin-left: 3%">
                                                <input type="text" placeholder="开始日期" class="form-control" id="filter_GTD_createDate">
                                            </li>
                                            <li style="width: 13%; margin-left: 3%">
                                                <input type="text" placeholder="结束日期" class="form-control" id="filter_LTD_createDate">
                                            </li>
                                            <li>
                                                <button type="button" id='searchBtn' class="btn btn-default btn-sm" style="margin-left: 20px; margin-right: 5px;"> <i class="icon-search"></i>&nbsp;查询
                                                </button>
                                                <button type="button" id='resetBtn' class="btn btn-default btn-sm"> <i class="icon-trash"></i>&nbsp;重置
                                                </button>
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                                <div class="col-sm-12" style="padding-left: 20px">
                                    <a class="btn btn-primary" href="javascript:void(0)" onclick="add();"><i class="zj-icon zj-d-apply"></i>添加</a>
                                    <a class="btn btn-primary" href="javascript:void(0)" onclick="upd()"><i class="zj-icon zj-d-modify"></i>修改</a>
                                    <a class="btn btn-primary  btn-lg" data-toggle="modal" data-target="#myModal" onclick="del()"><i class="zj-icon zj-d-delete_all"></i>删除</a>
                                    <a class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal1" onclick="pwd()"><i class=" zj-icon zj-d-chongzhi "></i>重置密码</a>
                                    <a class="btn btn-primary " href="javascript:void(0) " onclick="userForRole() "><i class="zj-icon zj-d-user "></i>用户角色</a>
                                    <div class="table-responsive zj-table-responsive">
                                        <table class="table mb30 table-hover table-responsive" id="dg_buser">
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </div>
</section>
<script type="text/javascript">
	var d;
	var url = '${ctx}/system/user/json';
	var table;
	$(function() {
		table = $('#dg_buser').bootstrapTable({
	        method: "get",
	        url: url,
	        cache: false,
	        fit: true,
	        fitColumns: true,
	        border: false,
	        idField: 'id',
	        uniqueId: 'id',
	        pagination: true,
	        checkbox: true,
	        rownumbers: true,
	        showRefresh: false,
	        pageNumber: 1,
	        pageSize: 10,
	        sidePagination: 'server',
	        pageList: [10, 20, 30, 40, 50],
	        singleSelect: true,
	       /*  striped: true, */
	        search: false,
	        clickToSelect: true,
	        queryParams: function(params) {
	            var param = {
	                limit: params.limit, //页面大小
	                offset: params.offset, //页码
	                filter_LIKES_name: $('#filter_LIKES_name').val(),
	                filter_LIKES_phone: $('#filter_LIKES_phone').val(),
	                filter_GTD_createDate: $('#filter_GTD_createDate').val(),
	                filter_LTD_createDate: $('#filter_LTD_createDate').val()
	            };
	            return param;
	        },
	        columns: [
	            
	             {
		            field : 'checked',
		            checkbox : true,
		        },{
	                field: 'id',
	                title: 'id',
	                visible: false
	            }, {
	                field: 'username',
	                title: '帐号',
	                align: 'center',
	                width: 120
	            }, {
	                field: 'name',
	                title: '昵称',
	                align: 'center',
	                width: 120
	            }, {
	                field: 'gender',
	                title: '性别',
	                align: 'center',
	                width: 80,
	                formatter: function(
	                    value, row,
	                    index) {
	                    return value == 1 ? '男' : '女';
	                } 
	            }, {
	                field: 'email',
	                title: 'email',
	                align: 'center',
	                width: 160
	            }, {
	                field: 'phone',
	                title: '手机号',
	                align: 'center',
	                width: 120
	            }, {
	                field: 'loginCount',
	                title: '登录次数',
	                align: 'center'
	            }, {
	                field: 'previousVisit',
	                title: '上一次登录',
	                align: 'center'
	            }
	        ],
	        dataPlain: true
	    });
	    $("#resetBtn").on('click', function(e) {
	        document.getElementById("searchForm").reset();
	        table.bootstrapTable('refresh', { url: url });
	    });
	
	    $("#searchBtn").on('click', function(e) {
	    	table.bootstrapTable('refresh', { url: url });
	    });
	
	});
	
	//弹窗增加
	function add() {
	    window.location.href = '${ctx}/system/user/create';
	}
	
	/*  toastr.options.positionClass = 'toast-bottom-right'; */
	
	//删除
	function del() {
	    var row = table.bootstrapTable('getSelections');
	    if (row.length == 0){
	    	toastr.warning( '请选择要处理的行。');
	    }else{
		    Ewin.confirm({ message: "确认要删除选择的数据吗？" }).on(function (e) {  
		        if (!e) {  
		          return;  
		        }  
		        
		        $.ajax({
	                type: 'get',
	                cache: false,
	                url: "${ctx}/system/user/delete/" + row[0].id,
	                success: function(data) {
	                	toastr.success( '删除成功。');
	                	table.bootstrapTable('refresh', { url: url });
	                }
	            });
		      });  
	    }
	}
	
	//弹窗修改
	function upd() {
	    var row = table.bootstrapTable('getSelections');
	    if (row.length == 0){
	    	toastr.warning( '请选择要处理的行。');
	    }else{
	        window.location.href='${ctx}/system/user/update/' + row[0].id;
		}
	}
	
	//用户角色弹窗
	function userForRole() {
		  var row = table.bootstrapTable('getSelections');
		    if (row.length == 0){
		    	toastr.warning( '请选择要处理的行。');
		    }else{
		        window.location.href='${ctx}/system/user/' + row[0].id + '/userRole';
		    }
	}
	
	
	
	//重置密码
	function pwd() {
		   var row = table.bootstrapTable('getSelections');
		    if (row.length == 0){
		    	toastr.warning( '请选择要处理的行。');
		    }else{
			    Ewin.confirm({ message: "确认要重置密码吗？" }).on(function (e) {  
			        if (!e) {  
			          return;  
			        }  
	            $.ajax({
	                type: 'get',
	                cache: false,
	                url: "${ctx}/system/user/resetPwd/ " + row[0].id,
	                success: function(data) {
	                	toastr.success( '重置密码成功。');
	                }
	            });
	        });
		    }
	    
	}
	
</script>
</body>
</html>