package com.centrin.veritas.service;
/**
 * Project Name：veritas
 * File Name：SnService
 * Package Name：com.centrin.veritas.service
 * Date：17/8/7 下午1:16
 */

import com.centrin.common.servcie.BaseService;
import com.centrin.veritas.entity.SnEntity;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service
 * @ClassName SnService
 * @author 张辉
 * @date 17/8/7 下午1:16
 * @version
 */
public interface SnService extends BaseService<SnEntity>{

    SnEntity findNew();
}
