
package com.centrin.common.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringContainer implements ApplicationContextAware {
	
	private static ApplicationContext applicationContext;
	
	/**
	 * byType 获取bean
	 * 
	 * @param clazz		bean类
	 * @return
	 */
	public static <T> T getBean(Class<T> clazz) {
		return applicationContext.getBean(clazz);
	}
	
	/**
	 * byName 获取bean
	 * 
	 * @param beanName	bean名称
	 * @param clazz		bean类
	 * @return
	 */
	public static <T> T getBean(String beanName, Class<T> clazz) {
		return applicationContext.getBean(beanName, clazz);
	}
	
	/**
	 * byName 获取bean
	 * 
	 * @param beanName	bean名称
	 * @return
	 */
	public static Object getBean(String beanName) {
		return applicationContext.getBean(beanName);
	}
	
	public static ApplicationContext getContext() {
		return applicationContext;
	}


	public void setApplicationContext(ApplicationContext ctx)
			throws BeansException {
		applicationContext = ctx;
	}
	
}
