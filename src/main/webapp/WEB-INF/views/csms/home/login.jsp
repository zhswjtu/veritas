<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="org.apache.shiro.web.filter.authc.FormAuthenticationFilter"%>
<%@ page import="org.apache.shiro.authc.ExcessiveAttemptsException"%>
<%@ page import="org.apache.shiro.authc.IncorrectCredentialsException"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%
String error = (String) request.getAttribute(FormAuthenticationFilter.DEFAULT_ERROR_KEY_ATTRIBUTE_NAME);
request.setAttribute("error", error);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>后台管理系统</title>
	<script src="${ctx}/static/plugins/easyui/jquery/jquery-1.11.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="${ctx}/static/css/bglogin.css" />
	<script>
	var captcha;
	function refreshCaptcha(){  
	    document.getElementById("img_captcha").src="${ctx}/static/images/kaptcha.jpg?t=" + Math.random();  
	}  
	</script>
	<style>
html, body, div, ul, li, a, h1, span, p, dl, dd, dt {
	margin: 0;
	padding: 0;
	list-style: none;
	font-family: "Microsoft YaHei" ! important;
}

html, body {
	background-color: #f2f2f2;
	height: 100%;
}

.clear {
	clear: both;
}

.main {
	background-image: url('${ctx}/static/images/bg-admin.png');
	background-repeat: no-repeat;
	background-size: 100% 100%;
	margin: 0 auto;
	width: 100%;
	height: 100%;
	position: absolute;
	top: 0;
}

.main .login-box {
	position: absolute;
	top: 34.5%;
	right: 165px;
	background-image: url('${ctx}/static/images/login-box.png');
	background-repeat: no-repeat;
	background-size: 421px 274px;
	width: 436px;
	height: 336px;
}

.main .hidden-div {
	position: relative;
    top: 2%;
    left: 110px;
    background-image: url(/csms/static/images/logo.png);
    background-repeat: no-repeat;
    width: 50%;
    height: 20%;
}

.main .footer {
	position: absolute;
	font-size: 14px;
	color: #ffffff;
	margin: 0 auto;
	bottom: 50px;
	width: 100%;
	text-align: center;
}

.main .login-box .title {
	position: absolute;
	top: 40px;
	right: 90px;
	font-size: 24px;
	color: #ffffff;
	font-family: "Microsoft YaHei" ! important;
	font-weight: bold;
	letter-spacing: 3px;
}

.main .login-box input {
	padding: 0;
	margin: 0;
	height: 22px;
	line-height: 22px;
	border-style: none;
	background: transparent;
	vertical-align: middle;
	outline: none;
}

.username {
	position: absolute;
	top: 82px;
	right: 60px;
	font-size: 14px;
	color: #a8a8a8;
	width: 266px;
}

.password {
	position: absolute;
	top: 135px;
	right: 60px;
	font-size: 14px;
	color: #a8a8a8;
	width: 266px;
}

.submit-btn {
	position: absolute;
	top: 196px;
	right: 58px;
	width: 308px !important;
	height: 34px !important;
	background-image: url('${ctx}/static/images/submit-btn.png') !important;
	background-repeat: no-repeat;
	cursor: pointer;
}

.submit-btn:hover {
	background-image: url('${ctx}/static/images/submit-btn-s.png')
		!important;
}

.err {
	position: absolute;
	/* top: 238px; */
	right: 180px;
	width: 400px;
	height: 30px;
	line-height: 30px;
	overflow: hidden;
	color: red;
}

.login-box img {
	position: absolute;
	top: 192px;
	right: 158px;
	border: 0;
	width: 77px;
	height: 28px;
	cursor: pointer;
}

.height-div {
	display: inline-block;
	*display: inline;
}
</style>
</head>
<body>
	 <!-- 垂直高度start-->
    <div class="height-div" >
    
    </div>
    
	<div class="main">
	    <div class="hidden-div"></div>
	    <!-- 垂直高度end-->
        <form id="loginForm" action="${ctx}/system/login" method="post">
            <div class="login-box">
                <input class="username" type="text" id="username" name="username" placeholder="用户名" title="用户名" value="" maxlength="20"/>
                <input class="password" type="password" id="password" name="password" placeholder="密码" title="密码" value="" maxlength="20"  autocomplete="off"/>
                <input class="submit-btn" type="submit" value="" />
            </div>
            <div class="err" id="login_main_errortip"></div>
        </form>
	    <div class="footer">&copy; 2016 北京中金云网科技有限公司 版权所有</div>
    </div>
	<c:choose>
		<c:when test="${error eq 'com.centrin.system.error.IncorrectCaptchaException'}">
			<script>
				$("#login_main_errortip").html("验证码错误，请重试");
			</script>
		</c:when>
		<c:when test="${error eq 'org.apache.shiro.authc.UnknownAccountException'}">
			<script>
				$("#login_main_errortip").html("帐号或密码错误，请重试");
			</script>
		</c:when>
		<c:when test="${error eq 'org.apache.shiro.authc.IncorrectCredentialsException'}">
			<script>
				$("#login_main_errortip").html("帐号或密码错误，请重试");
			</script>
		</c:when>
		<c:when test="${error eq 'org.apache.shiro.authc.ExcessiveAttemptsException'}">
			<script>
				$("#login_main_errortip").html("错误登录次数超限！");
			</script>
		</c:when>
	</c:choose>
</body>
</html>
