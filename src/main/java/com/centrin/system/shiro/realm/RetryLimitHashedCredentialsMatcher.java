package com.centrin.system.shiro.realm;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

public class RetryLimitHashedCredentialsMatcher extends HashedCredentialsMatcher {

	private final static String prefix = "cache_login_";
	
	private Cache passwordRetryCache;

    public RetryLimitHashedCredentialsMatcher(CacheManager cacheManager) {
        passwordRetryCache = cacheManager.getCache("passwordRetryCache");
    }

    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        //String username = (String)token.getPrincipal();
        
        ShiroUser user = (ShiroUser)info.getPrincipals().getPrimaryPrincipal();
        String username = prefix + user.getUsername();
        
        //retry count + 1
        AtomicInteger retryCount = null;
        if(passwordRetryCache.get(username)!=null) {
        	 retryCount = (AtomicInteger) passwordRetryCache.get(username).get();
        }
        if(retryCount == null) {
            retryCount = new AtomicInteger(0);
        }
        retryCount.incrementAndGet();
        passwordRetryCache.put(username, retryCount);
        
        if(retryCount.get() > 5) {
            //if retry count > 5 throw
            throw new ExcessiveAttemptsException();
        }

        boolean matches = super.doCredentialsMatch(token, info);
        if(matches) {
            //clear retry count
        	passwordRetryCache.evict(username);
        }
//		userService.updateLoginInfo(user.getId());
        return matches;
    }
}
