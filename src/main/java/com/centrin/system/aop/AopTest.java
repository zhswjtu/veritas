package com.centrin.system.aop;

import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class AopTest {

		@Pointcut("execution(* *.*.*.test.*.*(..))")
		public void myMethod(){};
		
		@Before("myMethod()")
		public void beforeMethod(JoinPoint point)
		{
			 System.out.println("@Before：模拟权限检查...");
		        System.out.println("@Before：目标方法为：" + 
		                point.getSignature().getDeclaringTypeName() + 
		                "." + point.getSignature().getName());
		        System.out.println("@Before：参数为：" + Arrays.toString(point.getArgs()));
		        System.out.println("@Before：被织入的目标对象为：" + point.getTarget());
		}
		
		//正常执行完后
		@AfterReturning("myMethod()")
		public void afterReturnning(JoinPoint point)
		{
			System.out.println("after save......");
		}
		
		//抛出异常时才调用
		@AfterThrowing("myMethod()")
		public void afterThrowing(JoinPoint point)
		{
			System.out.println("after throwing......");
		}
		
		
//		@Around("myMethod()")
//		public void aroundMethod(ProceedingJoinPoint pjp) throws Throwable
//		{
//			//加逻辑的时候, 不要依赖执行的的先后顺序
//			System.out.println("method around start!");
//			pjp.proceed();
//			System.out.println("method around end!");
//		}

}
