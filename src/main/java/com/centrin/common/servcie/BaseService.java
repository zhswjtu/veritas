/*
 * Copyright 2016-2017 the original author or authors.
 */
package com.centrin.common.servcie;

import java.util.List;

import com.centrin.common.dao.QueryFilter;
import com.centrin.common.entity.BaseEntity;
import com.centrin.common.entity.PagingEntity;


public interface BaseService<T extends BaseEntity>{

	String COMMON_ENTITY_ALIAS = " e";
	String COMMON_SELECT_JPQL = "SELECT e FROM ";
	String COMMON_COUNT_JPQL = "SELECT count(e) FROM ";
	
	/**
	 * 新增
	 * 
	 * @param entity
	 */
	public void add(T entity);
	
	/**
	 * 修改
	 * 
	 * @param entity
	 */
	public T update(T entity);
	
	/**
	 * @Title: delete
	 * @Description:删除
	 * @param clazz
	 * @param id
	 */
	public void delete(Class<T> clazz, int id);
	
	/**
	 * 删除
	 * @Title: delete
	 * @Description:
	 * @param clazz
	 * @param id
	 */
	public void delete(Class<T> clazz, long id);
	
	/**
	 * 按Id查找
	 * 
	 * @param id 实体id
	 * @return 实体对象
	 */
	public T findById(Class<T> clazz, int id);
	
	/**
	 * 
	 * findAll:获取所有. <br/> 
	 * @param c
	 * @return 
	 * @author changtao 
	 * 2017年6月9日 下午2:50:50
	 */
	public List<T> findAll(Class<T> c);
	/**
	 * 按Id查找
	 * 
	 * @param id 实体id
	 * @return 实体对象
	 */
	public T findById(Class<T> clazz, Long id);


	/**
	 * 
	 * @Title: findById
	 * @Description:
	 * @param clazz
	 * @param id
	 * @return
	 */
	public T findById(Class<T> clazz, String id);
	
	/**
	 * 返回实体列表
	 * 
	 * @param queryFilter 过滤参数
	 * @return 实体列表
	 */
	public List<T> findAsList(Class<T> clazz, QueryFilter queryFilter);
	
	/**
	 * 返回实体分页列表对象
	 * 
	 * @param
	 * @return 实体分页对象
	 */
	public PagingEntity<T> findAsPaging(Class<T> clazz, QueryFilter queryFilter);
	
	
	/**
	 * 返回实体
	 * 
	 * @param queryFilter 过滤参数
	 * @return 实体列表
	 */
	public T findAsSingle(Class<T> clazz, QueryFilter queryFilter);
	
	
	public <E> E getForUpdate(Class<E> c, int id);
	
	
	public <E> E getForUpdate(Class<E> c, long id);
	
	
	public <E> E getForUpdate(Class<E> c, String id);
	
}
