package com.centrin.common.dto.model.req;

public class ReqDeviceBean extends ReqBean {
	private static final long serialVersionUID = 1L;

	private ReqDeviceHeadBean head;

	public ReqDeviceHeadBean getHead() {
		return head;
	}

	public void setHead(ReqDeviceHeadBean head) {
		this.head = head;
	}
}
