package com.centrin.system.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.centrin.system.entity.RoleEntity;

@Repository
public interface IRoleDao extends JpaRepository<RoleEntity, Integer> {
	
}
