/** 
 * Project Name：csms-cservice 
 * File Name：md5.java 
 * Package Name：com.centrin.flowbank.utils 
 * Date：2016-8-8 下午2:40:06 
 * Copyright (c) 2016, Centrin Data Systems Ltd. All Rights Reserved. 
 * 中金数据系统有限公司 
 */
/**
 * 
 */
package com.centrin.common.utils;

import java.security.MessageDigest;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.flowbank.utils
 * @ClassName md5
 * @author john.he   
 * @date 2016-8-8 下午2:40:06
 * @version 
 */
public class MD5Util {

	    public final static String MD5(String s) {
	        char hexDigits[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};       
	        try {
	            byte[] btInput = s.getBytes();
	            // 获得MD5摘要算法的 MessageDigest 对象
	            MessageDigest mdInst = MessageDigest.getInstance("MD5");
	            // 使用指定的字节更新摘要
	            mdInst.update(btInput);
	            // 获得密文
	            byte[] md = mdInst.digest();
	            // 把密文转换成十六进制的字符串形式
	            int j = md.length;
	            char str[] = new char[j * 2];
	            int k = 0;
	            for (int i = 0; i < j; i++) {
	                byte byte0 = md[i];
	                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
	                str[k++] = hexDigits[byte0 & 0xf];
	            }
	            return new String(str);
	        } catch (Exception e) {
	            e.printStackTrace();
	            return null;
	        }
	    }

	    
		/**
		 * @Title: main
		 * @Description:
		 * @param args
		 */
		public static void main(String[] args) {
			//418674f88fe14991a1a486e1690d0067
//			System.out.println(MD5Util.MD5("﻿15910630780"));
			
//			var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
			
			
//			* 手机号码: 
//			     * 13[0-9], 14[5,7], 15[0, 1, 2, 3, 5, 6, 7, 8, 9], 17[6, 7, 8], 18[0-9], 170[0-9]
//			     * 移动号段: 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
//			     * 联通号段: 130,131,132,155,156,185,186,145,176,1709
//			     * 电信号段: 133,153,180,181,189,177,1700

			
			for(long i=13000000000L;i<140000000000L;i++){
				if("32dd2a4f4868f5f3cb75c484434e26e2".equals(MD5Util.MD5(i+""))){
					System.out.println(i);
					return;
				}
				System.out.println(i+"");
			}
			
			for(long i=15000000000L;i<160000000000L;i++){
				if("32dd2a4f4868f5f3cb75c484434e26e2".equals(MD5Util.MD5(i+""))){
					System.out.println(i);
					return;
					
				}
				System.out.println(i+"");
			}
			
			for(long i=17000000000L;i<190000000000L;i++){
				if("32dd2a4f4868f5f3cb75c484434e26e2".equals(MD5Util.MD5(i+""))){
					System.out.println(i);
					return;
				}
				System.out.println(i+"");
			}
			
			System.out.println("ok");

		}
}
