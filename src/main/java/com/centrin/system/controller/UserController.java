package com.centrin.system.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.SystemException;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.centrin.common.controller.BaseController;
import com.centrin.common.entity.PagingEntity;
import com.centrin.system.entity.OrgEntity;
import com.centrin.system.entity.RoleEntity;
import com.centrin.system.entity.UserEntity;
import com.centrin.system.error.SysException;
import com.centrin.system.interceptor.WriteLog;
import com.centrin.system.service.OrgService;
import com.centrin.system.service.PasswordHelper;
import com.centrin.system.service.RoleService;
import com.centrin.system.service.UserService;
import com.centrin.system.shiro.realm.ShiroUser;
import com.centrin.system.utils.UserUtil;

@Controller
@RequestMapping(value = "/system/user")
public class UserController extends BaseController {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;
    @Autowired
    private OrgService orgService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PasswordHelper passwordHelper;


    /**
     * 添加用户
     * @param
     * @return
     */
    @WriteLog("用户管理/添加用户")
    @RequiresPermissions("sys:user:add")
    @RequestMapping(value = ("create"), method = RequestMethod.POST)
    @ResponseBody
    public String addUser(UserEntity user) {
        passwordHelper.encryptPassword(user);
        userService.addUser(user);
        
        return "success";
    }

    /**
     * 验证用是否已户存在
     * @param request
     * @return
     */
    @RequestMapping(value = "/checkLoginName")
    @ResponseBody
    public String checkLoginName(HttpServletRequest request){
        String username = request.getParameter("username");
        UserEntity user = this.userService.getUser(username);
        String flag = "";
        if (null == user){
            flag = "true";
        } else {
            flag = "false";
        }
        return flag;
    }

    /**
     * 用户列表默认跳转页
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String list(){
        return "/system/userList";
    }
 

    /**
     * 获取用户列表数据
     * @param request
     * @return
     */
    @RequiresPermissions("sys:user:view")
    @RequestMapping(value = "/json")
    @ResponseBody
    public Map<String, Object> getUserList(HttpServletRequest request){
        //电话
        String phone = request.getParameter("filter_LIKES_phone");
        //昵称
        String name = request.getParameter("filter_LIKES_name");
        //开始时间
        String beginDate = request.getParameter("filter_GTD_createDate");
        //结束时间
        String endDate = request.getParameter("filter_LTD_createDate");
        //获取分页信息
        PagingEntity<UserEntity> page = getPage(request);
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("phone",phone);
        map.put("name",name);
        map.put("beginDate", beginDate);
        map.put("endDate", endDate);
        map.put("page",page);
        PagingEntity<UserEntity> list = this.userService.getUserList(map);
        Map<String, Object> renMap = getData(list);
        return renMap;
    }

    /**
     * 添加新用户跳转
     * @param
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createForm(Model model){
        model.addAttribute("user", new UserEntity());
        model.addAttribute("action", "create");
        model.addAttribute("orgs", orgService.getAvailableOrgs());
        return "system/userForm";
    }

    /**
     * 用户信息修改跳转
     * @param model
     * @return
     */
    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String updateFrom(@PathVariable("id") Integer id, Model model){
        model.addAttribute("user", userService.get(id));
        model.addAttribute("action", "update");
        model.addAttribute("orgs", orgService.getAvailableOrgs());
        return "system/userForm";
    }

    /**
     * 用户信息修改
     * @param userEntity
     * @return
     * @throws InvocationTargetException 
     * @throws IllegalAccessException 
     * @throws IllegalArgumentException 
     */
    @WriteLog("用户管理/信息修改")
    @RequiresPermissions("sys:user:update")
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ResponseBody
    public String updateUser(UserEntity userEntity) throws Exception{
        userService.updateUserInfo(userEntity);
        return "success";
    }

    /**
     * 逻辑删除用户
     * @param id
     * @return
     */
    @WriteLog("用户管理/删除用户")
    @RequiresPermissions("sys:user:delete")
    @RequestMapping(value = "/delete/{id}")
    @ResponseBody
    public String delete(@PathVariable("id") Integer id) {
        this.userService.delete(id);
        return "success";
    }
    
    
    /**
     * 重置用户密码
     * @Title: resetPwd
     * @Description:
     * @param id
     * @return
     */
    @WriteLog("用户管理/重置密码")
    @RequiresPermissions("sys:user:update")
    @RequestMapping(value = "/resetPwd/{id}")
    @ResponseBody
    public String resetPwd(@PathVariable("id") Integer id) {
        this.userService.resetPwd(id);
        return "success";
    }
    
    

    @RequestMapping(value = "/roleAll")
    @ResponseBody
    public List<RoleEntity> getRoleAll(){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("orderby", "");
        List<RoleEntity> roleEntities=roleService.findRoleAll(map);
        return roleEntities;
    }

    /**
     * 用户添加角色跳转
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/{id}/userRole",method = RequestMethod.GET)
    public String userRoleFrom(@PathVariable("id") Integer id,Model model){
        model.addAttribute("userId", id);
        return "/system/userRoleList";
    }

    /**
     * 获取用户角色
     * @param id
     * @return
     */
    @RequiresPermissions("sys:user:roleView")
    @RequestMapping(value = "/{id}/role",method = RequestMethod.GET)
    @ResponseBody
    public List<RoleEntity> getUserRoleIds(@PathVariable("id") Integer id){
        List<RoleEntity> list = userService.getUserRoleIds(id);
        return list;
    }

    /**
     * 修改用户角色关系
     * @param list
     * @param id
     * @return
     */
    @WriteLog("用户管理/添加角色")
    @RequiresPermissions("sys:user:roleUpd")
    @RequestMapping(value = "/{id}/updateRole")
    @ResponseBody
    public String updateRole(@RequestBody List<Integer> list,@PathVariable("id") Integer id){
        userService.addUserRole(id,list);
        return "success";
    }

    
    /**
     * 用户修改密码
     * @param list
     * @param id
     * @return
     * @throws SystemException 
     */
    @WriteLog("用户管理/修改密码")
    @RequestMapping(value = "/updatePassword")
    @ResponseBody
    public String updatePassword(String oldPassword, String newPassword){
    	ShiroUser shiroUser = UserUtil.getCurrentShiroUser();
		try {
			userService.updatePassword(shiroUser, oldPassword, newPassword);
			return "success";
		} catch (SysException e) { //用户旧密码不正确
			e.printStackTrace();
			return e.getMessage();
		}
    }
    
    
    /**
     * 获取机构信息
     * @return
     */
    @RequestMapping(value = "/getOrg")
    @ResponseBody
    public List<OrgEntity> getOrgAll(){
        return orgService.getAvailableOrgs();
    }
}
