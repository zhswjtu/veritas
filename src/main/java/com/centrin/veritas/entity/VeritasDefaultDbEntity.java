package com.centrin.veritas.entity;
/**
 * Project Name：veritas
 * File Name：VeritasDefaultDbEntity
 * Package Name：com.centrin.veritas.entity
 * Date：17/7/21 上午9:56
 */

import com.centrin.common.entity.BaseEntity;

import javax.persistence.*;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.entity
 * @ClassName VeritasDefaultDbEntity
 * @author 张辉
 * @date 17/7/21 上午9:56
 * @version
 */
@Entity
@Table(name = "veritas_oracle_db")
public class VeritasDefaultDbEntity extends BaseEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "parameter_name")
    private String parameterName;

    @Column(name = "default_value")
    private String defaultValue;

    @Column(name = "data_type")
    private String dataType;

    @Column(name = "data_unit")
    private String dataUnit;

    private String description;

    @Column(name = "chapter_head")
    private String chapterHead;

    private Integer type;

    private Integer status;

    private String ip;


    public VeritasDefaultDbEntity() {
    }

    public VeritasDefaultDbEntity(VeritasOracleEntity veritasOracleEntity) {
        this.parameterName = veritasOracleEntity.getParameterName();
        this.defaultValue = veritasOracleEntity.getDefaultValue();
        this.description = veritasOracleEntity.getDescription();
        this.chapterHead = veritasOracleEntity.getChapterHead();
        this.type = veritasOracleEntity.getType();
        this.status = veritasOracleEntity.getStatus();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataUnit() {
        return dataUnit;
    }

    public void setDataUnit(String dataUnit) {
        this.dataUnit = dataUnit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getChapterHead() {
        return chapterHead;
    }

    public void setChapterHead(String chapterHead) {
        this.chapterHead = chapterHead;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
