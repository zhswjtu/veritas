
package com.centrin.common.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;

public class ContentTransUtils {

	public static final Configuration cfg;
//	private static final Logger logger = LoggerFactory.getLogger(ContentTransUtil.class);
	static {
		cfg = new Configuration();
		String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		cfg.setEncoding(Locale.CHINESE, "UTF-8");
		try {
			cfg.setDirectoryForTemplateLoading(new File(path+"templete"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		cfg.setObjectWrapper(new DefaultObjectWrapper());
	}
			

	/**
	 * @Title: write
	 * @Description:
	 * @param rootMap
	 * @param
	 * @param ftlFile
	 */
	public static void write(Object rootMap, String ftlFile, OutputStream outStream) {
		try {
			//ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			Template tpl = cfg.getTemplate(ftlFile);			
			Writer out = new OutputStreamWriter(outStream);
			tpl.process(rootMap, out);
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @Title: write
	 * @Description:
	 * @param rootMap
	 * @param destFile
	 * @param ftlFile
	 */
	public static void write(Object rootMap, String destFile, String ftlFile) {
		try {
			Template tpl = cfg.getTemplate(ftlFile);
			Writer out = new FileWriter(destFile);
			tpl.process(rootMap, out);
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 文档内容转换
	 * @Title: transfer
	 * @Description:
	 * @param rootMap
	 * @param ftlFile
	 * @return
	 */
	public static String transfer(Object rootMap, String ftlFile) {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		write(rootMap, ftlFile, outStream);
		return outStream.toString();
	}
		
	public static void main(String[] args){
		System.out.println(System.getProperty("user.dir"));
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("ctx", "package");
		params.put("content", "icon");
//		List<Map<String,String>> ipInfos = new ArrayList<Map<String,String>>();
//		for(int i=0;i<3;i++) {
//			Map<String,String> map = new HashMap<String, String>();
//			map.put("mapType", "SNAT");
//			map.put("publicIp", "115.123.187.123");
//			map.put("publicPort", "8080");
//			map.put("privatePort", "8080");
//			map.put("privateIp", "192.168.111.191");
//			map.put("protocolType", "TCP");
//			map.put("operaterType", "删除");
//			ipInfos.add(map);
//		}
	}
}

