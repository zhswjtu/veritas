package com.centrin.common.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import jodd.io.FileUtil;
import jodd.util.Base64;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import com.centrin.common.controller.BaseController;
import com.centrin.common.dto.RspBaseInfo;
import com.centrin.common.utils.DateUtils;
import com.centrin.common.utils.ImageUtils;
import com.centrin.common.utils.PictureUploadUtils;

@Controller
public class PictureController extends BaseController{


//	private static String picRootPath = null;
//	
//	static {
//		picRootPath = ResourceBundle.getBundle("application").getString("pic.root.path");
//	}
	

	
	/**
	 * 上传图片
	 * @Title: uploadImg
	 * @Description:
	 * @param tempFile
	 * @return
	 */
	@RequestMapping("/picture/upload")
    @ResponseBody
    public RspBaseInfo uploadImg(@RequestParam CommonsMultipartFile imgFile, HttpServletRequest request) {
    	if (imgFile == null) {
			return new RspBaseInfo(RspBaseInfo.ERROR,"上传文件为空!");
		}
    	Integer width= null,height=null;
    	if(request.getParameter("width")!=null) {
    		width = Integer.valueOf(request.getParameter("width"));
    	}
    	
    	if(request.getParameter("height")!=null) {
    		height = Integer.valueOf(request.getParameter("height"));
    	}

    	String fileUrl = null, message="";
    	StringBuffer picPath = new StringBuffer();
    	StringBuffer fileId = new StringBuffer();
		try {
			String dateDir = DateUtils.format(new Date(),"yyyyMMdd");
			picPath.append(PictureUploadUtils.PIC_ROOT_PATH);
			picPath.append("/"+PictureUploadUtils.TEMP_FOLDER+"/");
			FileUtil.mkdirs(picPath.toString() + dateDir); //创建目录
			
			fileId.append(dateDir);
			fileId.append("/");
			fileId.append(UUID.randomUUID());
			String oriFilename = imgFile.getOriginalFilename();
			String prefix=oriFilename.substring(oriFilename.lastIndexOf(".")+1);
			fileId.append(".");
			fileId.append(prefix);
			
			picPath.append(fileId);
			
			if(width!=null || height!=null) {
				ImageUtils.scaleImage(imgFile.getBytes(), picPath.toString(), width, height, prefix);
			} else {
				FileUtil.writeBytes(picPath.toString(), imgFile.getBytes()); //写文件
			}
			
			fileUrl = request.getContextPath() + "/picture/show/" + PictureUploadUtils.TEMP_FOLDER + "/" + fileId.toString();
			return  new RspBaseInfo(RspBaseInfo.SUCCESS,fileUrl);
		} catch (Exception e) {
			e.printStackTrace();
			message = e.getMessage();
		}
		return new RspBaseInfo(RspBaseInfo.ERROR,message);
    }
	
	
	
	/**
	 * @Title: upload
	 * @Description: 图片上传
	 * @param cImage
	 * @return
	 */
	@RequestMapping("/picture/xheditor/upload")
	@ResponseBody
	public String xheditor_upload(@RequestParam MultipartFile filedata,HttpServletRequest request) {
		if (filedata == null) {
			return "{\"err\":1," + "\"msg\":\"上传文件为空。\"}";
		}
    	StringBuffer picPath = new StringBuffer();
    	StringBuffer fileId = new StringBuffer();
    	String fileUrl = null, message = "";
		try {
			String dateDir = DateUtils.format(new Date(),"yyyyMMdd");
			picPath.append(PictureUploadUtils.PIC_ROOT_PATH);
			picPath.append("/"+PictureUploadUtils.DATA_FOLDER+"/");
			FileUtil.mkdirs(picPath.toString() + dateDir); //创建目录
			
			fileId.append(dateDir);
			fileId.append("/");
			fileId.append(UUID.randomUUID());
			String oriFilename = filedata.getOriginalFilename();
			String prefix=oriFilename.substring(oriFilename.lastIndexOf(".")+1);
			fileId.append(".");
			fileId.append(prefix);
			
			picPath.append(fileId);
			FileUtil.writeBytes(picPath.toString(), filedata.getBytes()); //写文件
			fileUrl = request.getContextPath() 
					+ "/picture/show/" + PictureUploadUtils.DATA_FOLDER + "/" 
					+ fileId.toString();
			return "{\"err\":\"\"," + "\"msg\":\"" + fileUrl + "\"}";
		} catch (Exception e) {
			e.printStackTrace();
			message = e.getMessage();
		}
		return "{\"err\":\"" + message + "\"," + "\"msg\":\"\"}";
	}
	
	
	/**
	 * 自动保存图片
	 * @Title: upload_xheditor_auto
	 * @Description:
	 * @param urls
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/picture/xheditor/upload/auto")
	@ResponseBody
	public String upload_xheditor_auto(@RequestParam String urls,
			HttpServletRequest request) throws IOException {
		String[] arrUrl = urls.split("\\|");
    	String dateDir = DateUtils.format(new Date(),"yyyyMMdd");
    	StringBuffer result = new StringBuffer();
		for (int i = 0; i < arrUrl.length; i++) {
			StringBuffer picPath = new StringBuffer();
	    	StringBuffer fileId = new StringBuffer();
			picPath.append(PictureUploadUtils.PIC_ROOT_PATH);
			picPath.append("/"+PictureUploadUtils.DATA_FOLDER+"/");
			FileUtil.mkdirs(picPath.toString() + dateDir); //创建目录
			fileId.append(dateDir);
			fileId.append("/");
			fileId.append(UUID.randomUUID());
			String destFileId = saveRemoteImg(arrUrl[i], fileId.toString());
			if (destFileId != "") {
				if (i > 0) {
					result.append("|");
				}
				result.append(request.getContextPath() 
						+ "/picture/show/" + PictureUploadUtils.DATA_FOLDER + "/" 
						+ destFileId);
			}
		}
		return result.toString();
	}

	
	/**
	 * 保存文件
	 * 
	 * @Title: saveRemoteImg
	 * @Description:
	 * @param sUrl
	 * @param fileId
	 * @return
	 * @throws IOException
	 */
	private String saveRemoteImg(String sUrl, String fileId) throws IOException {
		byte[] fileContent = null;
		String sExt;
		String upExt = ",jpg,jpeg,gif,png,"; // 上传扩展名
		int maxAttachSize = 2097152; // 最大上传大小，默认是2M

		if (sUrl.startsWith("data:image")) // base64编码文件
		{
			int pstart = sUrl.indexOf('/') + 1;
			sExt = sUrl.substring(pstart, sUrl.indexOf(';')).toLowerCase();
			if (upExt.indexOf("," + sExt + ",") == -1) {
				return "";
			}
			fileContent = Base64
					.decode(sUrl.substring(sUrl.indexOf("base64,") + 7));
		} else // 图片网址
		{
			sExt = sUrl.substring(sUrl.lastIndexOf('.') + 1).toLowerCase();
			if (upExt.indexOf("," + sExt + ",") == -1) {
				return "";
			}
			fileContent = getUrl(sUrl);
		}

		if (fileContent.length > maxAttachSize) { // 超过最大上传大小忽略
			return "";
		}

		// 有效图片保存
		fileId = fileId + "." + sExt;
		FileUtil.writeBytes(PictureUploadUtils.PIC_ROOT_PATH + "/" 
		+ PictureUploadUtils.DATA_FOLDER +"/"+ fileId, fileContent);

		return fileId;
	}

	
	/**
	 * 获取图片内容
	 * 
	 * @Title: getUrl
	 * @Description:
	 * @param sUrl
	 * @return
	 * @throws IOException
	 */
	private byte[] getUrl(String sUrl) throws IOException {
		HttpRequest httpRequest = HttpRequest.get(sUrl);
		HttpResponse response = httpRequest.send();
		return response.bodyBytes();
	}
	
	
	/**
	 * @Title: upload
	 * @Description: 图片上传
	 * @param cImage
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/picture/show/{path}/{catalog}/{filename:.+}")
	public void show(@PathVariable("path") String path, @PathVariable("catalog") String catalog, @PathVariable("filename") String filename, HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		String fileId = catalog + "/" + filename;
		String prefix = fileId.substring(fileId.lastIndexOf(".") + 1)
				.toLowerCase();

		response.reset();
//		response.setHeader("Pragma", "No-cache");
//		response.setHeader("Cache-Control", "no-cache");
//		response.setDateHeader("Expires", 0);
//		response.setContentType("image/" + prefix);
		
		if("jpg".equals(prefix)) {
			response.setContentType("image/jpeg");
		} else {
			response.setContentType("image/" + prefix);
		}
		
		String picPath = PictureUploadUtils.PIC_ROOT_PATH + "/" + path +"/" + fileId;
		
		
		File file = new File(picPath);

		// 判断文件是否存在如果不存在就返回默认图标
		if (!(file.exists() && file.canRead())) {
			// file = new
			// File(request.getSession().getServletContext().getRealPath("/")
			// + "resource/icons/auth/root.png");
		}

//		 response.addHeader("Content-Disposition", "attachment;filename=" +
//		 new String(file.getName().getBytes("GB18030"), "iso8859-1"));
		
//		response.addHeader("Content-Disposition", "attachment;filename=" + file.getName());

		OutputStream out = null;
		FileInputStream in = null;
		try {
			out = response.getOutputStream();
			in = new FileInputStream(file);
			response.setContentLength(in.available());
			
			byte[] b = new byte[1024];
			int i = 0;

			while ((i = in.read(b)) > 0) {
				out.write(b, 0, i);
			}
			out.flush();
		} catch (Exception e) {
//			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
				in = null;
			}
		}
	}
	
	
}
 