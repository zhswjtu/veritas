package com.centrin.veritas.entity;
/**
 * Project Name：veritas
 * File Name：RestoreReportEntity
 * Package Name：com.centrin.veritas.entity
 * Date：17/7/26 下午3:31
 */

import com.centrin.common.entity.BaseEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.entity
 * @ClassName RestoreReportEntity
 * @author 张辉
 * @date 17/7/26 下午3:31
 * @version
 */
@Entity
@Table(name = "restore_report")
public class RestoreReportEntity extends BaseEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "sid")
    private String sid;

    @Column(name = "client_name")
    private String clientName;

    @Column(name = "Policy_name")
    private String policyName;

    @Column(name = "backup_time")
    private String backupTime;

    @Column(name = "re_hostname")
    private String reHostname;

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "end_time")
    private Date endTime;

    @Column(name = "Elapsed")
    private String elapsed;

    @Column(name = "Status")
    private String status;

    @Column(name = "Backup_Size")
    private Double backupSize;

    @Column(name = "Restored_DB_Size")
    private Double restoredDBSize;

    @Column(name = "Restored_Archivelog_Size")
    private Double restoredArchivelogSize;

    @Column(name = "Exported_Size")
    private Double exportedSize;

    @Column(name = "Remark")
    private String remark;

    public RestoreReportEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public String getBackupTime() {
        return backupTime;
    }

    public void setBackupTime(String backupTime) {
        this.backupTime = backupTime;
    }

    public String getReHostname() {
        return reHostname;
    }

    public void setReHostname(String reHostname) {
        this.reHostname = reHostname;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getElapsed() {
        return elapsed;
    }

    public void setElapsed(String elapsed) {
        this.elapsed = elapsed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getBackupSize() {
        return backupSize;
    }

    public void setBackupSize(Double backupSize) {
        this.backupSize = backupSize;
    }

    public Double getRestoredDBSize() {
        return restoredDBSize;
    }

    public void setRestoredDBSize(Double restoredDBSize) {
        this.restoredDBSize = restoredDBSize;
    }

    public Double getRestoredArchivelogSize() {
        return restoredArchivelogSize;
    }

    public void setRestoredArchivelogSize(Double restoredArchivelogSize) {
        this.restoredArchivelogSize = restoredArchivelogSize;
    }

    public Double getExportedSize() {
        return exportedSize;
    }

    public void setExportedSize(Double exportedSize) {
        this.exportedSize = exportedSize;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
