package com.centrin.veritas.dao;
/**
 * Project Name：veritas
 * File Name：VeritasOracleInstanceDao
 * Package Name：com.centrin.veritas.dao
 * Date：17/7/24 下午8:28
 */

import com.centrin.common.dao.BaseDao;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao
 * @ClassName VeritasOracleInstanceDao
 * @author 张辉
 * @date 17/7/24 下午8:28
 * @version
 */
public interface VeritasOracleInstanceDao extends BaseDao{
}
