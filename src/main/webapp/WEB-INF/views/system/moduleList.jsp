<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html lang="en">
<head>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<title>主页组件管理</title>
</head>
<body>
	<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
		<!--加载效果 start-->
		<div id="preloader">
			<div id="status">
				<i class="fa fa-spinner fa-spin"></i>
			</div>
		</div>
		<!--加载效果 end-->
		<section> <!--main start-->
		<div class="mainpanel_mian">
			<!-- main start -->
			<div class="contentpanel">
			<div class="row">
				<div class="col-sm-12">
					<!--list start-->
					<div class="panel panel-default">
						<div class="panel-heading zj-panel-heading-2">
							<h5 class="panel-title zj-panel-title-2">主页组件管理</h5>
						</div>
						<div class="panel-body zj-list-body">
							<div class="row">
								<div class="col-sm-12">
									<a class="btn btn-primary" href="javascript:void(0)"  onclick="add()"><i class="zj-icon zj-d-apply"></i>添加</a> 
									<a class="btn btn-primary" href="javascript:void(0)"  onclick="upd()"><i class="zj-icon zj-d-modify"></i>修改</a> 
									<a class="btn btn-primary  btn-lg" data-toggle="modal" data-target="#myModal"  onclick="del()"><i class="zj-icon zj-d-delete_all"></i>删除</a>
									<div class="table-responsive zj-table-responsive">
										<table class="table mb30 table-hover table-responsive" id="dg_b">

										</table>
									</div>
								</div>
							</div>

						</div>
					</div>
					<!--list end-->
				</div>
			</div>
		</div>
		</div>
	</section>
</body>
<script>
	var table;
	var url = '${ctx}/system/module/json';
	$(function() {
		table = $('#dg_b').bootstrapTable({
			method : "get",
			url : url,
			iconCls : 'icon',
			animate : true,
			fit : true,
			fitColumns : true,
			border : false,
			idField : 'id',
			uniqueId : 'id',
			pagination : true,
			checkbox : true,
			rownumbers : true,
			showRefresh : false,
	/* 		pageNumber : 1,
			pageSize : 10,
			sidePagination : 'server',
			pageList : [ 10, 20, 30, 40, 50 ], */
			singleSelect : true,
			/* striped : true, */
			search : false,
			clickToSelect : true,
			columns : [
			{
				field : 'checked',
				checkbox : true,
			}, {
				field : 'id',
				title : 'id',
			 	visible: false
			}, {
				field : 'title',
				title : '组件名称',
				 align: 'center',
				width : 100
			}, {
				field : 'code',
				title : '组件编码',
				 align: 'center',
				width : 100
			}, {
				field : 'sort',
				title : '排序',
				 align: 'center',
				width : 80
			}, {
				field : 'type',
				title : '类型',
				 align: 'center',
				width : 80 ,
				formatter : function(val, rec) {
					if (val == '1') {
						return '表格';
					} else if (val == '2') {
						return '列表';
					} else if (val == '3') {
						return '图表';
					} else if (val == '4') {
						return '按钮';
					} 
				} 
			} ],
			dataPlain : true
		});
	});

	//弹窗增加
	function add() {
		window.location.href = '${ctx}/system/module/create';
	}

	/*  toastr.options.positionClass = 'toast-bottom-right'; */

	//删除
	function del() {
		var row = table.bootstrapTable('getSelections');
		if (row.length == 0) {
			toastr.warning('请选择要处理的行。');
		} else {
			Ewin.confirm({
				message : "确认要删除选择的数据吗？"
			}).on(function(e) {
				if (!e) {
					return;
				}

				$.ajax({
					type : 'get',
					cache : false,
					url : "${ctx}/system/module/delete/" + row[0].id,
					success : function(data) {
						toastr.success('删除成功。');
						table.bootstrapTable('refresh', {
							url : url
						});
					}
				});
			});
		}
	}

	//弹窗修改
	function upd() {
		var row = table.bootstrapTable('getSelections');
		if (row.length == 0) {
			toastr.warning('请选择要处理的行。');
		} else {
			window.location.href = '${ctx}/system/module/update/' + row[0].id;
		}
	}
</script>
</html>