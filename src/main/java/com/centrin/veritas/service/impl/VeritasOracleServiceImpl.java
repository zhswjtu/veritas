package com.centrin.veritas.service.impl;
/**
 * Project Name：veritas
 * File Name：VeritasDefaultDbServiceImpl
 * Package Name：com.centrin.veritas.service.impl
 * Date：17/7/21 下午3:11
 */

import com.centrin.common.dao.BaseDao;
import com.centrin.common.dao.QueryFilter;
import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseServiceImpl;
import com.centrin.common.utils.StringUtils;
import com.centrin.veritas.dao.VeritasOracleDao;
import com.centrin.veritas.entity.VeritasDefaultDbEntity;
import com.centrin.veritas.entity.VeritasOracleDbInstanceParamEntity;
import com.centrin.veritas.entity.VeritasOracleEntity;
import com.centrin.veritas.service.VeritasOracleInstanceService;
import com.centrin.veritas.service.VeritasOracleService;
import com.centrin.veritas.utils.SystemUtils;
import com.centrin.veritas.utils.VeritasConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service.impl
 * @ClassName VeritasDefaultDbServiceImpl
 * @author 张辉
 * @date 17/7/21 下午3:11
 * @version
 */
@Service
public class VeritasOracleServiceImpl extends BaseServiceImpl<VeritasDefaultDbEntity> implements VeritasOracleService {

    @Autowired
    private VeritasOracleDao veritasOracleDao;

    @Autowired
    private VeritasOracleInstanceService veritasOracleInstanceService;

    private static final String RMANCAT = "[RMANCAT]";
    private static final String ORCL = "[ORCL]";
    private static final String PRD = "[prd]";
    private static final String GLOBAL = "[GLOBAL]";
    private static final String EMAIL = "[EMAIL]";
    private static final String POLICY = "[POLICY]";
    private static final String PFILE = "[PFILE]";
    private static final String INSTANCE = "[INSTANCE]";
    private static final String MYSQLDB = "[MYSQLDB]";

    /************************************************/

    private static final String DB_GLOBAL = "[GLOBAL]";
    private static final String DB_STORAGE = "[storage]";
    private static final String DB_DB2 = "[db2]";

    @Override
    protected BaseDao getSuperBaseDao() {
        return veritasOracleDao;
    }

    @Override
    public PagingEntity<VeritasDefaultDbEntity> findPageList(Map<String, String> param, PagingEntity<VeritasDefaultDbEntity> page) {
        QueryFilter qf = QueryFilter.newPagingFilter(page.getPageNum(), page.getSize());
        if (StringUtils.isNotBlank(param.get("head"))){
            qf.andEq("chapterHead", param.get("head"));
        }
        if (StringUtils.isNotBlank(param.get("type"))){
            qf.andEq("type", Integer.parseInt(param.get("type")));
        }
        String ip = SystemUtils.getHostIp(SystemUtils.getInetAddress());
        if (StringUtils.isNotBlank(ip)){
            qf.andEq("ip", ip);
        }
        return findAsPaging(VeritasDefaultDbEntity.class, qf);
    }

    @Override
    public List<VeritasDefaultDbEntity> findList(Map<String, String> param){
        QueryFilter qf = QueryFilter.newFilter();
        if (StringUtils.isNotBlank(param.get("head"))){
            qf.andEq("chapterHead", param.get("head"));
        }
        if (StringUtils.isNotBlank(param.get("type"))){
            qf.andEq("type", Integer.parseInt(param.get("type")));
        }
        String ip = SystemUtils.getHostIp(SystemUtils.getInetAddress());
        if (StringUtils.isNotBlank(ip)){
            qf.andEq("ip", ip);
        }
        return findAsList(VeritasDefaultDbEntity.class, qf);
    }

    @Override
    public List<VeritasDefaultDbEntity> findUsableList(Map<String, String> param) {
        List<VeritasOracleDbInstanceParamEntity> paramList = veritasOracleInstanceService.findParamList(Integer.parseInt(param.get("instanceId")));
        return veritasOracleDao.findUsableList(param, paramList);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 生成default文件
     * @date 17/7/22
     */
    @Override
    @Transactional
    public void buildDefault() throws Exception{
        Map<String, String> param = new HashMap<>();
        param.put("type", "0");
        param.put("head", GLOBAL);
        List<VeritasDefaultDbEntity> globalList = findList(param);
        param.put("head", EMAIL);
        List<VeritasDefaultDbEntity> emailList = findList(param);
        param.put("head", POLICY);
        List<VeritasDefaultDbEntity> policyList = findList(param);
        param.put("head", PFILE);
        List<VeritasDefaultDbEntity> pfileList = findList(param);
        param.put("head", INSTANCE);
        List<VeritasDefaultDbEntity> instanceList = findList(param);
        param.put("head", MYSQLDB);
        List<VeritasDefaultDbEntity> mysqlList = findList(param);
//        File file = new File(DEFAULT_PATH + File.separator + "default.conf");
        File file = new File(VeritasConstant.ORACLE_DEFAULT_PATH + File.separator + "default.conf");
        File fileParent = file.getParentFile();
        if (!fileParent.exists()){
            fileParent.mkdirs();
        }
        file.createNewFile();
        FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fileWriter);

        try {

            if (null != globalList && globalList.size() > 0) {
                writeFile(bw, globalList, GLOBAL);
            }
            if (null != emailList && emailList.size() > 0) {
                writeFile(bw, emailList, EMAIL);
            }
            if (null != policyList && policyList.size() > 0) {
                writeFile(bw, policyList, POLICY);
            }
            if (null != pfileList && pfileList.size() > 0) {
                writeFile(bw, pfileList, PFILE);
            }
            if (null != instanceList && instanceList.size() > 0) {
                writeFile(bw, instanceList, INSTANCE);
            }
            if (null != mysqlList && mysqlList.size() > 0) {
                writeFile(bw, mysqlList, MYSQLDB);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bw.close();
        }
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 生成DB2默认文件
     * @date 2017/8/30
     */
    @Override
    public void buildDbDefault() throws Exception {
        Map<String, String> param = new HashMap<>();
        param.put("type", "1");
        param.put("head", DB_GLOBAL);
        List<VeritasDefaultDbEntity> dbGlobalList = findList(param);
        param.put("head", DB_STORAGE);
        List<VeritasDefaultDbEntity> dbStorageList = findList(param);
        param.put("head", DB_DB2);
        List<VeritasDefaultDbEntity> dbDbList = findList(param);

        File file = new File(VeritasConstant.DB2_DEFAULT_PATH + File.separator + "default.conf");
        File fileParent = file.getParentFile();
        if (!fileParent.exists()){
            fileParent.mkdirs();
        }
        file.createNewFile();
        FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fileWriter);

        try {

            if (null != dbGlobalList && dbGlobalList.size() > 0) {
                writeFile(bw, dbGlobalList, DB_GLOBAL);
            }
            if (null != dbStorageList && dbStorageList.size() > 0) {
                writeFile(bw, dbStorageList, DB_STORAGE);
            }
            if (null != dbDbList && dbDbList.size() > 0) {
                writeFile(bw, dbDbList, DB_DB2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bw.close();
        }
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 创建DB配置文件
     * @date 17/7/23
     */
    @Override
    public void buildDb() throws Exception {
        Map<String, String> param = new HashMap<>();
        param.put("type", "1");
        //获取各个章节参数
        param.put("head", RMANCAT);
        List<VeritasDefaultDbEntity> rmancatList = findList(param);
        param.put("head", ORCL);
        List<VeritasDefaultDbEntity> orclList = findList(param);
        param.put("head", PRD);
        List<VeritasDefaultDbEntity> prdList = findList(param);
        param.put("head", GLOBAL);
        List<VeritasDefaultDbEntity> globalList = findList(param);
        param.put("head", EMAIL);
        List<VeritasDefaultDbEntity> emailList = findList(param);
        param.put("head", POLICY);
        List<VeritasDefaultDbEntity> policyList = findList(param);
        param.put("head", PFILE);
        List<VeritasDefaultDbEntity> pfileList = findList(param);
        param.put("head", INSTANCE);
        List<VeritasDefaultDbEntity> instanceList = findList(param);
        //获取文件
        File file = new File(VeritasConstant.ORACLE_DB_PATH + File.separator + "db.conf");
        File fileParent = file.getParentFile();
        if (!fileParent.exists()){
            fileParent.mkdirs();
        }
        file.createNewFile();
        FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fileWriter);

        try {
            if (null != rmancatList && rmancatList.size() > 0) {
                writeFile(bw, rmancatList, RMANCAT);
            }
            if (null != orclList && orclList.size() > 0) {
                writeFile(bw, orclList, ORCL);
            }
            if (null != prdList && prdList.size() > 0) {
                writeFile(bw, prdList, PRD);
            }
            if (null != globalList && globalList.size() > 0) {
                writeFile(bw, globalList, GLOBAL);
            }
            if (null != emailList && emailList.size() > 0) {
                writeFile(bw, emailList, EMAIL);
            }
            if (null != policyList && policyList.size() > 0) {
                writeFile(bw, policyList, POLICY);
            }
            if (null != pfileList && pfileList.size() > 0) {
                writeFile(bw, pfileList, PFILE);
            }
            if (null != instanceList && instanceList.size() > 0) {
                writeFile(bw, instanceList, INSTANCE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bw.close();
        }
    }

    @Override
    @Transactional
    public String paramBuild(Integer type) {
        try {
            String ip = SystemUtils.getHostIp(SystemUtils.getInetAddress());
            List<VeritasDefaultDbEntity> dbList = veritasOracleDao.findByIp(ip, type);
            if (null != dbList && dbList.size() > 0){
                return "202";
            }
            List<VeritasOracleEntity> listOracle = veritasOracleDao.findAll(type);
            for (VeritasOracleEntity veritasOracleEntity : listOracle) {
                VeritasDefaultDbEntity veritasDefaultDbEntity = new VeritasDefaultDbEntity(veritasOracleEntity);
                veritasDefaultDbEntity.setIp(ip);
                veritasOracleDao.add(veritasDefaultDbEntity);
            }
        } catch (Exception e) {
            return "201";
        }
        return "200";
    }

    public void writeFile(BufferedWriter bw, List<VeritasDefaultDbEntity> obj, String chapterName) throws Exception{
        bw.write(chapterName);
        bw.newLine();
        for (VeritasDefaultDbEntity veritasDefaultDbEntity : obj){
            if (null == veritasDefaultDbEntity.getDefaultValue()){
                veritasDefaultDbEntity.setDefaultValue("");
            }
            String instance = veritasDefaultDbEntity.getParameterName() + " = " + veritasDefaultDbEntity.getDefaultValue();
            bw.write(instance);
            bw.newLine();
        }
    }
}
