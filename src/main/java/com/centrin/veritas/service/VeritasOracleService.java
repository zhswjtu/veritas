package com.centrin.veritas.service;
/**
 * Project Name：veritas
 * File Name：VeritasOracleService
 * Package Name：com.centrin.veritas.service
 * Date：17/7/21 下午3:10
 */

import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseService;
import com.centrin.veritas.entity.VeritasDefaultDbEntity;

import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service
 * @ClassName VeritasOracleService
 * @author 张辉
 * @date 17/7/21 下午3:10
 * @version
 */
public interface VeritasOracleService extends BaseService<VeritasDefaultDbEntity>{

    PagingEntity<VeritasDefaultDbEntity> findPageList(Map<String, String> param, PagingEntity<VeritasDefaultDbEntity> page);

    List<VeritasDefaultDbEntity> findList(Map<String, String> param);

    List<VeritasDefaultDbEntity> findUsableList(Map<String, String> param);

    void buildDefault() throws Exception;

    /**
     * @param
     * @return
     * @author 张辉
     * @description 生成DB2默认文件
     * @date 2017/8/30
     */
    void buildDbDefault() throws Exception;

    void buildDb() throws Exception;

    String paramBuild(Integer type);
}
