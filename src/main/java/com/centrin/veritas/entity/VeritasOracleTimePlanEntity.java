package com.centrin.veritas.entity;
/**
 * Project Name：veritas
 * File Name：VeritasOracleTimePlanEntity
 * Package Name：com.centrin.veritas.entity
 * Date：17/7/26 下午4:45
 */

import com.centrin.common.entity.BaseEntity;

import javax.persistence.*;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.entity
 * @ClassName VeritasOracleTimePlanEntity
 * @author 张辉
 * @date 17/7/26 下午4:45
 * @version
 */
@Entity
@Table(name = "veritas_oracle_time_plan")
public class VeritasOracleTimePlanEntity extends BaseEntity{
    private static final long serialVersionUID = 1L;
    /**
     * 有效
     */
    public static Integer STATUS_YES = 0;
    /**
     * 无效
     */
    public static Integer STATUS_NO = 1;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "plan_name")
    private String planName;

    @Column(name = "create_time")
    private String createTime;

    @Column(name = "execute_time")
    private String executeTime;

    @Column(name = "instance_name")
    private String instanceName;

    private Integer status;

    private String remark;

    private String command;

    private String minute;

    private String hour;

    private String day;

    private String month;

    private String week;

    private String path;

    private String rid;

    private String ip;

    public VeritasOracleTimePlanEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(String executeTime) {
        this.executeTime = executeTime;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getMinute() {
        return minute;
    }

    public void setMinute(String minute) {
        this.minute = minute;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }
}
