package com.centrin.veritas.service.impl;
/**
 * Project Name：veritas
 * File Name：VeritasOraclePlanServiceImpl
 * Package Name：com.centrin.veritas.service.impl
 * Date：17/7/26 下午4:26
 */

import com.centrin.common.dao.BaseDao;
import com.centrin.common.dao.QueryFilter;
import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseServiceImpl;
import com.centrin.common.utils.DateUtils;
import com.centrin.common.utils.StringUtils;
import com.centrin.common.utils.UUID;
import com.centrin.veritas.dao.SnDao;
import com.centrin.veritas.dao.VeritasOracleInstanceDao;
import com.centrin.veritas.dao.VeritasOraclePlanDao;
import com.centrin.veritas.entity.SnEntity;
import com.centrin.veritas.entity.VeritasOracleDbInstanceEntity;
import com.centrin.veritas.entity.VeritasOracleTimePlanEntity;
import com.centrin.veritas.service.SnService;
import com.centrin.veritas.service.VeritasOraclePlanService;
import com.centrin.veritas.utils.VeritasConstant;
import net.redhogs.cronparser.CronExpressionDescriptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.text.ParseException;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service.impl
 * @ClassName VeritasOraclePlanServiceImpl
 * @author 张辉
 * @date 17/7/26 下午4:26
 * @version
 */
@Service
public class VeritasOraclePlanServiceImpl extends BaseServiceImpl implements VeritasOraclePlanService {

    @Autowired
    private VeritasOraclePlanDao veritasOraclePlanDao;

    @Autowired
    private SnService snService;

    @Autowired
    private SnDao snDao;

    @Autowired
    private VeritasOracleInstanceDao veritasOracleInstanceDao;

    @Override
    protected BaseDao getSuperBaseDao() {
        return veritasOraclePlanDao;
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 添加定时计划
     * @date 17/7/26
     */
    @Override
    @Transactional
    public void addPlan(VeritasOracleTimePlanEntity veritasOracleTimePlanEntity) {
        veritasOracleTimePlanEntity.setCreateTime(DateUtils.getDateTime());
        String path = VeritasConstant.RESTORE_ORACLE + " -sid " + veritasOracleTimePlanEntity.getInstanceName().replace("[", "").replace("]", "");
        String cron = veritasOracleTimePlanEntity.getMinute() + " " +
                veritasOracleTimePlanEntity.getHour() + " " +
                veritasOracleTimePlanEntity.getDay() + " " +
                veritasOracleTimePlanEntity.getMonth() + " " +
                veritasOracleTimePlanEntity.getWeek();
        String command = cron + " " + path;
        try {
            String cron_express = CronExpressionDescriptor.getDescription(cron);
            veritasOracleTimePlanEntity.setRemark(cron_express);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        veritasOracleTimePlanEntity.setCommand(command);
        veritasOracleTimePlanEntity.setPath(path);
        veritasOraclePlanDao.add(veritasOracleTimePlanEntity);
        //添加计划任务
        addCrontab(command);
    }

    @Override
    public PagingEntity<VeritasOracleTimePlanEntity> findPage(PagingEntity<VeritasOracleTimePlanEntity> page, Map<String, String> param) {
        QueryFilter qf = QueryFilter.newPagingFilter(page.getPageNum(), page.getSize());
        if (StringUtils.isNotBlank(param.get("planName"))){
            qf.andLk("planName", "%" + param.get("planName") + "%");
        }
        if (StringUtils.isNotBlank(param.get("instanceName"))){
            qf.andLk("instanceName", "%" + param.get("instanceName") + "%");
        }
        if (StringUtils.isNotBlank(param.get("status"))){
            qf.andLk("status", Integer.parseInt(param.get("status")));
        }
        qf.orderBy("id", QueryFilter.Order.DESC);
        return findAsPaging(VeritasOracleTimePlanEntity.class, qf);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 更新计划任务
     * @date 17/8/2
     */
    @Override
    @Transactional
    public void updatePlan(VeritasOracleTimePlanEntity veritasOracleTimePlanEntity) {
        VeritasOracleTimePlanEntity old = veritasOraclePlanDao.findById(VeritasOracleTimePlanEntity.class, veritasOracleTimePlanEntity.getId());
        //删除原有计划任务
        deleteCrontab(old.getPath());
        //更新命令
        String path = VeritasConstant.RESTORE_ORACLE + " -sid " + veritasOracleTimePlanEntity.getInstanceName().replace("[", "").replace("]", "");
        String cron = veritasOracleTimePlanEntity.getMinute() + " " +
                veritasOracleTimePlanEntity.getHour() + " " +
                veritasOracleTimePlanEntity.getDay() + " " +
                veritasOracleTimePlanEntity.getMonth() + " " +
                veritasOracleTimePlanEntity.getWeek();
        String command = cron + " " + path;
        try {
            String cron_express = CronExpressionDescriptor.getDescription(cron);
            veritasOracleTimePlanEntity.setRemark(cron_express);
        } catch (ParseException e) {
            veritasOracleTimePlanEntity.setRemark("表达式错误");
        }
        veritasOracleTimePlanEntity.setPath(path);
        veritasOracleTimePlanEntity.setCommand(command);
        veritasOraclePlanDao.update(veritasOracleTimePlanEntity);
        //添加计划任务
        addCrontab(command);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 删除计划任务
     * @date 17/8/2
     */
    @Override
    @Transactional
    public void deletePlan(Integer id) {
        VeritasOracleTimePlanEntity veritasOracleTimePlanEntity = veritasOraclePlanDao.findById(VeritasOracleTimePlanEntity.class, id);
        deleteCrontab(veritasOracleTimePlanEntity.getPath());
        veritasOraclePlanDao.delete(VeritasOracleTimePlanEntity.class, id);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 立即执行
     * @date 17/8/3
     */
    @Override
    @Transactional
    public void execImmediately(Integer id) {
        VeritasOracleDbInstanceEntity veritasOracleDbInstanceEntity = veritasOracleInstanceDao.findById(VeritasOracleDbInstanceEntity.class, id);
        SnEntity snEntity = snDao.findNew();
        veritasOracleDbInstanceEntity.setRid(veritasOracleDbInstanceEntity.getChapterHead().replace("[", "").replace("]", "") + "_" + snEntity.getSn());
        veritasOracleInstanceDao.update(veritasOracleDbInstanceEntity);
        try {
            String command = VeritasConstant.RESTORE_ORACLE + " -sid " + veritasOracleDbInstanceEntity.getChapterHead().replace("[", "").replace("]", "") + " -rid " + snEntity.getSn();
            Process process = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", command});
//            process.waitFor();
            System.out.println(command);
            System.out.println(process.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 添加计划任务
     * @date 17/8/2
     */
    private void addCrontab(String command){
        try {
//            Process process = Runtime.getRuntime().exec("echo \"" + command + "\" >> /var/spool/cron/root");
            Process process = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "echo \"" + command + "\" >> /var/spool/cron/root"});
//            process.waitFor();
            System.out.println("echo \"" + command + "\" >> /var/spool/cron/root");
            System.out.println(process.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 删除指定Crontab任务
     * @date 17/8/2
     */
    private void deleteCrontab(String command){
        try {
            command = command.replace("/", "\\/");
            Process process = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "sed -i \"/" + command + "/d\" /var/spool/cron/root"});
//            process.waitFor();
            System.out.println("sed -i \"/" + command + "/d\" /var/spool/cron/root");
            System.out.println(process.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
