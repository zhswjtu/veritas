package com.centrin.veritas.entity;
/**
 * Project Name：veritas
 * File Name：RestoreStateInfoEntity
 * Package Name：com.centrin.veritas.entity
 * Date：17/8/8 下午11:17
 */

import com.centrin.common.entity.BaseEntity;

import javax.persistence.*;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.entity
 * @ClassName RestoreStateInfoEntity
 * @author 张辉
 * @date 17/8/8 下午11:17
 * @version
 */
@Entity
@Table(name = "restore_state_info")
public class RestoreStateInfoEntity extends BaseEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "info_id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer infoId;

    private String rid;

    @Column(name = "phase_number")
    private String phaseNumber;

    @Column(name = "activity_status")
    private String activityStatus;

    private String messages;

    @Column(name = "activity_name")
    private String activityName;

    @Column(name = "activity_time")
    private String activityTime;

    public RestoreStateInfoEntity() {
    }

    public Integer getInfoId() {
        return infoId;
    }

    public void setInfoId(Integer infoId) {
        this.infoId = infoId;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getPhaseNumber() {
        return phaseNumber;
    }

    public void setPhaseNumber(String phaseNumber) {
        this.phaseNumber = phaseNumber;
    }

    public String getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(String activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getActivityTime() {
        return activityTime;
    }

    public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }
}
