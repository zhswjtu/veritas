<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html lang="en">
<head>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<title>角色管理</title>
</head>
<body>
	<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
	<link rel="stylesheet" href="${ctx}/resource-Main/userList/userList_new.css">
<!--加载效果 start-->
<div id="preloader">
    <div id="status">
        <i class="fa fa-spinner fa-spin"></i>
    </div>
</div>
<!--加载效果 end-->
<section>
    <!--main start-->
    <div class="mainpanel_mian">
        <!-- main start -->
        <div class="contentpanel">
            <div class="row" style="background-color: #fff;">
                <div class="col-sm-12">
                    <!--list start-->
                    <div class="panel panel-default">
                        <div class="panel-heading zj-panel-heading-2 clearfix" style="padding: 10px 0;">
                            <h5 class="panel-title zj-panel-title-2" style="float: left; height: 30px; line-height: 30px;">添加角色</h5>
                            <button type="button" class="btn btn-primary" style="float: right; height: 30px; width: 70px;" onclick="window.history.go(-1);"><i class="glyphicon glyphicon-backward" style="margin-right: 7px"></i>返回
                            </button>
                        </div>
                    </div>
                    <form id="mainform" action="${ctx}/system/role/${action}" method="post">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="padding-right: 1.7em">角色名：</label>
                            <div class="col-sm-6">
                                <input type="hidden" name="id" value="${role.id}" />
                                <input class="form-control" type="text" value="${role.name}" id="name" name="name" data-options="required:'required'" validType="checkByUrl['${ctx}/system/role/check?id=${role.id}&name=']" maxlength="32" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">角色编码：</label>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" id="code" name="code" value="${role.code}" data-options="required:'required'" validType="checkByUrl['${ctx}/system/role/check?id=${role.id}&code=']" maxlength="32" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="padding-right: 2.7em">排序：</label>
                            <div class="col-sm-6">
                                <input class="form-control" type="text"  id="sort" name="sort" value="${role.sort}">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 20px">
                            <label class="col-sm-3 control-label" style="padding-right: 2.7em" name="description">${role.description}</label>
                            <div class="col-sm-8">
                                <textarea class="form-control area-orgList" type="text" style="height: 100px;"></textarea>
                            </div>
                        </div>
                        <div class="sub_btn">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6" style="text-align: center">
                                <input type="submit" class="btn btn-primary sub1" value="确定" onclick="save()">
                                <input type="submit" class="btn btn-primary sub2" value="取消" onclick="window.history.go(-1);">
                            </div>
                            <div class="col-sm-3"></div>
                        </div>
                    </form>
                    <!--list end-->
                </div>
            </div>
        </div>
    </div>
</section>				
</body>
</html>
</div>
</section>
<script type="text/javascript">
	var action = "${action}";
	if (action == 'update') {
		$('#code').attr("readonly", true);
		$("#code").css("background", "#eee");
	}
	/* 点击确定按钮提交 */
	function save() {
		$('#mainform').bootstrapValidator('validate');
	}
	//提交表单
	$(document).ready(function() {

		$('#mainform').bootstrapValidator({
			container : 'tooltip',
			/* trigger: 'blur', */
			feedbackIcons : {
				valid : 'glyphicon glyphicon-ok',
				invalid : 'glyphicon glyphicon-remove',
				validating : 'glyphicon glyphicon-refresh'
			},
			fields : {
				name : {
					validators : {
						notEmpty : {
							message : '角色名不能为空'
						}
					}
				},
				code : {
					validators : {
						notEmpty : {
							message : '角色编码不能为空'
						}
					}
				},
				sort : {
					validators : {
						notEmpty : {
							message : '排序号不能为空'
						}
					}
				}
			}
		}).on('success.form.bv', function(e) {
			var $form = $(e.target);
			$form.ajaxSubmit(function(data) {
				if (data == 'success') {
					location.href = "${ctx}/system/role";
				} else {

				}
			});
		});
	});
</script>
</body>
</html>