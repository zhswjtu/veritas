package com.centrin.system.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.centrin.system.entity.OrgEntity;
import com.centrin.system.entity.UserEntity;
import com.centrin.system.interceptor.WriteLog;
import com.centrin.system.service.OrgService;

@Controller
@RequestMapping(value = "/system/org")
public class OrgController {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    protected OrgService orgService;
    
	
	/**
	 * 默认页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String list(){
		return "system/orgList";
	}

	/**
	 * @Title: add
	 * @Description:新增跳转
	 * @param model
	 * @return
	 */
	@WriteLog("部门管理/添加部门")
	@RequiresPermissions("sys:org:create")
	@RequestMapping(value = "create", method = RequestMethod.GET)
    public String add(Model model) {
    	log.info("新建部门");
        model.addAttribute("org", new OrgEntity());
        model.addAttribute("action", "create");
        return "/system/orgForm";
    }
	
	/**
	 * @Title: upd
	 * @Description:修改跳转
	 * @param model
	 * @return
	 */
	@WriteLog("部门管理/修改部门")
	@RequiresPermissions("sys:org:update")
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String upd(@PathVariable("id") Integer id, Model model) {
		log.info("修改部门、"+"部门id：" + id);
		model.addAttribute("org", orgService.getId(id));
		model.addAttribute("action", "update");
		return "/system/orgForm";
	}
	
	
	
	/**
	 * 部门集合(JSON)
	 */
	@RequiresPermissions("sys:org:view")
	@RequestMapping("operation/json")
	@ResponseBody
	public List<OrgEntity> getAllOrgs(){
		List<OrgEntity> orgList=orgService.getAllOrgs();
		return orgList;
	}
	
	/**
     * 验证部门名称是否存在
     * @param request
     * @return
     */
    @RequestMapping(value = "/checkName")
    @ResponseBody
    public String checkName(HttpServletRequest request){
    	String orgName = request.getParameter("name");
		OrgEntity name =orgService.getOrgByOrgName(orgName);
        String flag = "";
        if (null == name){
            flag = "true";
        } else {
            flag = "false";
        }
        return flag;
    }
	
    
    /**
     * 验证部门编码是否存在
     * @param request
     * @return
     */
    @RequestMapping(value = "/checkCode")
    @ResponseBody
    public String checkCode(HttpServletRequest request){
    	String orgCode = request.getParameter("code");
    	OrgEntity code =orgService.getOrgByOrgCode(orgCode);
    	String flag = "";
    	if (null == code){
    		flag = "true";
    	} else {
    		flag = "false";
    	}
    	return flag;
    }
    
	/**
	 * 添加部门
	 */

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public String add(OrgEntity org, Model model) {
		String orgName = org.getName();
		String orgCode = org.getCode();
		OrgEntity name =orgService.getOrgByOrgName(orgName);
		OrgEntity code =orgService.getOrgByOrgCode(orgCode);
		boolean nameExist = false;
		boolean codeExist = false;
		if(name != null && org.getId() != name.getId()){
			nameExist =true;
		}
		if(code != null && org.getId() != code.getId()){
			codeExist =true;
		}
		if(nameExist){
			return "部门名称已存在！";
		}else if(codeExist){
			return "部门编码已存在！";
		}
		orgService.addOrg(org);
		return "success";
	}
	/**
	 * 修改部门
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public String upd(OrgEntity org, Model model) {
		log.info("upd:" + org.toString());
		String orgName = org.getName();
		String orgCode = org.getCode();
		OrgEntity name =orgService.getOrgByOrgName(orgName);
		OrgEntity code =orgService.getOrgByOrgCode(orgCode);
		boolean nameExist = false;
		boolean codeExist = false;
		if(name != null && org.getId() != name.getId()){
			nameExist =true;
		}
		if(code != null && org.getId() != code.getId()){
			codeExist =true;
		}
		if(nameExist){
			return "部门名称已存在！";
		}else if(codeExist){
			return "部门编码已存在！";
		}
		orgService.updateOrg(org);
		return "success";
	}
	/**
	 * 删除部门
	 */
	@WriteLog("部门管理/删除部门")
	@RequiresPermissions("sys:org:delete")
	@RequestMapping(value = "delete/{id}")
	@ResponseBody
	public String del(@PathVariable("id") Integer id) {
		log.info("id:" + id);
		orgService.deleteOrg(id);
		return "success";
	}

}
