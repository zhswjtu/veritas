package com.centrin.veritas.service;
/**
 * Project Name：veritas
 * File Name：VeritasOracleInstanceService
 * Package Name：com.centrin.veritas.service
 * Date：17/7/24 下午8:29
 */

import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseService;
import com.centrin.veritas.entity.VeritasOracleDbInstanceEntity;
import com.centrin.veritas.entity.VeritasOracleDbInstanceParamEntity;

import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service
 * @ClassName VeritasOracleInstanceService
 * @author 张辉
 * @date 17/7/24 下午8:29
 * @version
 */
public interface VeritasOracleInstanceService extends BaseService {

    PagingEntity<VeritasOracleDbInstanceEntity> findPage(PagingEntity<VeritasOracleDbInstanceEntity> page, Map<String, String> param);

    List<VeritasOracleDbInstanceParamEntity> findParamList(Integer instanceId);

    void addParam(VeritasOracleDbInstanceEntity veritasOracleDbInstanceEntity, Integer instanceId);

    void instanceBuild();

    void instanceDelete(Integer id);

    List<VeritasOracleDbInstanceEntity> findList();
}
