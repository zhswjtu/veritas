
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html lang="en">
<head>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<title>用户管理</title>
</head>
<body>
	<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
<link rel="stylesheet" href="${ctx}/resource-Main/userList/userList_new.css">
<!--加载效果 start-->
<div id="preloader">
    <div id="status">
        <i class="fa fa-spinner fa-spin"></i>
    </div>
</div>
<!--加载效果 end-->
<section>
    <!--main start-->
    <div class="mainpanel_mian">
        <!-- main start -->
        <div class="contentpanel">
            <div class="row" style="background-color: #fff;">
                <div class="col-sm-12">
                    <!--list start-->
                    <div class="panel panel-default">
                        <div class="panel-heading zj-panel-heading-2 clearfix" style="padding: 10px 0;">
                            <h5 class="panel-title zj-panel-title-2" style="float: left; height: 30px; line-height: 30px;">添加菜单</h5>
                            <button type="button" class="btn btn-primary" style="float: right; height: 30px; width: 70px;" onclick="window.history.go(-1);">
                                <i class="glyphicon glyphicon-backward" style="margin-right: 7px"></i>返回
                            </button>
                        </div>
                    </div>
                    <form class="form-horizontal" role="form" id="mainform" action="${ctx}/system/permission/${action}" method="post">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">菜单名称：</label>
                            <div class="col-sm-6">
                                <input type="hidden" name="id" value="${permission.id}" />
                                <input type="hidden" name="type" value="1" />
                                <input class="form-control" type="text" id="name" name="name" value="${permission.name}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">菜单路径：</label>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" input id="url" name="url" value="${permission.url}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">菜单图标：</label>
                            <div class="col-sm-6">
                                <select name="" class="select-o" id="icon" name="icon" data-options="width: 180, autoShowPanel: false, multiple: false, size: '16', value: '${permission.icon}'"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">上级菜单：</label>
                            <div class="col-sm-6">
                                <input id="pid" value="${pName}" class="form-control" readonly="readonly"/>
								<input id="pid" name="pid" type="hidden" value="${permission.pid}" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="padding-right: 2.7em">状态：</label>
                            <div class="col-sm-6">
                                <select id="status1" name="status" class="select-o">
                                    	<option value="1" <c:if test="${'1' eq permission.status}">selected</c:if>>启用</option>
										<option value="0" <c:if test="${'0' eq permission.status}">selected</c:if>>禁用</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="padding-right: 2.7em">排序：</label>
                            <div class="col-sm-6">
                                <input class="form-control" id="sort" type="text" name="sort" value="${permission.sort}">
                            </div>
                        </div>
                        <div class="form-group" style="margin-bottom: 20px">
                            <label class="col-sm-3 control-label" style="padding-right: 2.7em">描述：</label>
                            <div class="col-sm-8">
                                <textarea class="form-control area-orgList" type="text" style="height: 100px;" name="description">${permission.description}</textarea>
                            </div>
                        </div>
                        <div class="sub_btn">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-6" style="text-align: center">
                                <input type="button" id="confirm" class="btn btn-primary sub1" value="确定" onclick="save()">
                                <input type="button" id="close" class="btn btn-primary sub2" onclick="window.history.go(-1);" value="取消">
                            </div>
                            <div class="col-sm-3"></div>
                        </div>
                    </form>
                    <!--list end-->
                </div>
            </div>
        </div>
    </div>
</section>
	<script type="text/javascript">
	$(document).ready(function() {
	    $('#mainform').bootstrapValidator({
	        container: 'tooltip',
//	        trigger: 'blur',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	        	name: {group: '.group',validators: {notEmpty: {}}}
	        }
	    }).on('success.form.bv', function(e) {
	    	var $form = $(e.target);
	    	$form.ajaxSubmit(function(data){
	   		 if(data=='success'){
	   			window.location.href="${ctx}/system/permission/menu";
	   			toastr.success(data);
	   		 }
	   	}); 
	    });
	});
	function save(){
		$('#mainform').bootstrapValidator('validate');
	}
		
		</script>
</body>
</html>