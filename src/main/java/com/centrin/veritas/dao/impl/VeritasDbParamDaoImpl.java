package com.centrin.veritas.dao.impl;
/**
 * Project Name：veritas
 * File Name：VeritasDbParamDaoImpl
 * Package Name：com.centrin.veritas.dao.impl
 * Date：2017/8/30 下午5:16
 */

import com.centrin.common.dao.BaseDaoImpl;
import com.centrin.veritas.dao.VeritasDbParamDao;
import org.springframework.stereotype.Repository;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao.impl
 * @ClassName VeritasDbParamDaoImpl
 * @author 张辉
 * @date 2017/8/30 下午5:16
 * @version
 */
@Repository
public class VeritasDbParamDaoImpl extends BaseDaoImpl implements VeritasDbParamDao {
}
