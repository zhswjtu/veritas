package com.centrin.veritas.dao;
/**
 * Project Name：veritas
 * File Name：VeritasDbParamDao
 * Package Name：com.centrin.veritas.dao
 * Date：2017/8/30 下午5:16
 */

import com.centrin.common.dao.BaseDao;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao
 * @ClassName VeritasDbParamDao
 * @author 张辉
 * @date 2017/8/30 下午5:16
 * @version
 */
public interface VeritasDbParamDao extends BaseDao{
}
