<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <title>首页</title>
    <link href="${ctx}/resource-Main/main/css/style.default.css" rel="stylesheet">
    <link href="${ctx}/resource-Main/main/css/demo.css" rel="stylesheet">
    <style>
        /*.backColor{*/
            /*background-color: aliceblue;*/
            /*margin-top: 1px;*/
        /*}*/
    </style>
</head>

<body>
    <!--加载效果 start-->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>
    <!--加载效果 end-->
    <section> <!-- leftpanel start -->
        <div class="leftpanel">
            <div class="logopanel">
                <h1>
                    <span>[</span> VERITAS <span>]</span>
                </h1>
            </div>
            <div id="sidebar-menu" class="scrollable-content active" tabindex="5000"
                 style="overflow: hidden; outline: none;">
                <h5 class="sidebartitle"></h5>
                <ul class="one_ul nav nav-bracket">
                    <li class="current-page">
                        <a href="javascript:;" title="总览"><i class="fa fa-home"></i> <span>总览</span></a>
                    </li>
                    <li class="active">
                        <a href="javascript:;" title="Oracle"><i class="glyphicon glyphicon-th-list"></i> <span>Oracle</span></a>
                        <ul class="second_ul" id="ss" style="display: block;">
                            <li><a href="javascript:;" style="color:#1CAF9A;" onclick="menuClick('${ctx}/oracle/default/', this)" title="默认配置"><i class="fa fa-caret-right"></i>默认配置</a></li>
                            <li><a href="javascript:;" onclick="menuClick('${ctx}/oracle/instance', this)" title="DB配置"><i class="fa fa-caret-right"></i>DB配置</a></li>
                            <li><a href="javascript:;" onclick="menuClick('${ctx}/oracle/plan', this)" title="执行计划"><i class="fa fa-caret-right"></i>执行计划</a></li>
                            <li><a href="javascript:;" onclick="menuClick('${ctx}/oracle/restore', this)" title="报表管理"><i class="fa fa-caret-right"></i>执行报表</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" title="DB2"><i class="glyphicon glyphicon-th-list"></i> <span>DB2</span></a>
                        <ul class="second_ul">
                            <li><a href="javascript:;" onclick="menuClick('${ctx}/db2/default/', this)" title="默认配置"><i class="fa fa-caret-right"></i>默认配置</a></li>
                            <li><a href="javascript:;" onclick="menuClick('${ctx}/db2/instance', this)" title="DB配置"><i class="fa fa-caret-right"></i>DB配置</a></li>
                            <li><a href="javascript:;" onclick="menuClick('${ctx}/db2/plan', this)" title="执行计划"><i class="fa fa-caret-right"></i>执行计划</a></li>
                            <li><a href="javascript:;" onclick="menuClick('${ctx}/oracle/restore', this)" title="报表管理"><i class="fa fa-caret-right"></i>执行报表</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- leftpanel end -->
        <div class="mainpanel">
            <!-- header start -->
            <div class="headerbar">
                <%--<a class="menutoggle"><i class="fa fa-bars"></i></a>--%>

                <div class="header-right">
                    <ul class="headermenu">
                        <li>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <img src="${ctx}/resource-Main/main/images/photos/loggeduser.png" alt=""/>${user.name}<span
                                        class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                                    <li><a href="${ctx}/logout"><i class="glyphicon glyphicon-log-out"></i>退出</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- header-right -->

            </div>
            <!-- header end -->
            <!-- main start -->
            <div class="contentpanel">
                <div class="row">
                    <iframe id="iframe-page-content" src="${ctx}/oracle/default/" width="100%" frameborder="no" border="0" marginwidth="0"
                            marginheight="0" scrolling="yes" allowtransparency="yes"></iframe>
                </div>
            </div>
            <!-- main end -->
        </div>
    </section>

    <script type="text/javascript">
        $(function () {
            var height = document.documentElement.clientHeight;
            document.getElementById('iframe-page-content').style.height = height + 'px';
        });

        var menuClick = function (menuUrl, a) {
//            $('.second_ul li a').each(function (index) {
//                $(this).removeClass('backColor');
//            });
//            $(a).addClass('backColor');
            $('.second_ul li a').css("cssText","color:#69737A;");
            $(a).css("cssText","color:#1CAF9A;");
            $("#iframe-page-content").attr('src', menuUrl);
        };
    </script>
</body>

</html>