
package com.centrin.system.error;


public class SysException extends RuntimeException {

	private static final long serialVersionUID = 2748979091222994521L;
	private String errMsg;
	
	public SysException(String errMsg) {
		super(errMsg);
		this.errMsg = errMsg;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
}
