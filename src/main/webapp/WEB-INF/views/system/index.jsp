<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>中华金融云运营平台</title>
<%@ include file="/common/easyui.jsp"%>
<script type="text/javascript"
	src="${ctx}/static/plugins/artTemplate/dist/template.js"></script>

<!--导入首页启动时需要的相应资源文件(首页相应功能的 js 库、css样式以及渲染首页界面的 js 文件)-->
<script src="${ctx}/static/plugins/easyui/common/index.js"
	type="text/javascript"></script>
<link href="${ctx}/static/plugins/easyui/common/index.css"
	rel="stylesheet" />
<script src="${ctx}/static/plugins/easyui/common/index-startup.js"></script>

<!-- 引入图形组件 -->
<script src="${ctx}/static/plugins/echarts/echarts.min.js"></script>
<!-- 引入 vintage 主题 -->
<script src="${ctx}/static/plugins/echarts/theme/macarons.js"></script>
<script src="${ctx}/static/plugins/echarts/theme/shine.js"></script>
<script src="${ctx}/static/plugins/echarts/theme/vintage.js"></script>
<script src="${ctx}/static/plugins/echarts/theme/roma.js"></script>
<script src="${ctx}/static/plugins/echarts/theme/infographic.js"></script>



<style type="text/css">
.chart_box_left {
	margin: 10px 0 10px 20px;
	padding: 10px;
	width: 500px;
	height: 320px;
	float: left;
	border: solid 1px #A3B5B5;
}

.chart_box_right {
	margin: 10px 0 10px 30px;
	padding: 10px;
	width: 500px;
	height: 320px;
	float: left;
	border: solid 1px #A3B5B5;
}
</style>
</head>
<body>
	<!-- 容器遮罩 -->
	<div id="maskContainer">
		<div class="datagrid-mask" style="display: block;"></div>
		<div class="datagrid-mask-msg"
			style="display: block; left: 50%; margin-left: -52.5px;">
			正在加载...</div>
	</div>
	<div id="mainLayout" class="easyui-layout hidden"
		data-options="fit: true">
		<div id="northPanel" data-options="region: 'north', border: false"
			style="height: 70px; overflow: hidden;">
			<div id="topbar" class="top-bar">
				<div class="top-bar-left">
					<h1 style="margin-left: 10px; margin-top: 8px; color: #fff">
						<span style="color: #fff; font-size: 19px">中华金融云运营平台</span>
					</h1>
				</div>
				<div class="top-bar-right">
					<div id="timerSpan"></div>
					<div id="themeSpan">
						<a id="btnHideNorth" class="easyui-linkbutton"
							data-options="plain: true, iconCls: 'layout-button-up'"></a>
					</div>
				</div>
			</div>
			<div id="toolbar"
				class="panel-header panel-header-noborder top-toolbar">
				<div id="infobar">
					<span class="icon-hamburg-user"
						style="padding-left: 25px; background-position: left center;">
						<shiro:principal property="name" />，您好，您有待办任务<a
						href="javascript:void(0)" id="toOrderCenter">&nbsp;<span
							style="color: red;" id="todoProc">0</span>&nbsp;
					</a>件
					</span>
				</div>

				<div id="buttonbar">
					<span>更换皮肤：</span> <select id="themeSelector"></select> <a
						href="javascript:void(0);" class="easyui-menubutton"
						data-options="menu:'#layout_north_set'"
						iconCls="icon-standard-cog">系统</a>
					<div id="layout_north_set">
						<div id="btnFullScreen" data-options="iconCls:'key'">全屏切换</div>
						<div id="btnUpdatePassword" data-options="iconCls:'key'">修改密码</div>
						<div id="btnExit" data-options="iconCls:'logout'">退出系统</div>
					</div>
					<a id="btnShowNorth" class="easyui-linkbutton"
						data-options="plain: true, iconCls: 'layout-button-down'"
						style="display: none;"></a>
				</div>
			</div>
		</div>

		<div
			data-options="region: 'west', title: '菜单导航栏', iconCls: 'icon-standard-map', split: true, minWidth: 200, maxWidth: 400"
			style="width: 220px; padding: 1px;">
			<div id="myMenu" class="easyui-accordion"
				data-options="fit:true,border:false">
				<script id="menu" type="text/html">
			{{each data as p_permission}}
				{{if (p_permission.pid==null)}}
   				 <div title="{{p_permission.name}}" style="padding: 5px;" data-options="border:false,iconCls:'{{p_permission.icon }}'">
					<div>
					{{each data as c_permission}}	
						{{if (c_permission.pid==p_permission.id)}}
						<a id="btn" class="easyui-linkbutton" data-options="plain:true,iconCls:'{{c_permission.icon }}'" style="width:98%;margin-bottom:5px;" onclick="window.mainpage.mainTabs.addModule('{{c_permission.name}}','{{c_permission.url}}','{{c_permission.icon}}')">{{c_permission.name}}</a>
						{{/if}}	
					{{/each}}
					</div>
				</div>
				{{/if}}	
			{{/each}}
			</script>
			</div>
		</div>

		<div data-options="region: 'center'">
			<div id="mainTabs_tools" class="tabs-tool">
				<table>
					<tr>
						<td><a id="mainTabs_jumpHome"
							class="easyui-linkbutton easyui-tooltip" title="跳转至主页选项卡"
							data-options="plain: true, iconCls: 'icon-hamburg-home'"></a></td>
						<td><div class="datagrid-btn-separator"></div></td>
						<td><a id="mainTabs_toggleAll"
							class="easyui-linkbutton easyui-tooltip" title="展开/折叠面板使选项卡最大化"
							data-options="plain: true, iconCls: 'icon-standard-arrow-out'"></a></td>
						<td><div class="datagrid-btn-separator"></div></td>
						<td><a id="mainTabs_refTab"
							class="easyui-linkbutton easyui-tooltip" title="刷新当前选中的选项卡"
							data-options="plain: true, iconCls: 'icon-standard-arrow-refresh'"></a></td>
						<td><div class="datagrid-btn-separator"></div></td>
						<td><a id="mainTabs_closeTab"
							class="easyui-linkbutton easyui-tooltip" title="关闭当前选中的选项卡"
							data-options="plain: true, iconCls: 'icon-standard-application-form-delete'"></a></td>
					</tr>
				</table>
			</div>
			<div id="mainTabs" class="easyui-tabs"
				data-options="fit: true, border: false, showOption: true, enableNewTabMenu: true, tools: '#mainTabs_tools', enableJumpTabMenu: true">
				<div id="homePanel"
					data-options="title: '主页', iconCls: 'icon-hamburg-home',refreshable:false">
					<div class="easyui-layout" data-options="fit: true">
						<div
							data-options="region: 'center', split: false, border: false,fit: true"
							style="height: auto; background-color: #ffffff;">
							
	                           	<div id="main" style="font-size:30px;text-align:center;margin-top:160px;color:#95B8E7;"><b>欢迎使用中华金融云运营平台</b></div>
	                        	<div style="text-align:center;font-size:13px;color:#b9b9b9;margin-top:200px;">&copy; 2016 北京中金云网科技有限公司 版权所有</div>
                        
							<!-- <div>
								<div style="text-align: center; font-size: 20px;">
									<p>平台客户统计</p>
									总客户数：<span id="customCount"></span>
								</div>
								<div style="width: 100%; height: 360px;">
									<div class="chart_box_left">
										<div id="chart1" style="width: 100%; height: 100%;"></div>
									</div>
									<div class="chart_box_right">
										<div id="chart2" style="width: 100%; height: 100%;"></div>
									</div>
								</div>
							</div>
							<div>
								<div style="text-align: center; font-size: 20px;">
									<p>平台服务统计</p>
									主机数量：<span id="hosts"></span>台&nbsp;&nbsp;&nbsp;&nbsp;存储容量数：<span id="stores"></span>G&nbsp;&nbsp;&nbsp;&nbsp;
									带宽：<span id="nets"></span>M
								</div>
								<div style="width: 100%; height: 360px;">
									<div class="chart_box_left">
										<div id="chart3" style="width: 100%; height: 100%;"></div>
									</div>
									<div class="chart_box_right">
										<div id="chart4" style="width: 100%; height: 100%;"></div>
									</div>
								</div>
							</div>
							<div>
								<div style="text-align: center; font-size: 20px;">
									<p>平台收入统计</p>
									上月应收：<span id="standardcharge"></span>元&nbsp;&nbsp;&nbsp;&nbsp;上月实收：<span
										id="actualcharge"></span>元
								</div>
								<div style="width: 100%; height: 360px;">
									<div class="chart_box_left">
										<div id="chart5" style="width: 100%; height: 100%;"></div>
									</div>
									<div class="chart_box_right">
										<div id="chart6" style="width: 100%; height: 100%;"></div>
									</div>
								</div>
							</div>
							<div>
								<div style="text-align: center; font-size: 20px;">
									<p>平台工单统计</p>
									本月完成工单：<span id="ordercount"></span>
								</div>
								<div style="width: 100%; height: 360px;">
									<div class="chart_box_left">
										<div id="chart7" style="width: 100%; height: 100%;"></div>
									</div>
									<div class="chart_box_right">
										<div id="chart8" style="width: 100%; height: 100%;"></div>
									</div>
								</div>
							</div>
						</div> -->
						<div data-options="region: 'center', border: false"
							style="overflow: hidden;"></div>
					</div>
				</div>
			</div>
		</div>

		<div
			data-options="region: 'east', title: '日历', iconCls: 'icon-standard-date', split: true,collapsed: true, minWidth: 160, maxWidth: 500"
			style="width: 220px;">
			<div id="eastLayout" class="easyui-layout" data-options="fit: true">
				<div data-options="region: 'north', split: false, border: false"
					style="height: 220px;">
					<div class="easyui-calendar"
						data-options="fit: true, border: false"></div>
				</div>
				<div id="linkPanel"
					data-options="region: 'center', border: false, title: '通知', iconCls: 'icon-hamburg-link', tools: [{ iconCls: 'icon-hamburg-refresh', handler: function () { window.link.reload(); } }]">

				</div>
			</div>
		</div>

		<div
			data-options="region: 'south', title: '关于...', iconCls: 'icon-standard-information', collapsed: true, border: false"
			style="height: 70px;">
			<div
				style="color: #4e5766; padding: 6px 0px 0px 0px; margin: 0px auto; text-align: center; font-size: 12px; font-family: 微软雅黑;">

			</div>
		</div>

	</div>

	<div class="easyui-dialog" id="form_dlg"
		style="width: 320px; height: 210px; padding: 10px 20px" closed="true"
		buttons="#form_dlg_btns" modal="true">
		<form id="updatePasswordForm" method="post" novalidate>
			<table class="myform-table">
				<tr>
					<td>旧密码:</td>
					<td><input type="password" class="easyui-validatebox"
						name="oldPassword" required="true"
						data-options="width: 150,required:'required',validType:'length[6,20]'"
						maxlength="20"></td>
				</tr>
				<tr>
					<td>新密码:</td>
					<td><input type="password" class="easyui-validatebox"
						id="newPassword" name="newPassword" required="true"
						data-options="width: 150,required:'required',validType:'length[6,20]'"
						maxlength="20"></td>
				</tr>
				<tr>
					<td>确认密码:</td>
					<td><input type="password" class="easyui-validatebox"
						id="reNewPassword" required="true"
						data-options="width: 150,required:'required',validType:'equals[$(\'#newPassword\').val()]',invalidMessage:'两次输入密码不匹配'"
						maxlength="20"></td>
				</tr>
			</table>
		</form>
		<!-- 新增、修改窗口底部按钮 -->
		<div id="form_dlg_btns">
			<a class="easyui-linkbutton" href="javascript:void(0)"
				iconCls="icon-ok"
				onclick="javascript:$('#updatePasswordForm').submit();">保存</a> <a
				class="easyui-linkbutton" href="javascript:void(0)"
				iconCls="icon-cancel"
				onclick="javascript:$('#form_dlg').dialog('close')">取消</a>
		</div>
	</div>

	<script>
		//获取用户菜单
		$.ajax({
			async : false,
			type : 'get',
			cache : false,
			url : "${ctx}/system/permission/currentUser/json",
			success : function(data) {
				var menuData = {
					data : data
				};
				var html = template('menu', menuData);
				$('#myMenu').html(html);
			}
		});

		var g_count = 0;
		var d;
		var myAlert = false;
		var XMLHttpReq;
		//ajax请求
		function createXMLHttpRequest() {
			try {
				XMLHttpReq = new ActiveXObject("Msxml2.XMLHTTP");//IE高版本创建XMLHTTP  
			} catch (E) {
				try {
					XMLHttpReq = new ActiveXObject("Microsoft.XMLHTTP");//IE低版本创建XMLHTTP  
				} catch (E) {
					XMLHttpReq = new XMLHttpRequest();//兼容非IE浏览器，直接创建XMLHTTP对象  
				}
			}

		}

		function sendAjaxRequest(url, processResponse) {
			createXMLHttpRequest(); //创建XMLHttpRequest对象  
			XMLHttpReq.open("get", url, true);
			XMLHttpReq.onreadystatechange = processResponse; //指定响应函数  
			XMLHttpReq.send(null);
		}

		//回调函数  
		function processResponse() {
			if (XMLHttpReq.readyState == 4) {
				if (XMLHttpReq.status == 200) {
					var text = XMLHttpReq.responseText;
					var data = window.decodeURI(text);
					var count = Number(data);
					if (count > g_count && !myAlert) {
						myAlert = true;
						//$.messager.show({ title : "新任务提示", msg: "您有新任务了！", position: "topCenter" });
						$.messager.alert("新任务提示", "您有新任务了！", null, function() {
							myAlert = false;
						});
					}
					g_count = count;
					document.getElementById("todoProc").innerHTML = data;
				}
			}
		}

		function showTodoProc() {
			sendAjaxRequest("${ctx}/workorder/task/todo/count", processResponse);
		}

		function showTodoProc_old() {
			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/workorder/task/todo/count",
				success : function(data) {
					var count = Number(data);
					if (count > g_count && !myAlert) {
						myAlert = true;
						//$.messager.show({ title : "新任务提示", msg: "您有新任务了！", position: "topCenter" });
						$.messager.alert("新任务提示", "您有新任务了！", null, function() {
							myAlert = false;
						});
					}
					g_count = count;
					document.getElementById("todoProc").innerHTML = data;
				},
				error : function(e) {
					alert(e);
				}
			});
		}

		$(function() {
			//修改密码
			$("#btnUpdatePassword").click(function() {
				$('#updatePasswordForm').form('clear');
				d = $('#form_dlg').dialog('open').dialog('setTitle', "修改密码");
			});

			$('#updatePasswordForm').form({
				url : "${ctx}/system/user/updatePassword",
				onSubmit : function() {
					var isValid = $(this).form('validate');
					return isValid; // 返回false终止表单提交
				},
				success : function(data) {
					successTip(data, null, d);
				}
			});

			//待办任务数
			showTodoProc();
			setInterval("showTodoProc()", 1000 * 30);

			$('.easyui-linkbutton').on('click', function() {
				$('.easyui-linkbutton').linkbutton({
					selected : false
				});
				$(this).linkbutton({
					selected : true
				});
			});

			$('#toOrderCenter').on(
					'click',
					function() {
						window.mainpage.mainTabs.addModule('工单中心',
								'workorder/ordercenter', 'icon-hamburg-cv');
					});
		});

		//##############################统计图表#######################################
		/* var optionCustomLevel = {
			title : {
				text : '客户统计划分',
				subtext : '按客户级别',
				x : 'center'
			},
			tooltip : {
				trigger : 'item',
				formatter : "{a} <br/>{b} : {c} ({d}%)"
			},
			legend : {
				orient : 'vertical',
				left : 'left',
				data : [ '一级客户', '二级客户', '三级客户' ]
			},
			series : [ {
				name : '客户数量',
				type : 'pie',
				radius : '55%',
				center : [ '50%', '60%' ],
				data : [ {
					value : 335,
					name : '一级客户'
				}, {
					value : 310,
					name : '二级客户'
				}, {
					value : 234,
					name : '三级客户'
				} ],
				itemStyle : {
					emphasis : {
						shadowBlur : 10,
						shadowOffsetX : 0,
						shadowColor : 'rgba(0, 0, 0, 0.5)'
					}
				}
			} ]
		};
		var optionCustomAsc = {
			title : {
				text : '客户数量变化',
				subtext : '三个月对比'
			},
			tooltip : {
				trigger : 'axis',
				axisPointer : {
					type : 'shadow'
				}
			},
			legend : {
				data : [ '客户数量' ]
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			yAxis : {
				type : 'value',
				boundaryGap : [ 0, 0.1 ]
			},
			xAxis : {
				type : 'category',
				data : []
			},
			series : [ {
				name : '客户数量',
				type : 'bar',
				barWidth:30,
				data : []
			} ]
		};
		var optionHosts = {
			title : {
				text : '本月云主机类型统计',
				left : 'center',
				x : 'center'
			},
			tooltip : {
				trigger : 'item',
				formatter : "{a} <br/>{b} : {c} ({d}%)"
			},
			legend : {
				x : 'center',
				y : 'bottom',
				data : []
			},
			calculable : true,
			series : [

			{
				name : '主机数量',
				type : 'pie',
				radius : [ 30, 110 ],
				center : [ '50%', '55%' ],
				roseType : 'area',
				data : []
			} ]
		};
		var useHostTop = {
			title : {
				text : '本月主机数量客户TOP3',
				subtext : ' 单位台'
			},
			tooltip : {
				trigger : 'axis',
				axisPointer : {
					type : 'shadow'
				}
			},
			legend : {
				data : [ '使用台数' ]
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			yAxis : {
				type : 'value',
				boundaryGap : [ 0, 0.1 ]
			},
			xAxis : {
				type : 'category',
				data : [ '巴西', '印尼', '美国' ]
			},
			series : [ {
				name : '使用台数',
				type : 'bar',
				barWidth:30,
				data : [ 18, 23, 29 ]
			} ]
		};
		var charges = {
			title : {
				text : '上月实收统计',
				subtext : '单位元'
			},
			tooltip : {
				trigger : 'item',
				formatter : "{a} <br/>{b}: {c} ({d}%)"
			},
			legend : {
				orient : 'vertical',
				x : 'right',
				data : [ '云主机服务', '存储服务', '带宽服务', '增值服务', '其它服务' ]
			},
			series : [ {
				name : '服务费用',
				type : 'pie',
				radius : [ '40%', '55%' ],

				data : [ {
					value : 335,
					name : '云主机服务'
				}, {
					value : 310,
					name : '存储服务'
				}, {
					value : 234,
					name : '带宽服务'
				}, {
					value : 135,
					name : '增值服务'
				}, {
					value : 147,
					name : '其它服务'
				} ]
			} ]
		};
		var chargeTop = {
			title : {
				text : '上月实收客户TOP3',
				subtext : ' 单位元'
			},
			tooltip : {
				trigger : 'axis',
				axisPointer : {
					type : 'shadow'
				}
			},
			legend : {
				data : [ '实收费用' ]
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			yAxis : {
				type : 'value',
				boundaryGap : [ 0, 0.1 ]
			},
			xAxis : {
				type : 'category',
				data : []
			},
			series : [ {
				name : '实收费用',
				type : 'bar',
				barWidth:30,
				data : []
			} ]
		};

		var workorders = {
			title : {
				text : '本月处理工单总量',
				subtext : '按工单类型区分'
			},
			tooltip : {
				trigger : 'axis',
				axisPointer : {
					type : 'shadow'
				}
			},
			legend : {
				data : [ '工单数量' ]
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			xAxis : {
				type : 'value',
				boundaryGap : [ 0, 0.01 ]
			},
			yAxis : {
				type : 'category',
				data : []
			},
			series : [ {
				name : '工单数量',
				type : 'bar',
				barWidth:30,
				data : []
			} ]
		};
		var doneTop = {
			title : {
				text : '本月处理工单TOP3',
				subtext : ''
			},
			tooltip : {
				trigger : 'axis',
				axisPointer : {
					type : 'shadow'
				}
			},
			legend : {
				data : [ '处理数量' ]
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			yAxis : {
				type : 'value',
				boundaryGap : [ 0, 0.1 ]
			},
			xAxis : {
				type : 'category',
				data : []
			},
			series : [ {
				name : '处理数量',
				type : 'bar',
				barWidth:30,
				data : []
			} ]
		};
		var chart1 = echarts.init(document.getElementById('chart1'),
				'infographic');
		var chart2 = echarts.init(document.getElementById('chart2'), 'shine');
		var chart3 = echarts
				.init(document.getElementById('chart3'), 'macarons');
		var chart4 = echarts
				.init(document.getElementById('chart4'), 'macarons');
		var chart5 = echarts
				.init(document.getElementById('chart5'), 'macarons');
		var chart6 = echarts.init(document.getElementById('chart6'), 'shine');
		var chart7 = echarts.init(document.getElementById('chart7'), 'shine');
		var chart8 = echarts
				.init(document.getElementById('chart8'), 'macarons');

		chart1.setOption(optionCustomLevel);
		chart2.setOption(optionCustomAsc);
		chart3.setOption(optionHosts);
		chart4.setOption(useHostTop);
		chart5.setOption(charges);
		chart6.setOption(chargeTop);
		chart7.setOption(workorders);
		chart8.setOption(doneTop);

		$(function() {

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/custom/analysis",
				success : function(data) {
					$('#customCount').text(data.total);
					// 填入数据
					chart1.setOption({
						series : [ {
							// 根据名字对应到相应的系列
							name : '客户数量',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/custom/compare",
				success : function(data) {
					// 填入数据
					chart2.setOption({
						xAxis : {
							type : 'category',
							data : data.categories
						},
						series : [ {
							// 根据名字对应到相应的系列
							name : '客户数量',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/service/info",
				success : function(data) {
					$('#hosts').text(data.host);
					$('#stores').text(data.store);
					$('#nets').text(data.net);
					// 填入数据
					chart3.setOption({
						series : [ {
							// 根据名字对应到相应的系列
							name : '主机数量',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});
			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/service/hosttop",
				success : function(data) {
					// 填入数据
					chart4.setOption({
						xAxis : {
							type : 'category',
							data : data.categories
						},
						series : [ {
							// 根据名字对应到相应的系列
							name : '主机数',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/home/summary",
				success : function(data) {
					$('#actualcharge').text(data.actCharge);
					$('#standardcharge').text(data.stdCharge);
					// 填入数据
					chart5.setOption({
						series : [ {
							// 根据名字对应到相应的系列
							name : '实收费用',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/home/chargetop",
				success : function(data) {
					// 填入数据
					chart6.setOption({
						xAxis : {
							type : 'category',
							data : data.categories
						},
						series : [ {
							// 根据名字对应到相应的系列
							name : '实收费用',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/workorder/info",
				success : function(data) {
					$('#ordercount').text(data.total);
					// 填入数据
					chart7.setOption({
						yAxis : {
							type : 'category',
							data : data.categories
						},
						series : [ {
							// 根据名字对应到相应的系列
							name : '工单数量',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/workorder/ordretop",
				success : function(data) {
					$('#ordercount').text(data.total);
					// 填入数据
					chart8.setOption({
						xAxis : {
							type : 'category',
							data : data.categories
						},
						series : [ {
							// 根据名字对应到相应的系列
							name : '工单数量',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

		});
 */
		//##########################################################################
	</script>

</body>
</html>
