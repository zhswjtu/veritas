package com.centrin.system.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.centrin.system.dao.PermissionDao;
import com.centrin.system.entity.PermissionEntity;
import com.centrin.system.exception.SysException;
import com.centrin.system.shiro.realm.ShiroUser;
import com.centrin.system.utils.UserUtil;

@Service
public class PermissionService {
	@Autowired
    private PermissionDao permissionDao;
	
	public List<PermissionEntity> getPermissionsByUser(ShiroUser user) {
		if(UserUtil.isSuperAdmin(user.getUsername())) {
			return permissionDao.getAllValidPermissions();
		} else {
			return permissionDao.getPermissionsByUserId(user.getId());
		}
	}
	
	public List<PermissionEntity> getMenus() {
		return permissionDao.getMenus();
	}
	
	public List<PermissionEntity> getRootMenus() {
		return permissionDao.getRootMenus();
	}
	
	public List<PermissionEntity> getMenusByPid(Integer pid) {
		return permissionDao.getMenusByPid(pid);
	}
	
	
	public PermissionEntity get(Integer id) {
		return permissionDao.findById(PermissionEntity.class, id);
	}
	
	
	public void add(PermissionEntity permission) {
		permissionDao.add(permission);
	}
	
	
	public void update(PermissionEntity permission) {
		permissionDao.update(permission);
	}
	
	@Transactional
	public void delete(Integer id) throws SysException{
		List<PermissionEntity> list = permissionDao.getPermissionByPid(id);
		if(list==null || list.size()==0) {
			permissionDao.delete(id);
		} else {
			throw new SysException("存在子节点，不能删除！");
		}
		
	}
	
	
	
	@Transactional(readOnly = false)
	public void addBaseOperation(Integer pid, String pClassName){
		List<PermissionEntity> pList=new ArrayList<PermissionEntity>();
		pList.add(new PermissionEntity(pid, "添加", 2, "", "sys:"+pClassName+":add"));
		pList.add(new PermissionEntity(pid, "删除", 2, "", "sys:"+pClassName+":delete"));
		pList.add(new PermissionEntity(pid, "修改", 2, "", "sys:"+pClassName+":update"));
		pList.add(new PermissionEntity(pid, "查看", 2, "", "sys:"+pClassName+":view"));
		
		//添加没有的基本操作
		List<PermissionEntity> existPList = getOperationsByMenuId(pid);
		for(PermissionEntity permission : pList) {
			boolean exist=false;
			for(PermissionEntity existPermission : existPList){
				if(permission.getPermCode().equals(existPermission.getPermCode())){
					exist = true;
					break;
				}else{
					exist = false;
				}
			}
			if(!exist) {
				add(permission);
			}
		}
	}
	
	
	public List<PermissionEntity> getOperationsByMenuId(Integer id) {
		return permissionDao.getOperationsByMenuId(id);
	}
	
	public List<PermissionEntity> findAll(){
		return permissionDao.findAll(PermissionEntity.class);
	}
	
	public List<PermissionEntity> findActives(){
		return permissionDao.getAllValidPermissions();
	}
	
	public List<PermissionEntity> getAllValidPermissionsByPid(Integer pid){
		return permissionDao.getAllValidPermissionsByPid(pid);
	}
	
}