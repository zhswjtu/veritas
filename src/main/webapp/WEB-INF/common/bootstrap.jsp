<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<link rel="stylesheet" href="${ctx}/resource-Main/main/css/bootstrap.css">
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/bootstrap-table.css">
<link rel="stylesheet" href="${ctx}/resource-Main/bootstrapvalidator/css/bootstrapValidator.css">
<link rel="stylesheet" href="${ctx}/resource-Main/bootstrapvalidator/css/tooltip.css">
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/jquery.toastr.min.css">
<link href="${ctx}/resource-Main/main/css/style.default.css" rel="stylesheet">
<link href="${ctx}/resource-Main/main/css/demo.css" rel="stylesheet">
<%-- <link rel="stylesheet" href="${ctx}/resource-Main/main/css/jquery.datatables.css"> --%>

<script src="${ctx}/resource-Main/main/js/jquery-1.11.1.min.js"></script>
<script src="${ctx}/resource-Main/main/js/jquery-migrate-1.2.1.min.js"></script>
<script src="${ctx}/resource-Main/main/js/jquery-ui-1.10.3.min.js"></script>
<script src="${ctx}/resource-Main/main/js/bootstrap.min.js"></script>
<script src="${ctx}/resource-Main/main/js/bootstrap-table.js"></script>
<script src="${ctx}/resource-Main/main/js/bootstrap-table-zh-CN.js"></script>
<script src="${ctx}/resource-Main/main/js/modernizr.min.js"></script>
<script src="${ctx}/resource-Main/main/js/jquery.sparkline.min.js"></script>
<script src="${ctx}/resource-Main/main/js/toggles.min.js"></script>
<script src="${ctx}/resource-Main/main/js/retina.min.js"></script>
<script src="${ctx}/resource-Main/main/js/jquery.cookies.js"></script>
<script src="${ctx}/resource-Main/main/js/jquery.form.js"></script>
<script src="${ctx}/resource-Main/main/js/jquery.toastr.min.js"></script>
<script src="${ctx}/resource-Main/main/js/model.table.js"></script>

<script src="${ctx}/resource-Main/main/js/flot/jquery.flot.min.js"></script>
<script src="${ctx}/resource-Main/main/js/flot/jquery.flot.resize.min.js"></script>
<script src="${ctx}/resource-Main/main/js/flot/jquery.flot.spline.min.js"></script>
<script src="${ctx}/resource-Main/main/js/morris.min.js"></script>
<script src="${ctx}/resource-Main/main/js/raphael-2.1.0.min.js"></script>

<script src="${ctx}/resource-Main/main/js/dashboard.js"></script>
<script src="${ctx}/resource-Main/main/js/custom.js"></script>
<script src="${ctx}/resource-Main/main/js/demo.js"></script>
<script src="${ctx}/resource-Main/main/js/action.js"></script>
<script src="${ctx}/resource-Main/main/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<%--<script src="${ctx}/resource-Main/main/js/bootstrap-datetimepicker.fr.js" charset="UTF-8"></script>--%>
<script src="${ctx}/resource-Main/main/js/bootstrap-datetimepicker.zh-CN.js"></script>

<script type="text/javascript" src="${ctx}/resource-Main/bootstrapvalidator/js/bootstrapValidator.js"></script>
<script type="text/javascript" src="${ctx}/resource-Main/bootstrapvalidator/js/language/zh_CN.js"/>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<script type="text/javascript">
    var ctx = '${pageContext.request.contextPath}';
</script>
<script type="text/javascript">
    toastr.options.positionClass = 'toast-top-center';
    toastr.options.closeButton = 'true';
</script>
<script>
    //全局的AJAX访问，处理AJAX清求时SESSION超时
    $.ajaxSetup({
        contentType: "application/x-www-form-urlencoded;charset=utf-8",
        complete: function (XMLHttpRequest, textStatus) {
            //通过XMLHttpRequest取得响应头，sessionstatus
            var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
            if (sessionstatus == "timeout") {
                //跳转的登录页面
                window.location.replace('${ctx}/');
            }
        }
    });

    window.onload = function () {
        document.getElementsByTagName("body")[0].onkeydown = function () {
            if (event.keyCode == 8) {
                var elem = event.srcElement;
                var name = elem.nodeName;

                if (name != 'INPUT' && name != 'TEXTAREA') {
                    event.returnValue = false;
                    return;
                }
                var type_e = elem.type.toUpperCase();
                if (name == 'INPUT' && (type_e != 'TEXT' && type_e != 'TEXTAREA' && type_e != 'PASSWORD' && type_e != 'FILE')) {
                    event.returnValue = false;
                    return;
                }
                if (name == 'INPUT' && (elem.readOnly == true || elem.disabled == true)) {
                    event.returnValue = false;
                    return;
                }
            }
        }
    };
</script>
<div class="modal fade bs-example-modal-lg" id="add-model" tabindex="-1" role="dialog" aria-labelledby="add-model">
    <div class="modal-dialog" role="document">
        <div class="modal-content"></div>
    </div>
</div>
<script>
    window.onload = function () {
        $('#add-model').on('hidden.bs.modal', function (e) {
            $(this).removeData('bs.modal');
        });
    };
</script>
