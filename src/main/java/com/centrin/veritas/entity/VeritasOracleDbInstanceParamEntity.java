package com.centrin.veritas.entity;
/**
 * Project Name：veritas
 * File Name：VeritasOracleDbInstanceParamEntity
 * Package Name：com.centrin.veritas.entity
 * Date：17/7/24 下午5:27
 */

import com.centrin.common.entity.BaseEntity;

import javax.persistence.*;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.entity
 * @ClassName VeritasOracleDbInstanceParamEntity
 * @author 张辉
 * @date 17/7/24 下午5:27
 * @version
 */
@Entity
@Table(name = "veritas_oracle_db_instance_param")
public class VeritasOracleDbInstanceParamEntity extends BaseEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "instance_id")
    private Integer instanceId;

    @Column(name = "parameter_id")
    private Integer parameterId;

    @Column(name = "parameter_key")
    private String parameterKey;

    @Column(name = "parameter_value")
    private String parameterValue;

    public VeritasOracleDbInstanceParamEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(Integer instanceId) {
        this.instanceId = instanceId;
    }

    public Integer getParameterId() {
        return parameterId;
    }

    public void setParameterId(Integer parameterId) {
        this.parameterId = parameterId;
    }

    public String getParameterKey() {
        return parameterKey;
    }

    public void setParameterKey(String parameterKey) {
        this.parameterKey = parameterKey;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }
}
