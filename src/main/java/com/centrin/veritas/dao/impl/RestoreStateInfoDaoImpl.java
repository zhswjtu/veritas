package com.centrin.veritas.dao.impl;
/**
 * Project Name：veritas
 * File Name：RestoreStateInfoDaoImpl
 * Package Name：com.centrin.veritas.dao.impl
 * Date：17/8/8 下午11:23
 */

import com.centrin.common.dao.BaseDaoImpl;
import com.centrin.veritas.dao.RestoreStateInfoDao;
import org.springframework.stereotype.Repository;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao.impl
 * @ClassName RestoreStateInfoDaoImpl
 * @author 张辉
 * @date 17/8/8 下午11:23
 * @version
 */
@Repository
public class RestoreStateInfoDaoImpl extends BaseDaoImpl implements RestoreStateInfoDao {
}
