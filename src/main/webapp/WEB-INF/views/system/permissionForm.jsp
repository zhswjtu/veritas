<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
	var ctx = '${pageContext.request.contextPath}';
</script>
<html lang="en">
<head>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/orgList_apply.css">
<title></title>
</head>
<body>
	<!--加载效果 start-->
	<div id="preloader">
		<div id="status">
			<i class="fa fa-spinner fa-spin"></i>
		</div>
	</div>
	<!--加载效果 end-->
	<section> <!--main start-->
	<div class="mainpanel_mian">
		<!-- main start -->
		<div class="contentpanel">
			<div class="row" style="background-color: #fff;">
				<div class="col-sm-12">
					<!--list start-->
					<div class="panel panel-default">
						<div class="panel-heading zj-panel-heading-2 clearfix"
							style="padding: 10px 0;">
							<h5 class="panel-title zj-panel-title-2"
								style="float: left; height: 30px; line-height: 30px;">添加权限</h5>
							<button type="button" class="btn btn-primary" style="float: right; height: 30px; width: 70px;" onclick="window.history.go(-1);">
								<i class="glyphicon glyphicon-backward" style="margin-right: 7px"></i>返回
							</button>
						</div>
					</div>
					<form id="mainform" role="form" class="form-horizontal"
						action="${ctx}/system/permission/${action}" method="post">
						<div class="form-group">
							<label class="col-md-3 control-label">权限名称：</label>
							<div class="col-md-6">
								<input type="hidden" name="id" value="${permission.id}" />
								<inputtype ="hidden" name="type" value="2" />
								<input id="name" name="name" type="text"
									value="${permission.name}" class="form-control"
									data-options="width: 180,required:'required',validType:'length[2,20]'" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">权限编码：</label>
							<div class="col-md-6">
								<input id="permCode" name="permCode" type="text"
									class="form-control" data-options="width: 180"
									value="${permission.permCode }" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">访问路径：</label>
							<div class="col-md-6">
								<input id="url" name="url" type="text" value="${permission.url}" class="form-control" data-options="width: 180" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">上级菜单：</label>
							<div class="col-md-6">
								<input id="pName" value="${pname}" class="form-control" />
								<input id="pid" name="pid" value="${permission.pid}" class="form-control" type="hidden" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"  style="padding-right: 2.7em">状态：</label>
							<div class="col-md-6">
								<select name="status" class="select-o">
									<option value="1">启用</option>
									<option value="0">禁用</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"  style="padding-right: 2.7em">排序：</label>
							<div class="col-md-6">
								<input id="sort" name="sort" type="text" value="${permission.sort}" class="form-control" data-options="width: 180" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"  style="padding-right: 2.7em">描述：</label>
							<div class="col-md-6">
								<textarea rows="3" cols="41" name="description"
									class="form-control">${permission.description}</textarea>
							</div>
						</div>
						<div class="sub_btn">
							<div class="col-sm-3"></div>
							<div class="col-sm-6" style="text-align: center">
								<input type="button" id="confirm" class="btn btn-primary sub1"
									value="确定" onclick="save()"> <input type="button"
									id="close" class="btn btn-primary sub2"
									onclick="window.history.go(-1);" value="取消">
							</div>
							<div class="col-sm-3"></div>
						</div>
					</form>
				</div>
				<script type="text/javascript">
					//状态
					$('#status').val('${permission.status}');

					$(document).ready(function() {
						$('#mainform').bootstrapValidator({
							container : 'tooltip',
							//        trigger: 'blur',
							feedbackIcons : {
								valid : 'glyphicon glyphicon-ok',
								invalid : 'glyphicon glyphicon-remove',
								validating : 'glyphicon glyphicon-refresh'
							},
							fields : {
								name : {
									group : '.group',
									validators : {
										notEmpty : {}
									}
								},
								permCode : {
									group : '.group',
									validators : {
										notEmpty : {}
									}
								}
							}
						}).on('success.form.bv', function(e) {
							var $form = $(e.target);
							$form.ajaxSubmit(function(data) {
								if (data == 'success') {
									toastr.success('添加成功');
									window.location.href="${ctx}/system/permission";
								}
							});
						});
					});
					function save() {
						$('#mainform').bootstrapValidator('validate');
					}
				</script>
</body>
</html>