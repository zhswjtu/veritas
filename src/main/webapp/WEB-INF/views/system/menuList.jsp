
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	var ctx = '${pageContext.request.contextPath}';
</script>
<html lang="en">
<head>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<title>用户管理</title>
<!-- <meta name="decorator" content="default" /> -->
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
</head>

<body style="color: black;font-size: 12;">
<style type="text/css">
	.pre-scrollable {
	            max-height: 800px;
	            overflow-y: scroll
	       
	}
</style>

	<!--加载效果 start-->
		<div id="preloader">
			<div id="status">
				<i class="fa fa-spinner fa-spin"></i>
			</div>
		</div>
		<section>
		    <!--main start-->
		    <div class="mainpanel_mian">
		        <!-- main start -->
		        <div class="contentpanel">
		            <div class="row ">
		                <div class="col-sm-12">
		                    <!--list start-->
		                    <div class="panel panel-default">
		                        <div class="panel-heading zj-panel-heading-2">
		                            <h5 class="panel-title zj-panel-title-2">菜单管理</h5>
		                        </div>
		                        <div class="panel-body zj-list-body">
		                            <div class="row">
		                              <div class="col-sm-12" style="padding-left: 20px">
		                                    <a class="btn btn-primary" href="javascript:void(0)" onclick="add();"><i class="zj-icon zj-d-apply"></i>添加</a> 
										 <div class="panel panel-default">
											<div class="pre-scrollable row" id="div1" ></div>
										</div>
									</div>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
	<script type='text/javascript' src='${ctx}/resource-Main/jquery-treegrid/js/TreeGrid.js' charset='utf-8'></script>
	<script type="text/javascript">
	var parentId;
	var parentName;
		var config = {
			id : "tg1",
			width : "800",
			renderTo : "div1",
			headerAlign : "left",
			headerHeight : "30",
			dataAlign : "left",
			indentation : "20",
			/*folderOpenIcon : "/images/folderOpen.gif",
			folderCloseIcon : "/images/folderClose.gif",
			defaultLeafIcon : "/images/defaultLeaf.gif",*/
			hoverRowBackground : "true",
			folderColumnIndex : "0",
			itemClick : "itemClickEvent",
			columns : [ {
				headerText : "名称",
				headerAlign : "center",
				dataField : "name",
				width : "260px"
			}, {
				headerText : "资源路径 ",
				headerAlign : "center",
				dataField : "url",
				width : "260px"
			}, {
				headerText : "状态 ",
				dataField : "status",
				headerAlign : "center",
				dataAlign : "center",
				handler : "customStatus",
				width : "100px"
			}, {
				headerText : "排序 ",
				dataField : "sort",
				headerAlign : "center",
				dataAlign : "center",
				width : "100px"
			}, {
				headerText : "描述  ",
				dataField : "description",
				headerAlign : "center",
				dataAlign : "center",
			}, {
				headerText : "操作",
				headerAlign : "center",
				dataAlign : "center",
				width : "260px",
				handler : "customLook"
			} ],
			data : [],
			url : '${ctx}/system/permission/menu/json',
			/* tbodyStyle : "max-height:800px;overflow:auto;display:block;", */
			method : 'get'
		};

		/*
		 * 单击数据行后触发该事件 id：行的id index：行的索引。 data：json格式的行数据对象。
		 */
		function itemClickEvent(id, index, data) {
			jQuery("#currentRow").val(
					id + ", " + index + ", " + TreeGrid.json2str(data));
		}

		/*
		 * 通过指定的方法来自定义栏数据
		 */
		function customCheckBox(row, col) {
			return "<input type='checkbox'>";
		}

		function customOrgName(row, col) {
			var name = row[col.dataField] || "";
			return name;
		}
		function customStatus(row, col) {
			var val = row[col.dataField];
			if (val == '1') {
				return '启用';
			} else {
				return '禁用';
			}
			return name;
		}

		function customLook(row, col) {
			var val = "";
				val+='<a href="javascript:addChilden('+row.id+')" style="color:blue;" >添加子菜单</a>&nbsp;&nbsp;';
				val+='<a href="javascript:update('+row.id+')" style="color:blue;">修改</a>&nbsp;&nbsp;';
				val+='<a href="javascript:del('+row.id+')" style="color:blue;">删除</a>';
			return val;
		}

		// 创建一个组件对象
		var treeGrid = new TreeGrid(config);
		treeGrid.show();
			
		/*
		 * 展开、关闭所有节点。 isOpen=Y表示展开，isOpen=N表示关闭
		 */
		function expandAll(isOpen) {
			treeGrid.expandAll(isOpen);
		}

		/*
		 * 取得当前选中的行，方法返回TreeGridItem对象
		 */
		function selectedItem() {
			var treeGridItem = treeGrid.getSelectedItem();
			if (treeGridItem != null) {
				// 获取数据行属性值
				// alert(treeGridItem.id + ", " + treeGridItem.index + ", " +
				// treeGridItem.data.name);

				// 获取父数据行
				var parent = treeGridItem.getParent();
				if (parent != null) {
					// jQuery("#currentRow").val(parent.data.name);
				}

				// 获取子数据行集
				var children = treeGridItem.getChildren();
				if (children != null && children.length > 0) {
					jQuery("#currentRow").val(children[0].data.name);
				}
			}
		}
		function calback(){
			$("#close").click();
			toastr.success('操作成功');
		}

		
		function add(id){
			window.location.href="${ctx}/system/permission/menu/create?pid=-1";
		}

		
		function addChilden(id){
			window.location.href="${ctx}/system/permission/menu/create?pid="+id;
		}
		
		function update(id){
			window.location.href="${ctx}/system/permission/menu/update/"+id;
		}
		
		function del(id){
				Ewin.confirm({
					message : "删除后无法恢复您确定要删除？"
				}).on(
						function(e) {
							if (!e) {
								return;
							}
					$.ajax({
						type:'get',
						cache: false,
						url:"${ctx}/system/permission/delete/"+id,
						success: function(data){
							if(data.code==0) {
								toastr.success("删除成功");
								window.location.href="${ctx}/system/permission/menu";
							} else {
								toastr.warning(data.msg);
							}
						}
					});
			});
		}
	</script>
</body>
</html>