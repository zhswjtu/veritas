package com.centrin.common.dto.model.rsp;

public class RspDeviceHeadBean extends RspBean {
	private static final long serialVersionUID = 1L;

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
