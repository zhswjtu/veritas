/** 
 * Project Name：csms 
 * File Name：OrgDao.java 
 * Package Name：com.centrin.system.dao 
 * Date：2015年12月8日 上午9:57:08 
 * Copyright (c) 2015, Centrin Data Systems Ltd. All Rights Reserved. 
 * 中金数据系统有限公司 
 */
package com.centrin.system.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.centrin.common.dao.SuperBaseDao;
import com.centrin.system.entity.OrgEntity;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.system.dao
 * @ClassName OrgDao
 * @author feichen
 * @date 2015年12月8日 上午9:57:08
 * @version
 */
@Repository
public class OrgDao extends SuperBaseDao {
	
	
	/**
	 * @Title: getOrgByOrgName
	 * @Description:根据部门名称获取部门
	 * @param name
	 * @return
	 */
	public OrgEntity getOrgByOrgName(String name) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", name);
		String jpql = "SELECT o FROM OrgEntity o WHERE o.name = :name";
		return super.getSingleByJpql(OrgEntity.class,jpql, params);
	}
	
	/**
	 * @Title: getOrgByOrgId
	 * @Description:根据id获取部门
	 * @param id
	 * @return
	 */
	public OrgEntity getOrgByOrgId(Integer id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		String jpql = "SELECT o FROM OrgEntity o WHERE o.id = :id";
		return super.getSingleByJpql(OrgEntity.class,jpql, params);
	}
	
	/**
	 * @Title: getOrgByOrgCode
	 * @Description:根据部门编码查询
	 * @param code
	 * @return
	 */
	public OrgEntity getOrgByOrgCode(String code) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("code", code);
		String jpql = "SELECT o FROM OrgEntity o WHERE o.code = :code";
		return super.getSingleByJpql(OrgEntity.class, jpql, params);
	}
	
	/**
	 * @Title: getAllOrgs
	 * @Description:当前的所有部门
	 * @return
	 */
	public List<OrgEntity> getAllOrgs(){
		String jpql = "select o from OrgEntity o order by o.sort";
		return findByJpql(OrgEntity.class, jpql, null, -1, -1);
	}
	
	/**
	 * @Title: getAvailableOrgs
	 * @Description:当前的所有有效部门
	 * @return
	 */
	public List<OrgEntity> getAvailableOrgs(){
		String jpql = "select o from OrgEntity o where o.status=1 order by o.sort";
		return findByJpql(OrgEntity.class, jpql, null, -1, -1);
	}
} 
