<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
	var ctx = '${pageContext.request.contextPath}';
</script>
<html lang="en">
<head>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<link rel="stylesheet" href="${ctx}/resource-Main/roleForm/roleForm.css">
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
<title>角色管理</title>
</head>
<body>
<%-- <link rel="stylesheet" href="${ctx}/resource-Main/jquery-treegrid/css/jquery.treegrid.css"> --%>
<!--加载效果 start-->
<div id="preloader">
    <div id="status">
        <i class="fa fa-spinner fa-spin"></i>
    </div>
</div>
<!--加载效果 end-->
	<section>
	<div class="mainpanel_mian">
		<div class="contentpanel">
			<div class="row">
				<div class="col-md-9">
					<div class="panel panel-default">
						<div class="panel-heading zj-panel-heading-2">
							<h5 class="panel-title zj-panel-title-2">角色列表</h5>
						</div>
						</div>
						<div style="margin-top: 15px; margin-left: 0px;">
							<input id="roleId" type="hidden" /> 
							<a class="btn btn-primary " href="javascript:void(0) " onclick="roleModule();"><i class="zj-icon zj-d-user "></i>角色组件</a>
							<a class="btn btn-primary"  id='add' href="${ctx}/system/role/create" data-toggle="modal" > <i class="zj-icon zj-d-apply"></i>添加</a> 
							<a class="btn btn-primary" href="javascript:void(0)" iconCls="icon-save" plain="true" onclick="savePerms();"><i class="zj-icon zj-d-apply"></i>保存授权</a>
						</div>
						<div class="table-responsive zj-table-responsive">
							<table class="bootstrap-table mb30 table-hover table-responsive" id="table">
							</table>
						</div>
					
				</div>
				<div class="col-md-3">
				 <div class="panel panel-default">
                        <div class="panel-heading zj-panel-heading-2">
                            <h5 class="panel-title zj-panel-title-2">权限列表</h5>
                        </div>
					</div>
						<div id="div1" style="margin-top: 43px;"></div>
				</div>
			</div>
		</div>
		<!-- main end -->
	</div>
	</section>

	<script type='text/javascript' src='${ctx}/resource-Main/jquery-treegrid/js/TreeGrid.js' charset='utf-8'></script>
  	<script type='text/javascript'>
			var table;
			//var rolePerData=[];
			var updateData = [];
			var activeElement = null;
			var roleId;
			var permsTr;
			jQuery(function($) {
				roleId = $("#roleId");
				permsTr = $("#tg1 tr");

				table = $('#table').bootstrapTable(
								{
									//height: getHeight(),
									method : "get",
									url : '${ctx}/system/role/json',
									cache : false,
									fit : true,
									fitColumns : true,
									border : false,
									idField : 'id',
									uniqueId : 'id',
									pagination : true,
									rownumbers : true,
									checkbox: true,
									showRefresh : false,
									pageNumber : 1,
									pageSize : 10,
									sidePagination : 'server',
									pageList : [ 10, 20 ],
									singleSelect : true,
									//striped:true,
									search : false,
									clickToSelect: true,
									//checkboxHeader: true,
									queryParams : function(params) {
										var param = {
											limit : params.limit, //页面大小
											offset : params.offset, //页码
											order : params.order,
											orderBy : params.sort,
											filter_LIKES_name : $(
													"#filter_LIKES_name").val(),
											filter_LIKES_phone : $(
													"#filter_LIKES_phone")
													.val(),
											filter_GTD_createDate : $("#start")
													.val(),
											filter_LTD_createDate : $(
													"#endTime").val()
										};
										return param;
									},
									columns : [
											 {
									            field : 'checked',
									            checkbox : true,
									        },{
												field : 'id',
												title : 'id',
												visible : false,
												width : 80
											},
											{
												field : 'pos',
												title : '序号',
												width : 50,
												align : 'center',
												formatter : function(value,
														row, index) {
													return index + 1;
												}
											},
											{
												field : 'name',
												title : '角色名称',
												sortable : true,
												width : 160,
												align : 'center'
											},
											{
												field : 'code',
												title : '角色编码',
												sortable : true,
												width : 200,
												align : 'center'
											},
											{
												field : 'description',
												title : '描述',
												sortable : true,
												tooltip : true,
												align : 'center'
											},
											{
												field : 'action',
												title : '操作',
												width : 120,
												align : 'center',
												formatter : function(value,
														row, index) {
													var val = "";
													/* val += '<a href="${ctx}/system/role/update/'+row.id+'" data-toggle="modal" >修改</a>&nbsp;&nbsp;'; */
													val += '<a href="javascript:update('+ row.id + ')" >修改</a>&nbsp;&nbsp;';
													val += '<a href="javascript:del('+ row.id + ')">删除</a>&nbsp;&nbsp;';
													//val+='<a href="javascript:lookP('+row.id+')">查看权限</a>';
													return val;
												}
											} ],
									toolbar : '#tg_tb',
									dataPlain : true,
									onClickRow : function(item, $element) {
										if (activeElement != null) {
											//activeElement.removeAttr("style");
											activeElement.removeClass("row_active");
										}
										activeElement = $element;
										//activeElement.css('background-color','#ffebee'); 
										activeElement.addClass("row_active");
										lookP(item["id"]);
										return false;
									}
								//onClickRow:onClickRow
								});
			});
			function onClickRow(row, index) {
				lookP(row["id"]);
			}
			function lookP(id) {
				$("#roleId").val(id);
				//for(var i=0,j=rolePerData.length;i<j;i++){
				//$("tr[trid="+rolePerData[i].id+"]").removeClass("row_active");
				//}
				$("#tg1 tr").removeClass("row_active");

				//rolePerData=[];
				$.ajax({
					async : false,
					type : 'get',
					cache : false,
					url : "${ctx}/system/role/" + id + "/json",
					success : function(data) {
						if (typeof data == 'object') {
							updateData = [];
							//rolePerData=data;
							for (var i = 0, j = data.length; i < j; i++) {
								$("tr[trid=" + data[i].id + "]").addClass(
										"row_active");
								updateData.push(data[i].id);
							}
						} else {
							toastr.warning(data);
						}
					}
				});
			}
			function del(id) {
				Ewin.confirm({
					message : "删除后无法恢复您确定要删除？"
				}).on(function(e) {
					if (!e) {
						return;
					}
					$.ajax({
						type : 'get',
						cache : false,
						url : "${ctx}/system/role/delete/" + id,
						success : function(data) {
							table.bootstrapTable('refresh');
							roleId.val('');
							permsTr.removeClass("row_active");
							toastr.success(data);
						}
					});
				});
			}
			function update(id) {
				window.location.href ="${ctx}/system/role/update/" + id;
			}
			
			function roleModule() {
				var roleId = $("#roleId").val();
				if (roleId == '') {
					toastr.warning('请选择角色！');
					return;
				}
				window.location.href ="${ctx}/system/role/module/" + roleId;
			}
			
			
			
			function getHeight() {
				return $(window).height() - $('#tg_tb').outerHeight(true) - 15;
			}

			//保存修改权限
			function savePerms() {
				var roleId = $("#roleId").val();
				if (roleId == '') {
					toastr.warning('请选择角色！');
					return;
				}
				Ewin.confirm({
					message : "确认要保存修改？"
				}).on(
						function(e) {
							if (!e) {
								return;
							}
							$.ajax({
								async : false,
								type : 'POST',
								data : JSON.stringify(updateData),
								contentType : 'application/json;charset=utf-8',
								url : "${ctx}/system/role/" + roleId
										+ "/updatePermission",
								success : function(data) {
									toastr.success(data);
								}
							});
						});
			}

			var config = {
				id : "tg1",
				renderTo : "div1",
				headerAlign : "left",
				headerHeight : "30",
				dataAlign : "left",
				indentation : "20",
				tbodyStyle : "max-height:800px;overflow:auto;display:block;",
				hoverRowBackground : "true",
				folderColumnIndex : "0",
				itemClick : "itemClickEvent",
				columns : [ {
					headerText : "名称",
					dataField : "name",
					headerAlign : "center",
					width : "260px",
					height: "60px"
				} ],
				data : [],
				url : '${ctx}/system/permission/json',
				method : 'get'
			};

			/*
			 * 单击数据行后触发该事件 id：行的id index：行的索引。 data：json格式的行数据对象。
			 */
			function itemClickEvent(id, index, data) {
				updateData = [];
				//jQuery("#currentRow").val(id);
				if ($("tr[id=" + id + "]").hasClass("row_active")) {
					$("tr[id^=" + id + "]").removeClass("row_active");//子菜单不选中

					//父菜单不选中
					var mid = id;
					var pid = id.substring(0, mid.lastIndexOf("_"));
					while (pid != 'tr' && pid != 'TR') {
						if (!$("tr[id^=" + pid + "_]").hasClass("row_active")) {
							$("tr[id=" + pid + "]").removeClass("row_active");
						}
						mid = pid;
						pid = id.substring(0, mid.lastIndexOf("_"));
					}
				} else {
					$("tr[id^=" + id + "]").addClass("row_active"); //子菜单选中

					//父菜单选中
					var mid = id;
					var pid = id.substring(0, mid.lastIndexOf("_"));
					while (pid != 'tr' && pid != 'TR') {
						$("tr[id=" + pid + "]").addClass("row_active");
						mid = pid;
						pid = id.substring(0, mid.lastIndexOf("_"));
					}
				}

				$("#tg1").find("tr").each(function() {
					if ($(this).hasClass("row_active")) {
						updateData.push($(this).attr("trid"));
					}
				});

			}

			/*
			 * 通过指定的方法来自定义栏数据
			 */
			function customCheckBox(row, col) {
				return "<input type='checkbox'>";
			}

			function customOrgName(row, col) {
				var name = row[col.dataField] || "";
				return name;
			}
			function customStatus(row, col) {
				var val = row[col.dataField];
				if (val == '1') {
					return '启用';
				} else {
					return '禁用';
				}
				return name;
			}

			/* function customLook(row, col) {
				return "<a href='javascript:onCreate();' style='color:blue;'>新增</a>&nbsp;&nbsp;<a href='' style='color:blue;'>修改</a>&nbsp;&nbsp;<a href='' style='color:blue;'>删除</a>";
			} */

			// 创建一个组件对象
			var treeGrid = new TreeGrid(config);
			treeGrid.show();

			/*
			 * 展开、关闭所有节点。 isOpen=Y表示展开，isOpen=N表示关闭
			 */
			function expandAll(isOpen) {
				treeGrid.expandAll(isOpen);
			}

			function back() {
				$("#close").click();
				table.bootstrapTable('refresh');
				toastr.success('操作成功');
			}
		</script>
</body>

</html>
