package com.centrin.veritas.service.impl;
/**
 * Project Name：veritas
 * File Name：VeritasDbParamServiceImpl
 * Package Name：com.centrin.veritas.service.impl
 * Date：2017/8/30 下午5:18
 */

import com.centrin.common.dao.BaseDao;
import com.centrin.common.dao.QueryFilter;
import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseServiceImpl;
import com.centrin.veritas.dao.VeritasDbParamDao;
import com.centrin.veritas.entity.VeritasDbParamEntity;
import com.centrin.veritas.service.VeritasDbParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service.impl
 * @ClassName VeritasDbParamServiceImpl
 * @author 张辉
 * @date 2017/8/30 下午5:18
 * @version
 */
@Service
public class VeritasDbParamServiceImpl extends BaseServiceImpl<VeritasDbParamEntity> implements VeritasDbParamService {

    @Autowired
    private VeritasDbParamDao veritasDbParamDao;

    @Override
    protected BaseDao getSuperBaseDao() {
        return veritasDbParamDao;
    }

    @Override
    public List<VeritasDbParamEntity> findByInstanceIdList(Integer instanceId) {
        QueryFilter qf = QueryFilter.newFilter();
        qf.andEq("instanceId", instanceId);
        qf.orderBy("id", QueryFilter.Order.DESC);
        return findAsList(VeritasDbParamEntity.class, qf);
    }
}
