package com.centrin.veritas.dao.impl;
/**
 * Project Name：veritas
 * File Name：VeritasDbDaoImpl
 * Package Name：com.centrin.veritas.dao.impl
 * Date：2017/8/30 下午4:59
 */

import com.centrin.common.dao.BaseDaoImpl;
import com.centrin.veritas.dao.VeritasDbDao;
import org.springframework.stereotype.Repository;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao.impl
 * @ClassName VeritasDbDaoImpl
 * @author 张辉
 * @date 2017/8/30 下午4:59
 * @version
 */
@Repository
public class VeritasDbDaoImpl extends BaseDaoImpl implements VeritasDbDao{
}
