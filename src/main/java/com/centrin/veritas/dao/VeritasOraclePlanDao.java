package com.centrin.veritas.dao;
/**
 * Project Name：veritas
 * File Name：VeritasOraclePlanDao
 * Package Name：com.centrin.veritas.dao
 * Date：17/7/26 下午4:24
 */

import com.centrin.common.dao.BaseDao;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao
 * @ClassName VeritasOraclePlanDao
 * @author 张辉
 * @date 17/7/26 下午4:24
 * @version
 */
public interface VeritasOraclePlanDao extends BaseDao{
}
