/** 
 * Project Name：jfshop2.0 
 * File Name：aaaaa.java 
 * Package Name：com.centrin.common.utils 
 * Date：2017-3-22 下午1:57:49 
 * Copyright (c) 2017, Centrin Data Systems Ltd. All Rights Reserved. 
 * 中金数据系统有限公司 
 */
package com.centrin.common.utils;

import java.util.Iterator;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.common.utils
 * @ClassName ReqBeanValidator
 * @author john.he   
 * @date 2017-3-22 下午1:57:49
 * @version 
 */
public class ReqBeanValidator {

	public static String validate(Object obj) {
		StringBuffer buffer = new  StringBuffer(); //用于存储验证后的错误信息   
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();  
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(obj);//验证某个对象,，其实也可以只验证其中的某一个属性的   
        Iterator<ConstraintViolation<Object>> iter = constraintViolations.iterator();  
        while  (iter.hasNext()) {  
            String message = iter.next().getMessage();  
            buffer.append(message+"\n");  
        }  
        return buffer.toString();
	}
}
