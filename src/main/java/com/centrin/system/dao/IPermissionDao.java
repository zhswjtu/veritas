package com.centrin.system.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.centrin.system.entity.PermissionEntity;
import com.centrin.system.entity.UserEntity;

@Repository
public interface IPermissionDao extends PagingAndSortingRepository<PermissionEntity, Integer>, JpaSpecificationExecutor<UserEntity> {
}
