
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html lang="en">
<head>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<title>用户角色管理</title>
</head>
<body>
	<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
	<link rel="stylesheet"href="${ctx}/resource-Main/userList/userList_new.css">
		<div class="mainpanel_mian">
					<!-- main start -->
					<div class="contentpanel">
						<div class="row">
							<div class="col-sm-12">
								<!--list start-->
								<div class="panel panel-default">
									<div class="panel panel-default">
									<div class="panel-heading zj-panel-heading-2 clearfix"  style="padding: 10px 0;">
										<h5 class="panel-title zj-panel-title-2" style="float: left; height: 30px; line-height: 30px;">角色组件</h5>
										<button type="button" class="btn btn-primary" style="float:right; height: 30px; width:70px;" onclick="window.history.go(-1);"><i class="glyphicon glyphicon-backward" style="margin-right: 7px"></i>返回 </button>
									</div>
								</div>
									<div class="panel-body zj-list-body">
										<div class="row">
											<div class="col-sm-12">
												<div class="table-responsive zj-table-responsive">
													<table class="table mb30 table-hover table-responsive" id="roleTb">

													</table>

												</div>
											</div>
											<div class="sub_btn">
												<div class="col-sm-3"></div>
												<div class="col-sm-6" style="text-align: center">
													<input id="submit" type="submit" class="btn btn-primary sub1"value="确定"> 
													<input type="button" id="close" class="btn btn-primary sub2"  onclick="window.history.go(-1);" value="取消">
												</div>
												<div class="col-sm-3"></div>
											</div>
										</div>

									</div>
								</div>
								<!--list end-->
							</div>
						</div>
					</div>
				</div>				
	<script>
    var roleTb;
    $(function () {
        roleTb = $('#roleTb').bootstrapTable({
            method: "get",
            url: '${ctx}/system/module/json',
            cache: false,
            fit : true,
            fitColumns : true,
            border : false,
            idField : 'id',
            uniqueId: 'id',
            pagination:true,
            checkbox:true,
            rownumbers:true,
            showRefresh: false,
 /*            pageNumber:1,
            pageSize : 10,
            sidePagination: 'server',
            pageList : [ 10, 20, 30, 40, 50 ], */
            singleSelect:false,
           /*  striped:true, */
            search:false,
            clickToSelect:true,
            columns: [
                {field:'ck',checkbox:true},
                {field:'id',title:'id',visible:false,align: 'center',width:100},
                {field:'title',title:'组件名称',align: 'center',width:100},
                {field:'code',title:'组件编码',align: 'center',width:100},
                {
    				field : 'type',
    				title : '类型',
    				 align: 'center',
    				width : 80 ,
    				formatter : function(val, rec) {
    					if (val == '1') {
    						return '表格';
    					} else if (val == '2') {
    						return '列表';
    					} else if (val == '3') {
    						return '图表';
    					} else if (val == '4') {
    						return '按钮';
    					} 
    				} 
    			},
                {field:'description',title:'描述',width:100,align: 'center',tooltip: true}
            ],
            onLoadSuccess:function(ss){
                //获取用户拥有角色,选中
                $.ajax({
                    async:false,
                    type:'get',
                    cache: false,
                    url:"${ctx}/system/module/${roleId}/module",
                    success: function(data){
                        if(data){
                            for(var i=0,j=data.length;i<j;i++){
                                roleTb.bootstrapTable('checkBy',{field:'id',values:[data[i].id]});
                            }
                        }
                    }
                });

            },
            dataPlain: true
        });

        $("#submit").on('click', function(e) {
            var list = $('#roleTb').bootstrapTable('getAllSelections');
            var ids = [];
            for(var i = 0; i < list.length; i++){
                ids.push(list[i].id);
            }
            $.ajax({
                async:false,
                type:'POST',
                data:JSON.stringify(ids),
                contentType:'application/json;charset=utf-8',	//必须
                url:"${ctx}/system/module/${roleId}/updateModule",
                success: function(data){
                    if(data=='success'){
                        	toastr.success('操作成功');
                       /*  location.href="${ctx}/system/role" */
                    }
                }
            });
        });
    });
</script>
</body>

</html>