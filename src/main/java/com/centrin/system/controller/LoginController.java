package com.centrin.system.controller;

import javax.servlet.http.HttpServletRequest;

import com.centrin.system.shiro.realm.ShiroUser;
import com.centrin.system.utils.UserUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.centrin.common.utils.StringUtils;
import com.centrin.system.shiro.authc.UsernamePasswordCaptchaToken;

@Controller
public class LoginController {

	private Logger log = LoggerFactory.getLogger(getClass());

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String toLogin(HttpServletRequest request, Model model) {
		return "system/login";
	}
	
	
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String login(HttpServletRequest request, Model model) {
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		if(StringUtils.isBlank("username") ||StringUtils.isBlank("password")) {
			model.addAttribute("error", "org.apache.shiro.authc.UnknownAccountException");
			return "login";
		}
		
		Subject subject = SecurityUtils.getSubject();
		String sessionCaptcha = (String) subject.getSession().getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
		subject.logout();//解决会话标识未更新问题
		subject.getSession().setAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY, sessionCaptcha);	
        try {
        	UsernamePasswordCaptchaToken token = new UsernamePasswordCaptchaToken(); 
        	token.setPassword(password.toCharArray());
        	token.setRememberMe(false);
        	token.setUsername(username);
        	token.setHost("");  
        	subject.login(token);
            ShiroUser user = UserUtil.getCurrentShiroUser();
            model.addAttribute("user", user);
            return "home";
        }catch(Exception e){
        	model.addAttribute("error", e.getClass().getName());
        	return "system/login";
        }
	}
	
	@RequestMapping(value="logout")
	public String logout(Model model) {
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "system/login";
	}

}
