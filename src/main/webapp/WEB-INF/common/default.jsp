<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<html lang="en">
<head>
    <%@ include file="/WEB-INF/common/bootstrap.jsp"%>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="images/favicon.png" type="image/png">
    <title>云管理平台</title>

    <decorator:head/>

</head>
<body>
<!--加载效果-->
<div id="preloader">
    <div id="status">
        <i class="fa fa-spinner fa-spin"></i>
    </div>
</div>
<section> <!-- leftpanel start -->
    <div class="leftpanel">
        <div class="logopanel">
            <h1>
                <span>[</span> 云管理平台 <span>]</span>
            </h1>
        </div>
        <div id="sidebar-menu" class="scrollable-content active" tabindex="5000"
             style="overflow: hidden; outline: none;">
            <h5 class="sidebartitle"></h5>
            <ul class="one_ul nav nav-bracket">
                <li class="current-page">
                    <a href="#" title="总览"><i class="fa fa-home"></i> <span>总览</span></a>
                </li>
                <li>
                    <a href="javascript:;" title="云主机"><i class="glyphicon glyphicon-home"></i> <span>我的主页</span></a>
                    <ul class="second_ul">
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>部门主管</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>运维主管</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>运维审核</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>运维复核</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>系统维护</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>网络维护</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>线路实施</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>线路复核</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>线路主管</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>线路主管</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>线路审核</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>客服经理</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>客服主管</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>BD工程师</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" title="云服务产品"><i class="glyphicon glyphicon-th-list"></i> <span>云服务产品</span></a>
                    <ul class="second_ul">
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>产品分类</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>产品名称</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>产品规格</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>渠道商管理</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" title="客户管理"><i class="glyphicon glyphicon-user"></i> <span>客户管理</span></a>
                    <ul class="second_ul">
                        <li><a href="#" title="带宽网络"><i class="fa fa-caret-right"></i>客户列表</a></li>
                        <li><a href="#" title="私有网络"><i class="fa fa-caret-right"></i>客户配额</a></li>
                        <li><a href="#" title="VPN服务"><i class="fa fa-caret-right"></i>客户配置</a></li>
                        <li><a href="#" title="带宽网络"><i class="fa fa-caret-right"></i>客户合同</a></li>
                        <li><a href="#" title="私有网络"><i class="fa fa-caret-right"></i>客户产品管理</a></li>
                        <li><a href="#" title="VPN服务"><i class="fa fa-caret-right"></i>客户计费</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" title="客户资源管理"><i class="glyphicon glyphicon-folder-close"></i> <span>客户资源管理</span></a>
                    <ul class="second_ul">
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>主机服务</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>存储服务</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>增值服务</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>裸光纤服务</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>点对点服务</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>互联网服务</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>公网IP服务</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>私有网段服务</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>VPN服务</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" title="工单服务"><i class="glyphicon glyphicon-file"></i> <span>工单服务</span></a>
                    <ul class="second_ul">
                        <li><a href="javascript:;" onclick="menuClick('${ctx}/center')" title="工单中心"><i
                                class="fa fa-caret-right"></i>工单中心</a></li>
                        <li><a href="javascript:;" title="工单统计"><i class="fa fa-caret-right"></i>工单统计</a></li>
                        <li><a href="javascript:;" title="流程审批"><i class="fa fa-caret-right"></i>流程审批</a></li>
                    </ul>
                </li>
                <li><a href="javascript:;" title="客户工单"><i class="glyphicon glyphicon-list-alt"></i> <span>客户工单</span></a>
                    <ul class="second_ul">
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>客户咨询工单</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>客户半自助工单</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" title="内部工单中心"><i class="glyphicon glyphicon-tasks"></i> <span>内部工单中心</span></a>
                    <ul class="second_ul">
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>工单提交</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>工单中心</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>工单统计</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" title="报表中心"><i class="glyphicon glyphicon-list-alt"></i> <span>报表中心</span></a>
                    <ul class="second_ul">
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>试用统计</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>费用统计</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" title="消息中心"><i class="glyphicon glyphicon-envelope"></i> <span>消息中心</span></a>
                    <ul class="second_ul">
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>消息模板设置</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>通知人管理</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>消息发送管理</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>消息发送记录</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" title="帮助中心"><i class="glyphicon glyphicon-question-sign"></i><span>帮助中心</span></a>
                    <ul class="second_ul">
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>用户指南页面</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>常见问题</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" title="知识库"><i class="glyphicon glyphicon-book"></i> <span>知识库</span></a>
                </li>
                <li>
                    <a href="javascript:;" title="审计中心"><i class="glyphicon glyphicon-eye-open"></i> <span>审计中心</span></a>
                    <ul class="second_ul">
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>任务记录</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>操作记录</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" title="资产管理"><i class="	glyphicon glyphicon-usd"></i> <span>资产管理</span></a>
                </li>
                <li>
                    <a href="javascript:;" title="系统设置"><i class="	glyphicon glyphicon-cog"></i> <span>系统设置</span></a>
                    <ul class="second_ul">
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>工作流设置</a></li>
                        <li><a href="#" title=""><i class="fa fa-caret-right"></i>定时任务</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" title="组织机构和权限"><i class="fa fa-suitcase"></i> <span>组织机构和权限</span></a>
                    <ul class="second_ul">
                        <li><a href="javascript:;" title="部门管理"><i class="fa fa-caret-right"></i>部门管理</a></li>
                        <li><a href="javascript:;"  onclick="menuClick('${ctx}/system/user')" title="用户管理"><i class="fa fa-caret-right"></i>用户管理</a></li>
                        <li><a href="javascript:;" title="角色管理"><i class="fa fa-caret-right"></i>角色管理</a></li>
                        <li><a href="javascript:;" title="菜单管理"><i class="fa fa-caret-right"></i>菜单管理</a></li>
                        <li><a href="javascript:;" title="权限管理"><i class="fa fa-caret-right"></i>权限管理</a></li>
                        <li><a href="javascript:;" title="字典管理"><i class="fa fa-caret-right"></i>字典管理</a></li>
                        <li><a href="javascript:;" title="角色主页"><i class="fa fa-caret-right"></i>角色主页</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;" title="任务中心"><i class="glyphicon glyphicon-flag"></i> <span>任务中心</span></a>
                </li>
            </ul>
        </div>
    </div>
    <!-- leftpanel end -->
    <div class="mainpanel">
        <!-- header start -->
        <div class="headerbar">
            <a class="menutoggle"><i class="fa fa-bars"></i></a>

            <div class="header-right">
                <ul class="headermenu">
                    <li>
                        <div class="btn-group">
                            <button class="btn btn-default dropdown-toggle tp-icon"
                                    data-toggle="dropdown">
                                <span class="fa fa-question-circle"></span> <span
                                    class="headerbar-tip">站内消息</span>
                            </button>
                        </div>
                    </li>
                    <li>
                        <div class="btn-group">
                            <button class="btn btn-default dropdown-toggle tp-icon"
                                    data-toggle="dropdown">
                                <span class="fa fa-question-circle"></span> <span
                                    class="headerbar-tip">帮助</span>
                            </button>
                        </div>
                    </li>
                    <li>
                        <div class="btn-group">
                            <button class="btn btn-default dropdown-toggle tp-icon"
                                    data-toggle="dropdown">
                                <span class="fa fa-file-text-o"></span> <span
                                    class="headerbar-tip">工单</span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                                <li><a href="#"><span
                                        class="pull-right badge badge-danger">3</span>待处理工单</a></li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <img src="${ctx}/resource-Main/main/images/photos/loggeduser.png" alt=""/> 李雷 <span
                                    class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                                <li><a href="#"><i class="glyphicon glyphicon-user"></i>账户信息</a></li>
                                <li><a href="#"><i class="glyphicon glyphicon-cog"></i>修改密码</a></li>
                                <li><a href="#"><i class="glyphicon glyphicon-log-out"></i>退出</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- header-right -->

        </div>
        <!-- header end -->
        <!-- main start -->
        <div class="contentpanel">
            <div class="row">
                <!-- <iframe id="iframe-page-content" src="" width="100%" frameborder="no" border="0" marginwidth="0"
                        marginheight="0" scrolling="no" allowtransparency="yes"></iframe> -->
                   
                        <decorator:body/>
            </div>
        </div>
        <!-- main end -->
    </div>
</section>
<!-- 弹出层 -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    提示
                </h4>
            </div>
            <div class="modal-body">
                删除后无法恢复您确定删除？
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">
                    确定
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal -->
</div>
<!--重置密码-->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    提示
                </h4>
            </div>
            <div class="modal-body">
                确定重置密码为111111吗？
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">
                    确定
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal -->
</div>

<script type="text/javascript">
    $(function () {
        var height = document.documentElement.clientHeight;
        document.getElementById('iframe-page-content').style.height = height + 'px';
    });

    var menuClick = function (menuUrl) {
        $("#iframe-page-content").attr('src', menuUrl);
    };
   
</script>

</body>

</html>