package com.centrin.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.centrin.common.dto.RspBaseInfo;
import com.centrin.system.entity.PermissionEntity;
import com.centrin.system.error.SysException;
import com.centrin.system.interceptor.WriteLog;
import com.centrin.system.service.PermissionService;
import com.centrin.system.utils.MyBeanUtils;
import com.centrin.system.utils.UserUtil;

/**
 * 权限controller
 */
@Controller
@RequestMapping("system/permission")
public class PermissionController{

	@Autowired
	private PermissionService permissionService;

	
	/**
	 * 默认页面
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String list(){
		return "system/permissionList";
	}
	
	/**
	 * 菜单页面
	 */
	@RequestMapping(value="menu",method = RequestMethod.GET)
	public String menuList(){
		return "system/menuList";
	}
	
	
	/**
	 * 菜单集合(JSON)
	 */
	@RequiresPermissions("sys:perm:menu:view")
	@RequestMapping(value="menu/json",method = RequestMethod.GET)
	@ResponseBody
	public List<Map<String, Object>>  menuDate(){
		List<PermissionEntity> roots =permissionService.getRootMenus();
		List<Map<String,Object>> lists = new ArrayList<Map<String,Object>>();
		for (int i = 0; i < roots.size(); i++) {
			PermissionEntity permissionEntity = roots.get(i);
			Map<String,Object> bean = MyBeanUtils.transBean2Map(roots.get(i));
			bean.put("children", "");
			
			List<PermissionEntity> permissionEntitys =  permissionService.getMenusByPid(permissionEntity.getId());
			if(permissionEntitys.size()>0){
				List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
				for (int j = 0; j < permissionEntitys.size(); j++) {
					Map<String,Object> m = MyBeanUtils.transBean2Map(permissionEntitys.get(j));
					list.add(m);
				}
			 bean.put("children", list);
			}
			lists.add(bean);
		}
		return lists;
	}
	
	/**
	 * 添加菜单跳转
	 */
	@RequestMapping(value = "menu/create", method = RequestMethod.GET)
	public String menuCreateForm(Model model,HttpServletRequest request) {
		Integer pid = Integer.valueOf(request.getParameter("pid"));
		PermissionEntity permissionEntity  = new PermissionEntity();
		if(pid!=null&&pid!=-1){
			PermissionEntity p = permissionService.get(pid);
			permissionEntity.setPid(pid);
			model.addAttribute("pName",p.getName());
		}
		model.addAttribute("permission", permissionEntity);
		model.addAttribute("action", "create");
		return "system/menuForm";
	}
	/**
	 * 添加菜单跳转
	 */
	@RequestMapping(value = "menu/icon", method = RequestMethod.GET)
	public String menuCreateForm(Model model) {
		return "system/icon";
	}
	
	
	/**
	 * 修改菜单跳转
	 */
	@RequestMapping(value = "menu/update/{id}", method = RequestMethod.GET)
	public String updateMenuForm(@PathVariable("id") Integer id, Model model) {
		PermissionEntity permissionEntity  = permissionService.get(id);
		if(permissionEntity.getPid()!=null){
			PermissionEntity p = permissionService.get(permissionEntity.getPid());
			model.addAttribute("pName",p.getName());
		}
		model.addAttribute("permission",permissionEntity);
		model.addAttribute("action", "update");
		return "system/menuForm";
	}
	
	
	/**
	 * 权限集合(JSON)
	 */
	@RequiresPermissions("sys:perm:view")
	@RequestMapping(value="json",method = RequestMethod.GET)
	@ResponseBody //getAllValidPermissionsByPid
	public List<Map<String,Object>> getData() {
		List<PermissionEntity> roots =permissionService.getRootMenus();
		List<Map<String,Object>> lists = new ArrayList<Map<String,Object>>();
		for (int i = 0; i < roots.size(); i++) {
			PermissionEntity permissionEntity = roots.get(i);
			Map<String,Object> bean = MyBeanUtils.transBean2Map(roots.get(i));
			bean.put("children", "");
			
			List<PermissionEntity> sons =  permissionService.getAllValidPermissionsByPid(permissionEntity.getId());
			if(sons.size()>0){
				List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
				for (int j = 0; j < sons.size(); j++) {
					PermissionEntity son = sons.get(j);
					Map<String,Object> sonMap = MyBeanUtils.transBean2Map(sons.get(j));
					sonMap.put("children", "");
					
					List<PermissionEntity> suns =  permissionService.getAllValidPermissionsByPid(son.getId());
					if(suns.size()>0){
						List<Map<String,Object>> sunlist = new ArrayList<Map<String,Object>>();
						for (int k = 0; k < suns.size(); k++) {
							Map<String,Object> sunMap = MyBeanUtils.transBean2Map(suns.get(k));
							sunlist.add(sunMap);
						}
						sonMap.put("children", sunlist);
					}
					list.add(sonMap);
				}
			 bean.put("children", list);
			}
			lists.add(bean);
		}
		return lists;
	}
	
	
	/**
	 * 获取菜单下的操作
	 */
	@RequiresPermissions("sys:perm:view")
	@RequestMapping("operation/json")
	@ResponseBody
	public Map<String, Object> menuOperationData(Integer pid){
		List<PermissionEntity> menuOperList=permissionService.getOperationsByMenuId(pid);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", menuOperList);
		map.put("total",menuOperList.size());
		return map;
	}
	
	
	/**
	 * 当前登录用户的权限集合
	 */
	@RequestMapping("currentUser/json")
	@ResponseBody
	public List<PermissionEntity> myPermissionData() {
//		return UserUtil.getCurrentShiroUser().getPermissions();
		return permissionService.getPermissionsByUser(UserUtil.getCurrentShiroUser());
	}
	
	
	
	/**
	 * 添加权限跳转
	 */
	@RequestMapping(value = "create/{pid}", method = RequestMethod.GET)
	public String createForm(@PathVariable("pid") Integer pid,Model model) {
		PermissionEntity permission = new PermissionEntity();
		permission.setPid(pid);
		model.addAttribute("permission", permission);
		model.addAttribute("action", "create");
		model.addAttribute("pname", permissionService.get(pid).getName());
		return "system/permissionForm";
	}
	
	
	/**
	 * 添加权限/菜单
	 */
	@RequiresPermissions("sys:perm:add")
	@WriteLog("新增权限/菜单")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public String create(PermissionEntity permission, Model model) {
		permissionService.add(permission);
		return "success";
	}
	
	
	/**
	 * 添加菜单基础操作
	 * @param pid
	 * @return
	 */
	@RequiresPermissions("sys:perm:add")
	@WriteLog("添加菜单基础操作")
	@RequestMapping(value = "createBase/{pname}/{pid}", method = RequestMethod.GET)
	@ResponseBody
	public String create(@PathVariable("pname") String pname,@PathVariable("pid") Integer pid){
		permissionService.addBaseOperation(pid, pname);
		return "success";
	}
	
	/**
	 * 修改权限跳转
	 */
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Integer id, Model model) {
		PermissionEntity permission = permissionService.get(id);
		model.addAttribute("permission", permission);
		model.addAttribute("pname", permissionService.get(permission.getPid()).getName());
		model.addAttribute("action", "update");
		return "system/permissionForm";
	}
	
	
	/**
	 * 修改权限/菜单
	 */
	@RequiresPermissions("sys:perm:update")
	@WriteLog("修改权限/菜单")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String update(@ModelAttribute("permission") PermissionEntity permission,Model model) {
		permissionService.update(permission);
		return "success";
	}

	/**
	 * 删除权限
	 */
	@RequiresPermissions("sys:perm:delete")
	@WriteLog("删除权限/菜单")
	@RequestMapping(value = "delete/{id}")
	@ResponseBody
	public RspBaseInfo delete(@PathVariable("id") Integer id) {
		try {
			permissionService.delete(id);
			return new RspBaseInfo(RspBaseInfo.SUCCESS,"删除成功！");
		} catch(SysException e) {
			return new RspBaseInfo(RspBaseInfo.ERROR,e.getErrMsg());
		}
	}
	
	
//	@ModelAttribute
//	public void getPermission(@RequestParam(value = "id", defaultValue = "-1") Integer id, Model model) {
//		if (id != -1) {
//			model.addAttribute("permission", permissionService.get(id));
//		}
//	}
}
