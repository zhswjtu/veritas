package com.centrin.veritas.dao.impl;
/**
 * Project Name：veritas
 * File Name：VeritasOraclePlanDaoImpl
 * Package Name：com.centrin.veritas.dao.impl
 * Date：17/7/26 下午4:24
 */

import com.centrin.common.dao.BaseDaoImpl;
import com.centrin.veritas.dao.VeritasOraclePlanDao;
import org.springframework.stereotype.Repository;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao.impl
 * @ClassName VeritasOraclePlanDaoImpl
 * @author 张辉
 * @date 17/7/26 下午4:24
 * @version
 */
@Repository
public class VeritasOraclePlanDaoImpl extends BaseDaoImpl implements VeritasOraclePlanDao {
}
