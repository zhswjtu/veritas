package com.centrin.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.centrin.common.controller.BaseController;
import com.centrin.common.entity.PagingEntity;
import com.centrin.system.entity.PermissionEntity;
import com.centrin.system.entity.RoleEntity;
import com.centrin.system.interceptor.WriteLog;
import com.centrin.system.service.RoleService;

/**
 * 权限controller
 */
@Controller
@RequestMapping("system/role")
public class RoleController extends BaseController{

	@Autowired
	private RoleService roleService;
	
	/**
	 * 默认页面
	 */
	@RequestMapping(value = "module/{roleId}", method = RequestMethod.GET)
	public String list(@PathVariable("roleId") Integer roleId,Model model){
		model.addAttribute("roleId", roleId);
		return "system/roleModuleList";
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String list(){
		return "system/roleList";
	}
	
	/**
	 * 获取角色
	 */
	@RequiresPermissions("sys:role:view")
	@RequestMapping("/json")
	@ResponseBody
	public Map<String, Object> listDatas(@RequestParam Map<String, Object> paramMap,HttpServletRequest request){
		PagingEntity<RoleEntity> page = getPage(request);
		page =roleService.findRoleAll(paramMap,page);
		return getData(page);
	}
	
	
	/**
	 * 添加角色跳转
	 */
	@RequestMapping(value = "create", method = RequestMethod.GET)
	public String createForm(Model model) {
		model.addAttribute("role", new RoleEntity());
		model.addAttribute("action", "create");
		return "system/roleForm";
	}
	
	
	@RequestMapping("/check")
	@ResponseBody
	public Map<String, Object> check(HttpServletRequest request,@RequestParam Map<String, Object> paramMap) {
		int count= roleService.checkRole(paramMap);
		Map<String, Object> map = new HashMap<String, Object>();
		if(count>0){
			map.put("msg", false);
		}else{
			map.put("msg", true);
		}
		return map;
	}
	
	/**
	 * 添加角色
	 */
	@WriteLog("角色管理/添加角色")
	@RequiresPermissions("sys:role:add")
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@ResponseBody
	public String create(RoleEntity role, Model model) {
		roleService.add(role);
		return "success";
	}
	
	
	
	/**
	 * 修改角色跳转
	 */
	@RequestMapping(value = "update/{id}", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") Integer id, Model model) {
		model.addAttribute("role", roleService.get(id));
		model.addAttribute("action", "update");
		return "system/roleForm";
	}
	
	
	/**
	 * 修改角色
	 */
	@WriteLog("角色管理/修改角色")
	@RequiresPermissions("sys:role:update")
	@RequestMapping(value = "update", method = RequestMethod.POST)
	@ResponseBody
	public String update(@ModelAttribute("role") RoleEntity role,Model model) {
		roleService.update(role);
		return "success";
	}
	
	/**
	 * 删除角色
	 */
	@WriteLog("角色管理/删除角色")
	@RequiresPermissions("sys:role:delete")
	@RequestMapping(value = "delete/{id}")
	@ResponseBody
	public String delete(@PathVariable("id") Integer id) {
		roleService.delete(id);
		return "success";
	}
	
	/**
	 * 获取角色的权限集合
	 */
	@RequiresPermissions("sys:perm:view")
	@RequestMapping("{roleId}/json")
	@ResponseBody
	public List<PermissionEntity> getPermissionsByRoleId(@PathVariable("roleId") Integer roleId) {
		List<PermissionEntity> permissionList= roleService.getPermissionsByRoleId(roleId);
		return permissionList;
	}
	
	/**
	 * 
	 * update:(这里用一句话描述这个方法的作用). <br/> 
	 * @param role
	 * @param model
	 * @return 
	 * @author changtao
	 */
	@RequiresPermissions("sys:role:permUpd")
	@RequestMapping(value = "{roleId}/updatePermission", method = RequestMethod.POST)
	@ResponseBody
	public String updatePermission(@PathVariable("roleId") Integer roleId,
			@RequestBody List<Integer> newPermissionList) {
		roleService.updatePermission(roleId,newPermissionList);
		return "success";
	}
	
}
