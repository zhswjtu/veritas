package com.centrin.common.utils;
import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;
import jodd.io.FileUtil;


public final class PictureUploadUtils {
	
	public final static String TEMP_FOLDER = "temp";
	
	public final static String DATA_FOLDER = "data";
	
	public static String PIC_ROOT_PATH = ResourceBundle.getBundle("application").getString("pic.root.path");
	public static String QRCODE_ROOT_PATH = ResourceBundle.getBundle("application").getString("qrcode.root.path");
	
	/**
	 * 复制临时文件到正式文件
	 * @Title: copyPicToRealPath
	 * @Description:
	 * @param tmpPath
	 * @return
	 */
	public static String copyPicToRealPath(String tmpPath) {
		String tmp = "/" + TEMP_FOLDER + "/";
		int p = tmpPath.indexOf(tmp);
		if(p>=0) {
			String fileId = tmpPath.substring(p + tmp.length());
			String destPath = PIC_ROOT_PATH + "/" + DATA_FOLDER + "/" + fileId;
			String srcPath = PIC_ROOT_PATH + "/" + TEMP_FOLDER + "/" + fileId;
//			String folder = destPath.substring(0,destPath.lastIndexOf("/")+1);
//			System.out.println(srcPath);
//			System.out.println(destPath);
//			System.out.println(folder);
			try {
//				FileUtil.mkdirs(folder);
				FileUtil.copy(srcPath, destPath);
				return fileId;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return tmpPath;
	}
	
	/**
	 * 获取文件id
	 * @Title: getFildId
	 * @Description:
	 * @param path
	 * @return
	 */
	public static String getFildId(String path) {
		int p = path.lastIndexOf("/");
		if(p>0) {
			return path.substring(p-8);
		} else {
			return null;
		}
	}
	
	/**
	 * 判断是否为临时文件
	 * @Title: isTempFile
	 * @Description:
	 * @param path
	 * @return
	 */
	public static boolean isTempFile(String path) {
		if(path.indexOf("/"+TEMP_FOLDER+"/")>=0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 获取显示url
	 * @Title: getFullUrl
	 * @Description:
	 * @param fileId
	 * @return
	 */
	public static String getFullUrl(String fileId) {
		return "/picture/show/" + DATA_FOLDER + "/" +fileId;
	}
	
	/**
	 * 获取显示根路径
	 * @Title: getRootUrl
	 * @Description:
	 * @return
	 */
	public static String getRootUrl() {
		return "/picture/show/" + DATA_FOLDER + "/" ;
	}
	
	public static String getQrcodeUrl(){
		File file = new File(QRCODE_ROOT_PATH);
		if(!file.exists()){
			file.mkdirs();
		}
		return file.getPath();
				
	}
	
	public  static final void main(String[] args) {
		//copyPicToRealPath("/flowbank/picture/show/temp/20160612/0e62171e-fef8-4d39-9afc-3627b16bc9cc.png");
//		System.out.println(getFildId("/flowbank/picture/show/temp/20160612/0e62171e-fef8-4d39-9afc-3627b16bc9cc.png"));
		System.out.println(isTempFile("/flowbank/picture/show/temp/20160612/0e62171e-fef8-4d39-9afc-3627b16bc9cc.png"));
	}
	
}
