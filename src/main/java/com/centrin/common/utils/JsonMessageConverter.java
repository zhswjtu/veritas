package com.centrin.common.utils;

import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.support.converter.AbstractMessageConverter;
import org.springframework.amqp.support.converter.MessageConversionException;
import com.alibaba.fastjson.JSON;

/**
 * 
 * @Title：
 * @Description：
 * @Package com.centrin.common.utils
 * @ClassName JsonMessageConverter
 * @author john.he   
 * @date 2017-6-5 下午4:20:47
 * @version
 */
public class JsonMessageConverter extends AbstractMessageConverter {
    private static final String DEFAULT_CHARSET = "UTF-8";
    private Map<String, Class<?>> classMap = new ConcurrentHashMap<String, Class<?>>();

    /**
     * @Title: createMessage
     * @Description: 序列化 Message
     * @param object
     * @param messageProperties
     * @return
     */
    @Override
    protected Message createMessage(Object object, MessageProperties messageProperties) {
        byte[] bytes = null;
        try {
            String jsonString = JSON.toJSONString(object);
            bytes = jsonString.getBytes(DEFAULT_CHARSET);
        } catch (UnsupportedEncodingException e) {
            throw new MessageConversionException("Failed to convert Message content", e);
        }
        messageProperties.setContentEncoding(DEFAULT_CHARSET);
        messageProperties.setContentType(object.getClass().getName());
        if (bytes != null) {
            messageProperties.setContentLength(bytes.length);
        }
        return new Message(bytes, messageProperties);
    }

    /**
     * @Title: fromMessage
     * @Description: 反序列化 Message
     * @param message
     * @return
     * @throws MessageConversionException
     */
    @Override
    public Object fromMessage(Message message) throws MessageConversionException {
        Class<?> clazz = this.getType(message.getMessageProperties().getContentType());
        return JSON.parseObject(message.getBody(), clazz);
    }

    /**
     * @Title: getType
     * @Description: 获取类型
     * @param contentType
     * @return
     * @throws MessageConversionException
     */
    private Class<?> getType(String contentType) throws MessageConversionException {
        if (!this.classMap.containsKey(contentType)) {
            try {
                Class<?> clazz = Class.forName(contentType);
                this.classMap.put(contentType, clazz);
            } catch (ClassNotFoundException e) {
                throw new MessageConversionException(e.getMessage());
            }
        }
        return this.classMap.get(contentType);
    }

}