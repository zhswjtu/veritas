package com.centrin.system.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import com.centrin.common.entity.BaseEntity;

/**
 * 权限 entity
 */
@Entity
@Table(name = "sys_permission")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class PermissionEntity extends BaseEntity implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer pid;
	private String name;
	private Integer type;
	private Integer sort;
	private String url;
	private String icon;
	private String permCode;
	private String description;
	private Integer status = 1;
	
	
	private Set<RoleEntity> roles = new HashSet<RoleEntity>(0);
	
	
	// Constructors

	/** default constructor */
	public PermissionEntity() {
	}

	/** minimal constructor */
	public PermissionEntity(String name) {
		this.name = name;
	}
	
	public PermissionEntity(Integer id) {
		this.id=id;
	}
	
	public PermissionEntity (Integer id,Integer pid,String name){
		this.id=id;
		this.pid=pid;
		this.name=name;
	}
	
	public PermissionEntity (Integer pid,String name, Integer type,String url,String permCode){
		this.pid=pid;
		this.name=name;
		this.type=type;
		this.url=url;
		this.permCode=permCode;
	}

	/** full constructor */
	public PermissionEntity(Integer pid, String name, Integer type, Integer sort,
			String url, String icon, String permCode, String description,
			Integer status) {
		this.pid = pid;
		this.name = name;
		this.type = type;
		this.sort = sort;
		this.url = url;
		this.icon = icon;
		this.permCode = permCode;
		this.description = description;
		this.status=status;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "pid")
	public Integer getPid() {
		return this.pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "type", length = 20)
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "sort")
	public Integer getSort() {
		return this.sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Column(name = "url", length = 256)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "icon", length = 256)
	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	@Column(name = "perm_code", length = 50)
	public String getPermCode() {
		return this.permCode;
	}

	public void setPermCode(String permCode) {
		this.permCode = permCode;
	}

	@Column(name = "description", length = 1024)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	@Column(name = "status", length = 1)
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}


//	@JsonIgnore
//	@ManyToMany(mappedBy = "permissions", fetch = FetchType.LAZY)
//	public Set<RoleEntity> getRoles() {
//		return roles;
//	}
//
//	public void setRoles(Set<RoleEntity> roles) {
//		this.roles = roles;
//	}

	

    
}