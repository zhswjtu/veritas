package com.centrin.veritas.dao;
/**
 * Project Name：veritas
 * File Name：VeritasDbDao
 * Package Name：com.centrin.veritas.dao
 * Date：2017/8/30 下午4:59
 */

import com.centrin.common.dao.BaseDao;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.dao
 * @ClassName VeritasDbDao
 * @author 张辉
 * @date 2017/8/30 下午4:59
 * @version
 */
public interface VeritasDbDao extends BaseDao{
}
