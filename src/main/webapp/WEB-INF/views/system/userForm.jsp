
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<html lang="en">
<head>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<title>用户管理</title>
</head>
<body>
	<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
	<link rel="stylesheet" href="${ctx}/resource-Main/userList/userList_new.css">
				<!--加载效果 start-->
				<div id="preloader">
					<div id="status">
						<i class="fa fa-spinner fa-spin"></i>
					</div>
				</div>
				<!--加载效果 end-->
				<section> <!--main start-->
				<div class="mainpanel_mian">
					<!-- main start -->
					<div class="contentpanel">
						<div class="row" style="background-color: #fff;">
							<div class="col-sm-12">
								<!--list start-->
								<div class="panel panel-default">
									<div class="panel-heading zj-panel-heading-2 clearfix"  style="padding: 10px 0;">
										<h5 class="panel-title zj-panel-title-2" style="float: left; height: 30px; line-height: 30px;">添加用户</h5>
										<button type="button" class="btn btn-primary" style="float:right; height: 30px; width:70px;" onclick="window.history.go(-1);"><i class="glyphicon glyphicon-backward" style="margin-right: 7px"></i>返回 </button>
									</div>
								</div>
								<form class="form-horizontal" role="form" id="mainform" action="${ctx }/system/user/${action}" method="post">
									<div class="form-group">
										<label class="col-sm-3 control-label"
											style="padding-right: 1.7em">用户名：</label>
										<div class="col-sm-6">
											<input type="hidden" name="id" value="${user.id }" /> <input
												class="form-control" type="text" id="username"
												name="username" value="${user.username }"
												onblur="checkUsername()">
										</div>
									</div>
									<div class="form-group" id="password-i">
										<label class="col-sm-3 control-label"
											style="padding-right: 2.7em">密码：</label>
										<div class="col-sm-6">
											<input class="form-control" type="password" id="password"
												name="password" value="${user.password }">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label"
											style="padding-right: 2.7em">昵称：</label>
										<div class="col-sm-6">
											<input class="form-control" type="text" name="name"
												value="${user.name }">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">出生日期：</label>
										<div class="col-sm-6">
											<input class="form-control" type="date" name="birthday"
												value="${user.birthday}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label"
											style="padding-right: 2.7em">性别：</label>
										<div class="col-sm-6">
											<div class="user_sexchoose">
												<label for="man"> <input type="radio" id="man"
													name="gender" value="1"> 男 </label> <label for="woman">
													<input type="radio" id="woman" name="gender" value="0">
														女 
												</label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label"
											style="padding-right: 2.7em">部门：</label>
										<div class="col-sm-6">
											<select name="depId" value="${user.depId }" class="select-o">
												<c:forEach var="org" items="${orgs}">
													<option value='${org.id}'>${org.name}</option>
												</c:forEach>
											</select>

										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label"
											style="padding-right: 2.7em">Email：</label>
										<div class="col-sm-6">
											<input class="form-control" type="email" name="email"
												value="${user.email}">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label"
											style="padding-right: 2.7em">手机号：</label>
										<div class="col-sm-6">
											<input class="form-control" type="phone" name="phone"
												value="${user.phone}">
										</div>
									</div>
									<div class="form-group" style="margin-bottom: 20px">
										<label class="col-sm-3 control-label"
											style="padding-right: 2.7em">描述：</label>
										<div class="col-sm-8">
											<textarea class="form-control area-orgList" type="text"
												name="description" style="height: 100px;">${user.description}</textarea>
										</div>
									</div>
									<div class="sub_btn">
										<div class="col-sm-3"></div>
										<div class="col-sm-6" style="text-align: center">
											<input type="button" id="confirm" class="btn btn-primary sub1" value="确定" onclick="save()">
											<input type="button" id="close" class="btn btn-primary sub2"  onclick="window.history.go(-1);" value="取消">
										</div>
										<div class="col-sm-3"></div>
									</div>
								</form>
								<!--list end-->
							</div>
						</div>
					</div>
				</div>
				</section>

				<script type="text/javascript">
					var action = "${action}";
					//用户 添加修改区分
					if (action == 'create') {
						$("input[name='gender'][value='1']").attr("checked",true);
					} else if (action == 'update') {
						$("input[name='username']").attr('readonly', 'readonly');
						$("input[name='username']").css('background', '#eee');
						 $('#password-i').attr('hidden','hidden');
						$("input[name='gender'][value=${user.gender}]").attr("checked", true);
					}

					function save() {
						$('#mainform').bootstrapValidator('validate');
					}
					function checkUsername() {
						var username = $('#username').val();
						$.ajax({
							url : '${ctx}/system/user/checkLoginName',
							method : 'POST',
							data : {
								'username' : username
							},
							success : function(opt) {
								if (opt == 'false') {
									falg_user = true;
									$('#username-i').attr('class',
											'has-error col-md-4');
									$('#username').tooltip('hide');
								} else {
									falg_user = false;
								}
							}
						});
					}
					//提交表单
					$(document).ready(function() {

						$('#mainform').bootstrapValidator({
							container : 'tooltip',
							/* trigger: 'blur', */
							feedbackIcons : {
								valid : 'glyphicon glyphicon-ok',
								invalid : 'glyphicon glyphicon-remove',
								validating : 'glyphicon glyphicon-refresh'
							},
							fields : {
								username : {
									validators : {
										notEmpty : {
											message : '登录名不能为空'
										}
									}
								},
								password : {
									validators : {
										notEmpty : {
											message : '密码不能为空'
										},
										stringLength : {
											/*长度提示*/
											min : 6,
											max : 30,
											message : '密码长度必须在6到30之间'
										},
										different : {//不能和用户名相同
											field : 'username',//需要进行比较的input name值
											message : '不能和用户名相同'
										},
										regexp : {
											regexp : /^[a-zA-Z0-9_\.]+$/,
											message : '密码由数字字母下划线和.组成'
										}
									}
								},
								email : {
									validators : {
										notEmpty : {
											message : '邮件地址不能为空'
										},
										emailAddress : {
											message : '请输入合法的邮件地址'
										}
									}
								},
								phone : {
									validators : {
										notEmpty : {
											message : '手机号不能为空'
										},
										phone : {
											message : '请填写合法的手机号',
											country : 'CN'
										}
									}
								}
							}
						}).on('success.form.bv', function(e) {
							var $form = $(e.target);
							$form.ajaxSubmit(function(data) {
								if (data == 'success') {
									location.href = "${ctx}/system/user";
								} else {

								}
							});
						});
					});
				</script>
</body>
</html>