package com.centrin.common.servcie;

public interface IncrementIdService {
	
	public Integer getId();
	
	public Integer getId(String name);
	
	public Integer getId(String name, long begin);
	
	public Integer getId(String name, long begin,boolean dayReset);
	
	public Long getLongId();
	
	public Long getLongId(String name);
	
	public Long getLongId(String name, long begin);
}
