package com.centrin.common.dto.model.rsp;

public class RspDeviceBean extends RspBean {
	private static final long serialVersionUID = 1L;

	private RspDeviceHeadBean head;

	public RspDeviceHeadBean getHead() {
		return head;
	}

	public void setHead(RspDeviceHeadBean head) {
		this.head = head;
	}
	

}
