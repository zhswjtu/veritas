package com.centrin.common.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ResourceBundle;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jodd.io.FileUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.centrin.common.utils.DateUtils;


@RequestMapping(value="/kindeditor", method={RequestMethod.GET, RequestMethod.POST})
public class KindeditorUploadController {
	
	private static String picRootPath = null;
	
	static {
		picRootPath = ResourceBundle.getBundle("application").getString("pic.path");
	}
	
	
	/**
	 * @Title: upload
	 * @Description:	图片上传
	 * @param cImage
	 * @return
	 */
    @RequestMapping("/upload")
    @ResponseBody
	public String upload(@RequestParam MultipartFile imgFile, HttpServletRequest request) {
		if (imgFile == null) {
			return "{\"error\":1," + "\"message\":\"上传文件为空。\"}";
		}
    	String fileUrl = null, message="";
    	StringBuffer picPath = new StringBuffer();
    	StringBuffer fileId = new StringBuffer();
		try {
			String dateDir = DateUtils.getDate("yyyyMMdd");
			picPath.append(picRootPath);
			picPath.append("/");
			FileUtil.mkdirs(picPath.toString() + dateDir); //创建目录
			
			fileId.append(dateDir);
			fileId.append("/");
			fileId.append(UUID.randomUUID());
			String oriFilename = imgFile.getOriginalFilename();
			String prefix=oriFilename.substring(oriFilename.lastIndexOf(".")+1);
			fileId.append(".");
			fileId.append(prefix);
			
			picPath.append(fileId);
			FileUtil.writeBytes(picPath.toString(), imgFile.getBytes()); //写文件
			
			fileUrl = request.getContextPath() + "/kindeditor/picture?fileId=" + fileId.toString();
			return "{\"error\":0," + "\"url\":\"" + fileUrl + "\"}";
		} catch (Exception e) {
			e.printStackTrace();
			message = e.getMessage();
		}
		return "{\"error\":1," + "\"url\":\"" + message + "\"}";
	}
    
    
    /**
	 * @Title: upload
	 * @Description:	图片上传
	 * @param cImage
	 * @return
     * @throws IOException 
	 */
    @RequestMapping("/picture")
	public void show(@RequestParam String fileId, HttpServletRequest request,
            HttpServletResponse response) throws IOException {
    	
    	String prefix=fileId.substring(fileId.lastIndexOf(".")+1).toLowerCase();
    	
    	response.reset();
    	response.setHeader("Pragma", "No-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		response.setContentType("image/" + prefix);
		
    	String picPath = picRootPath + "/" + fileId;
    	File file = new File(picPath);

       //判断文件是否存在如果不存在就返回默认图标
       if(!(file.exists() && file.canRead())) {
//           file = new File(request.getSession().getServletContext().getRealPath("/")
//                   + "resource/icons/auth/root.png");
       }

//		response.addHeader("Content-Disposition", "attachment;filename=" + 
//				new String(file.getName().getBytes("GB18030"), "iso8859-1"));
 
		OutputStream out = null;
		FileInputStream in = null;
		try {
			out = response.getOutputStream();
			in = new FileInputStream(file);

			byte[] b = new byte[1024];
			int i = 0;

			while ((i = in.read(b)) > 0) {
				out.write(b, 0, i);
			}
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
				in = null;
			}	
		}
		
	}
}
