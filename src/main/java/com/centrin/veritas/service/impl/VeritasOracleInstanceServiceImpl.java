package com.centrin.veritas.service.impl;
/**
 * Project Name：veritas
 * File Name：VeritasOracleInstanceServiceImpl
 * Package Name：com.centrin.veritas.service.impl
 * Date：17/7/24 下午8:29
 */

import com.centrin.common.dao.BaseDao;
import com.centrin.common.dao.QueryFilter;
import com.centrin.common.entity.PagingEntity;
import com.centrin.common.servcie.BaseServiceImpl;
import com.centrin.common.utils.StringUtils;
import com.centrin.veritas.dao.VeritasOracleInstanceDao;
import com.centrin.veritas.entity.VeritasOracleDbInstanceEntity;
import com.centrin.veritas.entity.VeritasOracleDbInstanceParamEntity;
import com.centrin.veritas.service.VeritasOracleInstanceService;
import com.centrin.veritas.utils.SystemUtils;
import com.centrin.veritas.utils.VeritasConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.service.impl
 * @ClassName VeritasOracleInstanceServiceImpl
 * @author 张辉
 * @date 17/7/24 下午8:29
 * @version
 */
@Service
public class VeritasOracleInstanceServiceImpl extends BaseServiceImpl implements VeritasOracleInstanceService {

    @Autowired
    private VeritasOracleInstanceDao veritasOracleInstanceDao;

    @Override
    protected BaseDao getSuperBaseDao() {
        return veritasOracleInstanceDao;
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 实例列表
     * @date 17/7/25
     */
    @Override
    @Transactional
    public PagingEntity<VeritasOracleDbInstanceEntity> findPage(PagingEntity<VeritasOracleDbInstanceEntity> page,
                                                                Map<String, String> param) {
        QueryFilter qf = QueryFilter.newPagingFilter(page.getPageNum(), page.getSize());
        if (StringUtils.isNotBlank(param.get("head"))){
            qf.andLk("chapterHead", "%" + param.get("head") + "%");
        }
        String ip = SystemUtils.getHostIp(SystemUtils.getInetAddress());
        if (StringUtils.isNotBlank(ip)){
            qf.andEq("ip", ip);
        }
        qf.orderBy("id", QueryFilter.Order.DESC);
        return findAsPaging(VeritasOracleDbInstanceEntity.class, qf);
    }

    List<VeritasOracleDbInstanceEntity> findInstanceList(){
        QueryFilter qf = QueryFilter.newFilter();
        String ip = SystemUtils.getHostIp(SystemUtils.getInetAddress());
        if (StringUtils.isNotBlank(ip)){
            qf.andEq("ip", ip);
        }
        return findAsList(VeritasOracleDbInstanceEntity.class, qf);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 获取实例属性列表
     * @date 17/7/25
     */
    @Override
    @Transactional
    public List<VeritasOracleDbInstanceParamEntity> findParamList(Integer instanceId) {
        QueryFilter qf = QueryFilter.newFilter();
        qf.andEq("instanceId", instanceId);
        return findAsList(VeritasOracleDbInstanceParamEntity.class, qf);
    }

    @Override
    @Transactional
    public void addParam(VeritasOracleDbInstanceEntity veritasOracleDbInstanceEntity, Integer instanceId) {
        String ip = SystemUtils.getHostIp(SystemUtils.getInetAddress());
        veritasOracleDbInstanceEntity.setIp(ip);
        veritasOracleInstanceDao.add(veritasOracleDbInstanceEntity);
        List<VeritasOracleDbInstanceParamEntity> list = findParamList(instanceId);
        for (VeritasOracleDbInstanceParamEntity veritasOracleDbInstanceParamEntity : list){
            veritasOracleDbInstanceParamEntity.setInstanceId(veritasOracleDbInstanceEntity.getId());
            veritasOracleInstanceDao.update(veritasOracleDbInstanceParamEntity);
        }
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 生成db配置文件
     * @date 17/7/25
     */
    @Override
    @Transactional
    public void instanceBuild() {
        List<VeritasOracleDbInstanceEntity> listInstance = findInstanceList();
        BufferedWriter bw = null;
        try {
            //获取文件
            File file = new File(VeritasConstant.ORACLE_DB_PATH + File.separator + "db.conf");
            File fileParent = file.getParentFile();
            if (!fileParent.exists()){
                fileParent.mkdirs();
            }
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
            bw = new BufferedWriter(fileWriter);
            for (VeritasOracleDbInstanceEntity veritasOracleDbInstanceEntity : listInstance){
                String chapterHead = veritasOracleDbInstanceEntity.getChapterHead();
                chapterHead = chapterHead.replace("[", "").replace("]", "");
                bw.write("[" + chapterHead + "]");
                bw.newLine();
                bw.write(veritasOracleDbInstanceEntity.getSidName()
                        + " = " + veritasOracleDbInstanceEntity.getSidValue());
                bw.newLine();
                bw.write(veritasOracleDbInstanceEntity.getClientName()
                        + " = " + veritasOracleDbInstanceEntity.getClientNameValue());
                bw.newLine();
                if (veritasOracleDbInstanceEntity.getStandByDatabaseStatus() == 0){
                    bw.write(veritasOracleDbInstanceEntity.getStandByDatabase() + " = " + veritasOracleDbInstanceEntity.getStandByDatabaseValue());
                    bw.newLine();
                }
                List<VeritasOracleDbInstanceParamEntity> listParam = findParamList(veritasOracleDbInstanceEntity.getId());
                for (VeritasOracleDbInstanceParamEntity veritasOracleDbInstanceParamEntity : listParam){
                    String key = veritasOracleDbInstanceParamEntity.getParameterKey() == null
                            ? "" : veritasOracleDbInstanceParamEntity.getParameterKey();
                    String value = veritasOracleDbInstanceParamEntity.getParameterValue() == null
                            ? "" : veritasOracleDbInstanceParamEntity.getParameterValue();
                    String instance = key + " = " + value;
                    bw.write(instance);
                    bw.newLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 删除实例 及 该实例下的属性
     * @date 17/7/25
     */
    @Override
    @Transactional
    public void instanceDelete(Integer id) {
        List<VeritasOracleDbInstanceParamEntity> list = findParamList(id);
        for (VeritasOracleDbInstanceParamEntity veritasOracleDbInstanceParamEntity : list){
            veritasOracleInstanceDao.delete(VeritasOracleDbInstanceParamEntity.class, veritasOracleDbInstanceParamEntity.getId());
        }
        veritasOracleInstanceDao.delete(VeritasOracleDbInstanceEntity.class, id);
    }

    /**
     * @param
     * @return
     * @author 张辉
     * @description 创建计划 获取DB配置实例列表
     * @date 2017/8/31
     */
    @Override
    public List<VeritasOracleDbInstanceEntity> findList() {
        QueryFilter qf = QueryFilter.newFilter();
        String ip = SystemUtils.getHostIp(SystemUtils.getInetAddress());
        if (StringUtils.isNotBlank(ip)){
            qf.andEq("ip", ip);
        }
        return findAsList(VeritasOracleDbInstanceEntity.class, qf);
    }
}
