/*
 * Copyright 2016-2017 the original author or authors.
 */
package com.centrin.common.dao;

import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.search.FullTextSession;

import com.centrin.common.dao.BaseDaoImpl.QueryType;

public interface BaseDao {
	
	public <T> T findById(Class<T> c, int id);
	
	public <T> T findById(Class<T> c, long id);
	
	public <T> T findById(Class<T> c, String id);
	
	public <T> T getForUpdate(Class<T> c, int id);
	
	public <T> T getForUpdate(Class<T> c, long id);
	
	public <T> T getForUpdate(Class<T> c, String id);
	
	
	public <T> T getForRead(Class<T> c, int id);
	
	public <T> T getForRead(Class<T> c, long id);
	
	public <T> T getForRead(Class<T> c, String id);
	
	
	public <T> List<T> findAll(Class<T> c);

	public <T> List<T> findAll(Class<T> c, String orderBy);
	
	public <T> List<T> findAll(Class<T> c, int start, int limit, String orderBy);
	
	public <T> Integer count(Class<T> c);
	
	public <T> T update(T t);
	
	public <T> void add(T t);
	
	public <T> void add(List<T> list);
	
	public <T> void delete(Class<T> c, int id);

	public <T> void delete(Class<T> c, long id);

	public <T> void deletes(Class<T> c, List<Integer> ids);
	
	public <T> List<T> find(Class<T> c, String value, Map<String, Object> params,
			int start, int limit, QueryType queryType);
	
	public <T> List<T> findBySql(String sql);
	
	public <T> List<T> findBySql(Class<T> c, String sql);
	
	public <T> List<T> findBySql(String sql, Map<String, Object> params);
	
	public <T> List<T> findBySql(Class<T> c, String sql, Map<String, Object> params);
	
	public <T> List<T> findBySql(String sql, Map<String, Object> params,
			int start, int limit);
	
	public <T> List<T> findBySql(Class<T> c, String sql, Map<String, Object> params,
			int start, int limit);
	
	public <T> List<T> findByJpql(String jpql);
	
	public <T> List<T> findByJpql(Class<T> c, String jpql);
	
	public <T> List<T> findByJpql(String jpql, Map<String, Object> params);
	
	public <T> List<T> findByJpql(Class<T> c, String jpql, Map<String, Object> params);
	
	public <T> List<T> findByJpql( String jpql, Map<String, Object> params,
			int start, int limit);
	
	public <T> List<T> findByJpql(Class<T> c, String jpql, Map<String, Object> params,
			int start, int limit);
	
	public Integer count(String value, Map<String, Object> params, QueryType queryType);
	
	public Integer countBySql(String sql);
	
	public Integer countBySql(String sql, Map<String, Object> params);
	
	
	public Integer countByJpql(String sql);
	
	public Integer countByJpql(String sql, Map<String, Object> params);
	
	public void executeSql(String[] sqls);
	
	public Integer executeSql(String sql);
	
	public <T> T getSingle(Class<T> c, String value, Map<String, Object> params, QueryType queryType);

	public <T> T getSingleByJpql(String Jpql, Map<String, Object> params);
	
	public <T> T getSingleByJpql(Class<T> c, String Jpql);
	
	
	public <T> T getSingleByJpql(Class<T> c, String Jpql, Map<String, Object> params);
	
	
	public <T> T getSingleBySql(Class<T> c, String sql);
	
	public <T> T getSingleBySql(String sql, Map<String, Object> params);
	
	public <T> T getSingleBySql(Class<T> c, String sql, Map<String, Object> params);
	
	/**
	 * 获取 Session
	 */
	public Session getSession();
	

	/**
	 * 获取全文Session
	 */
	public FullTextSession getFullTextSession();
	
	/**
	 * 建立索引
	 */
	public <T> void createFullTextIndex(Class<T> c);
}
