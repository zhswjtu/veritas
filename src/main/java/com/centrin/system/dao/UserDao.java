package com.centrin.system.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.centrin.common.dao.SuperBaseDao;
import com.centrin.common.entity.PagingEntity;
import com.centrin.common.utils.DateUtils;
import com.centrin.system.entity.OrgEntity;
import com.centrin.system.entity.RoleEntity;
import com.centrin.system.entity.UserEntity;

@Repository
public class UserDao extends SuperBaseDao{

    /**
     * 通过用户名获取有效用户。
     * @param username
     * @return
     */
    public UserEntity getValidUserByUsername(String username){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("username", username);
        String jpql = "SELECT u FROM UserEntity u WHERE u.username = :username AND u.status = " 
        + UserEntity.STATUS_VALID + " AND u.delFlag = " + UserEntity.DEL_FLAY_NO;
        return super.getSingleByJpql(UserEntity.class, jpql, params);
    }

    
    /**
     * 通过用户名获取用户信息,包括逻辑删除的。
     * @param username
     * @return
     */
    public UserEntity getUserByUsername(String username){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("username", username);
        String jpql = "SELECT u FROM UserEntity u WHERE u.username = :username";
        return super.getSingleByJpql(UserEntity.class, jpql, params);
    }
    
    
    
    /**
     * 根据条件获取所有用户
     * @param map
     * @return
     */
    public PagingEntity<UserEntity> findUserList(Map<String, Object> map){
    	PagingEntity<UserEntity> page = (PagingEntity<UserEntity>) map.get("page");
        StringBuffer buffer = new StringBuffer(" where 1=1 and x.username != 'admin' and x.delFlag = :delFlag ");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("delFlag", UserEntity.DEL_FLAY_NO);
        if (null != map && null != map.get("phone") && !"".equals(map.get("phone"))){
            buffer.append(" and x.phone like :phone");
            params.put("phone", "%" + map.get("phone") + "%");
        }
        if(null != map && null != map.get("name") && !"".equals(map.get("name"))){
            buffer.append(" and x.name like :name");
            params.put("name", "%" + map.get("name") + "%");
        }
        if(null != map && null != map.get("beginDate") && !"".equals(map.get("beginDate"))){
            buffer.append(" and x.createTime >= :beginDate");
            params.put("beginDate", map.get("beginDate") + " 00:00:00");
        }
        if (null != map && null != map.get("endDate") && !"".equals(map.get("endDate"))){
            buffer.append(" and x.createTime <= :endDate");
            params.put("endDate", map.get("endDate") + " 23:59:59");
        }
        String sql1 = "select x from UserEntity x" + buffer.toString();
        if (null != page.getSort() && !"".equals(page.getSort())){
            sql1 = sql1 +  " order by " + page.getSort() + " " + page.getOrder();
        }
        String sql2 = "select count(x) from UserEntity x" + buffer.toString();
        List<UserEntity> list = findByJpql(UserEntity.class, sql1, params, page.getFirst(), page.getSize());
        Integer total = countByJpql(sql2,params);
        page.setRows(list);
        page.setTotal(total);
        return page;
    }


    /**
     * 通过userId获取用户信息
     * @param id
     * @return
     */
    public List<RoleEntity> getUserRoleIds(Integer id){
        String sql = "select r.* from sys_role r WHERE r.id IN " +
                "(select ur.role_id from sys_user_role ur WHERE ur.user_id = " + id + ")";

        return super.findBySql(RoleEntity.class,sql,null,0,0);
    }

    /**
     * 通过用户ID删除用户的所有角色
     * @param id
     */
    @Transactional
    public void deleteUserRoleByUserId(Integer id){
        String sql = "delete from sys_user_role where user_id = " + id;
        String[] sqls = {sql};
        executeSql(sqls);
    }

    /**
     * 添加用户角色
     * @param id
     * @param list
     */
    @Transactional
    public void addUserRole(Integer id, List<Integer> list){
        //删除用户下的权限
        String sqlDel = "delete from sys_user_role where user_id = " + id;
        String[] sqlDels = {sqlDel};
        executeSql(sqlDels);
        //添加用户新权限
        String[] sqls = new String[list.size()];
        for (int i = 0; i < list.size(); i++){
            String sql = "insert into sys_user_role(user_id, role_id) values(" + id + ","+list.get(i)+")";
            sqls[i] = sql;
        }
        executeSql(sqls);
    }

    /**
     * 添加用户部门关联关系
     * @param userId
     * @param orgId
     */
    @Transactional
    public void addUserOrg(Integer userId, Integer orgId){
        String sql = "delete from sys_user_org where user_id = " + userId;
//
//        String[] sqls = {sql};
//        executeSql(sqls);

        String insert = "insert into sys_user_org(user_id, org_id) values(" + userId + "," + orgId + ")";
        String[] inserts = {sql,insert};
        executeSql(inserts);
    }

    /**
     * 获取用户所在机构信息
     * @param userId
     * @return
     */
    @Transactional
    public List<OrgEntity> getOrgByUserId(Integer userId){
        String sql = "select o.* from sys_org o WHERE o.id IN (select u.org_id from sys_user_org u WHERE u.user_id = "+userId+")";
        return findBySql(OrgEntity.class,sql,null,0,0);
    }

    /**
     * 删除用户关联部门信息
     * @param userId
     */
    @Transactional
    public void deleUserOrgByUserId(Integer userId){
        String sql = "delete from sys_user_org where user_id = " + userId;
        String[] sqls = {sql};
        executeSql(sqls);
    }
    
    
    /**
     * 更新用户登录信息
     * @Title: updateUserLoginInfo
     * @Description:
     * @param userId
     */
    public void updateUserLoginInfo(Integer userId){
    	StringBuffer sb = new StringBuffer();
    	sb.append("update sys_user set previous_visit=last_visit,last_visit='");
    	sb.append(DateUtils.getDateTime());
    	sb.append("',login_count=login_count+1 where id=");
    	sb.append(userId);
        executeSql(sb.toString());
    }

    /**
     * 获取该机构下的所有员工
     * @param map
     * @return
     */
    public List<UserEntity> getUserByOrg(Map<String, Object> map){
        StringBuffer buffer = new StringBuffer("select * from sys_user where 1=1 and del_flag = 0");
//        if (!(boolean)map.get("flag")){
        buffer.append(" and dep_id = " + map.get("orgId"));
//        }
        return findBySql(UserEntity.class, buffer.toString());
    }
}
