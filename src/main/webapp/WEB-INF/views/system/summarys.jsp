<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>中华金融云运营平台</title>
<%@ include file="/common/easyui.jsp"%>
<script type="text/javascript"
	src="${ctx}/static/plugins/artTemplate/dist/template.js"></script>

<!-- 引入图形组件 -->
<script src="${ctx}/static/plugins/echarts/echarts.min.js"></script>
<!-- 引入 vintage 主题 -->
<script src="${ctx}/static/plugins/echarts/theme/macarons.js"></script>
<script src="${ctx}/static/plugins/echarts/theme/shine.js"></script>
<script src="${ctx}/static/plugins/echarts/theme/vintage.js"></script>
<script src="${ctx}/static/plugins/echarts/theme/roma.js"></script>
<script src="${ctx}/static/plugins/echarts/theme/infographic.js"></script>



<style type="text/css">
.chart_box_left {
	margin: 10px 0 10px 20px;
	padding: 10px;
	width: 500px;
	height: 320px;
	float: left;
	border: solid 1px #A3B5B5;
}

.chart_box_right {
	margin: 10px 0 10px 30px;
	padding: 10px;
	width: 500px;
	height: 320px;
	float: left;
	border: solid 1px #A3B5B5;
}
</style>
</head>
<body>
	<!-- 容器遮罩 -->
	<div id="maskContainer">
		<div class="datagrid-mask" style="display: block;"></div>
		<div class="datagrid-mask-msg"
			style="display: block; left: 50%; margin-left: -52.5px;">
			正在加载...</div>
	</div>
	<div class="easyui-layout" data-options="fit: true">
		<div
			data-options="region: 'center', split: false, border: false,fit: true"
			style="height: auto; background-color: #ffffff;">
			<div>
				<div style="text-align: center; font-size: 20px;">
					<p>平台客户统计</p>
					总客户数：<span id="customCount"></span>
				</div>
				<div style="width: 100%; height: 360px;">
					<div class="chart_box_left">
						<div id="chart1" style="width: 100%; height: 100%;"></div>
					</div>
					<div class="chart_box_right">
						<div id="chart2" style="width: 100%; height: 100%;"></div>
					</div>
				</div>
			</div>
			<div>
				<div style="text-align: center; font-size: 20px;">
					<p>平台服务统计</p>
					主机数量：<span id="hosts"></span>台&nbsp;&nbsp;&nbsp;&nbsp;存储容量数：<span
						id="stores"></span>G&nbsp;&nbsp;&nbsp;&nbsp; 带宽：<span id="nets"></span>M
				</div>
				<div style="width: 100%; height: 360px;">
					<div class="chart_box_left">
						<div id="chart3" style="width: 100%; height: 100%;"></div>
					</div>
					<div class="chart_box_right">
						<div id="chart4" style="width: 100%; height: 100%;"></div>
					</div>
				</div>
			</div>
			<div>
				<div style="text-align: center; font-size: 20px;">
					<p>平台收入统计</p>
					上月应收：<span id="standardcharge"></span>元&nbsp;&nbsp;&nbsp;&nbsp;上月实收：<span
						id="actualcharge"></span>元
				</div>
				<div style="width: 100%; height: 360px;">
					<div class="chart_box_left">
						<div id="chart5" style="width: 100%; height: 100%;"></div>
					</div>
					<div class="chart_box_right">
						<div id="chart6" style="width: 100%; height: 100%;"></div>
					</div>
				</div>
			</div>
			<div>
				<div style="text-align: center; font-size: 20px;">
					<p>平台工单统计</p>
					本月完成工单：<span id="ordercount"></span>
				</div>
				<div style="width: 100%; height: 360px;">
					<div class="chart_box_left">
						<div id="chart7" style="width: 100%; height: 100%;"></div>
					</div>
					<div class="chart_box_right">
						<div id="chart8" style="width: 100%; height: 100%;"></div>
					</div>
				</div>
			</div>
		</div>
		<div data-options="region: 'center', border: false"
			style="overflow: hidden;"></div>
	</div>
	</div>
	</div>
	</div>

	<script>
		//##############################统计图表#######################################
		var optionCustomLevel = {
			title : {
				text : '客户统计划分',
				subtext : '按客户级别',
				x : 'center'
			},
			tooltip : {
				trigger : 'item',
				formatter : "{a} <br/>{b} : {c} ({d}%)"
			},
			legend : {
				orient : 'vertical',
				left : 'left',
				data : [ '一级客户', '二级客户', '三级客户' ]
			},
			series : [ {
				name : '客户数量',
				type : 'pie',
				radius : '55%',
				center : [ '50%', '60%' ],
				data : [ {
					value : 335,
					name : '一级客户'
				}, {
					value : 310,
					name : '二级客户'
				}, {
					value : 234,
					name : '三级客户'
				} ],
				itemStyle : {
					emphasis : {
						shadowBlur : 10,
						shadowOffsetX : 0,
						shadowColor : 'rgba(0, 0, 0, 0.5)'
					}
				}
			} ]
		};
		var optionCustomAsc = {
			title : {
				text : '客户数量变化',
				subtext : '三个月对比'
			},
			tooltip : {
				trigger : 'axis',
				axisPointer : {
					type : 'shadow'
				}
			},
			legend : {
				data : [ '客户数量' ]
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			yAxis : {
				type : 'value',
				boundaryGap : [ 0, 0.1 ]
			},
			xAxis : {
				type : 'category',
				data : []
			},
			series : [ {
				name : '客户数量',
				type : 'bar',
				barWidth : 30,
				data : []
			} ]
		};
		var optionHosts = {
			title : {
				text : '本月云主机类型统计',
				left : 'center',
				x : 'center'
			},
			tooltip : {
				trigger : 'item',
				formatter : "{a} <br/>{b} : {c} ({d}%)"
			},
			legend : {
				x : 'center',
				y : 'bottom',
				data : []
			},
			calculable : true,
			series : [

			{
				name : '主机数量',
				type : 'pie',
				radius : [ 30, 110 ],
				center : [ '50%', '55%' ],
				roseType : 'area',
				data : []
			} ]
		};
		var useHostTop = {
			title : {
				text : '本月主机数量客户TOP3',
				subtext : ' 单位台'
			},
			tooltip : {
				trigger : 'axis',
				axisPointer : {
					type : 'shadow'
				}
			},
			legend : {
				data : [ '使用台数' ]
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			yAxis : {
				type : 'value',
				boundaryGap : [ 0, 0.1 ]
			},
			xAxis : {
				type : 'category',
				data : [ '巴西', '印尼', '美国' ]
			},
			series : [ {
				name : '使用台数',
				type : 'bar',
				barWidth : 30,
				data : [ 18, 23, 29 ]
			} ]
		};
		var charges = {
			title : {
				text : '上月实收统计',
				subtext : '单位元'
			},
			tooltip : {
				trigger : 'item',
				formatter : "{a} <br/>{b}: {c} ({d}%)"
			},
			legend : {
				orient : 'vertical',
				x : 'right',
				data : [ '云主机服务', '存储服务', '带宽服务', '增值服务', '其它服务' ]
			},
			series : [ {
				name : '服务费用',
				type : 'pie',
				radius : [ '40%', '55%' ],

				data : [ {
					value : 335,
					name : '云主机服务'
				}, {
					value : 310,
					name : '存储服务'
				}, {
					value : 234,
					name : '带宽服务'
				}, {
					value : 135,
					name : '增值服务'
				}, {
					value : 147,
					name : '其它服务'
				} ]
			} ]
		};
		var chargeTop = {
			title : {
				text : '上月实收客户TOP3',
				subtext : ' 单位元'
			},
			tooltip : {
				trigger : 'axis',
				axisPointer : {
					type : 'shadow'
				}
			},
			legend : {
				data : [ '实收费用' ]
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			yAxis : {
				type : 'value',
				boundaryGap : [ 0, 0.1 ]
			},
			xAxis : {
				type : 'category',
				data : []
			},
			series : [ {
				name : '实收费用',
				type : 'bar',
				barWidth : 30,
				data : []
			} ]
		};

		var workorders = {
			title : {
				text : '本月处理工单总量',
				subtext : '按工单类型区分'
			},
			tooltip : {
				trigger : 'axis',
				axisPointer : {
					type : 'shadow'
				}
			},
			legend : {
				data : [ '工单数量' ]
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			xAxis : {
				type : 'value',
				boundaryGap : [ 0, 0.01 ]
			},
			yAxis : {
				type : 'category',
				data : []
			},
			series : [ {
				name : '工单数量',
				type : 'bar',
				barWidth : 30,
				data : []
			} ]
		};
		var doneTop = {
			title : {
				text : '本月处理工单TOP3',
				subtext : ''
			},
			tooltip : {
				trigger : 'axis',
				axisPointer : {
					type : 'shadow'
				}
			},
			legend : {
				data : [ '处理数量' ]
			},
			grid : {
				left : '3%',
				right : '4%',
				bottom : '3%',
				containLabel : true
			},
			yAxis : {
				type : 'value',
				boundaryGap : [ 0, 0.1 ]
			},
			xAxis : {
				type : 'category',
				data : []
			},
			series : [ {
				name : '处理数量',
				type : 'bar',
				barWidth : 30,
				data : []
			} ]
		};
		var chart1 = echarts.init(document.getElementById('chart1'),
				'infographic');
		var chart2 = echarts.init(document.getElementById('chart2'), 'shine');
		var chart3 = echarts
				.init(document.getElementById('chart3'), 'macarons');
		var chart4 = echarts
				.init(document.getElementById('chart4'), 'macarons');
		var chart5 = echarts
				.init(document.getElementById('chart5'), 'macarons');
		var chart6 = echarts.init(document.getElementById('chart6'), 'shine');
		var chart7 = echarts.init(document.getElementById('chart7'), 'shine');
		var chart8 = echarts
				.init(document.getElementById('chart8'), 'macarons');

		chart1.setOption(optionCustomLevel);
		chart2.setOption(optionCustomAsc);
		chart3.setOption(optionHosts);
		chart4.setOption(useHostTop);
		chart5.setOption(charges);
		chart6.setOption(chargeTop);
		chart7.setOption(workorders);
		chart8.setOption(doneTop);

		$(function() {

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/custom/analysis",
				success : function(data) {
					$('#customCount').text(data.total);
					// 填入数据
					chart1.setOption({
						series : [ {
							// 根据名字对应到相应的系列
							name : '客户数量',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/custom/compare",
				success : function(data) {
					// 填入数据
					chart2.setOption({
						xAxis : {
							type : 'category',
							data : data.categories
						},
						series : [ {
							// 根据名字对应到相应的系列
							name : '客户数量',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/service/info",
				success : function(data) {
					$('#hosts').text(data.host);
					$('#stores').text(data.store);
					$('#nets').text(data.net);
					// 填入数据
					chart3.setOption({
						series : [ {
							// 根据名字对应到相应的系列
							name : '主机数量',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});
			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/service/hosttop",
				success : function(data) {
					// 填入数据
					chart4.setOption({
						xAxis : {
							type : 'category',
							data : data.categories
						},
						series : [ {
							// 根据名字对应到相应的系列
							name : '主机数',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/home/summary",
				success : function(data) {
					$('#actualcharge').text(data.actCharge);
					$('#standardcharge').text(data.stdCharge);
					// 填入数据
					chart5.setOption({
						series : [ {
							// 根据名字对应到相应的系列
							name : '实收费用',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/home/chargetop",
				success : function(data) {
					// 填入数据
					chart6.setOption({
						xAxis : {
							type : 'category',
							data : data.categories
						},
						series : [ {
							// 根据名字对应到相应的系列
							name : '实收费用',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/workorder/info",
				success : function(data) {
					$('#ordercount').text(data.total);
					// 填入数据
					chart7.setOption({
						yAxis : {
							type : 'category',
							data : data.categories
						},
						series : [ {
							// 根据名字对应到相应的系列
							name : '工单数量',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

			$.ajax({
				async : false,
				cache : false,
				type : 'get',
				url : "${ctx}/billing/workorder/ordretop",
				success : function(data) {
					$('#ordercount').text(data.total);
					// 填入数据
					chart8.setOption({
						xAxis : {
							type : 'category',
							data : data.categories
						},
						series : [ {
							// 根据名字对应到相应的系列
							name : '工单数量',
							data : data.data
						} ]
					});
				},
				error : function(e) {
					alert(e);
				}
			});

		});

		//##########################################################################
	</script>

</body>
</html>
