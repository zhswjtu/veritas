package com.centrin.system.controller;


import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.activiti.engine.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.centrin.system.entity.DicItemEntity;
import com.centrin.system.service.DicService;
import com.centrin.system.shiro.realm.ShiroUser;
import com.centrin.system.utils.UserUtil;

@Controller
public class SystemHomeController {

	private Logger log = LoggerFactory.getLogger(getClass());

//	@Autowired
//	private TaskService taskService;

	@Autowired
	private DicService dicService;


	@RequestMapping(value = "/system/home", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {

		log.info("Welcome home! the client locale is " + locale.toString());

		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,
				DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		model.addAttribute("serverTime", formattedDate);
		return "/system/index";
	}
	
	@RequestMapping(value = "/chart/summary", method = RequestMethod.GET)
	public String summary(Locale locale, Model model) {
		return "/system/summarys";
	}

	@RequestMapping(value = "/workorder/task/todo/count", method = RequestMethod.GET)
	@ResponseBody
	public String getTodoCount() {
		ShiroUser user = UserUtil.getCurrentShiroUser();
//		Long count = taskService.createTaskQuery()
//				.taskCandidateOrAssigned(user.getUsername()).count();
//		return count.toString();
        return null;
	}
	
	 public String getCustomLevelById(String itemValue){
	        List<DicItemEntity> list = dicService.findDicItem("KHDJ", itemValue);
	        if (null != list && list.size() > 0){
	            return list.get(0).getDisplayName();
	        }
	        return null;
	}
 
 
}
