package com.centrin.veritas.entity;
/**
 * Project Name：veritas
 * File Name：VeritasDbEntity
 * Package Name：com.centrin.veritas.entity
 * Date：2017/8/30 下午3:45
 * Copyright (c) 2017, Centrin Data Systems Ltd. All Rights Reserved.
 * 中金数据系统有限公司
 */

import com.centrin.common.entity.BaseEntity;

import javax.persistence.*;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.entity
 * @ClassName VeritasDbEntity
 * @author 张辉
 * @date 2017/8/30 下午3:45
 * @version
 */
@Entity
@Table(name = "veritas_db")
public class VeritasDbEntity extends BaseEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "client_name")
    private String clientName;

    @Column(name = "client_name_value")
    private String clientNameValue;

    @Column(name = "policy_name")
    private String policyName;

    @Column(name = "policy_name_value")
    private String policyNameValue;

    @Column(name = "version_name")
    private String versionName;

    @Column(name = "version_value")
    private String versionValue;

    @Column(name = "instance_name")
    private String instanceName;

    @Column(name = "instance_value")
    private String instanceValue;

    @Column(name = "user_id_name")
    private String userIdName;

    @Column(name = "user_id_value")
    private String userIdValue;

    @Column(name = "channel_name")
    private String channelName;

    @Column(name = "channel_value")
    private String channelValue;

    @Column(name = "log_mode_name")
    private String logModeName;

    @Column(name = "log_mode_value")
    private String logModeValue;

    @Column(name = "log_policy_name")
    private String logPolicyName;

    @Column(name = "log_policy_value")
    private String logPolicyValue;

    @Column(name = "chapter_name")
    private String chapterName;

    @Column(name = "description_name")
    private String descriptionName;

    @Column(name = "description_value")
    private String descriptionValue;

    private String rid;

    private String ip;

    public VeritasDbEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientNameValue() {
        return clientNameValue;
    }

    public void setClientNameValue(String clientNameValue) {
        this.clientNameValue = clientNameValue;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public String getPolicyNameValue() {
        return policyNameValue;
    }

    public void setPolicyNameValue(String policyNameValue) {
        this.policyNameValue = policyNameValue;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getVersionValue() {
        return versionValue;
    }

    public void setVersionValue(String versionValue) {
        this.versionValue = versionValue;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getInstanceValue() {
        return instanceValue;
    }

    public void setInstanceValue(String instanceValue) {
        this.instanceValue = instanceValue;
    }

    public String getUserIdName() {
        return userIdName;
    }

    public void setUserIdName(String userIdName) {
        this.userIdName = userIdName;
    }

    public String getUserIdValue() {
        return userIdValue;
    }

    public void setUserIdValue(String userIdValue) {
        this.userIdValue = userIdValue;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getChannelValue() {
        return channelValue;
    }

    public void setChannelValue(String channelValue) {
        this.channelValue = channelValue;
    }

    public String getLogModeName() {
        return logModeName;
    }

    public void setLogModeName(String logModeName) {
        this.logModeName = logModeName;
    }

    public String getLogModeValue() {
        return logModeValue;
    }

    public void setLogModeValue(String logModeValue) {
        this.logModeValue = logModeValue;
    }

    public String getLogPolicyName() {
        return logPolicyName;
    }

    public void setLogPolicyName(String logPolicyName) {
        this.logPolicyName = logPolicyName;
    }

    public String getLogPolicyValue() {
        return logPolicyValue;
    }

    public void setLogPolicyValue(String logPolicyValue) {
        this.logPolicyValue = logPolicyValue;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getDescriptionName() {
        return descriptionName;
    }

    public void setDescriptionName(String descriptionName) {
        this.descriptionName = descriptionName;
    }

    public String getDescriptionValue() {
        return descriptionValue;
    }

    public void setDescriptionValue(String descriptionValue) {
        this.descriptionValue = descriptionValue;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }
}
