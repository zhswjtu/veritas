package com.centrin.veritas.entity;
/**
 * Project Name：veritas
 * File Name：SnEntity
 * Package Name：com.centrin.veritas.entity
 * Date：17/8/7 下午12:42
 */

import com.centrin.common.entity.BaseEntity;

import javax.persistence.*;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.entity
 * @ClassName SnEntity
 * @author 张辉
 * @date 17/8/7 下午12:42
 * @version
 */
@Entity
@Table(name = "SN")
public class SnEntity extends BaseEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SN")
    private Integer sn;

    public SnEntity() {
    }

    public Integer getSn() {
        return sn;
    }

    public void setSn(Integer sn) {
        this.sn = sn;
    }
}
