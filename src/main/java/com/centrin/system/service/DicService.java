package com.centrin.system.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.centrin.system.dao.DicDao;
import com.centrin.system.entity.DicDefEntity;
import com.centrin.system.entity.DicItemEntity;
import com.centrin.system.error.SysException;


@Service
public class DicService {
	
	@Autowired
    private DicDao dicDao;
	
	
	@Cacheable(value="sysDicDefCache", key="#root.methodName")
	public List<DicDefEntity> findDicDefs() {
		return dicDao.findDicDefs();
	}
	
	@Cacheable(value="sysDicItemCache", key="#root.methodName.concat(#dicId)")
	public List<DicItemEntity> findDicItems(Integer dicId) {
		return dicDao.findDicItems(dicId);
	}
	

	@Cacheable(value="sysDicItemCache", key="#root.methodName.concat(#dicCode)")
	public List<DicItemEntity> findDicItems(String dicCode) {
		return dicDao.findDicItems(dicCode);
	}

    public List<DicItemEntity> findDicItem(String dicCode, String itemValue) {
        return dicDao.findDicItem(dicCode, itemValue);
    }
	

	
//	@Caching(evict={@CacheEvict(value="sysCache", key="deflist" )},
//    put={@CachePut(value="sysCache", key="#dicDef.getId().concat('_defidobj')"),
//         @CachePut(value="sysCache", key="#dicDef.getCode().concat('_defcodeobj')" )})
	@CacheEvict(value="sysDicDefCache", allEntries=true)
	public void defAdd(DicDefEntity dicDef) throws RuntimeException {
		String name = dicDef.getName();
		String code = dicDef.getCode();
		DicDefEntity oldDicDef = dicDao.getDicDef(name, code);
		if(oldDicDef!=null) {
			if(oldDicDef.getCode().equals(code)) {
				throw new SysException("字典编码已存在！");
			} else {
				throw new SysException("字典名称已存在！");
			}	
		} else {
			dicDao.add(dicDef);
		}
		
	}
	
	@CacheEvict(value="sysDicItemCache", allEntries=true)
	public void itemAdd(DicItemEntity dicItem)throws RuntimeException {
		String displayName = dicItem.getDisplayName();
		String itemValue = dicItem.getItemValue();
		Integer dicId = dicItem.getDicId();
		DicItemEntity oldDicItem = dicDao.getDicItem(dicId, displayName, itemValue);
		if(oldDicItem!=null) {
			if(oldDicItem.getItemValue().equals(itemValue)) {
				throw new SysException("字典键值已存在！");
			} else {
				throw new SysException("字典显示名称已存在！");
			}
		} else {
			dicDao.add(dicItem);
		}
	}
	
	
	@CacheEvict(value="sysDicDefCache", allEntries=true)
	public void defUpdate(DicDefEntity dicDef) throws RuntimeException {
		Integer id = dicDef.getId();
		String name = dicDef.getName();
		String code = dicDef.getCode();
		DicDefEntity oldDicDef = dicDao.getDicDef(id,name,code);
		if(oldDicDef!=null) {
			if(oldDicDef.getCode().equals(code)) {
				throw new SysException("字典编码已存在！");
			} else {
				throw new SysException("字典名称已存在！");
			}
		} else {
			dicDao.update(dicDef);
		}
	}
	
	
	@CacheEvict(value="sysDicItemCache", allEntries=true)
	public void itemUpdate(DicItemEntity dicItem)throws RuntimeException {
		Integer id = dicItem.getId();
		String displayName = dicItem.getDisplayName();
		String itemValue = dicItem.getItemValue();
		Integer dicId = dicItem.getDicId();
		DicItemEntity oldDicItem = dicDao.getDicItem(dicId, id, displayName, itemValue);
		if(oldDicItem!=null) {
			if(oldDicItem.getItemValue().equals(itemValue)) {
				throw new SysException("字典键值已存在！");
			} else {
				throw new SysException("字典显示名成已存在！");
			}
		} else {
			dicDao.update(dicItem);
		}
	}
	
	
	@CacheEvict(value="sysDicDefCache", allEntries=true)
	public void defDelete(Integer id) {
		DicDefEntity dicDef = dicDao.findById(DicDefEntity.class, id);
		dicDef.setDelFlag("1");
		dicDao.update(dicDef);
	}
	
	
	@CacheEvict(value="sysDicItemCache", allEntries=true)
	public void itemDelete(Integer id) {
		DicItemEntity dicItem = dicDao.findById(DicItemEntity.class, id);
		dicItem.setDelFlag("1");
		dicDao.update(dicItem);
	}

	
	@Cacheable(value="sysDicDefCache", key="#root.methodName.concat(#id)")
	public DicDefEntity defGet(Integer id) {
		return dicDao.findById(DicDefEntity.class, id);
	}


	@Cacheable(value="sysDicItemCache", key="#root.methodName.concat(#id)")
	public DicItemEntity itemGet(Integer id) {
		return dicDao.findById(DicItemEntity.class, id);
	}

}
