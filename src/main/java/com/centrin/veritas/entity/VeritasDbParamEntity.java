package com.centrin.veritas.entity;
/**
 * Project Name：veritas
 * File Name：VeritasDbParamEntity
 * Package Name：com.centrin.veritas.entity
 * Date：2017/8/30 下午4:16
 */

import com.centrin.common.entity.BaseEntity;

import javax.persistence.*;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.entity
 * @ClassName VeritasDbParamEntity
 * @author 张辉
 * @date 2017/8/30 下午4:16
 * @version
 */
@Entity
@Table(name = "veritas_db_param")
public class VeritasDbParamEntity extends BaseEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "instance_id")
    private Integer instanceId;

    @Column(name = "param_name")
    private String paramName;

    @Column(name = "param_value")
    private String paramValue;

    @Column(name = "param_id")
    private Integer paramId;

    public VeritasDbParamEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(Integer instanceId) {
        this.instanceId = instanceId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public Integer getParamId() {
        return paramId;
    }

    public void setParamId(Integer paramId) {
        this.paramId = paramId;
    }
}
