<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
	var ctx = '${pageContext.request.contextPath}';
</script>
<html lang="en">
<head>
<%@ include file="/WEB-INF/common/bootstrap.jsp"%>
<link rel="stylesheet" href="${ctx}/resource-Main/roleForm/roleForm.css">
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/list.css">
<title></title>
</head>

<body>
	<div id="preloader">
		<div id="status">
			<i class="fa fa-spinner fa-spin"></i>
		</div>
	</div>
	<!--加载效果 end-->
	<section>
		<div class="mainpanel_mian">
			<div class="contentpanel">
				<div class="row">
					<div class="panel panel-default">
						<div class="panel-heading zj-panel-heading-2">
							<h5 class="panel-title zj-panel-title-2">权限列表</h5>
						</div>
					</div>
					<div class="col-md-3">
						<div style="margin-top: 15px; margin-left: 0px;">
							<a class="btn btn-primary" id='add' href="javascript:void(0)" onclick="add();"> <i class="zj-icon zj-d-apply"></i>添加</a> 
						</div>
						<input name="menuId" id="menuId" type="hidden" /> 
						<input id="rowdata" type="hidden" />
						
							<div id="div1"  ></div>
						</div>
						<div class="col-md-9" >
							<div style="margin-top: 60px; margin-left: -5px;">
								<table id="table" style="height: 100px"></table>
							</div>
						</div>
					</div>
			</div>
		</div>
	</section>
	
	<script type='text/javascript' src='${ctx}/resource-Main/jquery-treegrid/js/TreeGrid.js' charset='utf-8'></script>
	<script type="text/javascript">
	var parentPermId;
	var parentPermName;
	  var table;
	  $(function() {
          table = $('#table').bootstrapTable({
	  		//height: getHeight(),
	  		method: "get",
	  		url: '${ctx}/system/permission/operation/json',
	  		cache: false,
	  		fit : true,
	  		fitColumns : true,
	  		border : false,
	  		idField : 'id',
	  		uniqueId: 'id',
	  		pagination:true,
	  		rownumbers:true,
	  		showRefresh: false,
	  		pageNumber:1,
	  		pageSize : 10,
	  		sidePagination: 'server',
	  		pageList : [ 10, 20],
	  		singleSelect:true,
	  		/* striped:true, */
	  		search:false,
	  		queryParams: function (params) {
	  		    var param = { 
	  		      limit: params.limit,   //页面大小
	  		      offset: params.offset,  //页码
	  		      order: params.order,
  		    	  orderBy: params.sort,
  		    	  pid: $("#menuId").val(),
	  		    };
	  		    return param;
	  		  },
	  	    columns: [
	  		    {field:'id',title:'id',visible:false,width:80},  
	  		    {field:'pos',title:'序号',width:10,
	  		    	formatter: function (value, row, index) {
	  		            return index+1;
	  		        }
	  		      },
	  		    {field:'name',title:'名称',width:100},
	  	        {field:'url',title:'访问路径',width:100},
	  	        {field:'permCode',title:'权限编码',width:100},        
	  	        {field:'status',title:'状态',width:100,
	  	        	formatter:function(val,rec) {
	  	        		if(val=='1') {
	  	        			return '启用'; 
	  	        		} else {
	  	        			return '禁用';  
	  	        		}  
	  	            }
	  	        },
	  	        {field:'sort',title:'排序'},
	  	        {field:'description',title:'描述',width:100},
		  	      {field : 'action',title : '操作',
		  				formatter : function(value, row, index) {
		  					var val = "";
		  					val+='<a href="${ctx}/system/permission/update/'+row.id+'" data-toggle="modal" >修改</a>&nbsp;&nbsp;';
		  					val+='<a href="javascript:del('+row.id+')">删除</a>&nbsp;&nbsp;';
		  					return val;
	  			}}
	  	        ],
	  	     toolbar:'#tg_tb',
	  		 dataPlain: true
	  	});
  	  });

	  function getHeight() {
	      return $(window).height() - $('#tg_tb').outerHeight(true)- 15;
	  }	
	  
	  function add(){
		  if(typeof(parentPermId)=="undefined"){ 
			  toastr.warning( '请选择要添加权限的菜单。');
		  }else{
		 	window.location.href= "${ctx}/system/permission/create/"+parentPermId;
		  }
	  }
	  
	//删除
	  function del(id){
	  	    Ewin.confirm({ message: "删除后无法恢复您确定要删除？" }).on(function (e) {  
		        if (!e) {  
		          return;  
		        }  
		        
  			$.ajax({
  				type:'get',
  				cache: false,
  				url:"${ctx}/system/permission/delete/"+id,
  				success: function(data){
  					if(data.code==0) {
  						toastr.success('删除成功');
  						table.bootstrapTable('refresh');
  	    			} else {
  	    				toastr.warning(data.msg);
  	    			}
  				}
  			});
	  	});

	  }
		var config = {
			id : "tg1",
			width : "800",
			renderTo : "div1",
			headerAlign : "left",
			headerHeight : "30",
			dataAlign : "left",
			indentation : "20",
			tbodyStyle: "max-height:800px;overflow:auto;display:block;",
			/*folderOpenIcon : "/images/folderOpen.gif",
			folderCloseIcon : "/images/folderClose.gif",
			defaultLeafIcon : "/images/defaultLeaf.gif",*/
			hoverRowBackground : "true",
			folderColumnIndex : "0",
			itemClick : "itemClickEvent",
			columns : [ {
				headerText : "名称",
				dataField : "name",
				headerAlign : "center",
				width : "280px"
			}],
			data : [],
			url : '${ctx}/system/permission/menu/json',
			method : 'get'
		};

		
		/*
		 * 单击数据行后触发该事件 id：行的id index：行的索引。 data：json格式的行数据对象。
		 */
		function itemClickEvent(id, index, data) {
			$("#button").show();
			
			$("#tg1").find("tr").each(function(){
					$(this).removeClass("row_active");
			  });
			
			$("tr[id="+id+"]").addClass("row_active");
			$("#menuId").val(data.id);
			
			parentPermName = data.name;
			parentPermId = data.id;
			
			table.bootstrapTable('refresh');
			//jQuery("#rowdata").val(parentPermId+"，"+parentPermName);
		}

		/*
		 * 通过指定的方法来自定义栏数据
		 */
		function customCheckBox(row, col) {
			return "<input type='checkbox'>";
		}

		function customOrgName(row, col) {
			var name = row[col.dataField] || "";
			return name;
		}
		function customStatus(row, col) {
			var val = row[col.dataField];
			if (val == '1') {
				return '启用';
			} else {
				return '禁用';
			}
			return name;
		}

		function customLook(row, col) {
			return "<a href='javascript:onCreate();' style='color:blue;'>新增</a>&nbsp;&nbsp;<a href='' style='color:blue;'>修改</a>&nbsp;&nbsp;<a href='' style='color:blue;'>删除</a>";
		}

		// 创建一个组件对象
		var treeGrid = new TreeGrid(config);
		treeGrid.show();

		function onCreate() {
			$('#myModal').modal({
				show : true
			})
		}
		/*
		 * 展开、关闭所有节点。 isOpen=Y表示展开，isOpen=N表示关闭
		 */
		function expandAll(isOpen) {
			treeGrid.expandAll(isOpen);
		}

		/*
		 * 取得当前选中的行，方法返回TreeGridItem对象
		 */
		function selectedItem() {
			var treeGridItem = treeGrid.getSelectedItem();
			if (treeGridItem != null) {
				// 获取数据行属性值
				// alert(treeGridItem.id + ", " + treeGridItem.index + ", " +
				// treeGridItem.data.name);

				// 获取父数据行
				var parent = treeGridItem.getParent();
				if (parent != null) {
					// jQuery("#currentRow").val(parent.data.name);
				}

				// 获取子数据行集
				var children = treeGridItem.getChildren();
				if (children != null && children.length > 0) {
					jQuery("#currentRow").val(children[0].data.name);
				}
			}
		}
	</script>
</body>

</html>
