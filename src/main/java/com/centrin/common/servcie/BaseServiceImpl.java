/*
 * Copyright 2016-2017 the original author or authors.
 */
package com.centrin.common.servcie;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.centrin.common.dao.BaseDao;
import com.centrin.common.dao.QueryFilter;
import com.centrin.common.entity.BaseEntity;
import com.centrin.common.entity.PagingEntity;


public abstract class BaseServiceImpl<T extends BaseEntity>
	implements BaseService<T> {
	
	@Override
	public void add(T entity) {
		getSuperBaseDao().add(entity);
	}

	@Override
	public T update(T entity) {
		return getSuperBaseDao().update(entity);
	}

	@Override
	public void delete(Class<T> clazz, int id) {
		getSuperBaseDao().delete(clazz, id);
	}
	
	@Override
	public void delete(Class<T> clazz, long id) {
		getSuperBaseDao().delete(clazz, id);
	}
	
	@Override
	@Transactional
	public <E> E getForUpdate(Class<E> c, int id){
		return getSuperBaseDao().getForUpdate(c, id);
	}
	
	@Override
	@Transactional
	public <E> E getForUpdate(Class<E> c, long id){
		return getSuperBaseDao().getForUpdate(c, id);
	}
	
	@Override
	@Transactional
	public <E> E getForUpdate(Class<E> c, String id){
		return getSuperBaseDao().getForUpdate(c, id);
	}
	
	@Override
	public T findById(Class<T> clazz, int id) {
		return getSuperBaseDao().findById(clazz, id);
	}
	
	@Override
	public T findById(Class<T> clazz, Long id) {
		return getSuperBaseDao().findById(clazz, id);
	}
	
	
	@Override
	public T findById(Class<T> clazz, String id) {
		return getSuperBaseDao().findById(clazz, id);
	}
	
	@Override
	public List<T> findAll(Class<T> clazz) {
		return getSuperBaseDao().findAll(clazz);
	}
	
	@Override
	public T findAsSingle(Class<T> clazz, QueryFilter queryFilter) {
		StringBuilder jpql = new StringBuilder();
		jpql.append(COMMON_SELECT_JPQL).append(clazz.getName()).append(COMMON_ENTITY_ALIAS);
		Map<String, Object> filterParam = null;
		
		if (queryFilter != null) {
			jpql.append(queryFilter.toWhereJPQL());
			jpql.append(queryFilter.buildOrderbySQL());
			jpql.append(queryFilter.buildGroupbySQL());
			filterParam = queryFilter.getFilterParam();
		}
		
		return getSuperBaseDao().getSingleByJpql(clazz, jpql.toString(), filterParam);
	}
	

	@Override
	public List<T> findAsList(Class<T> clazz, QueryFilter queryFilter) {
		StringBuilder jpql = new StringBuilder();
		jpql.append(COMMON_SELECT_JPQL).append(clazz.getName()).append(COMMON_ENTITY_ALIAS);
		Map<String, Object> filterParam = null;
		
		if (queryFilter != null) {
			jpql.append(queryFilter.toWhereJPQL());
			jpql.append(queryFilter.buildOrderbySQL());
			jpql.append(queryFilter.buildGroupbySQL());
			filterParam = queryFilter.getFilterParam();
			if (queryFilter.isPagingFilter()) {
				return getSuperBaseDao().findByJpql(
						clazz, jpql.toString(), filterParam, 
						queryFilter.getStart(), queryFilter.getLimit());
			}
		}
		
		return getSuperBaseDao().findByJpql(clazz, jpql.toString(), filterParam);
	}
	
	

	@Override
	public PagingEntity<T> findAsPaging(Class<T> clazz, QueryFilter queryFilter) {
		if (!queryFilter.isPagingFilter()) {
			throw new RuntimeException("The type of QueryFilter is not PagingQueryFilter!");
		}
		
		StringBuilder countJpql = new StringBuilder(COMMON_COUNT_JPQL)
				.append(clazz.getName()).append(COMMON_ENTITY_ALIAS);
		StringBuilder jpql = new StringBuilder(COMMON_SELECT_JPQL)
				.append(clazz.getName()).append(COMMON_ENTITY_ALIAS);
		Map<String, Object> filterParam = null;
		if (queryFilter != null) {
			String where = queryFilter.toWhereJPQL();
			countJpql.append(where);
			jpql.append(where)
			    .append(queryFilter.buildOrderbySQL());
			filterParam = queryFilter.getFilterParam();
		}
		
		PagingEntity<T> pagingEntity = new PagingEntity<T>();
		
		int total = getSuperBaseDao().countByJpql(countJpql.toString(), 
				filterParam);
		pagingEntity.setTotal(total);
		
		if (total == 0) {
			pagingEntity.setRows(new ArrayList<T>());
			return pagingEntity;
		}
		
		List<T> resultList = getSuperBaseDao().findByJpql(
				clazz, jpql.toString(), filterParam, 
				queryFilter.getStart(), queryFilter.getLimit());
		
		pagingEntity.setRows(resultList);
		
		return pagingEntity;
	}

	protected abstract BaseDao getSuperBaseDao();
}
