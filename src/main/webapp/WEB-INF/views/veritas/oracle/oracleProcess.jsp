<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html lang="en">
<head>
    <title>参数修改</title>
</head>
<body>
<link rel="stylesheet" href="${ctx}/resource-Main/change/work-order.css">
<link rel="stylesheet" href="${ctx}/resource-Main/main/css/modal.css">
<!--新增 modal-->
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">计划进度</h4>
</div>
<div class="modal-body" style="padding: 0;">
    <form class="form-horizontal" id="form_fibre" action="${ctx}/oracle/plan/${action}">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" id="process" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                            0%
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="table-responsive">
                        <table class="table table-hover table-responsive" id="exec_process">
                        </table>
                    </div>
                </div>
            </div>
            <!-- panel-body -->
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4 "></div>
                    <div class="col-sm-2 control-center">
                        <button type="button" class="btn btn-default zj-btn-button" data-dismiss="modal" id="closeBtn">
                            取消
                        </button>
                    </div>
                </div>
            </div>
            <!-- panel-footer -->
        </div>
    </form>
</div>
<!--新增 modal-->
<script>
    var $exec_process;
    var $url_exec_process = '${ctx}/oracle/plan/exec/process';
    $(function () {
        $exec_process = $('#exec_process').bootstrapTable({
            method: "get",
            url: $url_exec_process,
            cache: false,
            fit : true,
            fitColumns : true,
            border : false,
            idField : 'infoId',
            uniqueId: 'infoId',
            undefinedText: '',
            checkbox:true,
            rownumbers:true,
            showRefresh: false,
            pagination: true,
            sidePagination: 'server',
            pageNumber: 1,
            pageSize: 10,
            pageList: [10, 20, 30, 40, 50],
            singleSelect:true,
            striped:true,
            search:false,
            clickToSelect:true,
            queryParams: function (params) {
                var param = {
                    rid: '${instance.rid}'
                };
                return param;
            },
            columns: [
                {field:'pos',title:'序号',width: 20,align: 'center',
                    formatter: function (value, row, index) {
                        return index+1;
                    }
                },
                {field:'infoId',title:'id',visible:false},
                {field:'messages',title:'执行信息',align: 'center',width:150},
                {field:'activityName',title:'执行名称',align: 'center',width:150},
                {field:'activityStatus',title:'状态',width:80,align: 'center'},
                {field:'activityTime',title:'执行时间',width:150,align: 'center'}
            ],
            dataPlain: true,
            onLoadSuccess: function () {
                var rows = $('#exec_process').bootstrapTable('getData');
                var phase, status;
                for(var i = rows.length - 1; i >= 0; i--){
                    if(rows[i].phaseNumber == 'Phase 1' && rows[i].activityStatus == 'Succeed'){
                        phase = rows[i].phaseNumber;
                        status = rows[i].activityStatus;
                    } else if(rows[i].phaseNumber == 'Phase 2' && rows[i].activityStatus == 'Succeed'){
                        phase = rows[i].phaseNumber;
                        status = rows[i].activityStatus;
                    } else if(rows[i].phaseNumber == 'Phase 3' && rows[i].activityStatus == 'Succeed'){
                        phase = rows[i].phaseNumber;
                        status = rows[i].activityStatus;
                    }
                }
                var process = $('#process');
                if(phase == 'Phase 1' && status == 'Succeed'){
                    process.attr('style', 'width:5%');
                    process.attr('aria-valuenow', '5');
                    process.html('5%');
                } else if(phase == 'Phase 2' && status == 'Succeed'){
                    process.attr('style', 'width:80%');
                    process.attr('aria-valuenow', '80');
                    process.html('80%');
                } else if(phase == 'Phase 3' && status == 'Succeed'){
                    process.attr('style', 'width:100%');
                    process.attr('aria-valuenow', '100');
                    process.html('100%');
                }
            }
        });

        function paramClick(){
            var opt = {
                url: $url_exec_process,
                silent: true,
                query:{
                    rid: '${instance.rid}'
                }
            };
            $('#exec_process').bootstrapTable('refresh', opt);
        }
        var pros;
        pros = setInterval(paramClick, 2000);

        $('#add-model').on('hidden.bs.modal', function () {
            clearInterval(pros);
        });
    });
</script>
</body>
</html>