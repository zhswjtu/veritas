package com.centrin.veritas.entity;
/**
 * Project Name：veritas
 * File Name：VeritasOracleDbInstanceEntity
 * Package Name：com.centrin.veritas.entity
 * Date：17/7/24 下午5:16
 */

import com.centrin.common.entity.BaseEntity;

import javax.persistence.*;

/**
 * @Title：
 * @Description：
 * @Package com.centrin.veritas.entity
 * @ClassName VeritasOracleDbInstanceEntity
 * @author 张辉
 * @date 17/7/24 下午5:16
 * @version
 */
@Entity
@Table(name = "veritas_oracle_db_instance")
public class VeritasOracleDbInstanceEntity extends BaseEntity{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "chapter_head")
    private String chapterHead;

    @Column(name = "sid_name")
    private String sidName;

    @Column(name = "sid_value")
    private String sidValue;

    @Column(name = "client_name")
    private String clientName;

    @Column(name = "client_name_value")
    private String clientNameValue;

    @Column(name = "stand_by_database")
    private String standByDatabase;

    @Column(name = "stand_by_database_value")
    private String standByDatabaseValue;

    @Column(name = "stand_by_database_status")
    private Integer standByDatabaseStatus;

    @Column(name = "instance_type")
    private Integer instanceType;

    private String rid;

    private String ip;

    public VeritasOracleDbInstanceEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChapterHead() {
        return chapterHead;
    }

    public void setChapterHead(String chapterHead) {
        this.chapterHead = chapterHead;
    }

    public String getSidName() {
        return sidName;
    }

    public void setSidName(String sidName) {
        this.sidName = sidName;
    }

    public String getSidValue() {
        return sidValue;
    }

    public void setSidValue(String sidValue) {
        this.sidValue = sidValue;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientNameValue() {
        return clientNameValue;
    }

    public void setClientNameValue(String clientNameValue) {
        this.clientNameValue = clientNameValue;
    }

    public String getStandByDatabase() {
        return standByDatabase;
    }

    public void setStandByDatabase(String standByDatabase) {
        this.standByDatabase = standByDatabase;
    }

    public String getStandByDatabaseValue() {
        return standByDatabaseValue;
    }

    public void setStandByDatabaseValue(String standByDatabaseValue) {
        this.standByDatabaseValue = standByDatabaseValue;
    }

    public Integer getStandByDatabaseStatus() {
        return standByDatabaseStatus;
    }

    public void setStandByDatabaseStatus(Integer standByDatabaseStatus) {
        this.standByDatabaseStatus = standByDatabaseStatus;
    }

    public Integer getInstanceType() {
        return instanceType;
    }

    public void setInstanceType(Integer instanceType) {
        this.instanceType = instanceType;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
