package com.centrin.system.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.centrin.common.dao.SuperBaseDao;
import com.centrin.system.entity.PermissionEntity;

@Repository
public class PermissionDao extends SuperBaseDao {

	public List<PermissionEntity> getPermissionsByUserId(Integer userId) {
		StringBuffer sb=new StringBuffer();
		sb.append("select x.* from sys_permission x where x.id in");
		sb.append(" (select distinct p.id from sys_permission p");
		sb.append(" inner join sys_role_permission rp on p.id=rp.permission_id");
		sb.append(" inner join sys_role r ON r.id=rp.role_id");
		sb.append(" inner join sys_user_role ur ON ur.role_id=rp.role_id");
		sb.append(" inner join sys_user u ON u.id = ur.user_id");
		sb.append(" where u.id=:userId) order by x.sort");
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("userId", userId);
		return findBySql(PermissionEntity.class, sb.toString(), params, -1, -1);
	}
	
	public List<PermissionEntity> getPermissionByPid(Integer id) {
		String jpql = "select p from PermissionEntity p where p.pid=" +id;
		return findByJpql(PermissionEntity.class, jpql);
	}
	
	public List<PermissionEntity> getAllValidPermissions() {
		String jpql = "select p from PermissionEntity p where p.status=1 order by p.sort";
		return findByJpql(PermissionEntity.class, jpql, null, -1, -1);
	}
	
	public List<PermissionEntity> getMenus() {
		String jpql = "select p from PermissionEntity p where p.type=1 order by p.sort";
		return findByJpql(PermissionEntity.class, jpql, null, -1, -1);
	}
	
	public List<PermissionEntity> getRootMenus() {
		String jpql = "select p from PermissionEntity p where p.type=1 and p.pid is null order by p.sort";
		return findByJpql(PermissionEntity.class, jpql, null, -1, -1);
	}
	
	public List<PermissionEntity> getAllValidPermissionsByPid(Integer pid) {
		String jpql = "select p from PermissionEntity p where p.status=1 and p.pid=:pid order by p.sort";
		if(pid==null) {
			pid= -1;
		}
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("pid", pid);
		return findByJpql(PermissionEntity.class, jpql, params, -1, -1);
	}
	
	public List<PermissionEntity> getMenusByPid(Integer pid) {
		String jpql = "select p from PermissionEntity p where p.type=1 and p.pid=:pid order by p.sort";
		if(pid==null) {
			pid= -1;
		}
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("pid", pid);
		return findByJpql(PermissionEntity.class, jpql, params, -1, -1);
	}
	
	public List<PermissionEntity> getOperationsByMenuId(Integer pid) {
		String jpql = "select p from PermissionEntity p where p.pid=:pid order by p.sort";
		if(pid==null) {
			pid= -1;
		}
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("pid", pid);
		return findByJpql(PermissionEntity.class, jpql, params, -1, -1);
	}
	
	

	public void delete(Integer id) {
		StringBuffer sb1 = new StringBuffer();
		sb1.append("delete from sys_role_permission");
		sb1.append(" where permission_id=");
		sb1.append(id);
		
		StringBuffer sb2 = new StringBuffer();
		sb2.append("delete from sys_permission");
		sb2.append(" where id=");
		sb2.append(id);
		
		String[] sqls = new String[] {sb1.toString(), sb2.toString()};
		executeSql(sqls);
	}

}
